<?php
require_once("../Includes/Fonctions.php");
require_once("../Includes/Config.php");

//Cr�ation des variables
$ip = ip2long($_SERVER['REMOTE_ADDR']);

//Requ�te
$query = $baseDeDonnes->prepare('INSERT INTO chat_online VALUES(:id, :time, :ip) ON DUPLICATE KEY UPDATE online_time = :time, online_id = :id');
if(isset($ID_MEMBRE))     $query->bindValue(':id', $ID_MEMBRE, PDO::PARAM_INT);
else                      $query->bindValue(':id', 0, PDO::PARAM_INT);

$query->bindValue(':time', time(), PDO::PARAM_INT);
$query->bindValue(':ip', $ip, PDO::PARAM_INT);
$query->execute();
$query->CloseCursor();

$time_max = time() - (60 * 5);
$query = $baseDeDonnes->prepare('DELETE FROM chat_online WHERE online_time < :timemax');
$query->bindValue(':timemax', $time_max, PDO::PARAM_INT);
$query->execute();
$query->CloseCursor();
?>