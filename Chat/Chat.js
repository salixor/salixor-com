//var host = "http://localhost/Salixor/";
var host = "http://www.salixor.com/";

var erreur = "";
var nbErreur = 0;

if($(".chatComplet").length == 0) {
	var rafraichir = 5000;
	var timer = setInterval(recupererMessages, rafraichir);
}

$('.chat .membreEnLigne').live("click", function() {
	$(this).toggleClass('active');
	$('.chat .nbConnectes').toggle();
});

//Historique des messages
$('.chat .historique').live("click", function() {
	$(this).toggleClass('active');
	$('.chat .chatForm').toggle();
	$('.chat .rafraichir').toggle();
		
	if($(this).hasClass('active')) {
		$('.encadreInfos .right').html('Voici l\'historique des messages du chat.<br />Les messages de plus d\'une semaine sont supprimés.')
	
		$.ajax({
			type: "GET",
			url: host+"Chat/Chat_Recup.php",
			data: "Historique",
			success:function(msg) {
				$(".chatMessages").remove();
				$(".chatForm").before(msg);
				
				$(".chatMessages").scrollTop(scroll).css({"height":"500px"});
			},
			error:function() {
				erreur = "Les messages n'ont pas pus être récupérés.";
				afficherErreurs();
			}
		});
		
		clearInterval(timer);
	}
	else {
		$('.encadreInfos .right').html('Voici le chat du site.<br />Pour discuter, vous devez être inscrit, mais aussi respecter deux petites règles : être poli et être compréhensible.')
	
		$.ajax({
			url: host+"Chat/Chat_Recup.php",
			success:function(msg) {
				$(".chatMessages").remove();
				$(".chatForm").before(msg);
				
				$(".chatMessages").scrollTop(scroll);
			},
			error:function() {
				erreur = "Les messages n'ont pas pus être récupérés.";
				afficherErreurs();
			}
		});
		
		clearInterval(timer);
		recupererMessages();
		timer = setInterval(recupererMessages, rafraichir);
	}
});

$('.chat span.rafraichir').click(function() {
	if($('.chat .historique').hasClass('active')) {
		clearInterval(timer);
	}
	else {
		clearInterval(timer);
		recupererMessages();
		timer = setInterval(recupererMessages, rafraichir);
	}
});

$('.chat div.menuChat span.smileys').live("click", function() {
	$(this).toggleClass('active');
	$(".listeSmileys").toggle();
});

$('.chat div.menuChat span.envoyer').click(function() {
	addMessage();
});

$('.chat #message').bind('keypress', function(e) {
	if(e.keyCode == 13) {
		addMessage();
		return false;
	}
	else { 
		return true;
	}
});

$('.chat #message').focus(function() {
	if($(this).val() == "Tapez ici votre message.")     $(this).val("");
});

$('.chat #message').blur(function() {
	if($(this).val() == "")                             $(this).val("Tapez ici votre message.");
});

//Ajout du message
function addMessage() {
	var message = document.formChat.message.value;
	
	if(message == "" || message == "Tapez ici votre message.") {
		erreur = "Votre message est vide.";
		afficherErreurs();
	}
	else {
		if($('input#pseudo').val() == '') {
			erreur = "Le pseudo n'a pas été renseigné ou n'est pas valide.";
			afficherErreurs();
		}
		else {
			$("#message").val("");
			
			
			$.post(host+"Chat/Chat_Post.php", { message: message }, function(data) {
				if(data != "") {
					erreur = data;
					afficherErreurs();
				}
				else {
					recupererMessages();
				}
			});
			
			$(".chatMessages").scrollTop(0);
		}
	}
}
//Récupération des messages et des membres
function recupererMessages() {
	var chatMessage	= $(".chatMessages");
	var scroll = chatMessage.scrollTop();
	var page = $("#pageChat").text();

	$.ajax({
		url: host+"Chat/Chat_Recup.php",
		success:function(msg) {
			$(".chatMessages").remove();
			$(".chatForm").before(msg);
			
			$(".chatMessages").scrollTop(scroll);
		},
		error:function() {
			erreur = "Les messages n'ont pas pus être récupérés.";
			afficherErreurs();
		}
	});
	
	$.ajax({
		url: host+"Chat/ConnectesChat.php",
		success:function(msg) {
			$(".nbConnectes").html(msg);
		}
	});
	
	$.ajax({ url: host+"Chat/OnlineChat.php" });
}

//Affichage des erreurs si une erreur est présente
function afficherErreurs() {
	if($('.notification').is(":hidden")) {
		nbErreur++;
		clearInterval(timer);
		timer = setInterval(recupererMessages, rafraichir);

		if(nbErreur > 5) {
			erreur = "Vous avez reçu plus de 5 erreurs. Merci d'actualiser la page.";
			$('.notification').html('<img src="'+host+'Images/Notifications/Erreur.jpg" /> '+erreur).fadeIn(300);
		}
		else {	
			$('.notification').html('<img src="'+host+'Images/Notifications/Erreur.jpg" /> '+erreur).fadeIn(300).delay(2000).fadeOut(200);
		}
	}
}