<?php
require_once("../Includes/Fonctions.php");
require_once("../Includes/Config.php");

if(!isset($_GET["Historique"])) {
	$requete = $baseDeDonnes->query('
	SELECT chat.*, membres.* FROM chat
	INNER JOIN membres
	ON chat.id_membre = membres.membre_id
	ORDER BY id DESC
	LIMIT 0, 25');
}
else {
	$requete = $baseDeDonnes->query('
	SELECT chat.*, membres.* FROM chat
	INNER JOIN membres
	ON chat.id_membre = membres.membre_id
	ORDER BY id DESC');
}

$requete->execute();

echo '<div class="chatMessages">';

	while($data = $requete->fetch()) {
		echo '<div class="message">';
		
			echo '<div class="topMessage">';
			
				echo '<a href="'.ABSPATH.'Membres/Profil.php?id='.$data["membre_id"].'">';
					if(!empty($data["membre_avatar"]))    echo '<p class="avatarMembre"><img src="'.ABSPATH.'Images/Avatars/'.$data["membre_avatar"].'" width="40" height="40" alt="" /></p>';
					else                                  echo '<p class="avatarMembre"><img src="'.ABSPATH.'Images/Avatars/avatar_normal.png" width="40" height="40" alt="" /></p>';
					
					echo '<span class="pseudo">'.stripslashes($data['membre_pseudo']).'</span>';
				echo '</a>';
				
				echo '<span class="date">'.afficher_date_difference($data["date"]).'</span><span class="dot"> &bull; </span>';
				
				if($data["membre_connecte"] == 1)   echo '<span class="connecte MembreConnecte">En ligne</span>';
				else                                echo '<span class="connecte MembreDeconnecte">Hors ligne</span>';

			echo '</div>';
			echo '<div class="miniFleche"></div>';
			
			echo '<div class="contentMessage">
					'.smileys(url2link(stripslashes($data['message']))).'
				  </div>';  
		echo '</div>';
	}

echo '</div>';

/*$requete = $baseDeDonnes->query('
SELECT chat.* FROM chat
ORDER BY id DESC
LIMIT 0, 50');

$requete->execute();

echo '<div class="chatMessages">';

	while($data = $requete->fetch()) {
		echo '<div class="message">';
		
			echo '<div class="topMessage">';
			
				echo '<span class="pseudo">'.stripslashes($data['pseudo_temp']).'</span>';

				echo '<p class="avatarMembre"><img src="'.ABSPATH.'Images/Avatars/avatar_normal.png" width="40" height="40" alt="" /></p>';

				echo '<span class="date">'.afficher_date_difference($data["date"]).'</span>';

			echo '</div>';
			echo '<div class="miniFleche"></div>';
			
			echo '<div class="contentMessage">
					'.smileys(url2link(stripslashes($data['message']))).'
				  </div>';  
		echo '</div>';
	}

echo '</div>';*/
?>