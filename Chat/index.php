﻿<?php 
$titre_page_nom = "Chat";
include("../Header.php");
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Chat.png" alt="Chat" /> Le Chat</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH ?>">Accueil</a> > Le Chat</p>

<?php
$requete = $baseDeDonnes->prepare("DELETE FROM chat WHERE date < :time");
$requete->bindValue(":time", time() - 60*60*24*7, PDO::PARAM_INT);
$requete->execute();
?>

<p class="encadreInfos">
	<span class="right">
		Voici le chat du site.<br />
		Pour discuter, vous devez être inscrit, mais aussi respecter deux petites règles : être poli et être compréhensible.
	</span>
</p>

<div class="chat">
	<span style="display:none;" id="pageChat"></span>
	
	<div class="onglets">
		<a href="javascript:void(0);"><span class="membreEnLigne active">En ligne <span class="fleche"></span></span></a>
		<a href="javascript:void(0);"><span class="historique">Historique</span></a>
		<span class="rafraichir">Rafraîchir</span>
	</div>
	
	<div style="clear:left"></div>
	
	<div class="nbConnectes"><?php include('ConnectesChat.php'); ?></div>

	<?php include('Chat_Recup.php'); ?>
	
		<div class="chatForm">
			<?php if(isset($ID_MEMBRE)) { ?>
				<div class="menuChat">
					<span class="smileys">Smileys<span class="fleche"></span></span>
					<div class="listeSmileys">
						<?php
							$smileys = array("blink", "clin", "euh", "grrr", "hihi", "honte", "huh", "langue", "lol", "triste", "pleure", "siffle", "smile", "smile_big", "soleil", "evil", "zip", "sweat", "sleep", "inlove", "heart", "confuse", "money", "green", "neutral", "kitty", "twist", "door");
							sort($smileys);
							for($i = 0; $i < count($smileys); $i++) {
								echo '<img src="'.ABSPATH.'Images/Smileys/'.$smileys[$i].'.png" onclick="add_tag(\':'.$smileys[$i].':\',\'\',\'message\');" alt="" title=":'.$smileys[$i].':" />';
							}
							echo '<img src="'.ABSPATH.'Images/Smileys/'.$smileys[1].'.png" onclick="add_tag(\':'.$smileys[1].':\',\'\',\'message\');" alt="" title=":'.$smileys[1].':" />';
						?>
						<div style="clear:left;"></div>
					</div>
					
					<span class="envoyer">Envoyer</span>
				</div>

				<form action="Chat_Post.php" method="post" name="formChat">
					<p>
						<textarea name="message" id="message">Tapez ici votre message.</textarea>
					</p>
				</form>
			<?php }
			else { ?>
				<p class="nonConnecteChat">
					Pour pouvoir poster un message sur le chat, vous devez <a href="javascript:connexion();">être connecté</a> sur le site.<br />
					Vous pouvez pour le moment uniquement visualiser les messages.
				</p>
			<?php } ?>
		</div>
</div>

<?php
echo '<script type="text/javascript" src="Chat.js"></script>';

include("../Footer.php");
?>