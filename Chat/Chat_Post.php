<?php
require_once("../Includes/Fonctions.php");
require_once("../Includes/Config.php");

if(isset($ID_MEMBRE)) {
	$requete = $baseDeDonnes->prepare("
	INSERT INTO chat(id_membre, message, date)
	VALUES(:membre, :message, :temps)
	");
	$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
	$requete->bindValue(":message", $_POST['message'], PDO::PARAM_STR);
	$requete->bindValue(":temps", time(), PDO::PARAM_INT);
	$requete->execute();
	
	if($requete->rowCount() == 0) {
		echo "Votre message n'a pas pu �tre enregistr�.";
	}
}
else {
	echo "Vous n'�tes pas connect�.";
}

/*$requete = $baseDeDonnes->prepare("
INSERT INTO chat(message, date, pseudo_temp)
VALUES(:message, :temps, :pseudo)
");
$requete->bindValue(":message", $_POST['message'], PDO::PARAM_STR);
$requete->bindValue(":temps", time(), PDO::PARAM_INT);
$requete->bindValue(":pseudo", $_POST['pseudo'], PDO::PARAM_STR);
$requete->execute();*/
?>