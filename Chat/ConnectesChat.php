<?php
require_once("../Includes/Fonctions.php");
require_once("../Includes/Config.php");

$textToShow = '';
$timeMax = time() - (60 * 5);

$requete2 = $baseDeDonnes->prepare('
SELECT membres.membre_pseudo, membres.membre_id
FROM chat_online
LEFT JOIN membres ON membres.membre_id = chat_online.online_id
WHERE online_time > :timemax AND online_id <> 0 ORDER BY membre_pseudo ASC');
$requete2->bindValue(':timemax', $timeMax, PDO::PARAM_INT);
$requete2->execute();
		
if($requete2->rowCount() == 0)    $messageConnectes = 'Il n\'y a actuellement personne en ligne.';
if($requete2->rowCount() == 1)    $messageConnectes = 'Il y a 1 membre en ligne : ';
if($requete2->rowCount() > 1)     $messageConnectes = 'Il y a '.$requete2->rowCount().' membres en ligne : ';
	
echo $messageConnectes;
	
while($data = $requete2->fetch()) {
	if(isset($PSEUDO_MEMBRE)) {
		if($data["membre_pseudo"] == $PSEUDO_MEMBRE)   echo ' Vous,';
		else                                           $textToShow .= ' <a href="'.ABSPATH.'Membres/Profil.php?id='.$data["membre_id"].'">'.$data["membre_pseudo"].'</a>,';
	}
	else {
		$textToShow .= ' <a href="'.ABSPATH.'Membres/Profil.php?id='.$data["membre_id"].'">'.$data["membre_pseudo"].'</a>,';
	}
}

$textToShow = substr($textToShow, 0, -1);

echo $textToShow;
?>