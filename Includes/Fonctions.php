<?php
$GLOBALS['normalizeChars'] = array(
    'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 
    'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 
    'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 
    'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 
    'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 
    'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 
    'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f', 'ü'=>'u'
);

$actualCommVal = 0;

function afficheCommentaireFils($idParent, $news, $limit_start, $pagination) {
	global $baseDeDonnes;
	global $actualCommVal;
	
	$requete = $baseDeDonnes->prepare("
	SELECT commentaires.*, membres.*, membres_titres.*, commentaires.id AS comm_id, membres_edit.membre_pseudo AS membre_pseudo_editeur, commentaires_reponse.pseudo AS membre_pseudo_reponse, commentaires_reponse.id AS id_commentaire_reponse
	FROM commentaires
	LEFT JOIN commentaires AS commentaires_reponse ON commentaires_reponse.id = commentaires.id_reponse
	LEFT JOIN membres ON commentaires.id_membre = membres.membre_id
	LEFT JOIN membres_titres ON membres.membre_titre_id = membres_titres.id
	LEFT JOIN membres AS membres_edit ON membres_edit.membre_id = commentaires.membre_edition
	WHERE commentaires.id_reponse = :id AND commentaires.id_news = :news
	ORDER BY commentaires.id ASC LIMIT $limit_start, $pagination");
	$requete->bindValue(":id", $idParent, PDO::PARAM_INT);
	$requete->bindValue(":news", $news, PDO::PARAM_INT);
	$requete->execute();
						
    while($data = $requete->fetch()) {
		if($data["id_reponse"] != 0)  	$actualCommVal++;
		showCommentaire($data, $actualCommVal);
		afficheCommentaireFils($data["comm_id"], $data["id_news"], $limit_start, $pagination);
	}
	$actualCommVal = 0;
}

function showCommentaire($data, $lvl) {
	global $MEMBRES_RANGS;
	global $ID_MEMBRE;
	global $NIVEAU_MEMBRE;
	global $MEMBRES_RANGS_IMAGES;
	
	if(!empty($data["membre_niveau"]))    $imageRang = $MEMBRES_RANGS_IMAGES[$data["membre_niveau"]];
	if(!empty($data["membre_niveau"]))    $rang = $MEMBRES_RANGS[$data["membre_niveau"]];
	
	/*echo '<div class="postForum niv'.$lvl.'" id="Commentaire'.$data['id'].'">';
		echo '<div class="head">';
			if(!empty($data["membre_pseudo"])) {
				if($data["membre_connecte"] == 1)      $image = '<img src="'.ABSPATH.'Images/Forum/BulletGreen.png" alt="" />';
				else                                   $image = '<img src="'.ABSPATH.'Images/Forum/BulletRed.png" alt="" />';
				echo '<a href="'.ABSPATH.'Membres/Profil.php?id='.$data['membre_id'].'" class="left">'.$image.stripslashes(utf8_encode(htmlspecialchars($data['membre_pseudo']))).'</a>';
			}
			else {
				echo '<span class="left">'.stripslashes(utf8_encode(htmlspecialchars($data['pseudo']))).'</span>';
			}
			
			echo '<span class="right"><img src="'.ABSPATH.'Images/Forum/Time.png" alt="" style="margin:3px 0 -3px;" /> Posté le '.afficherDate($data['date']).' à '.date('H\hi', strtotime($data['date'])).'</span>';
			
			echo '<div style="clear:left;"></div>';
		echo '</div>';
		
		echo '<div class="side">';
			if(!empty($data["membre_pseudo"])) {
				if(!empty($data["membre_avatar"]))     echo '<div class="avatar-forum"><img src="Images/Avatars/'.$data["membre_avatar"].'" width="100" height="100" alt="" /></div>';
				else                                   echo '<div class="avatar-forum"><img src="Images/Avatars/avatar_normal.png" width="100" height="100" alt="" /></div>';
				
				echo '<div class="contenu-centre contenu-gras">'.$rang.'</div>';   
				echo '<div class="contenu-centre bleu">&laquo;'.stripslashes(htmlspecialchars(utf8_encode($data['titre']))).'&raquo;</div>';   
				echo '<div>Inscrit le '.date('d/m/Y', strtotime($data['membre_inscription'])).'</div>';
				echo '<div>'.$data['membre_commentaires'].' commentaires</div>';  
			}
			else {
				echo '<div class="avatar-forum"><img src="Images/Avatars/not_membre.png" width="100" height="100" alt="" /></div>';
				echo '<div class="contenu-centre contenu-gras">Visiteur</div>';   
			}
		echo '</div>';		
			   
		echo '<div class="contenu">';
			echo smileys(nl2br(stripslashes(formatage($data['commentaire']))));

			if( (!empty($data["membre_pseudo_reponse"]) && $data["id_reponse"] != 0) || (!empty($data["membre_pseudo_editeur"]) && $data["membre_edition"] != 0) ) {
				echo '<div class="edition">';
					if(!empty($data["membre_pseudo_reponse"]) && $data["id_reponse"] != 0) {
						echo '<img src="Images/Reponse.png" alt="" />';
						echo '<b>En réponse à</b> <a href="#Commentaire'.$data["id_commentaire_reponse"].'">'.$data["membre_pseudo_reponse"].'</a>';
					}
					if(!empty($data["membre_pseudo_editeur"]) && $data["membre_edition"] != 0) {
						echo '<img src="Images/Forum/Edition.png" alt="" />';
						echo '<b>Édité par</b> '.$data["membre_pseudo_editeur"].' le '.date('d/m/Y \à H:i', $data['date_edition']);
					}
				echo '</div>';
			}
			
			if(!empty($data['membre_signature']))      echo "<hr />".smileys(nl2br(stripslashes(formatage($data['membre_signature']))));

			echo '<div style="clear:left;"></div>';
		echo '</div>';
		
		if(isset($ID_MEMBRE)) {
			echo '<div class="foot">';
				if($ID_MEMBRE != $data["id_membre"]) {
					echo '<a href="'.ABSPATH.'Membres/Messagerie.php?Action=Envoyer&amp;Pseudo='.$data['membre_pseudo'].'" class="lien"><img src="'.ABSPATH.'Images/Forum/MP.png" alt="" /> Message</a>';
					echo '<a href="#AjouterCommentaire" rel_pseudo="'.stripslashes($data['pseudo']).'" rel="'.$data['comm_id'].'" class="lien reply"><img src="'.ABSPATH.'Images/Reponse.png" alt="" /> Répondre</a>';
					echo '<a href="javascript:void(0);" class="lien reply_div_reponse" style="display:none;"><img src="'.ABSPATH.'Images/Reponse.png" /> Annuler</a>';
				}

				if($ID_MEMBRE == $data["id_membre"] || $NIVEAU_MEMBRE == 5) {
					echo '<a href="'.ABSPATH.'Supprimer_commentaire.php?id='.$data['comm_id'].'" class="lien"><img src="'.ABSPATH.'Images/Forum/Supprimer.png" alt="" /> Supprimer</a>';
					echo '<a href="'.ABSPATH.'Modifier_commentaire.php?id='.$data['comm_id'].'" class="lien"><img src="'.ABSPATH.'Images/Forum/Edition.png" alt="" /> Éditer</a>';
					echo '<a class="lien left">-</a>';
				}
				echo '<div style="clear:left;"></div>';
			echo '</div>';
		}
	echo '</div>';*/
	
	if($lvl == 0) {
		echo '<div class="message margin" id="Commentaire'.$data["comm_id"].'">';
	}
	else {
		echo '<div class="message niv'.$lvl.'" id="Commentaire'.$data["comm_id"].'">';
		echo '<div class="flecheReponse"></div>';
	}
		
		echo '<div class="topMessage">';
		
			if(!empty($data["membre_pseudo"])) {
				echo '<a href="'.ABSPATH.'Membres/Profil.php?id='.$data["membre_id"].'" class="infos">';
					if(!empty($data["membre_avatar"]))    echo '<p class="avatarMembre"><img src="'.ABSPATH.'Images/Avatars/'.$data["membre_avatar"].'" width="40" height="40" alt="" /></p>';
					else                                  echo '<p class="avatarMembre"><img src="'.ABSPATH.'Images/Avatars/avatar_normal.png" width="40" height="40" alt="" /></p>';
					
					echo '<span class="pseudo">'.stripslashes($data['membre_pseudo']).'</span>';
				echo '</a>';
				
				echo '<span class="date">'.afficher_date_difference(strtotime($data["date"])).'</span><span class="dot"> &bull; </span>';
				
				if($data["membre_connecte"] == 1)   echo '<span class="connecte MembreConnecte">En ligne</span><span class="dot"> &bull; </span>';
				else                                echo '<span class="connecte MembreDeconnecte">Hors ligne</span><span class="dot"> &bull; </span>';
				
				if($rang == "Membre Banni")         echo '<span class="MembreBanni rang">';
				elseif($rang == "Membre")           echo '<span class="Membre rang">';
				elseif($rang == "Béta Testeur")     echo '<span class="BetaTesteur rang">';
				elseif($rang == "Modérateur")       echo '<span class="Moderateur rang">';
				elseif($rang == "Administrateur")   echo '<span class="Administrateur rang">';
				
				echo '<img src="'.ABSPATH.'Images/Membres/'.$imageRang.'.png" alt="" /> '.$rang.'</span>';
			}
			else {
				echo '<span class="infos">';
					if(!empty($data["membre_avatar"]))    echo '<p class="avatarMembre"><img src="'.ABSPATH.'Images/Avatars/'.$data["membre_avatar"].'" width="40" height="40" alt="" /></p>';
					else                                  echo '<p class="avatarMembre"><img src="'.ABSPATH.'Images/Avatars/not_membre.png" width="40" height="40" alt="" /></p>';
					
					echo '<span class="pseudo">'.stripslashes($data['pseudo']).'</span>';
				echo '</span>';
				
				echo '<span class="date">'.afficher_date_difference(strtotime($data["date"])).'</span>';
			}

		echo '</div>';
		echo '<div class="miniFleche"></div>';
		
		echo '<div class="contentMessage">
				'.smileys(nl2br(stripslashes(formatage($data['commentaire'])))).'
			  </div>';
		
		if(!empty($data["membre_pseudo_reponse"]) && $data["id_reponse"] != 0) {
			echo '<div class="infosSuppComm reply">';
				echo 'En réponse à <a href="#Commentaire'.$data["id_commentaire_reponse"].'">'.$data["membre_pseudo_reponse"].'</a>';
			echo '</div>';
		}
		if(!empty($data["membre_pseudo_editeur"]) && $data["membre_edition"] != 0) {
			echo '<div class="infosSuppComm edition">';
				echo 'Édité par '.$data["membre_pseudo_editeur"].' le '.date('d/m/Y \à H:i', $data['date_edition']);
			echo '</div>';
		}
		
		if(isset($ID_MEMBRE)) {
			if($ID_MEMBRE != $data["id_membre"]) {
				echo '<a href="'.ABSPATH.'Membres/Messagerie.php?Action=Envoyer&amp;Pseudo='.$data['membre_pseudo'].'"><div class="buttons buttonSend" title="Envoyer un MP"></div></a>';
				echo '<div class="buttons buttonReply" rel_pseudo="'.stripslashes($data['pseudo']).'" rel="'.$data['comm_id'].'" title="Répondre"></div>';
			}
			if($ID_MEMBRE == $data["id_membre"] || $NIVEAU_MEMBRE == 5) {
				echo '<a href="'.ABSPATH.'Modifier_commentaire.php?id='.$data['comm_id'].'"><div class="buttons buttonEdit" title="Modifier"></div></a>';
				echo '<a href="'.ABSPATH.'Supprimer_commentaire.php?id='.$data['comm_id'].'"><div class="buttons buttonDelete" title="Supprimer"></div></a>';
			}
		}
		echo '<div class="clear"></div>';
	echo '</div>';
	
	/*echo '<div class="commentaireNews">';
		echo '<div class="top">';
			echo '<div class="fleche"></div>';
			
			if(!empty($data["membre_pseudo"])) {
				if($data["membre_connecte"] == 1)      $image = '<img src="'.ABSPATH.'Images/Forum/BulletGreen.png" alt="" />';
				else                                   $image = '<img src="'.ABSPATH.'Images/Forum/BulletRed.png" alt="" />';
				
				echo '<div class="membreInfos">';
					echo '<a href="'.ABSPATH.'Membres/Profil.php?id='.$data['membre_id'].'">'.$image.stripslashes(utf8_encode(htmlspecialchars($data['membre_pseudo']))).'</a>';
				echo '</div>';
				
				if(!empty($data["membre_avatar"]))     echo '<div class="avatarMembreComm"><img src="Images/Avatars/'.$data["membre_avatar"].'" alt="" /></div>';
				else                                   echo '<div class="avatarMembreComm"><img src="Images/Avatars/avatar_normal.png" alt="" /></div>';
			}
			else {
				echo '<div class="membreInfos">';
					echo stripslashes(utf8_encode(htmlspecialchars($data['pseudo'])));
				echo '</div>';
			
				echo '<span class="left"></span>';
				echo '<div class="avatarMembreComm"><img src="Images/Avatars/not_membre.png" alt="" /></div>';
			}
		echo '</div>';
		
		echo '<div class="contenu">';
			echo smileys(nl2br(stripslashes(formatage($data['commentaire']))));
		echo '</div>';
	echo '</div>';*/
}

function verifAuthForum($authNecessaire) {
	if(isset($GLOBALS['NIVEAU_MEMBRE'])) {
		$level = $GLOBALS['NIVEAU_MEMBRE'];
	}
	else {
		$level = 1;
	}
	
	return ($authNecessaire <= intval($level));
}

define("ERR_AUTH_ANNONCE", "Vous n'avez pas l'autorisation de créer une annonce.");
define("ERR_AUTH_VIEW", "Vous n'avez pas l'autorisation de voir ce forum.");
define("ERR_AUTH_BANNIR", "Vous n'avez pas l'autorisation de bannir un membre.");
define("ERR_AUTH_BANNIR_SELF", "Vous ne pouvez pas vous bannir.");
define("ERR_AUTH_SIGNALER_SELF", "Vous ne pouvez pas signaler un de vos messages.");
define("ERR_AUTH_DESIGNALER_SELF", "Vous ne pouvez pas désignaler un de vos messages.");
define("ERR_AUTH_VERR", "Vous n'avez pas l'autorisation de verrouiller ce forum.");
define("ERR_AUTH_EDIT", "Vous n'avez pas l'autorisation de modifier ce message.");
define("ERR_AUTH_DELETE", "Vous n'avez pas l'autorisation de supprimer ce message.");
define("ERR_AUTH_DELETE_TOPIC", "Vous n'avez pas l'autorisation de supprimer ce sujet.");
define("ERR_IS_CO", "Vous n'êtes pas connecté.");

function erreur($erreur = '') {
	$mess = ($erreur!='')? $erreur:'Une erreur inconnue s\'est produite';
	exit('
	<h2 class="titreSecond"><img src="../Images/icones_pages/Administration.png" alt="" /> Erreur</h2>
	<div class="encadre_infos">
		<p><img src="'.ABSPATH.'Images/erreur.png" alt="Erreur" align="top" /> '.$mess.'</p>
		<p>Cliquez <a href="index.php">ici</a> pour revenir à l\'accueil du forum.</p>
	</div>
	
	</div></body></html>');
}

function tronquer($chaine, $limite, $final = NULL) {
	if(strlen($chaine) > $limite) {
		$chaine = substr($chaine, 0, $limite);
		if($final == NULL)     $chaine .= '...';
		else                   $chaine .= $final;		
	}
	
	return $chaine;
}

function age($annee_naissance, $mois_naissance, $jour_naissance, $timestamp = '') {
 
	//Si on veut vérifier à la date actuelle ( par défaut )
	if(empty($timestamp))
		$timestamp = time();
 
	//On evalue l'age, à un an par exces
	$age = date('Y',$timestamp) - $annee_naissance;
 
	//On retire un an si l'anniversaire n'est pas encore passé
	if($mois_naissance > date('n', $timestamp) || ( $mois_naissance== date('n', $timestamp) && $jour_naissance > date('j', $timestamp)))
		$age--;
 
	return $age;
}

function afficherDate($date) {
	return date("j", strtotime($date)).' '.afficherMois(strtotime($date)).' '.date("Y", strtotime($date));
}

function convertirTimestamp($date) {
	return date("j", $date).' '.afficherMois($date).' '.date("Y", $date);
}

function afficherMois($mois) {
	$listeDesMois = array('Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre ','Octobre','Novembre','Décembre');
	$afficheMois = date("n", $mois) - 1;
	return $listeDesMois[$afficheMois];
}

function changerCraft($texte) {
    $craft = $texte;
    $chaine_craft = '';
	if(preg_match("#\n#", $craft))     $explode = explode("\n", $craft);
	if(preg_match("#\+#", $craft))     $explode = explode("+", $craft);
	
	if(isset($explode)) {
		natcasesort($explode);

		foreach($explode as $ligne_craft) {
			$chaine_craft .= $ligne_craft." + ";
		}
	}
	else {
		$chaine_craft = $craft;
	}

	$chaine_craft = preg_replace("#([0-9])x#isU", "$1 x", $chaine_craft);
	$chaine_craft = preg_replace("#\s\+\s#isU", "\n", $chaine_craft);

	return $chaine_craft;
}

function colorationDommages($chaine) {
    $effets = $chaine; // On sécurise la chaîne (rien à sécuriser donc sur l'affichage)
    $chaine_dommage = ''; // On initialise la variable qui contiendra notre liste des dommages avec icônes
    $explode = explode("\n", $effets); // On sépare chaque ligne (chaque nouvel effet de l'arme quoi)

    foreach($explode as $dommages) {
        if(stripos($dommages, '(neutre)')) {
            $chaine_dommage .= '<span class="dommagesNeutre">' . $dommages . '</span>';
        }
        elseif(stripos($dommages, '(terre)')) {
            $chaine_dommage .= '<span class="dommagesTerre">' . $dommages . '</span>';
        }
        elseif(stripos($dommages, '(feu)')) {
            $chaine_dommage .= '<span class="dommagesFeu">' . $dommages . '</span>';
        }
        elseif(stripos($dommages, '(eau)')) {
            $chaine_dommage .= '<span class="dommagesEau">' . $dommages . '</span>';
        }
        elseif(stripos($dommages, '(air)')) {
            $chaine_dommage .= '<span class="dommagesAir">' . $dommages . '</span>';
        }
        elseif(stripos($dommages, 'rendus')) {
            $chaine_dommage .= '<span class="pdvRendus">' . $dommages . '</span>';
        }
		elseif(preg_match('#\-[0-9]#', $dommages)) {
            $chaine_dommage .= '<span class="rouge">' . $dommages . '</span>';
        }
        else {
            $chaine_dommage .= $dommages;
        }
    }

    return $chaine_dommage;
}

function imageEquipement($type, $toClean) {
	$toClean = strtolower($toClean);
	$toClean = stripslashes($toClean);
	$toClean = utf8_encode($toClean);
    $toClean = str_replace('\'', '', $toClean);
    $toClean = str_replace('"', '', $toClean);
    $toClean = str_replace(' ', '_', $toClean);
    $toClean = str_replace('--', '_', $toClean);
    $toClean = strtr($toClean, $GLOBALS['normalizeChars']);
	$toClean = strtolower($toClean);
	return '<img src="'.ABSPATH.'Images/Items/'.$type.'/Images/'.$toClean.'.png" alt="" />';
}

function lienImageEquipement($type, $toClean) {
	$toClean = strtolower($toClean);
	$toClean = stripslashes($toClean);
	$toClean = utf8_encode($toClean);
    $toClean = str_replace('\'', '', $toClean);
    $toClean = str_replace('"', '', $toClean);
    $toClean = str_replace(' ', '_', $toClean);
    $toClean = str_replace('--', '_', $toClean);
    $toClean = strtr($toClean, $GLOBALS['normalizeChars']);
	$toClean = strtolower($toClean);
	return 'Images/Items/'.$type.'/Images/'.$toClean.'.png';
}

function optimiser_titre_et_classe($toClean) {
	$toClean = strtolower($toClean);
    $toClean = str_replace('\'', '', $toClean);
	$toClean = str_replace('!', '', $toClean);
	$toClean = str_replace(':', '', $toClean);
	$toClean = str_replace('.', '-', $toClean);
	$toClean = str_replace('-', '', $toClean);
    $toClean = str_replace(' ', '-', $toClean);
    $toClean = preg_replace('#-{2,}#', '-', $toClean);
    $toClean = preg_replace('#-$#', '', $toClean);
    $toClean = preg_replace('#^-#', '', $toClean);
    $toClean = strtr($toClean, $GLOBALS['normalizeChars']);
	return $toClean;
}

function actualiser_session() {
	if(isset($_SESSION['id_membre']) AND isset($_SESSION['membre_mdp'])) {
		$retour = mysql_query("SELECT * FROM membres WHERE membre_id = ".intval($_SESSION['id_membre']));
		$retour = mysql_fetch_assoc($retour);
			if($_SESSION['membre_mdp'] != $retour['membre_mdp']) {
				vider_cookie();
				session_destroy();
				mysql_query("UPDATE membres SET membre_connecte = 0 WHERE membre_id = ".$_SESSION['id_membre']);
				mysql_query("DELETE FROM forum_whosonline WHERE online_id = ".$_SESSION['id_membre']);
			}
			else {
				$_SESSION["pseudo"] = $retour["membre_pseudo"];
				$_SESSION["id_membre"] = $retour["membre_id"];
				$_SESSION["membre_mdp"] = $retour["membre_mdp"];
				$_SESSION["membre_niveau"] = $retour["membre_niveau"];
				
				mysql_query("UPDATE membres SET membre_connecte = 1, membre_derniere_visite_calcul = ".time()." WHERE membre_id = ".$_SESSION['id_membre']);
					
				$intervalle = time() - (60*60*3);
				$req = mysql_query("UPDATE membres SET membre_derniere_visite = ".time().", membre_nb_visite = membre_nb_visite + 1 WHERE membre_derniere_visite < ".$intervalle." AND membre_id = ".$_SESSION['id_membre']);
			}
	}
	if(isset($_COOKIE['id_membre']) AND isset($_COOKIE['password'])) {
		$retour = mysql_query("SELECT * FROM membres WHERE membre_id = ".intval($_COOKIE['id_membre']));
		$retour = mysql_fetch_assoc($retour);
			if($_COOKIE['password'] != $retour['membre_mdp']) {
				vider_cookie();
				session_destroy();
				mysql_query("UPDATE membres SET membre_connecte = 0 WHERE membre_id = ".$_COOKIE['id_membre']);
				mysql_query("DELETE FROM forum_whosonline WHERE online_id = ".$_COOKIE['id_membre']);
			}
			else {
				$_SESSION["pseudo"] = $retour["membre_pseudo"];
				$_SESSION["id_membre"] = $retour["membre_id"];
				$_SESSION["membre_mdp"] = $retour["membre_mdp"];
				$_SESSION["membre_niveau"] = $retour["membre_niveau"];
				
				mysql_query("UPDATE membres SET membre_connecte = 1, membre_derniere_visite_calcul = ".time()." WHERE membre_id = ".$_COOKIE['id_membre']);
					
				$intervalle = time() - (60*60*3);
				$req = mysql_query("UPDATE membres SET membre_derniere_visite = ".time().", membre_nb_visite = membre_nb_visite + 1 WHERE membre_derniere_visite < ".$intervalle." AND membre_id = ".$_COOKIE['id_membre']);
			}
	}

	$limite = time() - (60*5);
	$req = mysql_query("UPDATE membres SET membre_connecte = 0 WHERE membre_derniere_visite_calcul < ".$limite."");
}

function smileys($chaine) {
	$smileys = array("blink", "clin", "euh", "grrr", "hihi", "honte", "huh", "langue", "lol", "triste", "pleure", "siffle", "smile", "smile_big", "soleil", "evil", "zip", "sweat", "sleep", "inlove", "heart", "confuse", "green", "neutral", "kitty", "twist", "door", "thumb_up", "thumb_down", "tonerre", "bomb");
	for($i = 0; $i < count($smileys); $i++) {
		if($smileys[$i] == "twist") {
			$chaine = str_replace(":".$smileys[$i].":", '<img src="'.ABSPATH.'Images/Smileys/evil.png" alt="evil" style="margin:3px 0 -3px;" />', $chaine);
		}
		elseif($smileys[$i] == "neutral") {
			$chaine = str_replace(":".$smileys[$i].":", '', $chaine);
		}
		else {
			$chaine = str_replace(":".$smileys[$i].":", '<img src="'.ABSPATH.'Images/Smileys/'.$smileys[$i].'.png" alt="'.$smileys[$i].'" style="margin:3px 0 -3px;" />', $chaine);
		}
	}
	return $chaine;
}

function afficher_smileys($zone) {
	$smileys = array("smile", "smile_big", "triste", "clin", "langue", "hihi", "blink", "kitty", "lol", "inlove", "green", "grrr", "honte", "huh", "siffle", "zip", "sweat", "confuse", "heart", "sleep", "euh", "bomb", "door", "thumb_up", "thumb_down", "evil", "soleil", "tonerre", "pleure");

	echo '<p class="formatageListe">';
		echo '<span class="formatageSmileys">';
			echo '<span class="formatage_bloc">';
			for($i = 0; $i < count($smileys); $i++) {
				echo '<img src="'.ABSPATH.'Images/Smileys/'.$smileys[$i].'.png" class="smiley" onclick="add_tag(\':'.$smileys[$i].':\',\'\',\''.$zone.'\');" />';
			}
			echo '</span>';
		echo '</span>';
}

function formatage($chaine) {
	//Formatage
	$chaine = preg_replace("#\<gras\>(.+)\</gras\>#isU", '<span class="gras">$1</span>', $chaine);
	$chaine = preg_replace("#\<italique\>(.+)\</italique\>#isU", '<span class="italique">$1</span>', $chaine);
	$chaine = preg_replace("#\<souligne\>(.+)\</souligne\>#isU", '<span class="souligne">$1</span>', $chaine);
	$chaine = preg_replace("#\<surligne\>(.+)\</surligne\>#isU", '<span class="barre">$1</span>', $chaine);
	
	//Paragraphe
	$chaine = preg_replace("#\<paragraphe\>(.+)\</paragraphe\>#isU", '<p>$1</p>', $chaine);
	
	//Liens
	$chaine = preg_replace("#\<lien url=(.+)\>\<\/lien\>#isU", '<a href="$1">$1</a>', $chaine);
	$chaine = preg_replace("#\<lien url=(.+)\>(.+)\</lien\>#isU", '<a href="$1">$2</a>', $chaine);
	
	//Images
	$chaine = preg_replace("#\<image\>(.+)\<\/image\>#isU", '<img src="$1" alt="Image non chargée" />', $chaine);
	$chaine = preg_replace("#\<image titre=(.+)\>(.+)\<\/image\>#isU", '<img src="$2" alt="Image non chargée" title="$1" />', $chaine);

	//Couleurs
	$chaine = preg_replace("#\<couleur=(.+)\>(.+)\</couleur\>#isU", '<span style="color:$1 !important;">$2</span>', $chaine);
	
	//Alignements
	$chaine = preg_replace("#\<alignement=(.+)\>(.+)\</alignement\>#isU", '<div class="$1">$2</div>', $chaine);
	
	//Citation
	$chaine = preg_replace("#\<citation auteur=(.+)\>(.+)\</citation\>#isU", '<blockquote><div class="auteur">$1</div><p>$2</p></blockquote>', $chaine);
	$chaine = preg_replace("#\<citation\>(.+)\</citation\>#isU", '<blockquote><div class="auteurNotAfter">Citation</div><p>$1</p></blockquote>', $chaine);
	
	//Titre
	$chaine = preg_replace("#\<titre niveau=2\>(.+)\</titre\>#isU", '<h2 class="titreSecond">$1</h2>', $chaine);
	$chaine = preg_replace("#\<titre niveau=3\>(.+)\</titre\>#isU", '<h3 class="troisieme_titre">$1</h3>', $chaine);
	$chaine = preg_replace("#\<titre niveau=4\>(.+)\</titre\>#isU", '<h4 class="quatrieme_titre">$1</h4>', $chaine);
	
	//Flottants
	$chaine = preg_replace("#\<flottant alignement=droite\>(.+)\</flottant\>#isU", '<div class="flottantDroite">$1</div>', $chaine);
	$chaine = preg_replace("#\<flottant alignement=gauche\>(.+)\</flottant\>#isU", '<div class="flottantGauche">$1</div>', $chaine);
	
	//Code
	$chaine = preg_replace("#\<balisecode type=(.+)\>(.+)\</balisecode\>#isU", '<div class="code"><div class="titre">Code : $1</div><p>$2</p></div>', $chaine);
	$chaine = preg_replace("#\<balisecode\>(.+)\</balisecode\>#isU", '<div class="code"><div class="titre">Code</div><p>$1</p></div>', $chaine);
	
	//Liste
	$chaine = preg_replace("#\<liste\>(.+)\</liste\>#isU", '<ul>$1</ul>', $chaine);
	$chaine = preg_replace("#\<puce\>(.+)\</puce\>#isU", '<li>$1</li>', $chaine);
	
	//Equipement
	$chaine = preg_replace("#\<item\>(.+)\</item\>#isU", '<a rel="$1" href="javascript:void(0);" class="receive-item-fiche">$1</a>', $chaine);
	$chaine = preg_replace("#\<item subs=(.+)\>(.+)\</item\>#isU", '<a rel="$2" href="javascript:void(0);" class="receive-item-fiche">$1</a>', $chaine);

	//Code
	preg_match_all("#\<police taille=(.+)\>(.+)\</police\>#isU", $chaine, $results);
	foreach($results[1] as $result) {
		if($result > 40) {
			$chaine = preg_replace("#\<police taille=".$result."\>(.+)\</police\>#isU", '<span style="font-size:40px !important;">$1</span>', $chaine);
		}
		if($result < 6) {
			$chaine = preg_replace("#\<police taille=".$result."\>(.+)\</police\>#isU", '<span style="font-size:6px !important;">$1</span>', $chaine);
		}
	}
	$chaine = preg_replace("#\<police taille=(.+)\>(.+)\</police\>#isU", '<span style="font-size:$1px !important;">$2</span>', $chaine);

	return $chaine;
}

function afficher_date_difference($date) {
	if(intval($date) == 0) { return "Jamais"; }
	
	$tampon = time();
	$diff = $tampon - $date;
	
	$dateDay = date('d', $date);
	$tamponDay = date('d', $tampon);
	$diffDay = $tamponDay - $dateDay;
	
	if($diff < 60 AND $diffDay == 0) {
		return 'Il y a '.$diff.'s';
	}
	elseif($diff < 600 AND $diffDay == 0) {
		return 'Il y a '.floor($diff/60).'mn et '.floor($diff%60).'s';
	}
	elseif($diff < 3600 AND $diffDay == 0) {
		return 'Il y a '.floor($diff/60).'mn';
	}
	elseif($diff < 7200 AND $diffDay == 0) {
		return 'Il y a '.floor($diff/3600).'h et '.floor(($diff%3600)/60).'mn';
	}
	elseif($diff < 24*3600 AND $diffDay == 0) {
		return 'Aujourd\'hui à '.date('H\hi', $date);
	}
	elseif($diff < 48*3600 AND $diffDay == 1) {
		return 'Hier à '.date('H\hi', $date);
	}
	else {
		return 'Le '.date('d/m/Y', $date).' à '.date('H\hi', $date);
	}
}

function afficherDateDifferenceNoTime($date) {
	if(intval($date) == 0) { return "Jamais"; }
	
	$tampon = time();
	$diff = $tampon - $date;
	
	$dateDay = date('d', $date);
	$tamponDay = date('d', $tampon);
	$diffDay = $tamponDay - $dateDay;
	
	if($diff < 60 AND $diffDay == 0) {
		return 'Il y a '.$diff.'s';
	}
	elseif($diff < 600 AND $diffDay == 0) {
		return 'Il y a '.floor($diff/60).'mn et '.floor($diff%60).'s';
	}
	elseif($diff < 3600 AND $diffDay == 0) {
		return 'Il y a '.floor($diff/60).'mn';
	}
	elseif($diff < 7200 AND $diffDay == 0) {
		return 'Il y a '.floor($diff/3600).'h et '.floor(($diff%3600)/60).'mn';
	}
	elseif($diff < 24*3600 AND $diffDay == 0) {
		return 'Aujourd\'hui';
	}
	elseif($diff < 48*3600 AND $diffDay == 1) {
		return 'Hier';
	}
	else {
		return 'Le '.date('d/m/Y', $date);
	}
}

function vider_cookie() {
	if($_SERVER['SERVER_NAME'] == 'localhost') {
		setcookie("id_membre", NULL, time()-1, "/");
		setcookie("pseudo", NULL, time()-1, "/");
		setcookie("password", NULL, time()-1, "/");
	}
	else {
		setcookie("id_membre", NULL, time()-1, "/", ".salixor.com");
		setcookie("pseudo", NULL, time()-1, "/", ".salixor.com");
		setcookie("password", NULL, time()-1, "/", ".salixor.com");
	}
}

function under_zero($nombre) {
	if($nombre < 0) {
		$nombre = 0;
	}
	return $nombre;
}

function is_email($adresse) { 
	$syntaxe = '#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#'; 
	if(preg_match($syntaxe,$adresse)) {
		return true; 
	}
	else { 
		return false;
	}
}

function is_naissance($naissance) {
	$syntaxe = "^([0-9]){2}/([0-9]){2}/([1-2])([0-9]){3}$";
	if(preg_match($syntaxe,$naissance)) {
		return true; 
	}
	else { 
		return false;
	}
}

function is_http($adresse) {
	$syntaxe = '#^http(s)?://([A-Z0-9][A-Z0-9.]*.)?[A-Z0-9][A-Z0-9\-.]{2,}.[A-Z]{2,4}#i';
	
	if(!empty($adresse)) {
		if(preg_match($syntaxe, $adresse)) {
			return true; 
		}
		else { 
			return false;
		}
	}
	else { 
		return true;
	}
}

function session_membre($pseudo, $id) {
	$_SESSION["pseudo"] = $pseudo;
	$_SESSION["id_membre"] = $id;
}

function code_aleatoire($nb_caracteres) {
	$caracteres = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
	$code_aleatoire = NULL;

	for($i=0; $i < $nb_caracteres; $i++) { 
		$code_aleatoire .= substr($caracteres, rand()%(strlen($caracteres)), 1); 
	}
	return $code_aleatoire; 
}

function too_much_stats($nombre, $limite) {
	if($nombre > $limite) {
		$nombre = $limite;
	}
	return $nombre;
}

function too_much_roxx($nombre, $limite, $ordre) {
	if($ordre == "<") {
		if($nombre < $limite) {
			$nombre = $limite;
			$mini_erreur = "<";
		}
	}
	elseif($ordre == ">") {
		if($nombre > $limite) {
			$nombre = $limite;
			$mini_erreur = ">";
		}
	}
	return $nombre;
	$test = serialize("hello");
	return $test;
}
		
function url2link($txt) {
	return preg_replace("`((?:https?|ftp)://\S+)(\s|\z)`", "<a href=\"$1\">$1</a>$2", $txt);
}

function linkify_tweet($tweet) {
	$tweet = preg_replace('/(^|\s)@(\w+)/',
		'\1<a href="http://www.twitter.com/\2">\@2</a>',
		$tweet);
	return preg_replace('/(^|\s)#(\w+)/',
		'\1<a href="http://search.twitter.com/search?q=%23\2">\#2</a>',
		$tweet);
}

function limiternews($text, $length, $ending = ' ...', $exact = false) {
    if(strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
        return $text;
    }
    preg_match_all('/(<.+?>)?([^<>]*)/is', $text, $matches, PREG_SET_ORDER);
    $total_length = 0;
    $arr_elements = array();
    $truncate = '';
    foreach($matches as $element) {
        if(!empty($element[1])) {
            if(preg_match('/^<\s*.+?\/\s*>$/s', $element[1])) {
            } else if(preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $element[1], $element2)) {
                $pos = array_search($element2[1], $arr_elements);
                if($pos !== false) {
                    unset($arr_elements[$pos]);
                }
            } else if(preg_match('/^<\s*([^\s>!]+).*?>$/s', $element[1], $element2)) {
                array_unshift($arr_elements,
                strtolower($element2[1]));
            }
            $truncate .= $element[1];
        }
        $content_length = strlen(preg_replace('/(&[a-z]{1,6};|&#[0-9]+;)/i', ' ', $element[2]));
        if($total_length >= $length) {
            break;
        } elseif ($total_length+$content_length > $length) {
            $left = $total_length>$length?$total_length-$length:$length-$total_length;
            $entities_length = 0;
            if(preg_match_all('/&[a-z]{1,6};|&#[0-9]+;/i', $element[2], $element3, PREG_OFFSET_CAPTURE)) {
                foreach($element3[0] as $entity) {
                    if($entity[1]+1-$entities_length <= $left) {
                        $left--;
                        $entities_length += strlen($entity[0]);
                    } else break;
                }
            }
            $truncate .= substr($element[2], 0, $left+$entities_length);
            break;
        } else {
            $truncate .= $element[2];
            $total_length += $content_length;
        }
    }
    if(!$exact) {
        $spacepos = strrpos($truncate, ' ');
        if(isset($spacepos)) {
            $truncate = substr($truncate, 0, $spacepos);
        }
    }
    $truncate .= $ending;
    foreach($arr_elements as $element) {
        $truncate .= '</' . $element . '>';
    }
    return $truncate;
}

$nb_requetes = 0;

function affiche_mois($date) {
	$mois = array('Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre ','Octobre','Novembre','Décembre');
	$affiche_mois = date("n", strtotime($date)) - 1;
	return $mois[$affiche_mois];
}

function date_jour_mois_annee($date) {
	return date("j", strtotime($date)).' '.affiche_mois($date).' '.date("Y", strtotime($date));
}

function date_heure($date) {
	return date("H:m:s", strtotime($date));
}

function query($requete) {
	global $nb_requetes;
	$nb_requetes++;
	$req_return = mysql_query($requete);
	return $req_return;
} 

function limiter($text) {
	$lg_max = 150;	
	if(strlen($text) > $lg_max) {
		$text = substr($text, 0, $lg_max);
		$last_space = strrpos($text, " ");
		$text .= " ...";
	}
	return strip_tags($text);
}

function limiterEffets($effets, $nbLignes, $href) {
	$effets = explode("\n", $effets);

	$i = 0;
	$effetsFinaux = "";
	
	foreach($effets as $effet) {
		if($i < $nbLignes - 1) {
			$effetsFinaux .= colorationDommages($effet);
		}
		elseif($i == $nbLignes) {
			$effetsFinaux .= colorationDommages(preg_replace("#\r#", " ", $effet));
		}
		elseif($i == $nbLignes + 1) {
			$effetsFinaux .= '<a href="'.$href.'" title="Voir la suite" class="suiteEquipement"> ...</a>';
		}
		$i++;
	}
	
	return $effetsFinaux;
}

function pagination($current_page, $nb_pages, $link, $around = 4, $firstlast = 1) {
	$pagination = '';
	$link = preg_replace('`%([^d])`', '%%$1', $link);
	
	if(!preg_match('`(?<!%)%d`', $link)) $link .= '%d';
		
	//Lien précédent
	if($current_page > 1)      $pagination .= '<a class="prevnext" href="'.sprintf($link, $current_page-1).'">&laquo;</a>';

	//Lien(s) début
	for($i = 1 ; $i <= $firstlast; $i++) {
		$pagination .= ' ';
		$pagination .= ($current_page==$i) ? '<span class="current">'.$i.'</span>' : '<a href="'.sprintf($link, $i).'">'.$i.'</a>';
	}

	// ... après pages début ?
	if(($current_page - $around) > $firstlast + 1)           $pagination .= ' <a href="'.sprintf($link, $i).'">&hellip;</a>';

	//On boucle autour de la page courante
	$start = ($current_page-$around)>$firstlast ? $current_page-$around : $firstlast+1;
	$end = ($current_page+$around)<=($nb_pages-$firstlast) ? $current_page+$around : $nb_pages-$firstlast;
	
	for($i = $start; $i <= $end; $i++) {
		$pagination .= ' ';
		if($i == $current_page)   $pagination .= '<span class="current">'.$i.'</span>';
		else                      $pagination .= '<a href="'.sprintf($link, $i).'">'.$i.'</a>';
	}

	//... avant page nb_pages ?
	if(($current_page + $around) < $nb_pages - $firstlast)    $pagination .= ' <a href="'.sprintf($link, $i).'">&hellip;</a>';

	//Lien(s) fin
	$start = $nb_pages - $firstlast + 1;
	
	if($start <= $firstlast)    $start = $firstlast+1;
	
	for($i=$start ; $i<=$nb_pages ; $i++ ) {
		$pagination .= ' ';
		$pagination .= ($current_page==$i) ? '<span class="current">'.$i.'</span>' : '<a href="'.sprintf($link, $i).'">'.$i.'</a>';
	}
	
	$pagination .= ' ';
	//Lien suivant
	if($current_page < $nb_pages)    $pagination .= '<a class="prevnext" href="'.sprintf($link, $current_page+1).'">&raquo;</a>';

	return $pagination;
}

//Trouver la location relative du document
function testLocation($dirNom) {
	$dir = opendir($dirNom);
	$fichier = array();
	$dossier = array();

	while($element = readdir($dir)) {
		if(!is_dir($dirNom.'/'.$element))      $fichier[] = $element;
		else                                   $dossier[] = $element;
	}

	closedir($dir);
	
	if(!empty($dossier)) {
		if(!in_array("Admin", $dossier)) {
			if($dirNom == '.')       $dirNom .= "./";
			else                     $dirNom .= "../";
			
			testLocation($dirNom);
		}
	}
	
	$firstLetter = $dirNom{0};
	$threeFirstLetters = substr($dirNom, 0, 3);

	if($firstLetter == "." && $threeFirstLetters != "../")     $dirNom = substr($dirNom, 1);
	
	return $dirNom;
}

//-----------------------------------------------------------------------------------------------//
//-------------------------Fonctions pour messages d'actions sur le site-------------------------//
//-----------------------------------------------------------------------------------------------//

$Location = testLocation('.');

$Arianne = '';
$arrayErreurs = array();
$arrayValides = array();
$arrayInformations = array();
$arrayAides = array();

function showArianne($Arianne) {
	global $Location;
	
	if(!empty($Arianne)) {
		echo '<div class="Arianne">';
			echo '<span><a href="'.$Location.'" class="home"></a> <a href="'.$Location.'">Accueil</a> > '.$Arianne.'</span>';
		echo '</div>';
	}
	else {
		echo '<div class="Arianne">';
			echo '<span><a href="'.$Location.'" class="home"></a> Accueil</span>';
		echo '</div>';
	}
}

function showErreur($arrayErreurs) {
	if(!empty($arrayErreurs)) {
		echo '<div class="encadreInfos erreur">';
			foreach($arrayErreurs as $erreur) {
				echo '<span>'.$erreur.'</span>';
			}
		echo '</div>';
	}
}

function showValide($arrayValides) {
	if(!empty($arrayValides)) {
		echo '<div class="encadreInfos valide">';
			foreach($arrayValides as $valide) {
				echo '<span>'.$valide.'</span>';
			}
		echo '</div>';
	}
}

function showInformation($arrayInformations) {
	if(!empty($arrayValides)) {
		echo '<div class="encadreInfos information">';
			foreach($arrayInformations as $information) {
				echo '<span>'.$information.'</span>';
			}
		echo '</div>';
	}
}

function showAide($arrayAides) {
	if(!empty($arrayValides)) {
		echo '<div class="encadreInfos aide">';
			foreach($arrayAides as $aide) {
				echo '<span>'.$aide.'</span>';
			}
		echo '</div>';
	}
}
?>