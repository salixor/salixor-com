<?php
class AfficherNews {
	public $nbNews;
	public $newsId;
	public $titreNews;
	public $page;
	public $nbPages;
	public $classe;
	public $requeteSql;
	public $querySql;
	public $sql;
	public $startRecup;
	public $erreur;
	public $nombreComms;
	public $nbTotal;
	public $sqlNews;

	public function __construct($nbNews, $newsId = NULL, $classe = NULL, $page = 1) {
		$this->nbNews = $nbNews;
		$this->newsId = $newsId;
		$this->classe = addslashes(addslashes($classe));
		$this->page   = $page;
		
		$this->start();
	}
	
	public function start() {
		$this->startRecup = ($this->page - 1) * $this->nbNews;
	
		if($this->classe != NULL) {
			$nbTotal  = query("SELECT COUNT(*) AS nb_total, titre FROM news WHERE classe = '".$this->classe."'");
			$this->sql = "WHERE news.classe = '".$this->classe."'";
		}
		elseif($this->newsId != NULL) {
			$nbTotal  = query("SELECT COUNT(*) AS nb_total, titre FROM news WHERE id = ".$this->newsId."");
			$this->sql = "
					LEFT JOIN news AS news_suivant 
					ON news.id + 1 = news_suivant.id
					LEFT JOIN news AS news_precedent 
					ON news.id - 1 = news_precedent.id
					WHERE news.id = ".$this->newsId."";
			$this->sqlNews = ", news_suivant.id AS suivant_id, news_suivant.titre AS suivant_titre, news_precedent.id AS precedent_id, news_precedent.titre AS precedent_titre";
		}
		else {
			$nbTotal  = query("SELECT COUNT(*) AS nb_total, titre FROM news");
			$this->sql = NULL;
		}
		
		$nbTotalReq  = mysql_fetch_assoc($nbTotal);
		$nbTotal  = $nbTotalReq['nb_total'];
		
		if($nbTotal == 0) {
			$this->hasErreur("NoFound");
		}
		
		$this->titreNews = stripslashes(utf8_encode($nbTotalReq['titre']));
		$this->nbPages  = ceil($nbTotal / $this->nbNews);
		
		if($this->page > $this->nbPages AND $this->erreur == NULL) {
			$this->hasErreur("PageUnfound");
		}
		
		$this->buildNews();
	}
	
	private function buildNews() {		
		$this->requeteSql = "SELECT news.*, COUNT(commentaires.id) AS nb_commentaires".$this->sqlNews."
							 FROM news 
							 LEFT JOIN commentaires ON news.id = commentaires.id_news
							 ".$this->sql."
							 GROUP BY news.id
							 ORDER BY news.id DESC
							 LIMIT ".$this->startRecup.", ".$this->nbNews."";
		$this->querySql = query($this->requeteSql);
		
		if(mysql_num_rows($this->querySql) == 0 AND $this->erreur == NULL) {
			$this->hasErreur("NoFound");
		}
	}
	
	public function showHead() {
		$this->nbTotal = $this->nbPages*$this->nbNews;
		$nbTotal = $this->nbPages*$this->nbNews;
		$nbActuelStart = $this->startRecup+1;
		$nbActuelEnd = $this->startRecup+mysql_num_rows($this->querySql);
		$nbActuel = $this->page*$this->nbNews;

		if($this->classe != NULL) {
			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Tag.png" alt="News" /> News classées sous \''.stripslashes($this->classe).'\' <a class="searchNews">Faire une recherche</a></h2>';
			$this->showSearch();
			$Arianne = '<a href="?Page=1">Les News</a> > <b>Tag :</b> '.stripslashes($this->classe);
		}
		elseif($this->newsId != NULL) {
			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/News.png" alt="News" /> Lecture d\'une News</h2>';
			$Arianne = '<a href="?Page=1">Les News</a> > Lecture d\'une News';
		}
		elseif(isset($_GET["Page"])) {
			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/News.png" alt="News" /> Les News <a class="searchNews">Faire une recherche</a></h2>';
			$this->showSearch();
			echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > Les News</p>';
		}
		else {
			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/News.png" alt="News" /> Les News <a class="searchNews">Faire une recherche</a></h2>';
			$this->showSearch();
		}
	}
	
	public function showSearch() { ?>
		<div id="rechercheNews" class="itemShowerSearch">
			<div class="top"></div>
				<div class="contenu">
					<div class="head">
						<span class="titre">Recherche de news</span>
					</div>
					
					<div class="wrap">
						<p>
							Vous devez remplir le premier champ avec jusqu'à 4 mots-clefs de plus de 3 lettres.
						</p>
						<p>
							<label for="recherche_champ">Mots-clefs : </label><input type="text" onkeyup="rechercheNews();" onchange="rechercheNews();" style="width:300px" name="recherche" id="recherche_champ" />
						</p>
						<p>
							<b>Filtre de modération : </b>
							<input onchange="rechercheNews();" type="radio" value="2" id="modere" name="filtre" checked="checked" /> <label for="modere" title="Un des mots-clefs entrés">Modéré</label>
								  
							<input onchange="rechercheNews();" type="radio" value="1" id="strict" name="filtre" /> <label for="strict" title="Tous les mots-clefs entrés">Strict</label>
						</p>
						
						<p>
							<b>Filtre de contenu : </b>
							<input onchange="rechercheNews();" type="radio" value="titre" id="titre" name="filtre_contenu" checked="checked" /> <label for="titre" title="Rechercher par Titre">Titre</label>
								  
							<input onchange="rechercheNews();" type="radio" value="contenu" id="contenu" name="filtre_contenu" /> <label for="contenu" title="Rechercher par Contenu">Contenu</label>
								  
							<input onchange="rechercheNews();" type="radio" value="classe" id="classe" name="filtre_contenu" /> <label for="classe" title="Rechercher par Classe">Classe</label>
						</p>
					</div>
					
					<div style="clear:both;">
				</div>
				<div class="foot"><span id="loading"><img src="Images/ajax-loader.gif" alt="" /> Chargement</span><div class="clear"></div></div>
			</div>
		</div>

		<div id="resultats"></div>		
	<?php }
	
	public function showNews() {
		while($dataSql       =   mysql_fetch_assoc($this->querySql)) {
		
			  $titreNews     =   stripslashes(utf8_encode($dataSql['titre']));
			  $dateEdit      =   $dataSql['edit_date']; 
			  $dateNews      =   $dataSql['date']; 
			  $classeNews    =   stripslashes(utf8_encode($dataSql['classe']));
			  $contenuNews   =   formatage(smileys(nl2br(stripslashes($dataSql['contenu']))));
			  $idNews        =   intval($dataSql['id']);
			  
			  $images        =   $dataSql["images"];
			  $imageExplode  =   explode(";", $images);
			  if(isset($imageExplode[0]))      $imageMini = $imageExplode[0];
			  if(isset($imageExplode[1]))      $imageBig = $imageExplode[1];
			  
			  $nombreComms   =   $dataSql['nb_commentaires']." commentaires";
			  $this->nombreComms   =  $nombreComms;

			  switch($dataSql['nb_commentaires']) {
				  case "0":
					  $nombreComms = "Aucun commentaire";
				  break;
				  
				  case "1":
					  $nombreComms = "1 commentaire";
				  break;
			  }
			  
			  $date = afficherDate($dateNews);
			  
			
			if(mysql_num_rows($this->querySql) > 1 || !empty($this->classe)) {
				$contenuNews = strip_tags(limiternews(preg_replace("#<p>#", " ", $contenuNews), 250));
				
				echo '<div class="news" id="News'.$this->newsId.'">';
					if(!empty($imageMini) && !empty($imageBig)) {
						if(file_exists($imageMini) && file_exists($imageBig)) {
							echo '
							<div class="imageLeft">
								<a href="'.ABSPATH.$imageBig.'" class="fancybox-news" title="'.$titreNews.'"><img src="'.ABSPATH.$imageMini.'" alt="" /></a>
							</div>';
						}
						else {
							echo '<div class="imageLeft"><img src="'.ABSPATH.'Images/News/Default-mini.png" alt="" /></div>';
						}
					}
					else {
						echo '<div class="imageLeft"><img src="'.ABSPATH.'Images/News/Default-mini.png" alt="" /></div>';
					}
					
					echo '<div class="head">';
						echo '<h4><a href="'.ABSPATH.'?News='.$idNews.'&amp;Titre='.optimiser_titre_et_classe($titreNews).'#News">'.$titreNews.'</a></h4>';
						echo '<div class="infos">';
							if($dateEdit == 0)    echo '<span class="date"><img src="'.ABSPATH.'Images/Forum/Time.png" alt="" /> Écrite le '.$date.'</span>';
							else                  echo '<span class="date"><img src="'.ABSPATH.'Images/Forum/Edition.png" alt="" /> Éditée le '.convertirTimestamp($dateEdit).'</span>';
							
							echo '
							<span class="tag"><a href="'.ABSPATH.'?Classe='.$classeNews.'#News"><img src="'.ABSPATH.'Images/Tag.png" alt="" /> '.$classeNews.'</a></span>
							<span class="comm"><a href="'.ABSPATH.'?News='.$idNews.'&amp;Titre='.optimiser_titre_et_classe($titreNews).'#Commentaires"><img src="'.ABSPATH.'Images/CommentaireInfos.png" alt="" /> '.$nombreComms.'</a></span>';
						echo '</div>';
					echo '</div>';
					
					echo '<div class="contenu">';
						echo $contenuNews;
					echo '</div>';
					
					echo '<a class="readRest" href="'.ABSPATH.'?News='.$idNews.'&amp;Titre='.optimiser_titre_et_classe($titreNews).'#News">Lire la suite</a>';

					echo '<div style="clear:left;"></div>';
				echo '</div>';
			}
			else {
				echo '<div class="news big" id="News'.$this->newsId.'" rel="'.$this->newsId.'">';
					
					echo '<div class="head">';
						echo '<h4><a href="'.ABSPATH.'?News='.$idNews.'&amp;Titre='.optimiser_titre_et_classe($titreNews).'#News">'.$titreNews.'</a></h4>';
						echo '<div class="infos">';
							if($dateEdit == 0)    echo '<span class="date"><img src="'.ABSPATH.'Images/Forum/Time.png" alt="" /> Écrite le '.$date.'</span>';
							else                  echo '<span class="date"><img src="'.ABSPATH.'Images/Forum/Edition.png" alt="" /> Édité le '.convertirTimestamp($dateEdit).'</span>';
						
							echo '
							<span class="tag"><a href="'.ABSPATH.'?Classe='.$classeNews.'#News"><img src="'.ABSPATH.'Images/Tag.png" alt="" /> '.$classeNews.'</a></span>
							<span class="comm"><a href="'.ABSPATH.'?News='.$idNews.'&amp;Titre='.optimiser_titre_et_classe($titreNews).'#Commentaires"><img src="'.ABSPATH.'Images/CommentaireInfos.png" alt="" /> '.$nombreComms.'</a></span>
							<span class="comm"><a href="'.ABSPATH.'?News='.$idNews.'&amp;Titre='.optimiser_titre_et_classe($titreNews).'#AjouterCommentaire"><img src="'.ABSPATH.'Images/Formatage/More.png" alt="" /> Ajouter son commentaire</a></span>';
						echo '</div>';
					echo '</div>';
					
					if(!empty($imageBig)) {
						if(file_exists($imageBig))    echo '<div class="image"><img src="'.ABSPATH.$imageBig.'" alt="" /></div>';
					}
					
					echo '<div class="contenu">';
						echo $contenuNews;
					echo '</div>';
					
					if(isset($_GET["News"]) && isset($_GET["Titre"])) {
						echo '<div class="navNews">';
							if(!empty($dataSql["precedent_id"])) {
								echo '<a href="'.ABSPATH.'?News='.$dataSql["precedent_id"].'&amp;Titre='.optimiser_titre_et_classe(utf8_encode($dataSql["precedent_titre"])).'#News"><div class="leftNews">&laquo; Précédente</div></a>';
							}
							if(!empty($dataSql["suivant_id"])) {
								echo '<a href="'.ABSPATH.'?News='.$dataSql["suivant_id"].'&amp;Titre='.optimiser_titre_et_classe(utf8_encode($dataSql["suivant_titre"])).'#News"><div class="rightNews">Suivante &raquo;</div></a>';
							}
						echo '</div>';
					}
					
					echo '<div style="clear:left;"></div>';
				echo '</div>';
			}
		}
	}
	
	public function showPagination() {
		if($this->erreur == NULL) {
			if($this->nbTotal > 5) {
				if($this->classe != NULL) {
					echo '<p class="pagination" align="center" style="width:760px;">'.pagination($this->page, $this->nbPages, "?Page=%d&amp;Classe=".$this->classe."").'</p>';
				}
				else {
					echo '<p class="pagination" align="center" style="width:760px;">'.pagination($this->page, $this->nbPages, "?Page=%d").'</p>';
				}
			}
		}
	}
	
	public function hasErreur($type) {
		switch($type) {
			case "NoFound":
				$this->erreur = 'Aucune news n\'a été trouvée. <a href="index.php">Retour</a>.';
			break;
			case "PageUnfound":
				$this->erreur = 'Cette page est introuvable. <a href="index.php">Retour</a>.';
			break;
			case "NotNumber":
				$this->erreur = "La page n'est pas un nombre.";
			break;
			case "Vide":
				$this->erreur = 'Un des paramètres est vide ou incorrect. <a href="index.php">Retour</a>.';
			break;
		}
	}
	
	public function showErreurs() {
		if($this->erreur != NULL) {
			echo '<div class="encadre_infos"><p><img src="'.ABSPATH.'Images/erreur.png" alt="Erreur" align="top" /> '.$this->erreur.'</p></div>';
		}
	}
}
?>