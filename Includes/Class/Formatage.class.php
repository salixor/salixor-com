<?php
class FormatagePanel {
	private $basique;
	private $paragraphe;
	private $couleur;
	private $zone;
	
	public function __construct($basique, $paragraphe, $couleur, $zone) {
		$this->basique = $basique;
		$this->paragraphe = $paragraphe;
		$this->couleur = $couleur;
		$this->zone = $zone;
	}
	
	public function showFormatagePanel() {
		$zone = $this->zone;
		
		echo '
			<span class="formatageNormal">
				<span class="formatage_bloc">
					<img src="'.ABSPATH.'Images/Formatage/Gras.png" title="Mettre en gras" class="formatage" onclick="add_tag(\'<gras>\', \'</gras>\', \''.$zone.'\');" />
					<img src="'.ABSPATH.'Images/Formatage/Italique.png" title="Mettre en italique" class="formatage" onclick="add_tag(\'<italique>\', \'</italique>\', \''.$zone.'\');" />
					<img src="'.ABSPATH.'Images/Formatage/Souligne.png" title="Souligner le texte" class="formatage" onclick="add_tag(\'<souligne>\', \'</souligne>\', \''.$zone.'\');" />
					<img src="'.ABSPATH.'Images/Formatage/Barre.png" title="Barrer le texte" class="formatage" onclick="add_tag(\'<surligne>\', \'</surligne>\', \''.$zone.'\');" />
				</span>
				<span class="formatage_bloc">
					<img onclick="add_tag(\'<alignement=gauche>\', \'</alignement>\', \''.$zone.'\');" src="'.ABSPATH.'Images/Formatage/Texte_gauche.png" title="Texte à gauche" class="formatage" />
					<img onclick="add_tag(\'<alignement=centre>\', \'</alignement>\', \''.$zone.'\');" src="'.ABSPATH.'Images/Formatage/Texte_centre.png" title="Texte au centre" class="formatage" />
					<img onclick="add_tag(\'<alignement=droite>\', \'</alignement>\', \''.$zone.'\');" src="'.ABSPATH.'Images/Formatage/Texte_droite.png" title="Texte à droite" class="formatage" />
					<img onclick="add_tag(\'<alignement=justifie>\', \'</alignement>\', \''.$zone.'\');" src="'.ABSPATH.'Images/Formatage/Texte_justify.png" title="Texte justifié" class="formatage" />
				</span>
				<span class="formatage_bloc">
					<img onclick="add_tag(\'<flottant alignement=gauche>\', \'</flottant>\', \''.$zone.'\');" src="'.ABSPATH.'Images/Formatage/FloatLeft.png" title="Flottant à gauche" class="formatage" />
					<img onclick="add_tag(\'<flottant alignement=droite>\', \'</flottant>\', \''.$zone.'\');" src="'.ABSPATH.'Images/Formatage/FloatRight.png" title="Flottant à droite" class="formatage" />
				</span>
				<span class="formatage_bloc">
					<img onclick="add_tag(\'\', \'\', \''.$zone.'\', \'citation\');" src="'.ABSPATH.'Images/Formatage/Citation.png" title="Citation" class="formatage" />
					<img onclick="add_tag(\'\', \'\', \''.$zone.'\', \'image\');" src="'.ABSPATH.'Images/Formatage/Image.png" title="Ajouter une image" class="formatage" />
					<img onclick="add_tag(\'\', \'\', \''.$zone.'\', \'lien\');" src="'.ABSPATH.'Images/Formatage/Lien.png" title="Ajouter un lien" class="formatage" />
					<img onclick="add_tag(\'\', \'\', \''.$zone.'\', \'liste\');" src="'.ABSPATH.'Images/Formatage/Liste.png" title="Ajouter une Liste" class="formatage" />
					<img onclick="add_tag(\'<balisecode>\', \'</balisecode>\', \''.$zone.'\');" src="'.ABSPATH.'Images/Formatage/Code.png" title="Ajouter du code" class="formatage" />
					<img onclick="add_tag(\'<balisecode type=\', \'></balisecode>\', \''.$zone.'\');" src="'.ABSPATH.'Images/Formatage/CodeType.png" title="Ajouter du code avec son type" class="formatage" />
				</span>
				<span class="formatage_bloc">
					<img onclick="add_tag(\'<item>\', \'</item>\', \''.$zone.'\');" src="'.ABSPATH.'Images/Formatage/Equipement.png" title="Ajouter une fiche d\'Équipement" class="formatage" />
					<img onclick="add_tag(\'<item subs=\', \'></item>\', \''.$zone.'\');" src="'.ABSPATH.'Images/Formatage/EquipementTitle.png" title="Ajouter une fiche d\'Équipement avec un texte de substitution" class="formatage" />
				</span>
				<span class="formatage_bloc">
					<img onclick="add_tag(\'<titre niveau=2>\', \'</titre>\', \''.$zone.'\');" src="'.ABSPATH.'Images/Formatage/Titre2.png" title="Ajouter un grand titre" class="formatage" />
					<img onclick="add_tag(\'<titre niveau=3>\', \'</titre>\', \''.$zone.'\');" src="'.ABSPATH.'Images/Formatage/Titre3.png" title="Ajouter un titre moyen" class="formatage" />
					<img onclick="add_tag(\'<titre niveau=4>\', \'</titre>\', \''.$zone.'\');" src="'.ABSPATH.'Images/Formatage/Titre4.png" title="Ajouter un petit titre" class="formatage" />
					<img onclick="add_tag(\'<paragraphe>\', \'</paragraphe>\', \''.$zone.'\');" src="'.ABSPATH.'Images/Formatage/Paragraphe.png" title="Ajouter un paragraphe" class="formatage" />
				</span>
			</span>';
		echo '
			<span class="formatageTexte">
				<span class="formatage_bloc" style="text-align:left; width:220px;">
					<img onclick="add_tag(\'<couleur=\', \'></couleur>\', \''.$zone.'\');" src="'.ABSPATH.'Images/Formatage/Brush.png" class="formatage" style="margin-right:5px;" title="Personnaliser la couleur du texte" />';
					$couleurs = array("#A0A", "#BF4040", "#D98026", "#EDB71D", "#284F1E", "#63BB4A", "#3F4C6B", "#317C70", "#06B");
					foreach($couleurs as $couleur) {
						echo '<img onclick="add_tag(\'<couleur='.$couleur.'>\', \'</couleur>\', \''.$zone.'\');" src="'.ABSPATH.'Images/Formatage/Vide.png" class="formatage" style="background:'.$couleur.' !important;" title="Colorer le texte" />';
					}
			echo '</span>
				<span class="formatage_bloc" style="text-align:right; width:300px;">
					<img onclick="add_tag(\'<police taille=\', \'></police>\', \''.$zone.'\');" src="'.ABSPATH.'Images/Formatage/Taille.png" class="formatage" style="margin-right:5px;" title="Personnaliser la taille du texte" />
					<a href="javascript:add_tag(\'<police taille=10>\', \'</police>\', \''.$zone.'\');" class="formatage" title="Texte de taille 10">10</a>
					<a href="javascript:add_tag(\'<police taille=12>\', \'</police>\', \''.$zone.'\');" class="formatage" title="Texte de taille 12">12</a>
					<a href="javascript:add_tag(\'<police taille=14>\', \'</police>\', \''.$zone.'\');" class="formatage" title="Texte de taille 14">14</a>
					<a href="javascript:add_tag(\'<police taille=16>\', \'</police>\', \''.$zone.'\');" class="formatage" title="Texte de taille 16">16</a>
					<a href="javascript:add_tag(\'<police taille=18>\', \'</police>\', \''.$zone.'\');" class="formatage" title="Texte de taille 18">18</a>
					<a href="javascript:add_tag(\'<police taille=20>\', \'</police>\', \''.$zone.'\');" class="formatage" title="Texte de taille 20">20</a>
					<a href="javascript:add_tag(\'<police taille=22>\', \'</police>\', \''.$zone.'\');" class="formatage" title="Texte de taille 22">22</a>
					<a href="javascript:add_tag(\'<police taille=24>\', \'</police>\', \''.$zone.'\');" class="formatage" title="Texte de taille 24">24</a>
					<a href="javascript:add_tag(\'<police taille=26>\', \'</police>\', \''.$zone.'\');" class="formatage" title="Texte de taille 26">26</a>
					<a href="javascript:add_tag(\'<police taille=28>\', \'</police>\', \''.$zone.'\');" class="formatage" title="Texte de taille 28">28</a>
					<a href="javascript:add_tag(\'<police taille=30>\', \'</police>\', \''.$zone.'\');" class="formatage" title="Texte de taille 30">30</a>
				</span>
			</span>
			<div style="clear:left;"></div>
		</p>';
		
		
		echo '
		<div class="pop_up" id="pop-up-lien" rel="'.$zone.'">
			<div class="titre">
				<div class="annuler" onclick="close_popup(200);"></div>
				<h3 class="troisieme_titre h3_titre">Insérer un lien</h3>
			</div>
			<div class="contenu" id="base">
				<p><label for="lien-url-pop-up">Lien </label><input type="text" id="lien-url-pop-up" /><span id="information-url-pop-up">http://www.exemple.fr</span></p>
				<p><label for="lien-text-pop-up">Libellé </label><input type="text" id="lien-text-pop-up" /><span id="information-text-pop-up">Facultatif</span></p>
			</div>
			<div class="footer_pop">
				<a href="javascript:void(0);" id="lien-send-pop-up">Ok</a>
				<a href="javascript:close_popup(200);">Annuler</a>
				<div style="clear:right;"></div>
			</div>
		</div>
		';
		
		echo '
		<div class="pop_up" id="pop-up-quote" rel="'.$zone.'">
			<div class="titre">
				<div class="annuler" onclick="close_popup(200);"></div>
				<h3 class="troisieme_titre h3_titre">Insérer une citation</h3>
			</div>
			<div class="contenu" id="base">
				<p><label for="quote-author-pop-up">Auteur </label><input type="text" id="quote-author-pop-up" /><span id="information-author-pop-up">Facultatif</span></p>
				<p><label for="quote-text-pop-up">Citation </label><textarea id="quote-text-pop-up"></textarea><span id="information-quote-pop-up">+ de 4 caractères</span></p>
			</div>
			<div class="footer_pop">
				<a href="javascript:void(0);" id="quote-send-pop-up">Ok</a>
				<a href="javascript:close_popup(200);">Annuler</a>
				<div style="clear:right;"></div>
			</div>
		</div>
		';
		
		echo '
		<div class="pop_up" id="pop-up-image" rel="'.$zone.'">
			<div class="titre">
				<div class="annuler" onclick="close_popup(200);"></div>
				<h3 class="troisieme_titre h3_titre">Insérer une image</h3>
			</div>
			<div class="contenu" id="base">
				<p><label for="image-url-pop-up">Lien </label><input type="text" id="image-url-pop-up" /><span id="information-url-image-pop-up">http://www.exemple.fr</span></p>
				<p><label for="image-titre-pop-up">Titre </label><input type="text" id="image-titre-pop-up" /><span id="information-image-pop-up">Facultatif</span></p>
			</div>
			<div class="footer_pop">
				<a href="javascript:void(0);" id="image-send-pop-up">Ok</a>
				<a href="javascript:close_popup(200);">Annuler</a>
				<div style="clear:right;"></div>
			</div>
		</div>
		<div class="full_black" onclick="close_popup(200);"></div>
		';
	}
}
?>