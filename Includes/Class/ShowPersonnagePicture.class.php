<?php
class ShowPersonnagePicture {
	private $pseudo;
	private $serveur;
	private $niveau;
	private $file;
	private $lien;
	private $erreur;
	private $problemeAnkama = "Non";
	
	public function __construct($pseudo, $serveur, $niveau) {
		echo '<div id="membre_personnage">';
		$this->pseudo = $pseudo;
		$this->serveur = ucfirst($serveur);
		if($niveau != "Inconnu") {
			$this->niveau = $niveau;
		}
		$this->file = "http://www.dofus.com/fr/persos-guildes/resultat?na=".$pseudo."&ji=26&s=0&type=char";
		
		switch($this->problemeAnkama) {
			case "Oui":
				$this->erreur = "Les serveurs d'Ankama sont inaccessibles.";
				$this->ShowErreur();
			break;
			case "Non":
				$this->VerificationPersonnage();
			break;
		}
	}
	
	public function VerificationPersonnage() {
		if($this->pseudo == "Inconnu" OR $this->serveur == "Inconnu") {
			$this->erreur = "Personnage non trouvé";
			$this->ShowErreur();
		}
		elseif($this->niveau <= 20) {
			$this->erreur = "Personnage non trouvé";
			$this->ShowErreur();
		}
		else {
			$this->GetPagePersonnage();
		}
	}
	
	public function GetPagePersonnage() {
		$GetPagePersonnage = fopen($this->file, "r");
	
		$i = 0;
		while($line = fgets($GetPagePersonnage)) {
			if(preg_match("#\<a title=\"".$this->pseudo." ".$this->serveur."\"#", $line)) {
				$this->lien = preg_replace("#\<a title=\"(.+)\" href=\"(.+)\"\>(.+)</a>#", "$2", $line);
				$this->lien = "http://www.dofus.com".trim($this->lien);
				$this->i++;
			}
			if(preg_match("#<strong>0</strong>#", $line)) {
				$this->erreur = "Personnage non trouvé";
			}
		}
		if($this->erreur != NULL) {
			$this->ShowErreur();
		}
		else {
			$this->ShowPicture();
		}

		fclose($GetPagePersonnage);
	}
	
	public function ShowPicture() {
		echo '<p id="membre_personnage_link"><a href="'.$this->lien.'">Accéder à la page personnelle</a></p>';
		
		if(!isset($this->erreur) AND isset($this->lien)) {
			$ShowPicture = fopen($this->lien, "r");
			
			while($line = fgets($ShowPicture)) {
				if(preg_match('#\<object id="perso_avatar"#', $line)) {
					echo '<object type="application/x-shockwave-flash" data="http://staticns.ankama.com/dofus/www/swf/pages_persos/DofusPersos.swf" width="154" height="200">';
					$line = preg_replace('#height="100"#', 'height="200"', $line);
					$line = preg_replace('#width="100"#', 'width="154"', $line);
					echo '<param name="allowscriptaccess" value="always" />';
				}
				if(preg_match('#\<param name="flashvars" value="align#', $line)) {
					$line = preg_replace('#&focus=face#', '', $line);
					$line = preg_replace('#align=TL&#', '', $line);
					$line = preg_replace('#&#', '&amp;', $line);
					echo $line;
				}
			}
			echo '
			<param name="quality" value="high" />
			<param name="wmode" value="transparent" />
			<param name="menu" value="false" />
			<p class="erreur" style="text-align:left;">
				<a href="http://www.adobe.com/go/gntray_dl_getflashplayer_fr">Adobe Flash Player</a> est requis.<br />
				Déjà installé ? Allez <a href="'.$this->lien.'">ici</a>.
			</p>
			</object>';

			fclose($ShowPicture);
			$this->Finalisation();
		}
		elseif(!isset($this->lien) AND !isset($this->erreur)) {
			$this->erreur = "Image non récupérée";
			$this->ShowErreur();
		}
	}
	
	public function ShowErreur() {
		echo '<p class="erreur">'.$this->erreur."</p>";
		$this->Finalisation();
	}
	
	public function Finalisation() {
		echo '</div>';
	}
}
?>