<?php
class verificationValidite {
	private $lien;
	private $valide;
	public $retour;
	
	public function __construct($lien) {
		$this->lien = "http://validator.w3.org/check?uri=".$lien;
		
		$this->Open();
	}
	
	public function Open() {
		$verificationValidite = fopen($this->lien, "r");
	
		while($line = fgets($verificationValidite)) {
			if(preg_match("#\<h2 id=\"results\" class=\"invalid\"\>Errors found while checking this document as XHTML 1.0 Transitional!\</h2\>#", $line)) {
				$this->valide = 'false';
				$this->ReturnEnd();

				fclose($verificationValidite);
			}
			if(preg_match("#\<h2 class=\"valid\"\>This document was successfully checked as 
     XHTML 1.0 Transitional!\</h2\>#", $line)) {
				$this->valide = 'true';
				$this->ReturnEnd();
			
				fclose($verificationValidite);
			}
			else {
				$this->valide = 'euhhhh ?';
				$this->ReturnEnd();
				
				fclose($verificationValidite);
			}
		}
	}
	
	public function ReturnEnd() {
		switch($this->valide) {
			case 'true':
				$this->retour = "Cette page est valide XHTML 1.0 Transitional";
			break;
			case 'false':
				$this->retour = "Cette page est non valide XHTML 1.0 Transitional";
			break;
			case 'euhhhh':
				$this->retour = "Erreur lors de la vérification de validité de la page";
			break;
		}
	}
}
?>