<?php
if($_SERVER["PHP_SELF"] == "/Salixor/Includes/itemSidebar.php" || $_SERVER["PHP_SELF"] == "/Includes/itemSidebar.php") {
	require_once('../Includes/Fonctions.php');
	require_once('../Includes/Config.php');
}
if($_SERVER["PHP_SELF"] == "/Salixor/index.php" || $_SERVER["PHP_SELF"] == "/index.php") {
	require_once('Includes/Fonctions.php');
	require_once('Includes/Config.php');
}
		
$requete = $baseDeDonnes->query('
SELECT equipements.id AS equi_id, equipements.*, panoplies.*
FROM equipements
LEFT JOIN panoplies ON panoplies.id = equipements.panoplie
WHERE equipements.equipement_ajoute = \'2\'
ORDER BY rand() DESC
LIMIT 0, 1');
$requete->execute();

$data = $requete->fetch();

$image = imageEquipement(ucfirst($data["type"]), $data['nom']);
$nom = tronquer(stripslashes(utf8_encode($data['nom'])), 23, "...");
$panoplie = tronquer(utf8_encode($data["nom_panoplie"]), 23, "...");

$niveau = $data["niveau"];
$id = intval($data['equi_id']);
$listeTypes = array('Chapeaux' => 'Chapeau', 'Capes' => 'Cape', 'Sacs' => 'Sac', 'Anneaux' => 'Anneau', 'Amulettes' => 'Amulette', 'Bottes' => 'Botte', 'Ceintures' => 'Ceinture', 'Dofus' => 'Dofus', 'Boucliers' => 'Bouclier', 'Arcs' => "Arc", "Baguettes" => "Baguette", "Batons" => "Baton", "Dagues" => "Dague", "Epees" => "Epee", "Marteaux" => "Marteau", "Pelles" => "Pelle", "Haches" => "Hache", "Pioches" => "Pioche", "Faux" => "Faux", "Familiers" => "Familier");
$lien = ABSPATH.'Equipements/?Page='.$listeTypes[ucfirst($data["type"])].'#Item-'.$id;
$boost = limiterEffets($data['boost'], 5, $lien);
$boost = stripslashes(utf8_encode(nl2br($boost)));


if($requete->rowCount() > 0) {
	echo '<div class="sideEqui">';
		echo '<div class="infoEquipement">';
			echo '<p class="imageEquipement"><a href="'.$lien.'">'.$image.'</a></p>';
			echo '<p class="diversEquipement">';
				echo '<span><a href="'.$lien.'">'.$nom.'</a></span><br />';
				echo '<span>Niveau '.$niveau.'</span><br />';
				if(!empty($data["nom_panoplie"]) AND !empty($data["niveau_panoplie"]))      echo '<span><a href="'.ABSPATH.'Equipements/Panoplies.php?Panoplie='.$data["id"].'">'.$panoplie.'</a></span>';
			echo '</p>';
		echo '</div>';
		echo '<p class="anEquipement">';
			echo $boost;
		echo '</p>';	
	echo '</div>';
}
else {
	echo '<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" align="top" /> Aucun équipement.';
} 
?>