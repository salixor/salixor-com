<?php
define('TEMPS_DEBUT', microtime(true));
define('RCDofus', 'Desactive');
define('VERSION', '2.3');

//Les fichiers utiles
require_once('Includes/Fonctions.php');
require_once('Includes/Config.php');

//Les variables utiles un peu partout
$MEMBRES_RANGS = array(0 => "Membre Banni", 1 => "Non Membre", 2 => "Membre", 3 => "Béta Testeur", 4 => "Modérateur", 5 => "Administrateur");
$MEMBRES_RANGS_IMAGES = array(0 => "Banni", 2 => "Membre", 3 => "BetaTesteur", 4 => "Moderateur", 5 => "Administrateur");
$GLOBALS["TABLEAU_VERSIONS"] = array("1.0" => "&quot;Commencement&quot;",
									 "1.1" => "Goultarminator II",
									 "1.2" => "Les News",
									 "1.3" => "&quot;Nowel&quot;",
									 "1.4" => "&quot;Fonte des Glaces&quot;",
									 "1.5" => "Refonte Graphique",
									 "1.6" => "AJAXisation et modifications PHP",
									 "1.6.1" => "Amélioration de la version 1.6",
									 "1.7.a01" => "Espace Membre - Version Alpha 01",
									 "1.7.a02" => "Espace Membre - Version Alpha 02",
									 "1.7.a03" => "Espace Membre - Version Alpha 03",
									 "1.7.b01" => "Espace Membre - Version Beta 01",
									 "1.7.b02" => "Espace Membre - Beta 02",
									 "1.7" => "Espace Membre",
									 "1.8" => "Pâques !",
									 "1.8.1" => "Panoplies",
									 "1.8.2" => "Le Chat",
									 "1.8.3" => "Nouveau Menu",
									 "1.9" => "Les Dossiers",
									 "1.10.b00" => "Le Forum - Premier Test",
									 "1.10.b01" => "Le Forum - Béta Test 01",
									 "1.10" => "Le Forum",
									 "1.10.1" => "&quot;Salix'kode Complete&quot;",
									 "2.0" => "&quot;Happy Birthday Salixor.com&quot;",
									 "2.1" => "Le Goultarminator",
									 "2.2" => "Les Guildes",
									 "2.3" => "Le Retour");

$actualLocation = testLocation('.');
									 
//Si le site est en maintenance
if(defined('MAINTENANCE')) {
	if(isset($NIVEAU_MEMBRE)) {
		if($NIVEAU_MEMBRE < 5) {
			if($titre_page_nom != "Maintenance")     header("Location: ".$actualLocation."Maintenance");
		}
	}
	else {
		if($titre_page_nom != "Maintenance")     header("Location: ".$actualLocation."Maintenance");
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title><?php echo $titre_page_nom." - Salixor.com"; ?></title>

<meta http-equiv="Content-Type" content="charset=UTF-8" />
<meta http-equiv="Content-language" content="fr" />
<meta name="robots" content="follow, index, all" />
<meta name="description" content="Salixor.com, c'est une encyclopédie crée spécialement pour les Dofusiens avide d'informations sur le jeu. Retrouvez un tas de ressources sur le jeu aisi qu'une partie communautaire que vous pouvez rejoindre. Des outils sont disponibles sur le site, pour améliorer votre expérience globale du jeu." />
<meta name="keywords" content="dofus, salixor, ankama, mmorpg, communauté, aide, astuces, wakfu, donjons, espace membre" />

<link href="<?php echo ABSPATH; ?>Styles.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" type="image/x-icon" href="<?php echo ABSPATH; ?>Images/Favicon.ico" />
<link rel="icon" type="image/png" href="<?php echo ABSPATH; ?>Images/Favicon.png" />
<link rel="apple-touch-icon-precomposed" href="<?php echo ABSPATH; ?>Images/Apple_icon-precomposed.png"/>

<link href="<?php echo ABSPATH; ?>Scripts/tipsy.css" rel="stylesheet" type="text/css" />
<link href="<?php echo ABSPATH; ?>Scripts/Fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" />

<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php echo ABSPATH; ?>Includes/RSS.xml" />

<script type="text/javascript" src="<?php echo ABSPATH; ?>Scripts/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="<?php echo ABSPATH; ?>Scripts/Fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="<?php echo ABSPATH; ?>Scripts/AJAX.js"></script>
<script type="text/javascript" src="<?php echo ABSPATH; ?>Scripts/general.js"></script>
<script type="text/javascript" src="<?php echo ABSPATH; ?>Scripts/jquery-ui-1.8.12.custom.min.js"></script>
<script type="text/javascript" src="<?php echo ABSPATH; ?>Membres/ModificationEquipementPersonnage.jquery.ajax.js"></script>
<script type="text/javascript" src="<?php echo ABSPATH; ?>Scripts/magalerie.jquery.js"></script>

</head>

<body>

<?php
if(isset($ID_MEMBRE)) {
	echo '<span style="display:none;" id="MEMBRE_ID">'.$ID_MEMBRE.'</span>';
	echo '<span style="display:none;" id="MEMBRE_PSEUDO">'.$PSEUDO_MEMBRE.'</span>';
	
	//Thème et Navigation globale
	$requete = $baseDeDonnes->prepare("SELECT membre_style, membre_avatar FROM membres WHERE membre_id = :id");
	$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
	$requete->execute();

	$dataTheme = $requete->fetch();

	$themeMembre = $dataTheme["membre_style"];	

	if(!empty($dataTheme["membre_avatar"]))      $avatarMembre = ABSPATH."Images/Avatars/".$dataTheme["membre_avatar"];
	else                                         $avatar = ABSPATH."Images/Avatars/avatar_normal.png";
	
	$avatarAffiche = '<img src='.$avatarMembre.' alt="" class="avatarMembre" />';
	
	$administration = '';
	if($NIVEAU_MEMBRE >= 5)     $administration = '<a href="'.ABSPATH.'Admin" class="admin">Administration</a>';

	echo '
	<div class="navigationGenerale">
		<span class="left">
			<span class="deroulerMenu">
				'.$avatarAffiche.' '.$PSEUDO_MEMBRE.'
				<span class="fleche"></span>
			</span>';
			
			$requete = $baseDeDonnes->prepare("SELECT COUNT(*) AS nb_messages_non_lus FROM messagerie WHERE destinataire = :id AND vu = '0'");
			$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
			$requete->execute();
			
			$data = $requete->fetch();
			
			$nbMessagesNonLus = '';
			$styleMessagesRecus = '';
			$messageAAfficher = '';
			
			if($data["nb_messages_non_lus"] > 0)           $nbMessagesNonLus = $data["nb_messages_non_lus"];
			if($nbMessagesNonLus > 0)                      $styleMessagesRecus = 'newMessages';
		
			$requete->closeCursor();
			
			if($nbMessagesNonLus > 0)                      $messageAAfficher = '('.$nbMessagesNonLus.')';
			
			echo '
			<span class="bouton messages '.$styleMessagesRecus.'">
				Mes Messages '.$messageAAfficher.'
				<span class="fleche"></span>
				
				<ul class="menuMessages">';
					$requete = $baseDeDonnes->prepare("
					SELECT * FROM messagerie
					LEFT JOIN membres ON messagerie.expediteur = membres.membre_id
					WHERE destinataire = :id ORDER BY id DESC LIMIT 0, 5");
					$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
					$requete->execute();
					
					if($requete->rowCount() > 0) {
						while($data = $requete->fetch()) {
							if(!empty($data["membre_avatar"]))      $avatar = ABSPATH."Images/Avatars/".$data["membre_avatar"];
							else                                    $avatar = ABSPATH."Images/Avatars/avatar_normal.png";
							
							$avatar = '<img class="avatar" src='.$avatar.' alt="" />';
							
							$rouge = '';
							if($data["vu"] != '1')      $rouge = 'class="rouge"';

							echo '
							<a href="'.ABSPATH.'Membres/Messagerie.php?Action=Voir&amp;Message='.$data["id"].'" '.$rouge.'>
								<li>
									'.$avatar.'
									<div class="infosMessage">
										<div class="pseudo">'.stripslashes($data["membre_pseudo"]).'<span class="subject">'.stripslashes($data["sujet"]).'</span></div>
										<div class="message">'.smileys(tronquer(stripslashes($data["message"]), 50)).'</div>
									</div>
									<div class="clear"></div>
								</li>
							</a>';
						}
					}
					else {
						echo '
						<a href="'.ABSPATH.'Membres/Messagerie.php?Action=Envoyer">
							<li>
								<img class="avatar" src='.$avatarMembre.' alt="" />
								<div class="infosMessage">
									<div class="pseudo">Aucun message</div>
									<div class="message">Cliquez sur ce lien pour en composer un.</div>
								</div>
								<div class="clear"></div>
							</li>
						</a>';
					}
					
					echo '<div class="others">';
						if($nbMessagesNonLus == 1)      echo '<a href="javascript:void(0)" class="read-messages admin">Marquer comme lu</a>';
						if($nbMessagesNonLus > 1)       echo '<a href="javascript:void(0)" class="read-messages admin">Marquer comme lus</a>';
						
						echo '
						<a href="'.ABSPATH.'Membres/Messagerie.php" class="green">Ma Messagerie</a>
						<div class="clear"></div>
					</div>
				</ul>
			</span>
			
			<span class="selectStyle" href="'.ABSPATH.'Membres/Membres.php">
				Changer de thème
				<span class="fleche"></span>
				
				<ul class="menuStyle">';					
					if((!file_exists($actualLocation.'Images/Themes/'.$PSEUDO_MEMBRE.'.png') AND $themeMembre == $PSEUDO_MEMBRE) || $themeMembre == "") {
						$requete = $baseDeDonnes->prepare("UPDATE membres SET membre_style = :theme WHERE membre_id = :id");
						$requete->bindValue(":theme", "Aucun", PDO::PARAM_STR);
						$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
						$requete->execute();
						
						$themeMembre = "Aucun";
					}
					
					if(!file_exists($actualLocation.'Images/Themes/'.$PSEUDO_MEMBRE.'.png')) {
						$arrayRels = array("Aucun", "Normal", "Ete", "Noel", "Ballotin");
						$arrayThemes = array("Aucun thème", "Thème Normal", "Thème d'Été", "Thème de Noël", "Thème de la Saint Ballotin");
						$arrayThemesDescriptions = array("Désactiver le système de thème.", "Le thème par défaut.", "Le thème spécial d'été.", "Le thème spécial de Noël", "Le thème spécial de la Saint Ballotin.");
						$choisirTheme =
						'<a href="'.ABSPATH.'Membres/Modifier_Profil.php?Action=Style" class="none">
							<li>
								<img src="'.$avatarMembre.'" alt="" class="avatar" />
								<div class="infosMessage">
									<div class="pseudo">Thème personnalisé.</div>
									<div class="message">Page pour créer le thème personnalisé.</div>
								</div>
								<div class="clear"></div>
							</li>
						</a>';
					}
					else {
						$arrayRels = array("Aucun", "Normal", "Ete", "Noel", "Ballotin", $PSEUDO_MEMBRE);
						$arrayThemes = array("Aucun thème", "Thème Normal", "Thème d'Été", "Thème de Noël", "Thème de la Saint Ballotin", "Thème personnalisé");
						$arrayThemesDescriptions = array("Désactiver le thème.", "Le thème par défaut.", "Le thème spécial d'été.", "Le thème spécial de Noël", "Le thème spécial de la Saint Ballotin.", "Le thème personnalisé selon vos goûts.");
						$choisirTheme = '';
					}

					$i = 0;
					$final = '';
					$stock = '';
					
					function showTheme($rel, $theme, $themeDescription, $selected = 0) {
						global $PSEUDO_MEMBRE;
						global $avatarMembre;
						$ajoutSelected = '';
						
						if($selected == 1) {
							$selected = 'class="selected"';
							$ajoutSelected = '<div class="avatarIconeTheme"></div>';
							$themeDescription = 'Ce thème est actuellement sélectioné.';
						}
						
						if($rel == $PSEUDO_MEMBRE)  $imagePreview = $avatarMembre;
						else                        $imagePreview = ABSPATH.'/Images/Themes/Preview/'.$rel.'.png';
						
						return
						'<a href="javascript:void(0);" rel="'.$rel.'" '.$selected.'>
							<li>
								<img src="'.$imagePreview.'" alt="" class="avatar" /> '.$ajoutSelected.'
								<div class="infosMessage">
									<div class="pseudo">'.$theme.'</div>
									<div class="message">'.$themeDescription.'</div>
								</div>
								<div class="clear"></div>
							</li>
						</a>';
					}
					
					if($titre_page_nom != "Modifier le Thème personnalisé")   echo $choisirTheme;
					
					foreach($arrayRels as $rel) {
						$theme = $arrayThemes[$i];
						$themeDescription = $arrayThemesDescriptions[$i];
						
						if($themeMembre == $rel && in_array($themeMembre, $arrayRels) && $rel != "Perso")        $final = showTheme($rel, $theme, $themeDescription, 1);
						elseif($themeMembre != $rel && !in_array($themeMembre, $arrayRels))                      $final = showTheme($rel, $theme, $themeDescription, 1);
						elseif($themeMembre != $rel && in_array($themeMembre, $arrayRels) && $rel != "Perso")    echo showTheme($rel, $theme, $themeDescription);
						
						$i++;
					}
					
					echo $final.'
				</ul>
			</span>
			
			<span class="leftInterne">
				<a class="bouton" href="'.ABSPATH.'Membres/Profil.php">Mon Profil</a>
				<a class="bouton" href="'.ABSPATH.'Membres/Membres.php">Les Membres</a>
				<a class="bouton" href="'.ABSPATH.'Membres/Personnages.php">Les Personnages</a>
				<a class="bouton" href="'.ABSPATH.'Membres/Guildes">Les Guildes</a>
			</span>
		</span>
		
		<span class="right">
			<a href="#Haut" class="bouton" style="opacity:0; cursor:default;">Haut</a>
			<a href="#Bas" class="bouton">Bas</a>
		</span>
		
		<ul class="menuProfil">
			<a href="'.ABSPATH.'Membres/Modifier_Profil.php?Action=MotDePasse">
				<li>
					<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/MotDePasse.png" alt="" /> Modifier mon Mot de Passe
					<span class="fleche"></span>
				</li>
			</a>
			<a href="'.ABSPATH.'Membres/Modifier_Profil.php?Action=Email">
				<li>
					<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/EmailEdit.png" alt="" /> Modifier mon adresse Mail
					<span class="fleche"></span>
				</li>
			</a>
			<a href="'.ABSPATH.'Membres/Modifier_Profil.php?Action=Titre">
				<li>
					<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/TitreEdit.png" alt="" /> Modifier le Titre
					<span class="fleche"></span>
				</li>
			</a>
			<a href="'.ABSPATH.'Membres/Modifier_Profil.php?Action=Signature">
				<li>
					<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Signature.png" alt="" /> Modifier ma Signature
					<span class="fleche"></span>
				</li>
			</a>
			<a href="'.ABSPATH.'Membres/Modifier_Profil.php?Action=Citation">
				<li>
					<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Citation.png" alt="" /> Modifier ma Citation
					<span class="fleche"></span>
				</li>
			</a>
			<a href="'.ABSPATH.'Membres/Modifier_Profil.php?Action=Dofus">
				<li>
					<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Personnages.png" alt="" /> Gérer mes Personnages
					<span class="fleche"></span>
				</li>
			</a>
			<a href="'.ABSPATH.'Membres/Modifier_Profil.php?Action=Publiques">
				<li>
					<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Publiques.png" alt="" /> Gérer mes informations publiques
					<span class="fleche"></span>
				</li>
			</a>
			<a href="'.ABSPATH.'Membres/Modifier_Profil.php?Action=Avatar">
				<li>
					<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Avatar.png" alt="" /> Modifier mon Avatar
					<span class="fleche"></span>
				</li>
			</a>
			
			<div class="others">
				'.$administration.'
				<a href="'.ABSPATH.'Membres/Deconnexion.php">Déconnexion</a>
				<div class="clear"></div>
			</div>
		</ul>
	</div>';
}
else {	
	$avatar = ABSPATH."Images/Avatars/avatar_normal.png";
	if(!isset($_SERVER["HTTP_REFERER"]))  $_SERVER["HTTP_REFERER"] = ABSPATH;
	
	echo '
	<div class="navigationGenerale">
		<span class="left">
			<span class="deroulerMenu">
				<img src='.$avatar.' alt="" class="avatarMembre" /> Connexion
				<span class="fleche"></span>
			</span>
			
			<span class="leftInterne">
				<a class="bouton" href="'.ABSPATH.'Membres/Membres.php">Les Membres</a>
				<a class="bouton" href="'.ABSPATH.'Membres/Personnages.php">Les Personnages</a>
				<a class="bouton" href="'.ABSPATH.'Membres/Guildes">Les Guildes</a>
			</span>
		</span>
		
		<span class="right">
			<a href="#Haut" class="bouton" style="opacity:0; cursor:default;">Haut</a>
			<a href="#Bas" class="bouton">Bas</a>
		</span>

		<ul class="menuProfil">
			<div class="others bottom">
				<a href="'.ABSPATH.'Membres/Inscription.php" class="admin">Inscription</a>
				<span class="border" style="left:120px;"></span>
				<a href="'.ABSPATH.'Membres/Modifier_Profil.php?Action=Restauration">Mot de Passe oublié</a>
				<div class="clear"></div>
			</div>
			<form action="'.ABSPATH.'Membres/Connexion.php" method="post">
				<p><label for="pseudo-topbar">Pseudo</label> <input type="text" name="pseudo" id="pseudo-topbar" autocomplete="off" /></p>
				<p><label for="pasword-topbar">Mot de passe</label> <input type="password" name="password" id="pasword-topbar" autocomplete="off" /></p>
				<p><label for="save_pseudo" title="Permet d\'être connecté à chaque visite">Automatique</label> <input type="checkbox" name="save_pseudo" id="save_pseudo" /></p>
				<input type="text" name="adresse_redirection" style="display:none;" value="'.$_SERVER["HTTP_REFERER"].'" />
				<p align="center"><input type="submit" name="submit" value="Connexion"  /></p>
			</form>
		</ul>
	</div>';
}

echo '<div class="fullBlackNaviGenerale"></div>';

if(in_array($themeMembre, $arrayRels))      				                 $imageTheme = ABSPATH.'Images/Themes/'.$themeMembre.'.png';
else                                        				                 $imageTheme = ABSPATH.'Images/Themes/Aucun.png';

if(!file_exists($actualLocation.'Images/Themes/'.$themeMembre.'.png'))       $imageTheme = ABSPATH.'Images/Themes/Aucun.png';

$classeHeader = "headerContent";
if($themeMembre == "Ballotin")          $classeHeader = "headerContent ballotin";
?>

<div class="header" id="Haut">
	<div class="<?php echo $classeHeader; ?>">
		<a href="<?php echo ABSPATH; ?>" class="retourAccueil"></a>

		<noscript class="javascriptDisabled">
		Activez JavaScript pour une navigation optimale.
		</noscript>
	</div>
	<div class="background" style="background:url(<?php echo $imageTheme; ?>) no-repeat;"></div>
</div>

<div id="menuHeader">
	<ul class="content">
		<a href="<?php echo ABSPATH; ?>"><li>Accueil</li></a>
		<a href="<?php echo ABSPATH; ?>Forum"><li>Forum</li></a>
		<a href="<?php echo ABSPATH; ?>Equipements"><li>Équipements</li></a>
		<a href="<?php echo ABSPATH; ?>Guides/Donjons"><li>Donjons</li></a>
		<a href="<?php echo ABSPATH; ?>Membres/Guildes" class="new"><li>Guildes</li></a>
		<a href="<?php echo ABSPATH; ?>Membres"><li>Membres</li></a>
		<a href="<?php echo ABSPATH; ?>Chat"><li>Chat</li></a>
		<div class="clear"></div>
	</ul>
</div>

<div class="clear"></div>

<span class="loader"><img src="<?php echo ABSPATH; ?>Images/LoaderGeneral.gif" alt="" /> Chargement</span>

<?php
	showArianne($Arianne);
	showErreur($arrayErreurs);
	showValide($arrayValides);
	showInformation($arrayInformations);
	showAide($arrayAides);
?>

<div id="conteneur">
	
<?php
	echo '<div class="itemShower"><div class="bottom"></div></div>';
	echo '
	<div class="ajouter_equipement" id="information_ajouter_equipement">
		<span class="delete_value_champ" style="margin:0; top:12px; right:12px; background:none;" onclick="$(\'#information_ajouter_equipement\').fadeOut(300);">×</span>
				
		<div class="conteneur">
			<div class="contenu"></div>
			<div class="validation">
				<p class="Arianne" style="text-align:left; margin:0; padding:5px;" id="info-equipement"></p>
				
				<div class="choixPersonnageInfos validate">
					<span class="changerPerso"><a href="javascript:void(0);">Changer de personnage</a></span>
				
					<div class="image">-</div>
					<div class="pseudo">-</div>
					<div class="little">-</div>
					<div class="clear"></div>
				</div>
				
				<div class="fleche"></div>
				
				<div class="choixPersonnageInfos equipement">
					<div class="image">-</div>
					<div class="pseudo">-</div>
					<div class="little">-</div>
				</div>

				<p><input type="submit" value="Confirmer" id="sendPersonnage" /></p>
			</div>
		</div>
	</div>';
	echo '<div class="notification"></div>';
	echo '
	<div class="maGalerie">
		<div class="bigImage"></div>
		<div class="prev"><span></span></div>
		<div class="next"><span></span></div>
		<div class="close"></div>
		<div class="full_black" style="background:rgba(0,0,0,0.2)"></div>
	</div>';
?>