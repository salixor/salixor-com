<?php
$titre_page_nom = "Admin";
include("../Header.php");


include("OnlineForum.php");

if($NIVEAU_MEMBRE == 5) {
	$Action = htmlspecialchars($_GET['Action']);

	switch($Action) {
		case "Forum":
			$gesture = htmlspecialchars($_GET['Faire']);
			switch($gesture) {
				case "Creer":
					if($_GET['c'] == "f") {
						$titre = $_POST['nom'];
						$desc = $_POST['desc'];
						$cat = (int) $_POST['cat'];

						$query = $baseDeDonnes->prepare('INSERT INTO forum_forum(forum_cat_id, forum_name, forum_desc) VALUES(:cat, :titre, :desc)');
						$query->bindValue(':cat', $cat,PDO::PARAM_INT);
						$query->bindValue(':titre', $titre, PDO::PARAM_STR);
						$query->bindValue(':desc', $desc,PDO::PARAM_STR);
						$query->execute();
						
						echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Création d\'un forum</h2>';
										
						echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="admin.php">Administration</a> > <a href="admin.php?Action=Forum">Forum</a> > <a href="admin.php?Action=Forum&amp;Faire=Creer">Création</a> > <a href="admin.php?Action=Forum&amp;Faire=Creer&amp;c=f">Créer un Forum</a> > Résultat</p>';
						
						echo '
						<p class="ForumSend">
							<span class="titreSend">Le forum a été crée.</span>
							<span class="contenuSend">
								<span id="a">Retour à l\'<a href="index.php">accueil</a>.</span>
								<span id="b">Retour à l\'<a href="admin.php">administration</a>.</span>
							</span>
						</p>';
							
						$query->CloseCursor();
					}
					elseif($_GET['c'] == "c") {
						$titre = $_POST['nom'];
						
						$query = $baseDeDonnes->prepare('INSERT INTO forum_categorie(cat_nom, cat_ordre) VALUES(:titre, :rand)');
						$query->bindValue(':titre', $titre, PDO::PARAM_STR); 
						$query->bindValue(':rand', rand(1, 1000), PDO::PARAM_INT); 
						$query->execute();  
						
						echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Création d\'une catégorie</h2>';
										
						echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="admin.php">Administration</a> > <a href="admin.php?Action=Forum">Forum</a> > <a href="admin.php?Action=Forum&amp;Faire=Creer">Création</a> > <a href="admin.php?Action=Forum&amp;Faire=Creer&amp;c=c">Créer une Catégorie</a> > Résultat</p>';
						
						echo '
						<p class="ForumSend">
							<span class="titreSend">La catégorie a été crée.</span>
							<span class="contenuSend">
								<span id="a">Retour à l\'<a href="index.php">accueil</a>.</span>
								<span id="b">Retour à l\'<a href="admin.php">administration</a>.</span>
							</span>
						</p>';

						$query->CloseCursor();
					}
				break;
				
				case "Editer":
					if($_GET['e'] == "editf") {
						echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Édition d\'un forum</h2>';
									
						echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="admin.php">Administration</a> > <a href="admin.php?Action=Forum">Forum</a> > <a href="admin.php?Action=Forum&amp;Faire=Editer">Édition</a> > <a href="admin.php?Action=Forum&amp;Faire=Editer&amp;e=editf">Éditer un Forum</a> > Résultat</p>';

						$titre = $_POST['nom'];
						$desc = $_POST['desc'];
						$cat = (int) $_POST['depl'];       

						$query = $baseDeDonnes->prepare('SELECT COUNT(*) FROM forum_forum WHERE forum_id = :id');
						$query->bindValue(':id', (int) $_POST['forum_id'], PDO::PARAM_INT);
						$query->execute();
						$forum_existe = $query->fetchColumn();
						$query->CloseCursor();
						if($forum_existe == 0) erreur(ERR_FORUM_EXIST);

						$query = $baseDeDonnes->prepare('UPDATE forum_forum SET forum_cat_id = :cat, forum_name = :name, forum_desc = :desc WHERE forum_id = :id');
						$query->bindValue(':cat', $cat, PDO::PARAM_INT);  
						$query->bindValue(':name', $titre, PDO::PARAM_STR);
						$query->bindValue(':desc', $desc, PDO::PARAM_STR);
						$query->bindValue(':id', (int) $_POST['forum_id'], PDO::PARAM_INT);
						$query->execute();
						$query->CloseCursor();
						
						echo '
						<p class="ForumSend">
							<span class="titreSend">Le forum a été modifié.</span>
							<span class="contenuSend">
								<span id="a">Retour à l\'<a href="index.php">accueil</a>.</span>
								<span id="b">Retour à l\'<a href="admin.php">administration</a>.</span>
							</span>
						</p>';
					}
					elseif($_GET['e'] == "editc") {
						$titre = $_POST['nom'];

						$query = $baseDeDonnes->prepare('SELECT COUNT(*) FROM forum_categorie WHERE cat_id = :cat');
						$query->bindValue(':cat', (int) $_POST['cat'], PDO::PARAM_INT);
						$query->execute();
						$cat_existe=$query->fetchColumn();
						$query->CloseCursor();
						if($cat_existe == 0) erreur(ERR_CAT_EXIST);
						
						$query = $baseDeDonnes->prepare('UPDATE forum_categorie SET cat_nom = :name WHERE cat_id = :cat');
						$query->bindValue(':name', $titre, PDO::PARAM_STR);
						$query->bindValue(':cat', (int) $_POST['cat'], PDO::PARAM_INT);
						$query->execute();
						$query->CloseCursor();
						
						echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Édition d\'une catégorie</h2>';
									
						echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="admin.php">Administration</a> > <a href="admin.php?Action=Forum">Forum</a> > <a href="admin.php?Action=Forum&amp;Faire=Editer">Édition</a> > <a href="admin.php?Action=Forum&amp;Faire=Editer&amp;e=editc">Éditer une Catégorie</a> > Résultat</p>';
						
						echo '
						<p class="ForumSend">
							<span class="titreSend">La catégorie a été modifiée.</span>
							<span class="contenuSend">
								<span id="a">Retour à l\'<a href="index.php">accueil</a>.</span>
								<span id="b">Retour à l\'<a href="admin.php">administration</a>.</span>
							</span>
						</p>';					
					}
					elseif($_GET['e'] == "ordref") {
						$query = $baseDeDonnes->query('SELECT forum_id, forum_ordre FROM forum_forum');
            
						while($data= $query->fetch()) {
							$ordre = (int) $_POST[$data['forum_id']]; 
        
							if($data['forum_ordre'] != $ordre) {
								$query = $baseDeDonnes->prepare('UPDATE forum_forum SET forum_ordre = :ordre
								WHERE forum_id = :id');
								$query->bindValue(':ordre', $ordre, PDO::PARAM_INT);
								$query->bindValue(':id', $data['forum_id'], PDO::PARAM_INT);
								$query->execute();
								$query->CloseCursor();
							}
						} 
						$query->CloseCursor();
								
						echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Changer l\'ordre des forums</h2>';
									
						echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="admin.php">Administration</a> > <a href="admin.php?Action=Forum">Forum</a> > <a href="admin.php?Action=Forum&amp;Faire=Editer">Édition</a> > <a href="admin.php?Action=Forum&amp;Faire=Editer&amp;e=ordref">Changer l\'ordre des forums</a> > Résultat</p>';

						echo '
						<p class="ForumSend">
							<span class="titreSend">L\'ordre des forums a été modifié.</span>
							<span class="contenuSend">
								<span id="a">Retour à l\'<a href="index.php">accueil</a>.</span>
								<span id="b">Retour à l\'<a href="admin.php">administration</a>.</span>
							</span>
						</p>';					
					}
					elseif($_GET['e'] == "ordrec") {
						$query = $baseDeDonnes->query('SELECT cat_id, cat_ordre FROM forum_categorie');
					
						while($data = $query->fetch()) {
							$ordre = (int) $_POST[$data['cat_id']]; 
					
							if($data['cat_ordre'] != $ordre) {
								$query = $baseDeDonnes->prepare('UPDATE forum_categorie SET cat_ordre = :ordre WHERE cat_id = :id');
								$query->bindValue(':ordre', $ordre, PDO::PARAM_INT);
								$query->bindValue(':id', $data['cat_id'], PDO::PARAM_INT);
								$query->execute();
								$query->CloseCursor();
							}
						}
								
						echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Changer l\'ordre des catégories</h2>';
									
						echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="admin.php">Administration</a> > <a href="admin.php?Action=Forum">Forum</a> > <a href="admin.php?Action=Forum&amp;Faire=Editer">Édition</a> > <a href="admin.php?Action=Forum&amp;Faire=Editer&amp;e=ordrec">Changer l\'ordre des catégories</a> > Résultat</p>';

						echo '
						<p class="ForumSend">
							<span class="titreSend">L\'ordre des catégories a été modifié.</span>
							<span class="contenuSend">
								<span id="a">Retour à l\'<a href="index.php">accueil</a>.</span>
								<span id="b">Retour à l\'<a href="admin.php">administration</a>.</span>
							</span>
						</p>';					
					}					
				break;
				
				case "Droits":    
					$auth_view = (int) $_POST['auth_view'];
					$auth_post = (int) $_POST['auth_post'];
					$auth_topic = (int) $_POST['auth_topic'];
					$auth_annonce = (int) $_POST['auth_annonce'];
					$auth_modo = (int) $_POST['auth_modo'];
					
					$query=$baseDeDonnes->prepare('UPDATE forum_forum
					SET auth_view = :view, auth_post = :post, auth_topic = :topic,
					auth_annonce = :annonce, auth_modo = :modo WHERE forum_id = :id');
					$query->bindValue(':view',$auth_view,PDO::PARAM_INT);
					$query->bindValue(':post',$auth_post,PDO::PARAM_INT);
					$query->bindValue(':topic',$auth_topic,PDO::PARAM_INT);
					$query->bindValue(':annonce',$auth_annonce,PDO::PARAM_INT);
					$query->bindValue(':modo',$auth_modo,PDO::PARAM_INT);
					$query->bindValue(':id',(int) $_POST['forum_id'],PDO::PARAM_INT);
					$query->execute();
					$query->CloseCursor();
				  
					echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Gestion des Droits</h2>';
								
					echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="admin.php">Administration</a> > <a href="admin.php?Action=Forum">Forum</a> > <a href="admin.php?Action=Forum&amp;Faire=Droits">Droits</a> > Résultat</p>';
				  
					echo '
					<p class="ForumSend">
						<span class="titreSend">Les droits du forum ont été modifiés.</span>
						<span class="contenuSend">
							<span id="a">Retour à l\'<a href="index.php">accueil</a>.</span>
							<span id="b">Retour à l\'<a href="admin.php">administration</a>.</span>
						</span>
					</p>';					
				break;
			}
		break;
	}
}
?>