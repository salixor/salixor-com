<?php
$titre_page_nom = "Forum";
include("../Header.php");


$action = (isset($_GET['action']))?utf8_encode($_GET['action']):'';

if(!isset($ID_MEMBRE)) erreur(ERR_IS_CO);

if(isset($_GET['f'])) {
	$forum = (int) $_GET['f'];

	$query = $baseDeDonnes->prepare('SELECT * FROM forum_forum WHERE forum_id = :forum');
	$query->bindValue(':forum', $forum, PDO::PARAM_INT);
	$query->execute();
	$data = $query->fetch();
	
	if(!verifAuthForum($data['auth_view'])) {
		erreur(ERR_AUTH_VIEW);
	}

	switch($action) {
		case "nouveautopic":
			$page = "Nouveau Sujet";
		break;
		case "annonce":
			$page = "Nouvelle Annonce";
			if(!verifAuthForum($data['auth_annonce'])) {
				erreur(ERR_AUTH_ANNONCE);
			}
		break;
	}

	echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Nouveau Sujet" /> '.$page.'</h2>';
	
	echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="forum.php?f='.$data['forum_id'].'">'.stripslashes($data['forum_name']).'</a> > '.$page.'</p>';
	
	$query->CloseCursor();
}
elseif(isset($_GET['t']) && !isset($_GET["m"])) {
	$topic = (int) $_GET['t'];
	
	$query = $baseDeDonnes->prepare('SELECT topic_titre, forum_topic.forum_id,
	forum_name, auth_view, auth_post, auth_topic, auth_modo
	FROM forum_topic
	LEFT JOIN forum_forum ON forum_forum.forum_id = forum_topic.forum_id
	WHERE topic_id =:topic');
	$query->bindValue(':topic',$topic,PDO::PARAM_INT);
	$query->execute();
	$data=$query->fetch();
	
	if(!verifAuthForum($data['auth_view'])) {
		erreur(ERR_AUTH_VIEW);
	}

	$forum = $data['forum_id']; 
	
	echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Réponse" /> Répondre au Sujet</h2>';
	
	echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="forum.php?f='.$data['forum_id'].'">'.stripslashes($data['forum_name']).'</a> > <a href="voirsujet.php?t='.$topic.'">'.stripslashes($data['topic_titre']).'</a> > Répondre au Sujet</p>'; 
	
	$query->CloseCursor();
}
elseif(isset($_GET['p'])) {
	$post = (int) $_GET['p'];
	
	$query = $baseDeDonnes->prepare('SELECT post_createur, forum_post.topic_id, topic_titre, forum_topic.forum_id, forum_name, auth_view, auth_post, auth_topic, auth_modo FROM forum_post LEFT JOIN forum_topic ON forum_topic.topic_id = forum_post.topic_id LEFT JOIN forum_forum ON forum_forum.forum_id = forum_topic.forum_id WHERE forum_post.post_id = :post');
	$query->bindValue(':post', $post, PDO::PARAM_INT);
	$query->execute();
	$data = $query->fetch();

	$topic = $data['topic_id'];
	$forum = $data['forum_id'];
	
	switch($action) {
		case "edit":
			$page = "Éditer un Message";
		break;
		case "delete":
			$page = "Supprimer un Message";
		break;
		case "signaler":
			$page = "Signaler un Message";
		break;
		case "designaler":
			$page = "Désignaler un Message";
		break;
	}
	
	echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Réponse" /> '.$page.'</h2>';
	
	echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="forum.php?f='.$data['forum_id'].'">'.stripslashes($data['forum_name']).'</a> > <a href="voirsujet.php?t='.$topic.'">'.stripslashes($data['topic_titre']).'</a> > '.$page.'</p>'; 
	
	$query->CloseCursor();	
}
elseif(isset($_GET['m']) && isset($_GET['t'])) {
	$topic = (int) $_GET['t'];
	$membre = (int) $_GET['m'];
	
	$query = $baseDeDonnes->prepare('SELECT membre_pseudo, membre_id FROM membres WHERE membre_id = :membre');
	$query->bindValue(':membre', $membre, PDO::PARAM_INT);
	$query->execute();
	$data = $query->fetch();

	$membrePseudo = stripslashes(utf8_encode($data['membre_pseudo']));
	$membreId	= $data['membre_id'];
	
	$query->CloseCursor();	
		
	echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Réponse" /> Bannir un Membre</h2>';
	
	echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > Bannir <a href="../Membres/Profil.php?id='.$membreId.'">'.$membrePseudo.'</a></p>'; 
	
}
else {
	echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Réponse" /> Vous êtes perdu !</h2>';
	
	echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a></p>';
	
	echo '<p class="encadreInfos"><span class="right">Cette page n\'a pas été trouvée.</span></p>';
}

switch($action) {
	case "repondre": 
		if(verifAuthForum($data['auth_post'])) { ?>
	
			<div class="bloc">
				<form method="post" action="post.php?action=repondre&amp;t=<?php echo $topic ?>">
					<?php
					afficher_smileys('message');
					$FormatagePanel = new FormatagePanel(true, true, true, 'message');
					$FormatagePanel->showFormatagePanel();
					?>

					<p><textarea id="message" name="message" style="height:250px;"></textarea></p>
										
					<p align="center">
						<input type="button" id="previsualiser_bouton" value="Prévisualiser" onclick="previsualiser('message', 'previsualiserMessage' , '../AJAX_news.php');" />
						<input type="reset" value="Réinitialiser" />
						<input type="submit" name="submit" value="Envoyer" />
					</p>
				</form>
			</div>
			
			<p align="center" id="previsualiser_zone">
				<div class="preview" id="previsualiserMessage" style="display:none;"></div>
			</p>
			
	<?php }
	break;
	 
	case "nouveautopic":
		if(verifAuthForum($data['auth_topic'])) { ?>
		
			<div class="bloc">
				<form method="post" action="post.php?action=nouveautopic&amp;f=<?php echo $forum ?>">
					<p><label for="titre">Titre : </label><input type="text" id="titre" name="titre" style="width:500px;" /></p>
					<p><label for="description">Description : </label><input type="text" id="description" name="description" style="width:500px;" /></p>

					<?php
					afficher_smileys('message');
					$FormatagePanel = new FormatagePanel(true, true, true, 'message');
					$FormatagePanel->showFormatagePanel();
					?>

					<p><textarea id="message" name="message" style="height:250px;"></textarea></p>
										
					<p align="center">
						<input type="button" id="previsualiser_bouton" value="Prévisualiser" onclick="previsualiser('message', 'previsualiserMessage' , '../AJAX_news.php');" />
						<input type="reset" value="Réinitialiser" />
						<input type="submit" name="submit" value="Envoyer" />
					</p>
				</form>
			</div>
			
			<p align="center" id="previsualiser_zone">
				<div class="preview" id="previsualiserMessage" style="display:none;"></div>
			</p>

	<?php }
	break;
	 
	case "annonce":
		if(verifAuthForum($data['auth_annonce'])) { ?>
		
			<div class="bloc">
				<form method="post" action="post.php?action=annonce&amp;f=<?php echo $forum ?>">
					<p><label for="titre">Titre : </label><input type="text" id="titre" name="titre" style="width:500px;" /></p>
					<p><label for="description">Description : </label><input type="text" id="description" name="description" style="width:500px;" /></p>

					<?php
					afficher_smileys('message');
					$FormatagePanel = new FormatagePanel(true, true, true, 'message');
					$FormatagePanel->showFormatagePanel();
					?>

					<p><textarea id="message" name="message" style="height:250px;"></textarea></p>
										
					<p align="center">
						<input type="button" id="previsualiser_bouton" value="Prévisualiser" onclick="previsualiser('message', 'previsualiserMessage' , '../AJAX_news.php');" />
						<input type="reset" value="Réinitialiser" />
						<input type="submit" name="submit" value="Envoyer" />
					</p>
				</form>
			</div>
			
			<p align="center" id="previsualiser_zone">
				<div class="preview" id="previsualiserMessage" style="display:none;"></div>
			</p>

	<?php }
	break;

	case "edit":
		$post = (int) $_GET['p'];
		 
		$query=$baseDeDonnes->prepare('SELECT post_createur, post_texte, auth_modo FROM forum_post
		LEFT JOIN forum_forum ON forum_post.post_forum_id = forum_forum.forum_id
		WHERE post_id=:post');
		$query->bindValue(':post',$post,PDO::PARAM_INT);
		$query->execute();
		$data=$query->fetch();

		$text_edit = stripslashes($data['post_texte']); 

		if(!verifAuthForum($data['auth_modo']) && $data['post_createur'] != $ID_MEMBRE) {
			erreur(ERR_AUTH_EDIT);
		}
		else { ?>
		
			<div class="bloc">
				<form method="post" action="post.php?action=edit&amp;p=<?php echo $post ?>">
					<?php
					afficher_smileys('message');
					$FormatagePanel = new FormatagePanel(true, true, true, 'message');
					$FormatagePanel->showFormatagePanel();
					?>

					<p><textarea id="message" name="message" style="height:250px;"><?php echo $text_edit ?></textarea></p>
										
					<p align="center">
						<input type="button" id="previsualiser_bouton" value="Prévisualiser" onclick="previsualiser('message', 'previsualiserMessage' , '../AJAX_news.php');" />
						<input type="reset" value="Réinitialiser" />
						<input type="submit" name="submit" value="Envoyer" />
					</p>
				</form>
			</div>
			
			<p align="center" id="previsualiser_zone">
				<div class="preview" id="previsualiserMessage" style="display:none;"></div>
			</p>
			
	<?php }
	break;
	
	case "delete":
	
		$post = (int) $_GET['p'];

		$query=$baseDeDonnes->prepare('SELECT post_createur, auth_modo
		FROM forum_post
		LEFT JOIN forum_forum ON forum_post.post_forum_id = forum_forum.forum_id
		WHERE post_id= :post');
		$query->bindValue(':post',$post,PDO::PARAM_INT);
		$query->execute();
		$data = $query->fetch();
	 
		if(!verifAuthForum($data['auth_modo']) && $data['post_createur'] != $ID_MEMBRE) {
			erreur(ERR_AUTH_DELETE); 
		}
		else {
			echo '
			<p class="ForumSend">
				<span class="titreSend"><img src="../Images/Forum/Erreur.png" /> Attention</span>
				<span class="contenuSend">
					<span id="a">Êtes vous certain de vouloir supprimer ce message ? Cette action est irréversible.</span>
					<span id="b" align="center"><a href="post.php?action=delete&amp;p='.$post.'" class="choice">Oui</a><a href="index.php" class="choice">Non</a></span>
				</span>
			</p>';
		}
		$query->CloseCursor();
		
	break;
	
	case "bannir":
		$membre = (int) $_GET['m'];
		
		$query = $baseDeDonnes->prepare('SELECT forum_id FROM forum_topic WHERE topic_id = :topic');
		$query->bindValue(':topic', $topic, PDO::PARAM_INT);
		$query->execute();
		$data = $query->fetch();
		
		$forum = $data["forum_id"];
		
		$query->CloseCursor();

		$query = $baseDeDonnes->prepare('SELECT auth_modo FROM forum_forum WHERE forum_id = :forum');
		$query->bindValue(':forum', $forum, PDO::PARAM_INT);
		$query->execute();
		$data = $query->fetch();
	 
		if(!verifAuthForum($data['auth_modo'])) {
			erreur(ERR_AUTH_BANNIR); 
		}
		elseif($ID_MEMBRE == $membre) {
			erreur(ERR_AUTH_BANNIR_SELF);
		}
		else {
			echo '
			<p class="ForumSend">
				<span class="titreSend"><img src="../Images/Forum/Erreur.png" /> Attention</span>
				<span class="contenuSend">
					<span id="a">Êtes vous certain de vouloir bannir ce membre ?</span>
					<span id="b" align="center"><a href="post.php?action=bannir&amp;m='.$membre.'&amp;t='.$topic.'" class="choice">Oui</a><a href="index.php" class="choice">Non</a></span>
				</span>
			</p>';
		}
		$query->CloseCursor();
		
	break;
	
	case "signaler":
		$post = (int) $_GET['p'];

		$query = $baseDeDonnes->prepare('SELECT post_createur, auth_view
		FROM forum_post
		LEFT JOIN forum_forum ON forum_post.post_forum_id = forum_forum.forum_id
		WHERE post_id= :post');
		$query->bindValue(':post',$post,PDO::PARAM_INT);
		$query->execute();
		$data = $query->fetch();
		
		if(!verifAuthForum($data['auth_view'])) {
			erreur(ERR_AUTH_VIEW); 
		}
		elseif($data['post_createur'] == $ID_MEMBRE) {
			erreur(ERR_AUTH_SIGNALER_SELF); 
		}
		else {
			echo '
			<p class="ForumSend">
				<span class="titreSend"><img src="../Images/Forum/Erreur.png" /> Attention</span>
				<span class="contenuSend">
					<span id="a">Êtes vous certain de vouloir signaler ce message ? Tout signalement abusif sera sanctionné.</span>
					<span id="b" align="center"><a href="post.php?action=signaler&amp;p='.$post.'" class="choice">Oui</a><a href="index.php" class="choice">Non</a></span>
				</span>
			</p>';
		}
		$query->CloseCursor();
		
	break;
	
	case "designaler":
		$post = (int) $_GET['p'];

		$query = $baseDeDonnes->prepare('SELECT post_createur, auth_view
		FROM forum_post
		LEFT JOIN forum_forum ON forum_post.post_forum_id = forum_forum.forum_id
		WHERE post_id= :post');
		$query->bindValue(':post',$post,PDO::PARAM_INT);
		$query->execute();
		$data = $query->fetch();

		if(!verifAuthForum($data['auth_view'])) {
			erreur(ERR_AUTH_VIEW); 
		}
		elseif($data['post_createur'] == $ID_MEMBRE) {
			erreur(ERR_AUTH_DESIGNALER_SELF); 
		}
		else {
			echo '
			<p class="ForumSend">
				<span class="titreSend"><img src="../Images/Forum/Erreur.png" /> Attention</span>
				<span class="contenuSend">
					<span id="a">Êtes vous certain de vouloir désignaler ce message ?</span>
					<span id="b" align="center"><a href="post.php?action=designaler&amp;p='.$post.'" class="choice">Oui</a><a href="index.php" class="choice">Non</a></span>
				</span>
			</p>';
		}
		$query->CloseCursor();
		
	break;
}

include("../Footer.php"); ?>