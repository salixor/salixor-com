<?php
$titre_page_nom = "Forum";
include("../Header.php");
include("OnlineForum.php");
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Forum.png" alt="Forum" /> Le Forum</h2>

<?php
$categorie = NULL;

$lvl = (isset($NIVEAU_MEMBRE))?intval($NIVEAU_MEMBRE):0;

$add1='';
$add2='';
if(isset($ID_MEMBRE)) {
	$add1 = 'tv_id, tv_post_id, tv_forum_id, tv_poste, '; 
	$add2 = 'LEFT JOIN forum_topic_view 
	ON forum_topic.topic_id = forum_topic_view.tv_topic_id AND forum_topic_view.tv_id = :id';
}

$query = $baseDeDonnes->prepare('SELECT cat_id, cat_nom, forum_forum.forum_id, forum_name, forum_desc, forum_post, forum_topic, forum_forum.auth_view, forum_topic.topic_id, forum_topic.topic_post, post_id, post_time, post_createur, membre_pseudo, membre_id, '.$add1.'forum_last_post_id, topic_locked, topic_genre, topic_titre
FROM forum_categorie
LEFT JOIN forum_forum ON forum_categorie.cat_id = forum_forum.forum_cat_id
LEFT JOIN forum_post ON forum_post.post_id = forum_forum.forum_last_post_id
LEFT JOIN forum_topic ON forum_topic.topic_id = forum_post.topic_id
LEFT JOIN membres ON membres.membre_id = forum_post.post_createur
'.$add2.'
WHERE auth_view <= :lvl 
ORDER BY cat_ordre, forum_ordre ASC');
$query->bindValue(':lvl', $lvl, PDO::PARAM_INT);
if(isset($ID_MEMBRE)) {
	$query->bindValue(':id',$ID_MEMBRE,PDO::PARAM_INT);
}
$query->execute();

echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a></p>';

echo '<ul class="navigationPage">';
if(isset($ID_MEMBRE)) {
	if($NIVEAU_MEMBRE == 5) {
		echo '<a href="admin.php?Action=ViewSignale"><li><img src="../Images/Forum/MessagesSignales.png" alt="" /> Messages Signalés</li></a>';
		echo '<a href="admin.php?Action=Forum"><li><img src="../Images/Forum/AdminForum.png" alt="" /> Gestion du Forum</li></a>';
	}
}
echo '</ul>';

echo "<table>";

while($data = $query->fetch()) {
	if(isset($ID_MEMBRE)) {
		if($data['tv_id'] == $ID_MEMBRE) {
			if($data['tv_poste'] == '0') {
				if($data['tv_post_id'] == $data['forum_last_post_id']) {
					$messageIcone = 'MessageLu.png';
				}
				else {
					$messageIcone = 'MessageNonLu.png';
				}
			}
			else {
				if($data['tv_post_id'] == $data['forum_last_post_id']) {
					$messageIcone = 'MessageLu.png';
				}
				else {
					$messageIcone = 'MessageNonLu.png';
				}
			}
		}
		else {
			$messageIcone = 'AnnonceNonLue.png';
		}
	}
	else {
		$messageIcone = 'MessageLu.png';
	}	

	if($data['forum_topic'] == 0) {
		$messageIcone = 'MessageLu.png';
	}
	
	if($data['topic_genre'] == "Annonce") {
		$messageIcone = 'Annonce.png';
	}
	
	if($data["topic_locked"] == 1) {
		$messageIcone = 'MessageLock.png';
	}
	
	if(verifAuthForum($data['auth_view'])) {
		if($categorie != $data['cat_id']) {  
			$categorie = $data['cat_id']; ?>
			<tr>
				<th colspan="2" width="500"><?php echo stripslashes(htmlspecialchars($data['cat_nom'])); ?></th>             
				<th width="40">Sujets</th>       
				<th width="160">Dernier message</th>   
			</tr>
			<?php
		}
		echo '
			<tr>
				<td align="center" width="40"><img src="../Images/Forum/'.$messageIcone.'" alt="message" /></td>
				<td style="font-size:13px;"><a href="./forum.php?f='.$data['forum_id'].'">'.stripslashes(htmlspecialchars($data['forum_name'])).'</a><br /><span style="font-size:12px;">'.nl2br(stripslashes(htmlspecialchars($data['forum_desc']))).'</span></td>
				<td align="center" style="font-size:13px;">'.$data['forum_topic'].'</td>
		';	

		if(!empty($data['forum_post'])) {
			$nombreDeMessagesParPage = 10;
			$nbPost = $data['topic_post'];
			$page = ceil($nbPost / $nombreDeMessagesParPage);

			echo '
			<td style="font-size:13px;">
				<a href="voirsujet.php?t='.$data['topic_id'].'&amp;page='.$page.'#Post-'.$data['post_id'].'">
					<img src="../Images/Forum/PageGo.png" alt="Go" style="margin:2px 0 -2px;" /> '.tronquer(stripslashes($data["topic_titre"]), 18, "...").'
				</a><br />
				'.afficher_date_difference($data["post_time"]).'
			</td>';
		}
		else {
			echo '<td align="center" style="font-size:13px;">Pas de message</td>';
		}
	}
	
	echo '</tr>';
}

$query->CloseCursor();

echo '</table>';

echo '<ul class="navigationPage">';
if(isset($ID_MEMBRE)) {
	if($NIVEAU_MEMBRE == 5) {
		echo '<a href="admin.php?Action=ViewSignale"><li><img src="../Images/Forum/MessagesSignales.png" alt="" /> Messages Signalés</li></a>';
		echo '<a href="admin.php?Action=Forum"><li><img src="../Images/Forum/AdminForum.png" alt="" /> Gestion du Forum</li></a>';
	}
}
echo '</ul>';

echo '<div style="clear:left;"></div>';

include("FooterForum.php");

include("../Footer.php"); ?>