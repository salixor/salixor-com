<?php
include("../Includes/Fonctions.php");
include("../Includes/Config.php");

$topic = (int) $_GET['t'];
 
$query = $baseDeDonnes->prepare('SELECT topic_titre, topic_post, forum_topic.forum_id, topic_last_post, topic_descri, topic_id,
forum_name, auth_view, auth_topic, auth_post, auth_modo, auth_annonce
FROM forum_topic 
LEFT JOIN forum_forum ON forum_topic.forum_id = forum_forum.forum_id 
WHERE topic_id = :topic');
$query->bindValue(':topic',$topic,PDO::PARAM_INT);
$query->execute();
$data=$query->fetch();

$topic_descri = stripslashes(htmlspecialchars($data["topic_descri"]));
$auth_modo = $data["auth_modo"];
$auth_annonce = $data["auth_annonce"];
$auth_post = $data["auth_post"];
$auth_view = $data["auth_view"];
$auth_topic = $data["auth_topic"];

$query2 = $baseDeDonnes->prepare('SELECT topic_locked FROM forum_topic WHERE topic_id = :topic');
$query2->bindValue(':topic',$topic,PDO::PARAM_INT);
$query2->execute();
$data2 = $query2->fetch();
$topic_locked = $data2['topic_locked'];
$query2->CloseCursor();

if(!verifAuthForum($data['auth_view'])) {
	erreur(ERR_AUTH_VIEW);
}

$forum=$data['forum_id']; 
$topic=$data['topic_id']; 

//Sujet déjà consulté ?
$query=$baseDeDonnes->prepare('SELECT COUNT(*) FROM forum_topic_view WHERE tv_topic_id = :topic AND tv_id = :id');
$query->bindValue(':topic',$topic,PDO::PARAM_INT);
$query->bindValue(':id',$ID_MEMBRE,PDO::PARAM_INT);
$query->execute();
$nbr_vu=$query->fetchColumn();
$query->CloseCursor();

if($nbr_vu == 0) {
	$query=$baseDeDonnes->prepare('INSERT INTO forum_topic_view 
	(tv_id, tv_topic_id, tv_forum_id, tv_post_id)
	VALUES (:id, :topic, :forum, :last_post)');
	$query->bindValue(':id',$ID_MEMBRE,PDO::PARAM_INT);
	$query->bindValue(':topic',$topic,PDO::PARAM_INT);
	$query->bindValue(':forum',$forum,PDO::PARAM_INT);
	$query->bindValue(':last_post',$data['topic_last_post'],PDO::PARAM_INT);
	$query->execute();
	$query->CloseCursor();
	  
}
else {
	$query=$baseDeDonnes->prepare('UPDATE forum_topic_view SET tv_post_id = :last_post 
	WHERE tv_topic_id = :topic 
	AND tv_id = :id');
	$query->bindValue(':last_post',$data['topic_last_post'],PDO::PARAM_INT);
	$query->bindValue(':topic',$topic,PDO::PARAM_INT);
	$query->bindValue(':id',$ID_MEMBRE,PDO::PARAM_INT);
	$query->execute();
	$query->CloseCursor();
}

$totalDesMessages = $data['topic_post'];
$nombreDeMessagesParPage = 10;
$nombreDePages = ceil($totalDesMessages / $nombreDeMessagesParPage);

$topic_titre = htmlspecialchars(stripslashes($data['topic_titre'])); 
$forum_name = stripslashes(htmlspecialchars($data['forum_name'])); 

$titre_page_nom = $topic_titre;
include("../Header.php");

echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Forum" /> '.$topic_titre.'</h2>';

echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="forum.php?f='.$forum.'">'.$forum_name.'</a> > <a href="voirsujet.php?t='.$topic.'">'.$topic_titre.'</a> > Lecture du Sujet</p>';

$page = (isset($_GET['page']))?intval($_GET['page']):1;

echo '<ul class="navigationPage">';
if(isset($ID_MEMBRE)) {
	if(verifAuthForum($auth_post) && $topic_locked != 1)   echo '<a href="poster.php?action=repondre&amp;t='.$topic.'"><li><img src="../Images/Forum/RepondreSujet.png" alt="" /> Répondre</li></a>';
	if(verifAuthForum($auth_post) && $topic_locked != 1)   echo '<a href="javascript:showQuickReponse(200);" class="show-fast-reponse"><li><img src="../Images/Forum/RepondreSujet.png" alt="" /> Réponse Rapide</li></a>';
	if(verifAuthForum($auth_modo))                         echo '<a href="post.php?action=delete_topic&amp;t='.$topic.'"><li><img src="../Images/Forum/SupprimerSujet.png" alt="" /> Supprimer</li></a>';
	if(verifAuthForum($auth_modo) && $topic_locked == 1)   echo '<a href="post.php?action=unlock&t='.$topic.'"><li><img src="../Images/Forum/LockBreak.png" alt="" /> Déverrouiller</li></a>';
	if(verifAuthForum($auth_modo) && $topic_locked == 0)   echo '<a href="post.php?action=lock&t='.$topic.'"><li><img src="../Images/Forum/Lock.png" alt="" /> Verrouiller</li></a>';
}
echo '</ul>';

if($nombreDePages > 1) {
	echo '<p class="pagination" align="right">'.pagination($page, $nombreDePages, "?t=".$topic."&amp;page=%d").'</p>';
}

echo '
<div id="quickReponse">
	<form method="post" action="post.php?action=repondre&amp;t='.$topic.'" name="quickReponseForm">
		<div class="head">
			<a href="javascript:closeQuickReponse(200);"></a>
		</div>
		<textarea id="messageQuickReponse" name="message" onkeyup="verifQuickReponse();"></textarea>
		<div class="foot">
			<a href="javascript:sendQuickReponse();">Envoyer</a>
			<a href="javascript:document.quickReponseForm.reset();">Effacer</a>
			<div style="clear:right;"></div>
		</div>
		<span id="informationQuickReponse">Au moins 10 caractères</span>
	</form>
</div>

<div class="full_black" onclick="closeQuickReponse(200);"></div>';

$premierMessageAafficher = ($page - 1) * $nombreDeMessagesParPage;

$query->CloseCursor(); 

$query = $baseDeDonnes->prepare('
SELECT forum_post.*, membres.*, membres_titres.*
FROM forum_post
LEFT JOIN membres ON membres.membre_id = forum_post.post_createur
LEFT JOIN membres_titres ON membres.membre_titre_id = membres_titres.id
WHERE topic_id = :topic
ORDER BY post_id
LIMIT :premier, :nombre');
$query->bindValue(':topic', $topic, PDO::PARAM_INT);
$query->bindValue(':premier', (int) $premierMessageAafficher, PDO::PARAM_INT);
$query->bindValue(':nombre', (int) $nombreDeMessagesParPage, PDO::PARAM_INT);
$query->execute();
 
if($query->rowCount() < 1) {
	echo '<div class="encadre_infos"><p>Il n y a aucun post sur ce sujet !</p></div>';
}
else {
	while($data = $query->fetch()) {
		if($data["post_editeur"] != 0) {
			$query2 = $baseDeDonnes->prepare('SELECT membre_pseudo FROM membres WHERE membre_id = '.$data["post_editeur"].'');
			$query2->execute();
			$data2 = $query2->fetch();
		}

		$avatar = $data["membre_avatar"];
		$rang = $MEMBRES_RANGS[$data["membre_niveau"]];
		
		echo '<div class="postForum" id="Post-'.$data['post_id'].'">';
		
			echo '<div class="head">';
				$image = '';
				if($data["membre_connecte"] == 1) {
					$image = '<img src="../Images/Forum/BulletGreen.png" alt="" />';
				}
				else {
					$image = '<img src="../Images/Forum/BulletRed.png" alt="" />';
				}
				echo '<a href="../Membres/Profil.php?id='.$data['membre_id'].'" class="left">'.$image.stripslashes(utf8_encode(htmlspecialchars($data['membre_pseudo']))).'</a>';  
				echo '<span class="right"><img src="../Images/Forum/Time.png" alt="" style="margin:3px 0 -3px;" /> Posté le '.convertirTimestamp($data['post_time']).' à '.date('H\hi',$data['post_time']).'</span>';
				echo '<div style="clear:left;"></div>';
			echo '</div>';
			
			echo '<div class="side">';
				if(!empty($avatar)) {
					echo '<div class="avatar-forum"><img src="../Images/Avatars/'.$avatar.'" width="100" height="100" alt="" /></div>';
				}
				else {
					echo '<div class="avatar-forum"><img src="../Images/Avatars/avatar_normal.png" width="100" height="100" alt="" /></div>';
				}
				echo '<div class="contenu-centre contenu-gras">'.$rang.'</div>';   
				echo '<div class="contenu-centre bleu">&laquo;'.stripslashes(htmlspecialchars(utf8_encode($data['titre']))).'&raquo;</div>';   
				echo '<div>Inscrit le '.date('d/m/Y', strtotime($data['membre_inscription'])).'</div>';
				echo '<div>'.$data['membre_post'].' messages</div>';  
				echo '<div>';
					if(!empty($data['membre_sexe'])) {
						if($data['membre_sexe'] == "Homme") {
							echo '<span class="sexe male"><img src="../Images/Forum/Male.png" alt="" /> Homme</span>';
						}
						elseif($data['membre_sexe'] == "Femme") {
							echo '<span class="sexe female"><img src="../Images/Forum/Femelle.png" alt="" /> Femme</span>';
						}
					}
				echo '</div>';
			echo '</div>';		
				   
			echo '<div class="contenu">';
				echo smileys(nl2br(stripslashes(formatage($data['post_texte']))));
				if(!empty($data2["membre_pseudo"]) AND $data["post_editeur"] != 0) {
					echo '<div class="edition">';
						echo '<img src="../Images/Forum/Edition.png" alt="" />';
						echo '<b>Édité par</b> '.$data2["membre_pseudo"].' le '.date('d/m/Y \à H:i', $data['post_edit_time']);
					echo '</div>';
					$query2->CloseCursor();
				}
				if(!empty($data['membre_signature'])) {
					echo "<hr />";
					echo smileys(nl2br(stripslashes(formatage($data['membre_signature']))));
				}

				echo '<div style="clear:left;"></div>';
			echo '</div>';
			
			if(isset($ID_MEMBRE)) {
				echo '<div class="foot">';
					if($NIVEAU_MEMBRE == 5 && $ID_MEMBRE != $data['membre_id']) {
						echo '<a href="poster.php?m='.$data['membre_id'].'&amp;t='.$data["topic_id"].'&amp;action=bannir" class="lien"><img src="../Images/Forum/Bannir.png" alt="" /> Bannir</a>';
					}
					
					if($data["post_signale"] == "1") {
						if($data["post_signaleur"] == $ID_MEMBRE || $NIVEAU_MEMBRE == 5 && $ID_MEMBRE != $data["post_createur"]) {
							echo '<a href="poster.php?p='.$data["post_id"].'&amp;action=designaler" class="lien"><img src="../Images/Forum/Designaler.png" alt="" /> Désignaler</a>';
						}
						else {
							echo '<a href="javascript:void(0);" class="lien"><img src="../Images/Forum/Signal.png" alt="" /> Signalé</a>';
						}
					}
					if($ID_MEMBRE != $data["post_createur"]) {
						if($data["post_signale"] == "0") {
							echo '<a href="poster.php?p='.$data["post_id"].'&amp;action=signaler" class="lien"><img src="../Images/Forum/Signaler.png" alt="" /> Signaler</a>';
						}
						
						echo '<a href="../Membres/Messagerie.php?Action=Envoyer&amp;Pseudo='.$data['membre_pseudo'].'" class="lien"><img src="../Images/Forum/MP.png" alt="" /> Message</a>';
					}
					
					if($ID_MEMBRE == $data["post_createur"] || $NIVEAU_MEMBRE == 5) {
						echo '<a href="poster.php?p='.$data['post_id'].'&amp;action=delete" class="lien"><img src="../Images/Forum/Supprimer.png" alt="" /> Supprimer</a>';
						echo '<a href="poster.php?p='.$data['post_id'].'&amp;action=edit" class="lien"><img src="../Images/Forum/Edition.png" alt="" /> Éditer</a>';
						echo '<a class="lien left">-</a>';
					}
					echo '<div style="clear:left;"></div>';
				echo '</div>';
			}
			
		echo '</div>';
	}
	$query->CloseCursor();

	echo '<ul class="navigationPage">';
	if(isset($ID_MEMBRE)) {
		if(verifAuthForum($auth_post) && $topic_locked != 1)   echo '<a href="poster.php?action=repondre&amp;t='.$topic.'"><li><img src="../Images/Forum/RepondreSujet.png" alt="" /> Répondre</li></a>';
		if(verifAuthForum($auth_post) && $topic_locked != 1)   echo '<a href="javascript:showQuickReponse(200);" class="show-fast-reponse"><li><img src="../Images/Forum/RepondreSujet.png" alt="" /> Réponse Rapide</li></a>';
		if(verifAuthForum($auth_modo))                         echo '<a href="post.php?action=delete_topic&amp;t='.$topic.'"><li><img src="../Images/Forum/SupprimerSujet.png" alt="" /> Supprimer</li></a>';
		if(verifAuthForum($auth_modo) && $topic_locked == 1)   echo '<a href="post.php?action=unlock&t='.$topic.'"><li><img src="../Images/Forum/LockBreak.png" alt="" /> Déverrouiller</li></a>';
		if(verifAuthForum($auth_modo) && $topic_locked == 0)   echo '<a href="post.php?action=lock&t='.$topic.'"><li><img src="../Images/Forum/Lock.png" alt="" /> Verrouiller</li></a>';
	}
	echo '</ul>';

	if($nombreDePages > 1) {
		echo '<p class="pagination" align="right">'.pagination($page, $nombreDePages, "?t=".$topic."&amp;page=%d").'</p>';
	}
}

include("../Footer.php"); ?>