<?php
$titre_page_nom = "Admin";
include("../Header.php");


include("OnlineForum.php");

if($NIVEAU_MEMBRE == 5) {
	if(isset($_GET["Action"])) {
		switch($_GET["Action"]) {
			case "ViewSignale":
				$query = $baseDeDonnes->query("
				SELECT post_id, post_createur, membre_signaleur.membre_pseudo AS membre_signaleur_pseudo, membre_signaleur.membre_id AS membre_signaleur_id, membre_posteur.membre_pseudo AS membre_post_pseudo, membre_posteur.membre_id AS membre_post_id
				FROM forum_post
				LEFT JOIN membres AS membre_signaleur ON membre_signaleur.membre_id = post_signaleur
				LEFT JOIN membres AS membre_posteur ON membre_posteur.membre_id = post_createur
				WHERE post_signale != '0'");
				
				echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Forum" /> Messages Signalés</h2>';
				
				echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="admin.php">Administration</a> > <a href="admin.php?Action=ViewSignale">Messages Signalés</a></p>';
				
				echo '<table>';
				echo '
				<tr>
					<th style="width:300px;">Auteur</th>
					<th style="width:300px;">Signalé par</th>
					<th style="width:160px;">Désignaler</th>
				</tr>';
				
				if($query->rowCount() > 0) {				
					while($data = $query->fetch()) {
						echo '
						<tr>
							<td><a href="../Membres/Profil.php?id='.$data["membre_post_id"].'">'.$data["membre_post_pseudo"].'</a></td>
							<td><a href="../Membres/Profil.php?id='.$data["membre_signaleur_id"].'">'.$data["membre_signaleur_pseudo"].'</a></td>
							<td><a href="poster.php?p='.$data["post_id"].'&amp;action=designaler">Désignaler</a></td>						
						</tr>';
					}
				}
				else {
					echo '
					<tr>
						<td colspan="3" align="center" style="padding:5px; font-size:13px;">Aucun message n\'a été signalé.</td>					
					</tr>';
				}
				
				echo '</table>';
			break;
			
			case "Forum":
				$gesture = htmlspecialchars($_GET['Faire']);
				
				switch($gesture) {
					case "Creer":
						if(empty($_GET['c'])) {
							echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Création</h2>';
											
							echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="admin.php">Administration</a> > <a href="admin.php?Action=Forum">Forum</a> > <a href="admin.php?Action=Forum&amp;Faire=Creer">Création</a></p>';

							echo '
							<p class="ForumSend">
								<span class="titreSend">Que voulez-vous faire ?</span>
								<span class="contenuSend">
									<span id="a">Cliquez <a href="?Action=Forum&Faire=Creer&c=f">ici</a> pour créer un forum.</span>
									<span id="b">Cliquez <a href="?Action=Forum&Faire=Creer&c=c">ici</a> pour créer une catégorie.</span>
								</span>
							</p>';	 
						}
						elseif($_GET['c'] == "f") {
							$query = $baseDeDonnes->query('SELECT cat_id, cat_nom FROM forum_categorie ORDER BY cat_ordre DESC');
							
							echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Création d\'un forum</h2>';
											
							echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="admin.php">Administration</a> > <a href="admin.php?Action=Forum">Forum</a> > <a href="admin.php?Action=Forum&amp;Faire=Creer">Création</a> > <a href="admin.php?Action=Forum&amp;Faire=Creer&amp;c=f">Créer un Forum</a></p>';
							
							echo '<form method="post" action="adminok.php?Action=Forum&Faire=Creer&c=f">';
								echo '
								<p><label for="nom">Titre : </label><input type="text" id="nom" name="nom" /></p>
								<p><label for="desc">Description : </label><input type="text" id="desc" name="desc" style="width:500px;" /></p>
								<p><label for="cat">Catégorie : </label><select name="cat" id="cat">';
									while($data = $query->fetch()) {
										echo'<option value="'.$data['cat_id'].'">'.$data['cat_nom'].'</option>';
									}
								echo'</select></p>
								<p align="center"><input type="submit" value="Envoyer" /></p>
							</form>';
							$query->CloseCursor();
						}       
						elseif($_GET['c'] == "c") {
							echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Création d\'une catégorie</h2>';
											
							echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="admin.php">Administration</a> > <a href="admin.php?Action=Forum">Forum</a> > <a href="admin.php?Action=Forum&amp;Faire=Creer">Création</a> > <a href="admin.php?Action=Forum&amp;Faire=Creer&amp;c=c">Créer une Catégorie</a></p>';
							
							echo '<form method="post" action="adminok.php?Action=Forum&Faire=Creer&c=c">';
								echo '
									<p><label for="nom">Nom de la catégorie : </label><input type="text" id="nom" name="nom" /></p>
									<p align="center"><input type="submit" value="Envoyer" /></p>
								</form>';
						}
					break;
					
					case "Editer":
						if(!isset($_GET['e'])) {
							echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Édition</h2>';
											
							echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="admin.php">Administration</a> > <a href="admin.php?Action=Forum">Forum</a> > <a href="admin.php?Action=Forum&amp;Faire=Editer">Édition</a></p>';
							
							echo '
							<p class="ForumSend">
								<span class="titreSend">Que voulez-vous faire ?</span>
								<span class="contenuSend">
									<span id="a">Cliquez <a href="?Action=Forum&Faire=Editer&e=editf">ici</a> pour éditer un forum.</span>
									<span id="b">Cliquez <a href="?Action=Forum&Faire=Editer&e=editc">ici</a> pour éditer une catégorie.</span>
									<span id="a">Cliquez <a href="?Action=Forum&Faire=Editer&e=ordref">ici</a> pour changer l\'ordre des forums.</span>
									<span id="b">Cliquez <a href="?Action=Forum&Faire=Editer&e=ordrec">ici</a> pour changer l\'ordre des catégories.</span>
								</span>
							</p>';	 
						}
						elseif($_GET['e'] == "editf") {
							if(!isset($_POST['forum_id'])) {
								echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Édition d\'un forum</h2>';
											
								echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="admin.php">Administration</a> > <a href="admin.php?Action=Forum">Forum</a> > <a href="admin.php?Action=Forum&amp;Faire=Editer">Édition</a> > <a href="admin.php?Action=Forum&amp;Faire=Editer&amp;e=editf">Éditer un Forum</a></p>';
								
								$query = $baseDeDonnes->query('SELECT forum_id, forum_name FROM forum_forum ORDER BY forum_ordre DESC');
									   
								echo '<form method="post" action="admin.php?Action=Forum&Faire=Editer&e=editf">';
								echo '<p><label for="forum">Choisir un forum :</label>
									<select name="forum_id" id="forum">';
									while($data = $query->fetch()) {
										echo'<option value="'.$data['forum_id'].'">
										'.stripslashes(htmlspecialchars($data['forum_name'])).'</option>';
									}
								echo '</select></p><p align="center"><input type="submit" value="Envoyer" /></p></form>';
								$query->CloseCursor();
							}               
							else {
								$query = $baseDeDonnes->prepare('SELECT forum_id, forum_name, forum_desc, forum_cat_id FROM forum_forum WHERE forum_id = :forum');
								$query->bindValue(':forum', (int) $_POST['forum_id'], PDO::PARAM_INT);
								$query->execute();
										
								$data1 = $query->fetch();

								echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Édition d\'un forum</h2>';
								
								echo '<p class="encadreInfos"><span class="left">Forum concerné</span><span class="right">'.stripslashes(htmlspecialchars($data1['forum_name'])).'</span></p>';
										   
								echo '<form method="post" action="adminok.php?Action=Forum&amp;Faire=Editer&amp;e=editf">
								<p><label for="nom">Nom du forum : </label><input type="text" id="nom" name="nom" value="'.$data1['forum_name'].'" /></p>
								<p><label for="desc">Description : </label><input type="text" id="desc" name="desc" style="width:500px;" value="'.stripslashes($data1['forum_desc']).'" /></p>';
								$query->CloseCursor();				  

								$query = $baseDeDonnes->query('SELECT cat_id, cat_nom
								FROM forum_categorie ORDER BY cat_ordre DESC');
								echo '<p><label for="depl">Déplacer le forum vers : </label>
								<select name="depl" id="depl">';
								while($data2 = $query->fetch()) {
									if($data2['cat_id'] == $data1['forum_cat_id']) {
										echo'<option value="'.$data2['cat_id'].'" 
												selected="selected">'.stripslashes($data2['cat_nom']).' 
												</option>';
									}
									else {
										echo'<option value="'.$data2['cat_id'].'">'.$data2['cat_nom'].'</option>';
									}
								}
								echo'</select></p><input type="hidden" name="forum_id" value="'.$data1['forum_id'].'">';
								echo'<p align="center"><input type="submit" value="Envoyer"></p></form>';
								$query->CloseCursor();				  
							}
						}
						elseif($_GET['e'] == "editc") {
							if(!isset($_POST['cat'])) {
								$query = $baseDeDonnes->query('SELECT cat_id, cat_nom FROM forum_categorie ORDER BY cat_ordre DESC');
								
								echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Édition d\'une catégorie</h2>';
											
								echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="admin.php">Administration</a> > <a href="admin.php?Action=Forum">Forum</a> > <a href="admin.php?Action=Forum&amp;Faire=Editer">Édition</a> > <a href="admin.php?Action=Forum&amp;Faire=Editer&amp;e=editc">Éditer une Catégorie</a></p>';
								
								echo '<form method="post" action="admin.php?Action=Forum&Faire=Editer&e=editc ">';
								echo '<p><label for="cat">Choisir une catégorie : </label>
								<select name="cat" id="cat">';
								while($data = $query->fetch()) {
									echo'<option value="'.$data['cat_id'].'">'.$data['cat_nom'].'</option>';
								}
								echo '</select></p><p align="center"><input type="submit" value="Envoyer"></p></form>';		
								$query->CloseCursor();				  					
							}         
							else {
								$query = $baseDeDonnes->prepare('SELECT cat_nom FROM forum_categorie WHERE cat_id = :cat');
								
								echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Édition d\'une catégorie</h2>';
											
								echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="admin.php">Administration</a> > <a href="admin.php?Action=Forum">Forum</a> > <a href="admin.php?Action=Forum&amp;Faire=Editer">Édition</a> > <a href="admin.php?Action=Forum&amp;Faire=Editer&amp;e=editc">Éditer une Catégorie</a></p>';
								
								$query->bindValue(':cat', (int) $_POST['cat'], PDO::PARAM_INT);
								$query->execute();
								$data = $query->fetch();
								
								echo '<form method="post" action="adminok.php?Action=Forum&amp;Faire=Editer&amp;e=editc">';
								
								echo '<p class="encadreInfos"><span class="left">Catégorie concernée</span><span class="right">'.stripslashes(htmlspecialchars($data['cat_nom'])).'</span></p>';
								
								echo '<p><label for="nom">Nom de la catégorie : </label>
								<input type="text" id="nom" name="nom" value="'.stripslashes(htmlspecialchars($data['cat_nom'])).'" /></p> 
								<input type="hidden" name="cat" value="'.$_POST['cat'].'" />
								<p align="center"><input type="submit" value="Envoyer" /></p></form>';
								$query->CloseCursor();				  
							}
						}
						elseif($_GET['e'] == "ordref") {
							$categorie = "";
							$query = $baseDeDonnes->query('SELECT forum_id, forum_name, forum_ordre, forum_cat_id, cat_id, cat_nom FROM forum_categorie	LEFT JOIN forum_forum ON cat_id = forum_cat_id
							ORDER BY cat_ordre DESC');
								
							echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Changer l\'ordre des forums</h2>';
										
							echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="admin.php">Administration</a> > <a href="admin.php?Action=Forum">Forum</a> > <a href="admin.php?Action=Forum&amp;Faire=Editer">Édition</a> > <a href="admin.php?Action=Forum&amp;Faire=Editer&amp;e=ordref">Changer l\'ordre des forums</a></p>';
							
							echo '<form method="post" action="adminok.php?Action=Forum&amp;Faire=Editer&amp;e=ordref">';
							   
							echo '<table>';

							while($data = $query->fetch()) {
								if($categorie !== $data['cat_id'] ) {
									$categorie = $data['cat_id'];
									echo'
									<tr>       
										<th style="width:610px;">'.stripslashes(htmlspecialchars($data['cat_nom'])).'</th>
										<th style="width:150px;">Ordre</th>
									</tr>';
								}
								echo '
								<tr>
									<td><a href="forum.php?f='.$data['forum_id'].'">'.stripslashes(htmlspecialchars($data['forum_name'])).'</a></td>
									<td><input type="text" value="'.$data['forum_ordre'].'" name="'.$data['forum_id'].'" /></td>
								</tr>';
							}
							echo '</table>
							<p align="center"><input type="submit" value="Envoyer" /></p></form>';	
						}
						elseif($_GET['e'] == "ordrec") {
							$query = $baseDeDonnes->query('SELECT cat_id, cat_nom, cat_ordre FROM forum_categorie ORDER BY cat_ordre DESC');
							
							echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Changer l\'ordre des catégories</h2>';
										
							echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="admin.php">Administration</a> > <a href="admin.php?Action=Forum">Forum</a> > <a href="admin.php?Action=Forum&amp;Faire=Editer">Édition</a> > <a href="admin.php?Action=Forum&amp;Faire=Editer&amp;e=ordrec">Changer l\'ordre des catégories</a></p>';
				 
							echo '<form method="post" action="adminok.php?Action=Forum&amp;Faire=Editer&amp;e=ordrec">';
							while($data = $query->fetch()) {
								echo '<p><label for="'.$data['cat_id'].'">'.stripslashes(htmlspecialchars($data['cat_nom'])).' : </label>
								<input type="text" value="'.$data['cat_ordre'].'" name="'.$data['cat_id'].'" /></p>';
							}
							echo '<p align="center"><input type="submit" value="Envoyer" /></p></form>';
							$query->CloseCursor();				  					
						}
					break;
					
					case "Droits":
						if(!isset($_POST['forum'])) {
							echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Gestion des Droits</h2>';
										
							echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="admin.php">Administration</a> > <a href="admin.php?Action=Forum">Forum</a> > <a href="admin.php?Action=Forum&amp;Faire=Droits">Droits</a></p>';
						
							$query = $baseDeDonnes->query('SELECT forum_id, forum_name FROM forum_forum ORDER BY forum_ordre DESC');
							echo '<form method="post" action="admin.php?Action=Forum&Faire=Droits">';
							
							echo '<p><label for="forum">Choisir un forum : </label>
							<select name="forum" id="forum">';
							while($data = $query->fetch()) {
								echo '<option value="'.$data['forum_id'].'">'.$data['forum_name'].'</option>';
							}
							echo '</select></p><p align="center"><input type="submit" value="Envoyer"></p></form>';
							$query->CloseCursor();				  					
						}
						else {
							echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Gestion des Droits</h2>';
										
							echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="admin.php">Administration</a> > <a href="admin.php?Action=Forum">Forum</a> > <a href="admin.php?Action=Forum&amp;Faire=Droits">Droits</a></p>';

							$query = $baseDeDonnes->prepare('SELECT forum_id, forum_name, auth_view, auth_post, auth_topic, auth_annonce, auth_modo FROM forum_forum WHERE forum_id = :forum');
							$query->bindValue(':forum', (int) $_POST['forum'], PDO::PARAM_INT);
							$query->execute();
					 
							echo '<form method="post" action="adminok.php?Action=Forum&amp;Faire=Droits">
							
							<table>
								<tr>
									<th>Lire</th>
									<th>Répondre</th>
									<th>Poster</th>
									<th>Annonce</th>
									<th>Modérer</th>
								</tr>';
							$data = $query->fetch();
							   
							$list_champ = array("auth_view", "auth_post", "auth_topic", "auth_annonce", "auth_modo");
						 
							foreach($list_champ as $champ) {
								echo '<td><select name="'.$champ.'">';
								for($i = 1; $i < 6; $i++) {
									if($i == $data[$champ]) {
										echo '<option value="'.$i.'" selected="selected">'.$MEMBRES_RANGS[$i].'</option>';
									}	
									else {
										echo '<option value="'.$i.'">'.$MEMBRES_RANGS[$i].'</option>';
									}
								}
								echo '</select></td>';
							}
							echo '</table>';
							echo '<input type="hidden" name="forum_id" value="'.$data['forum_id'].'" />
							<p align="center"><input type="submit" value="Envoyer"></p></form>';			          

							$query->CloseCursor();
						}
					break;
					
					default;
						echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Administration du forum</h2>';
						
						echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="admin.php">Administration</a> > <a href="admin.php?Action=Forum">Forums</a></p>';

						echo '
						<p class="ForumSend">
							<span class="titreSend">Que voulez-vous faire ?</span>
							<span class="contenuSend">
								<span id="a">Cliquez <a href="?Action=Forum&Faire=Creer">ici</a> pour entrer dans la création.</span>
								<span id="b">Cliquez <a href="?Action=Forum&Faire=Editer">ici</a> pour entrer dans l\'édition.</span>
								<span id="b">Cliquez <a href="?Action=Forum&Faire=Droits">ici</a> pour gérer les droits.</span>
							</span>
						</p>';	 
					break;
				}
			break;

		}
	}
	else {
		echo 'Page masquée';
	}
}
else {
	echo 'Page masquée';
}

include("FooterForum.php");
include("../Footer.php");
?>