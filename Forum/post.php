<?php
$titre_page_nom = "Forum";
include("../Header.php");


$action = (isset($_GET['action']))?utf8_encode($_GET['action']):'';

if(!isset($ID_MEMBRE)) erreur(ERR_IS_CO);

switch($action) {
	case "nouveautopic":
		$message = $_POST['message'];
		$description = $_POST['description'];
		$titre = $_POST['titre'];

		$forum = (int) $_GET['f'];
		$temps = time();

		$query = $baseDeDonnes->prepare('SELECT post_createur, post_texte, topic_id, auth_view FROM forum_forum WHERE forum_id = :forum');
		$query->bindValue(':forum', $forum, PDO::PARAM_INT);
		$query->execute();
		$data1 = $query->fetch();
		
		$topic = $data1['topic_id'];
		$auth_view = $data1["auth_view"];
		
		$query->CloseCursor();

		if(!verifAuthForum($auth_view)) {
			erreur(ERR_AUTH_VIEW);    
		}
		else {
			if(empty($message) || empty($titre)) {
				echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Nouveau Sujet" /> Nouveau Sujet</h2>';
				echo '
				<p class="ForumSend">
					<span class="titreSend"><img src="../Images/Forum/BugPost.png" alt="" /> Problème ...</span>
					<span class="contenuSend">
						<span id="a">Il semble que le message ou le titre soit vide.</span>
						<span id="b">Cliquez <a href="poster.php?action=nouveautopic&amp;f='.$forum.'">ici</a> pour recommencer.</span>
					</span>
				</p>';	
			}
			else {
				//Création du sujet
				$query = $baseDeDonnes->prepare('SELECT topic_titre FROM forum_topic WHERE topic_titre = :titre');
				$query->bindValue(':titre', $titre, PDO::PARAM_STR);
				$query->execute();
				
				$query->CloseCursor(); 

				$query = $baseDeDonnes->prepare('INSERT INTO forum_topic(forum_id, topic_titre, topic_descri, topic_createur, topic_time) VALUES(:forum, :titre, :description, :id, :temps)');
				$query->bindValue(':forum', $forum, PDO::PARAM_INT);
				$query->bindValue(':titre', $titre, PDO::PARAM_STR);
				$query->bindValue(':description', $description, PDO::PARAM_STR);
				$query->bindValue(':id', $ID_MEMBRE, PDO::PARAM_INT);
				$query->bindValue(':temps', $temps, PDO::PARAM_INT);
				$query->execute();

				$nouveautopic = $baseDeDonnes->lastInsertId();
				$query->CloseCursor(); 

				//Création du message
				$query = $baseDeDonnes->prepare('INSERT INTO forum_post(post_createur, post_texte, post_time, topic_id, post_forum_id) VALUES (:id, :mess, :temps, :nouveautopic, :forum)');
				$query->bindValue(':id', $ID_MEMBRE, PDO::PARAM_INT);
				$query->bindValue(':mess', $message, PDO::PARAM_STR);
				$query->bindValue(':temps', $temps,PDO::PARAM_INT);
				$query->bindValue(':nouveautopic', (int) $nouveautopic, PDO::PARAM_INT);
				$query->bindValue(':forum', $forum, PDO::PARAM_INT);
				$query->execute();

				$nouveaupost = $baseDeDonnes->lastInsertId();
				$query->CloseCursor(); 

				//Modification du premier et du dernier message
				$query = $baseDeDonnes->prepare('UPDATE forum_topic SET topic_last_post = :nouveaupost, topic_first_post = :nouveaupost, topic_post = topic_post + 1 WHERE topic_id = :nouveautopic');
				$query->bindValue(':nouveaupost', (int) $nouveaupost, PDO::PARAM_INT);    
				$query->bindValue(':nouveautopic', (int) $nouveautopic, PDO::PARAM_INT);
				$query->execute();
				$query->CloseCursor();

				//Mise à jour des comptes
				$query = $baseDeDonnes->prepare('UPDATE forum_forum SET forum_post = forum_post + 1, forum_topic = forum_topic + 1, forum_last_post_id = :nouveaupost
				WHERE forum_id = :forum');
				$query->bindValue(':nouveaupost', (int) $nouveaupost, PDO::PARAM_INT);    
				$query->bindValue(':forum', (int) $forum, PDO::PARAM_INT);
				$query->execute();
				$query->CloseCursor();
				
				if($forum != 7) {
					//Mise à jour du compte de messages du membre
					$query = $baseDeDonnes->prepare('UPDATE membres SET membre_post = membre_post + 1 WHERE membre_id = :id');
					$query->bindValue(':id', $ID_MEMBRE, PDO::PARAM_INT);    
					$query->execute();
					$query->CloseCursor();
				}
				
				//On ajoute une ligne dans la table forum_topic_view
				$query=$baseDeDonnes->prepare('INSERT INTO forum_topic_view(tv_id, tv_topic_id, tv_forum_id, tv_post_id, tv_poste) VALUES(:id, :topic, :forum, :post, :poste)');
				$query->bindValue(':id', $ID_MEMBRE, PDO::PARAM_INT);
				$query->bindValue(':topic', $nouveautopic, PDO::PARAM_INT);
				$query->bindValue(':forum', $forum , PDO::PARAM_INT);
				$query->bindValue(':post', $nouveaupost, PDO::PARAM_INT);
				$query->bindValue(':poste', '1', PDO::PARAM_STR);
				$query->execute();
				$query->CloseCursor();
				
				//Message
				echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Nouveau Sujet" /> Nouveau Sujet</h2>';
				echo '
				<p class="ForumSend">
					<span class="titreSend">Le sujet a bien été ajouté.</span>
					<span class="contenuSend">
						<span id="a">Cliquez <a href="index.php">ici</a> pour revenir à l\'accueil du forum.</span>
						<span id="b">Cliquez <a href="voirsujet.php?t='.$nouveautopic.'">ici</a> pour voir le sujet.</span>
					</span>
				</p>';
			}
		}
	break;
	
	case "annonce":
		$message = $_POST['message'];
		$description = $_POST['description'];
		$titre = $_POST['titre'];

		$forum = (int) $_GET['f'];
		$temps = time();
		
		$query = $baseDeDonnes->prepare('SELECT post_createur, post_texte, topic_id, auth_annonce FROM forum_forum WHERE forum_id = :forum');
		$query->bindValue(':forum', $forum, PDO::PARAM_INT);
		$query->execute();
		$data1 = $query->fetch();
		
		$topic = $data1['topic_id'];
		$auth_annonce = $data1["auth_annonce"];
		
		$query->CloseCursor();
		
		if(!verifAuthForum($auth_annonce)) {
			erreur(ERR_AUTH_ANNONCE);    
		}
		else {
			if(empty($message) || empty($titre)) {
				echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Nouveau Sujet" /> Nouvelle Annonce</h2>';
				echo '
				<p class="ForumSend">
					<span class="titreSend"><img src="../Images/Forum/BugPost.png" alt="" /> Problème ...</span>
					<span class="contenuSend">
						<span id="a">Il semble que le message ou le titre soit vide.</span>
						<span id="b">Cliquez <a href="poster.php?action=annonce&amp;f='.$forum.'">ici</a> pour recommencer.</span>
					</span>
				</p>';	
			}
			else {
				//Création du sujet
				$query = $baseDeDonnes->prepare('SELECT topic_titre FROM forum_topic WHERE topic_titre = :titre');
				$query->bindValue(':titre', $titre, PDO::PARAM_STR);
				$query->execute();
				
				$query->CloseCursor(); 

				$query = $baseDeDonnes->prepare('INSERT INTO forum_topic(forum_id, topic_titre, topic_descri, topic_createur, topic_time, topic_genre) VALUES(:forum, :titre, :description, :id, :temps, "Annonce")');
				$query->bindValue(':forum', $forum, PDO::PARAM_INT);
				$query->bindValue(':titre', $titre, PDO::PARAM_STR);
				$query->bindValue(':description', $description, PDO::PARAM_STR);
				$query->bindValue(':id', $ID_MEMBRE, PDO::PARAM_INT);
				$query->bindValue(':temps', $temps, PDO::PARAM_INT);
				$query->execute();

				$nouveautopic = $baseDeDonnes->lastInsertId();
				$query->CloseCursor(); 

				//Création du message
				$query = $baseDeDonnes->prepare('INSERT INTO forum_post(post_createur, post_texte, post_time, topic_id, post_forum_id) VALUES (:id, :mess, :temps, :nouveautopic, :forum)');
				$query->bindValue(':id', $ID_MEMBRE, PDO::PARAM_INT);
				$query->bindValue(':mess', $message, PDO::PARAM_STR);
				$query->bindValue(':temps', $temps,PDO::PARAM_INT);
				$query->bindValue(':nouveautopic', (int) $nouveautopic, PDO::PARAM_INT);
				$query->bindValue(':forum', $forum, PDO::PARAM_INT);
				$query->execute();

				$nouveaupost = $baseDeDonnes->lastInsertId();
				$query->CloseCursor(); 

				//Modification du premier et du dernier message
				$query = $baseDeDonnes->prepare('UPDATE forum_topic SET topic_last_post = :nouveaupost, topic_first_post = :nouveaupost, topic_post = topic_post + 1 WHERE topic_id = :nouveautopic');
				$query->bindValue(':nouveaupost', (int) $nouveaupost, PDO::PARAM_INT);    
				$query->bindValue(':nouveautopic', (int) $nouveautopic, PDO::PARAM_INT);
				$query->execute();
				$query->CloseCursor();

				//Mise à jour des comptes
				$query = $baseDeDonnes->prepare('UPDATE forum_forum SET forum_post = forum_post + 1, forum_topic = forum_topic + 1, forum_last_post_id = :nouveaupost
				WHERE forum_id = :forum');
				$query->bindValue(':nouveaupost', (int) $nouveaupost, PDO::PARAM_INT);    
				$query->bindValue(':forum', (int) $forum, PDO::PARAM_INT);
				$query->execute();
				$query->CloseCursor();
				
				if($forum != 7) {
					//Mise à jour du compte de messages du membre
					$query = $baseDeDonnes->prepare('UPDATE membres SET membre_post = membre_post + 1 WHERE membre_id = :id');
					$query->bindValue(':id', $ID_MEMBRE, PDO::PARAM_INT);    
					$query->execute();
					$query->CloseCursor();
				}
				
				//On ajoute une ligne dans la table forum_topic_view
				$query=$baseDeDonnes->prepare('INSERT INTO forum_topic_view(tv_id, tv_topic_id, tv_forum_id, tv_post_id, tv_poste) VALUES(:id, :topic, :forum, :post, :poste)');
				$query->bindValue(':id', $ID_MEMBRE, PDO::PARAM_INT);
				$query->bindValue(':topic', $nouveautopic, PDO::PARAM_INT);
				$query->bindValue(':forum', $forum , PDO::PARAM_INT);
				$query->bindValue(':post', $nouveaupost, PDO::PARAM_INT);
				$query->bindValue(':poste', '1', PDO::PARAM_STR);
				$query->execute();
				$query->CloseCursor();
				
				//Message
				echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Nouveau Sujet" /> Nouvelle Annonce</h2>';
				echo '
				<p class="ForumSend">
					<span class="titreSend">L\'annonce a bien été ajoutée.</span>
					<span class="contenuSend">
						<span id="a">Cliquez <a href="index.php">ici</a> pour revenir à l\'accueil du forum.</span>
						<span id="b">Cliquez <a href="voirsujet.php?t='.$nouveautopic.'">ici</a> pour voir l\'annonce.</span>
					</span>
				</p>';
			}
		}
	break;
	
	case "repondre":
		$message = $_POST['message'];
		
		$topic = (int) $_GET['t'];
		
		$query=$baseDeDonnes->prepare('SELECT topic_locked FROM forum_topic WHERE topic_id = :topic');
		$query->bindValue(':topic',$topic,PDO::PARAM_INT);
		$query->execute(); 
		$data=$query->fetch();
		if($data['topic_locked'] != 0) {
			erreur(ERR_TOPIC_VERR);
		}
		$query->CloseCursor();
	
		$temps = time();

		if(empty($message)) {
			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Répondre au Sujet</h2>';
			echo '
			<p class="ForumSend">
				<span class="titreSend"><img src="../Images/Forum/BugPost.png" alt="" /> Problème ...</span>
				<span class="contenuSend">
					<span id="a">Il semble que le message soit vide.</span>
					<span id="b">Cliquez <a href="poster.php?action=repondre&amp;t='.$topic.'">ici</a> pour recommencer.</span>
				</span>
			</p>';	
		}
		else {
			//Forum
			$query = $baseDeDonnes->prepare('SELECT forum_id, topic_post FROM forum_topic WHERE topic_id = :topic');
			$query->bindValue(':topic', $topic, PDO::PARAM_INT);    
			$query->execute();
			$data = $query->fetch();
			$forum = $data['forum_id'];

			//Enregistrement du message
			$query = $baseDeDonnes->prepare('INSERT INTO forum_post(post_createur, post_texte, post_time, topic_id, post_forum_id) VALUES(:id,:mess,:temps,:topic,:forum)');
			$query->bindValue(':id', $ID_MEMBRE, PDO::PARAM_INT);   
			$query->bindValue(':mess', $message, PDO::PARAM_STR);  
			$query->bindValue(':temps', $temps, PDO::PARAM_INT);  
			$query->bindValue(':topic', $topic, PDO::PARAM_INT);   
			$query->bindValue(':forum', $forum, PDO::PARAM_INT); 
			$query->execute();

			$nouveaupost = $baseDeDonnes->lastInsertId();
			$query->CloseCursor(); 

			//Modifications de la table forum_topic
			$query=$baseDeDonnes->prepare('UPDATE forum_topic SET topic_post = topic_post + 1, topic_last_post = :nouveaupost WHERE topic_id =:topic');
			$query->bindValue(':nouveaupost', (int) $nouveaupost, PDO::PARAM_INT);   
			$query->bindValue(':topic', (int) $topic, PDO::PARAM_INT); 
			$query->execute();
			$query->CloseCursor(); 

			//Modifications des autres tables
			$query = $baseDeDonnes->prepare('UPDATE forum_forum SET forum_post = forum_post + 1 , forum_last_post_id = :nouveaupost WHERE forum_id = :forum');
			$query->bindValue(':nouveaupost', (int) $nouveaupost, PDO::PARAM_INT);   
			$query->bindValue(':forum', (int) $forum, PDO::PARAM_INT); 
			$query->execute();
			$query->CloseCursor(); 
			
			if($forum != 7) {
				$query = $baseDeDonnes->prepare('UPDATE membres SET membre_post = membre_post + 1 WHERE membre_id = :id');
				$query->bindValue(':id', $ID_MEMBRE, PDO::PARAM_INT); 
				$query->execute();
				$query->CloseCursor(); 
			}

			//On update la table forum_topic_view
			$query=$baseDeDonnes->prepare('UPDATE forum_topic_view 
			SET tv_post_id = :post, tv_poste = :poste
			WHERE tv_id = :id AND tv_topic_id = :topic');
			$query->bindValue(':post',$nouveaupost,PDO::PARAM_INT);
			$query->bindValue(':poste','1',PDO::PARAM_STR);
			$query->bindValue(':id',$ID_MEMBRE,PDO::PARAM_INT);
			$query->bindValue(':topic',$topic,PDO::PARAM_INT);
			$query->execute();
			$query->CloseCursor();
			
			//Message
			$nombreDeMessagesParPage = 10;
			$nbr_post = $data['topic_post'];
			$page = ceil($nbr_post / $nombreDeMessagesParPage);
			
			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Répondre au Sujet</h2>';
			echo '
			<p class="ForumSend">
				<span class="titreSend">Le message a bien été ajouté.</span>
				<span class="contenuSend">
					<span id="a">Cliquez <a href="index.php">ici</a> pour revenir à l\'accueil du forum.</span>
					<span id="b">Cliquez <a href="voirsujet.php?t='.$topic.'&amp;page='.$page.'#Post-'.$nouveaupost.'">ici</a> pour voir le message.</span>
				</span>
			</p>';	
		}
	break;
	
	case "edit":
		$post = (int) $_GET['p'];
	 
		$message = $_POST['message'];

		$query = $baseDeDonnes->prepare('SELECT post_createur, post_texte, topic_id, auth_modo FROM forum_post LEFT JOIN forum_forum ON forum_post.post_forum_id = forum_forum.forum_id WHERE post_id=:post');
		$query->bindValue(':post', $post, PDO::PARAM_INT);
		$query->execute();
		$data1 = $query->fetch();
		
		$topic = $data1['topic_id'];
		
		$query->CloseCursor();

		$query = $baseDeDonnes->prepare('SELECT COUNT(*) AS nbr FROM forum_post WHERE topic_id = :topic');
		$query->bindValue(':topic', $topic, PDO::PARAM_INT);
		$query->execute();
		$data2 = $query->fetch();
		
		$nbPosts = $data2['nbr'];
		
		$query->CloseCursor();

		if(!verifAuthForum($data1['auth_modo']) && $data1['post_createur'] != $ID_MEMBRE) {
			erreur(ERR_AUTH_EDIT);    
		}
		else {
			$time = time();
			$query = $baseDeDonnes->prepare('UPDATE forum_post SET post_texte = :message, post_editeur = :id, post_edit_time = :time WHERE post_id = :post');
			$query->bindValue(':message', $message, PDO::PARAM_STR);
			$query->bindValue(':id', $ID_MEMBRE, PDO::PARAM_INT);
			$query->bindValue(':time', $time, PDO::PARAM_INT);
			$query->bindValue(':post', $post, PDO::PARAM_INT);
			$query->execute();
			
			$nombreDeMessagesParPage = 15;
			$page = ceil($nbPosts / $nombreDeMessagesParPage);
			
			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Éditer un Message</h2>';
			echo '
			<p class="ForumSend">
				<span class="titreSend">Le message a bien été édité.</span>
				<span class="contenuSend">
					<span id="a">Cliquez <a href="index.php">ici</a> pour revenir à l\'accueil du forum.</span>
					<span id="b">Cliquez <a href="voirsujet.php?t='.$topic.'&amp;page='.$page.'#Post-'.$post.'">ici</a> pour voir le message.</span>
				</span>
			</p>';
			
			$query->CloseCursor();
		}
	break;
	
	case "delete":
		$post = (int) $_GET['p'];
		$query=$baseDeDonnes->prepare('SELECT post_createur, post_texte, forum_id, topic_id, auth_modo
		FROM forum_post
		LEFT JOIN forum_forum ON forum_post.post_forum_id = forum_forum.forum_id
		WHERE post_id=:post');
		$query->bindValue(':post',$post,PDO::PARAM_INT);
		$query->execute();
		$data = $query->fetch();
		$topic = $data['topic_id'];
		$forum = $data['forum_id'];
		$poster = $data['post_createur'];

		if(!verifAuthForum($data['auth_modo']) && $poster != $ID_MEMBRE) {
			erreur(ERR_AUTH_DELETE); 
		}
		else { 
			$query = $baseDeDonnes->prepare('SELECT topic_first_post, topic_last_post FROM forum_topic WHERE topic_id = :topic');
			$query->bindValue(':topic', $topic, PDO::PARAM_INT);
			$query->execute();
			$data_post = $query->fetch();

			if($data_post['topic_first_post'] == $post) {
				if(!verifAuthForum($data['auth_modo'])) {
					erreur(ERR_AUTH_DELETE_TOPIC);
				}
	 
				echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Supprimer un Message</h2>';
				echo '
				<p class="ForumSend">
					<span class="titreSend"><img src="../Images/Forum/Erreur.png" /> Attention</span>
					<span class="contenuSend">
						<span id="a">Vous avez choisi de supprimer un message. Cependant il est le premier du sujet. Voulez vous supprimer le sujet ?</span>
						<span id="b" align="center"><a href="post.php?action=delete_topic&amp;t='.$topic.'" class="choice">Oui</a><a href="voirsujet.php?t='.$topic.'" class="choice">Non</a></span>
					</span>
				</p>';
				$query->CloseCursor();                     
			}
			elseif($data_post['topic_last_post']==$post) {
				$query=$baseDeDonnes->prepare('DELETE FROM forum_post WHERE post_id = :post');
				$query->bindValue(':post',$post,PDO::PARAM_INT);
				$query->execute();
				$query->CloseCursor();
			   
				$query=$baseDeDonnes->prepare('SELECT post_id FROM forum_post WHERE topic_id = :topic 
				ORDER BY post_id DESC LIMIT 0,1');
				$query->bindValue(':topic',$topic,PDO::PARAM_INT);
				$query->execute();
				$data=$query->fetch();             
				$last_post_topic=$data['post_id'];
				$query->CloseCursor();

				$query=$baseDeDonnes->prepare('SELECT post_id FROM forum_post WHERE post_forum_id = :forum
				ORDER BY post_id DESC LIMIT 0,1');
				$query->bindValue(':forum',$forum,PDO::PARAM_INT);
				$query->execute();
				$data=$query->fetch();             
				$last_post_forum=$data['post_id'];
				$query->CloseCursor();   
					   
				$query=$baseDeDonnes->prepare('UPDATE forum_topic SET topic_last_post = :last
				WHERE topic_last_post = :post');
				$query->bindValue(':last',$last_post_topic,PDO::PARAM_INT);
				$query->bindValue(':post',$post,PDO::PARAM_INT);
				$query->execute();
				$query->CloseCursor();
	 
				$query=$baseDeDonnes->prepare('UPDATE forum_forum SET forum_post = forum_post - 1, forum_last_post_id = :last
				WHERE forum_id = :forum');
				$query->bindValue(':last',$last_post_forum,PDO::PARAM_INT);
				$query->bindValue(':forum',$forum,PDO::PARAM_INT);
				$query->execute();
				$query->CloseCursor(); 
							
				$query=$baseDeDonnes->prepare('UPDATE forum_topic SET  topic_post = topic_post - 1
				WHERE topic_id = :topic');
				$query->bindValue(':topic',$topic,PDO::PARAM_INT);
				$query->execute();
				$query->CloseCursor(); 
						   
				if($forum != 7) {
					$query=$baseDeDonnes->prepare('UPDATE membres SET  membre_post = membre_post - 1
					WHERE membre_id = :id');
					$query->bindValue(':id',$poster,PDO::PARAM_INT);
					$query->execute();
					$query->CloseCursor();  
				}
					
				echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Supprimer un Message</h2>';
				echo '
				<p class="ForumSend">
					<span class="titreSend">Le message a bien été supprimé.</span>
					<span class="contenuSend">
						<span id="a">Cliquez <a href="index.php">ici</a> pour revenir à l\'accueil du forum.</span>
						<span id="b">Cliquez <a href="voirsujet.php?t='.$topic.'">ici</a> pour retourner au sujet.</span>
					</span>
				</p>';	 
			}
			else {
				$query=$baseDeDonnes->prepare('DELETE FROM forum_post WHERE post_id = :post');
				$query->bindValue(':post',$post,PDO::PARAM_INT);
				$query->execute();
				$query->CloseCursor();
						   
				$query=$baseDeDonnes->prepare('UPDATE forum_forum SET forum_post = forum_post - 1  WHERE forum_id = :forum');
				$query->bindValue(':forum',$forum,PDO::PARAM_INT);
				$query->execute();
				$query->CloseCursor(); 
							
				$query=$baseDeDonnes->prepare('UPDATE forum_topic SET  topic_post = topic_post - 1
				WHERE topic_id = :topic');
				$query->bindValue(':topic',$topic,PDO::PARAM_INT);
				$query->execute();
				$query->CloseCursor(); 
						   
				if($forum != 7) {
					$query=$baseDeDonnes->prepare('UPDATE membres SET membre_post = membre_post - 1 WHERE membre_id = :id');
					$query->bindValue(':id',$data['post_createur'],PDO::PARAM_INT);
					$query->execute();
					$query->CloseCursor();  
				}
					
				echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Supprimer un Message</h2>';
				echo '
				<p class="ForumSend">
					<span class="titreSend">Le message a bien été supprimé.</span>
					<span class="contenuSend">
						<span id="a">Cliquez <a href="index.php">ici</a> pour revenir à l\'accueil du forum.</span>
						<span id="b">Cliquez <a href="voirsujet.php?t='.$topic.'">ici</a> pour retourner au sujet.</span>
					</span>
				</p>';	 
			}		   
		}
	break;
	
	case "delete_topic":
		$topic = (int) $_GET['t'];
		$query=$baseDeDonnes->prepare('SELECT forum_topic.forum_id, auth_modo
		FROM forum_topic
		LEFT JOIN forum_forum ON forum_topic.forum_id = forum_forum.forum_id
		WHERE topic_id=:topic');
		$query->bindValue(':topic',$topic,PDO::PARAM_INT);
		$query->execute();
		$data = $query->fetch();
		$forum = $data['forum_id'];

		if(!verifAuthForum($data['auth_modo']))	{
			erreur(ERR_AUTH_DELETE_TOPIC);
		}
		else {
			$query->CloseCursor();

			$query=$baseDeDonnes->prepare('SELECT topic_post FROM forum_topic WHERE topic_id = :topic');
			$query->bindValue(':topic',$topic,PDO::PARAM_INT);
			$query->execute();
			$data = $query->fetch();
			$nombrepost = $data['topic_post'];
			$query->CloseCursor();

			$query=$baseDeDonnes->prepare('DELETE FROM forum_topic
			WHERE topic_id = :topic');
			$query->bindValue(':topic',$topic,PDO::PARAM_INT);
			$query->execute();
			$query->CloseCursor();
		   
			$query=$baseDeDonnes->prepare('SELECT post_createur, COUNT(*) AS nombre_mess FROM forum_post
			WHERE topic_id = :topic GROUP BY post_createur');
			$query->bindValue(':topic',$topic,PDO::PARAM_INT);
			$query->execute();

			while($data = $query->fetch()) {
				$query=$baseDeDonnes->prepare('UPDATE forum_membres
				SET membre_post = membre_post - :mess
				WHERE membre_id = :id');
				$query->bindValue(':mess',$data['nombre_mess'],PDO::PARAM_INT);
				$query->bindValue(':id',$data['post_createur'],PDO::PARAM_INT);
				$query->execute();
			}

			$query->CloseCursor();       

			$query=$baseDeDonnes->prepare('DELETE FROM forum_post WHERE topic_id = :topic');
			$query->bindValue(':topic',$topic,PDO::PARAM_INT);
			$query->execute();
			$query->CloseCursor(); 

			$query=$baseDeDonnes->prepare('SELECT post_id FROM forum_post
			WHERE post_forum_id = :forum ORDER BY post_id DESC LIMIT 0,1');
			$query->bindValue(':forum',$forum,PDO::PARAM_INT);
			$query->execute();
			$data = $query->fetch();
	 
			$query=$baseDeDonnes->prepare('UPDATE forum_forum
			SET forum_topic = forum_topic - 1, forum_post = forum_post - :nbr, forum_last_post_id = :id
			WHERE forum_id = :forum');
			$query->bindValue(':nbr',$nombrepost,PDO::PARAM_INT);
			$query->bindValue(':id',$data['post_id'],PDO::PARAM_INT);
			$query->bindValue(':forum',$forum,PDO::PARAM_INT);
			$query->execute(); 
			$query->CloseCursor();

			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Supprimer un Sujet</h2>';
			echo '
			<p class="ForumSend">
				<span class="titreSend">Le sujet a bien été supprimé.</span>
				<span class="contenuSend">
					<span id="a">Cliquez <a href="index.php">ici</a> pour revenir à l\'accueil du forum.</span>
					<span id="b">Cliquez <a href="forum.php?f='.$forum.'">ici</a> pour retourner au forum.</span>
				</span>
			</p>';	 
		}
	break;
	
	case "lock":
		$topic = (int) $_GET['t'];
		$query = $baseDeDonnes->prepare('SELECT forum_topic.forum_id, auth_modo FROM forum_topic
		LEFT JOIN forum_forum ON forum_forum.forum_id = forum_topic.forum_id
		WHERE topic_id = :topic');
		$query->bindValue(':topic',$topic,PDO::PARAM_INT);
		$query->execute();
		$data = $query->fetch();

		if(!verifAuthForum($data['auth_modo'])) {
			erreur(ERR_AUTH_VERR);
		}  
		else {
			$query->CloseCursor();
			$query=$baseDeDonnes->prepare('UPDATE forum_topic SET topic_locked = :lock WHERE topic_id = :topic');
			$query->bindValue(':lock',1,PDO::PARAM_STR);
			$query->bindValue(':topic',$topic,PDO::PARAM_INT);
			$query->execute(); 
			$query->CloseCursor();

			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Verrouiller un Sujet</h2>';
			echo '
			<p class="ForumSend">
				<span class="titreSend">Le sujet a bien été verrouillé.</span>
				<span class="contenuSend">
					<span id="a">Cliquez <a href="index.php">ici</a> pour revenir à l\'accueil du forum.</span>
					<span id="b">Cliquez <a href="voirsujet.php?t='.$topic.'">ici</a> pour retourner au sujet.</span>
				</span>
			</p>';	 
		}
	break;
	 
	case "unlock":
		$topic = (int) $_GET['t'];
		$query = $baseDeDonnes->prepare('SELECT forum_topic.forum_id, auth_modo FROM forum_topic
		LEFT JOIN forum_forum ON forum_forum.forum_id = forum_topic.forum_id
		WHERE topic_id = :topic');
		$query->bindValue(':topic',$topic,PDO::PARAM_INT);
		$query->execute();
		$data = $query->fetch();
	 
		if(!verifAuthForum($data['auth_modo'])) {
			erreur(ERR_AUTH_VERR);
		}  
		else {
			$query->CloseCursor();
			$query=$baseDeDonnes->prepare('UPDATE forum_topic SET topic_locked = :lock WHERE topic_id = :topic');
			$query->bindValue(':lock',0,PDO::PARAM_STR);
			$query->bindValue(':topic',$topic,PDO::PARAM_INT);
			$query->execute(); 
			$query->CloseCursor();
			
			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Déverrouiller un Sujet</h2>';
			echo '
			<p class="ForumSend">
				<span class="titreSend">Le sujet a bien été déverrouillé.</span>
				<span class="contenuSend">
					<span id="a">Cliquez <a href="index.php">ici</a> pour revenir à l\'accueil du forum.</span>
					<span id="b">Cliquez <a href="voirsujet.php?t='.$topic.'">ici</a> pour retourner au sujet.</span>
				</span>
			</p>';	 
		}
	break;
	
	case "bannir":
		$membre = (int) $_GET['m'];
		$topic = (int) $_GET['t'];
		
		$query = $baseDeDonnes->prepare('SELECT forum_id FROM forum_topic WHERE topic_id = :topic');
		$query->bindValue(':topic', $topic, PDO::PARAM_INT);
		$query->execute();
		$data = $query->fetch();
		
		$forum = $data["forum_id"];
		
		$query->CloseCursor();

		$query = $baseDeDonnes->prepare('SELECT auth_modo FROM forum_forum WHERE forum_id = :forum');
		$query->bindValue(':forum', $forum, PDO::PARAM_INT);
		$query->execute();
		
		$data = $query->fetch();
	 
		if(!verifAuthForum($data['auth_modo'])) {
			erreur(ERR_AUTH_BANNIR);
		}  
		elseif($ID_MEMBRE == $membre) {
			erreur(ERR_AUTH_BANNIR_SELF);
		}
		else {
			$query->CloseCursor();
			$query = $baseDeDonnes->prepare('UPDATE membres SET membre_niveau = 0 WHERE membre_id = :membre');
			$query->bindValue(':membre', $membre, PDO::PARAM_INT);
			$query->execute(); 
			$query->CloseCursor();
			
			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Bannir un Membre</h2>';
			echo '
			<p class="ForumSend">
				<span class="titreSend">Le membre a bien été banni.</span>
				<span class="contenuSend">
					<span id="a">Cliquez <a href="index.php">ici</a> pour revenir à l\'accueil du forum.</span>
					<span id="b">Cliquez <a href="voirsujet.php?t='.$topic.'">ici</a> pour retourner au sujet.</span>
				</span>
			</p>';	 
		}
	break;
	
	case "signaler":
		$post = (int) $_GET['p'];
		
		$query = $baseDeDonnes->prepare('SELECT topic_id, post_forum_id, post_createur FROM forum_post WHERE post_id = :post');
		$query->bindValue(':post', $post, PDO::PARAM_INT);
		$query->execute();
		$data = $query->fetch();
		
		$topic = $data["topic_id"];
		$forum = $data["post_forum_id"];
		$membre = $data["post_createur"];
		
		$query->CloseCursor();

		$query = $baseDeDonnes->prepare('SELECT auth_view FROM forum_forum WHERE forum_id = :forum');
		$query->bindValue(':forum', $forum, PDO::PARAM_INT);
		$query->execute();
		
		$data = $query->fetch();
	 
		if(!verifAuthForum($data['auth_view'])) {
			erreur(ERR_AUTH_VIEW);
		}  
		elseif($ID_MEMBRE == $membre) {
			erreur(ERR_AUTH_SIGNALER_SELF);
		}
		else {
			$query->CloseCursor();
			$query = $baseDeDonnes->prepare('UPDATE forum_post SET post_signale = "1", post_signaleur = :membre WHERE post_id = :post');
			$query->bindValue(':post', $post, PDO::PARAM_INT);
			$query->bindValue(':membre', $ID_MEMBRE, PDO::PARAM_INT);
			$query->execute(); 
			$query->CloseCursor();
			
			$message = '<titre niveau=3><image>http://www.salixor.com/Images/Forum/Signal.png</image> Signalement de Message</titre><paragraphe>Un message a été signalé par un membre.
			Accédez à la liste des messages signalés <lien url=http://www.salixor.com/Forum/admin.php?Action=ViewSignale>ici</lien>.
			Vous pourrez ensuite décider de supprimer ou non ce message ou d\'appliquer une sanction pour signalement abusif.</paragraphe>';
			$postMsg = 'Signalement du Message #'.$post;
			
			$query2 = $baseDeDonnes->prepare("
			INSERT INTO messagerie(sujet, expediteur, destinataire, message, date, vu)
			VALUES(:post, :membre, 1, :message, '".time()."', '0')");
			$query2->bindValue(':post', $postMsg, PDO::PARAM_STR);
			$query2->bindValue(':membre', $ID_MEMBRE, PDO::PARAM_INT);
			$query2->bindValue(':message', $message, PDO::PARAM_STR);
			$query2->execute();
			
			$message = '<titre niveau=3><image>http://www.salixor.com/Images/Forum/Signal.png</image> Signalement de Message</titre><paragraphe><italique>Il s\'agit d\'un message automatique.</italique>
			Un de vos messages a été signalé par un membre.
			Si ce signalement est véridique, des sanctions pourront être apportés selon la gravité de l\'acte.
			Sinon, l\'auteur de ce signalement se verra sanctionné.</paragraphe>';
			$postMsg = 'Signalement d\'un de vos messages';
			
			$query2 = $baseDeDonnes->prepare("
			INSERT INTO messagerie(sujet, expediteur, destinataire, message, date, vu)
			VALUES(:post, 1, :membre, :message, '".time()."', '0')");
			$query2->bindValue(':post', $postMsg, PDO::PARAM_STR);
			$query2->bindValue(':membre', $membre, PDO::PARAM_INT);
			$query2->bindValue(':message', $message, PDO::PARAM_STR);
			$query2->execute();
			
			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Signaler un Message</h2>';
			echo '
			<p class="ForumSend">
				<span class="titreSend">Le message a bien été signalé.</span>
				<span class="contenuSend">
					<span id="a">Cliquez <a href="index.php">ici</a> pour revenir à l\'accueil du forum.</span>
					<span id="b">Cliquez <a href="voirsujet.php?t='.$topic.'">ici</a> pour retourner au sujet.</span>
				</span>
			</p>';	 
		}
	break;
	
	case "designaler":
		$post = (int) $_GET['p'];
		
		$query = $baseDeDonnes->prepare('SELECT topic_id, post_forum_id, post_createur FROM forum_post WHERE post_id = :post');
		$query->bindValue(':post', $post, PDO::PARAM_INT);
		$query->execute();
		$data = $query->fetch();
		
		$topic = $data["topic_id"];
		$forum = $data["post_forum_id"];
		$membre = $data["post_createur"];
		
		$query->CloseCursor();

		$query = $baseDeDonnes->prepare('SELECT auth_view FROM forum_forum WHERE forum_id = :forum');
		$query->bindValue(':forum', $forum, PDO::PARAM_INT);
		$query->execute();
		
		$data = $query->fetch();
	 
		if(!verifAuthForum($data['auth_view'])) {
			erreur(ERR_AUTH_VIEW);
		}  
		elseif($ID_MEMBRE == $membre) {
			erreur(ERR_AUTH_DESIGNALER_SELF);
		}
		else {
			$query->CloseCursor();
			$query = $baseDeDonnes->prepare('UPDATE forum_post SET post_signale = "0", post_signaleur = 0 WHERE post_id = :post');
			$query->bindValue(':post', $post, PDO::PARAM_INT);
			$query->execute(); 
			$query->CloseCursor();
			
			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Répondre au Sujet" /> Signaler un Message</h2>';
			echo '
			<p class="ForumSend">
				<span class="titreSend">Le message a bien été désignalé.</span>
				<span class="contenuSend">
					<span id="a">Cliquez <a href="index.php">ici</a> pour revenir à l\'accueil du forum.</span>
					<span id="b">Cliquez <a href="voirsujet.php?t='.$topic.'">ici</a> pour retourner au sujet.</span>
				</span>
			</p>';	 
		}
	break;
}

$query = $baseDeDonnes->prepare('SELECT nb_posts, id_autorise, id FROM membres_titres');
$query->execute();

$query2 = $baseDeDonnes->prepare('SELECT membre_post FROM membres');
$query2->execute();

$data2 = $query2->fetch();
while($data = $query->fetch()) {
	$autorises = explode(" ", $data["id_autorise"]);
	if($data2["membre_post"] > $data["nb_posts"] && !in_array($ID_MEMBRE, $autorises)) {
		$id = $data["id"];
		array_push($autorises, $ID_MEMBRE);
		$updateAutorises = NULL;
		foreach($autorises as $autorise) {
			$updateAutorises .= $autorise." ";
		}
		
		$query3 = $baseDeDonnes->prepare("UPDATE membres_titres SET id_autorise = '".$updateAutorises."' WHERE id = ".$id."");
		$query3->execute();
		
		$query4 = $baseDeDonnes->prepare("UPDATE membres SET membre_titre_id = ".$id." WHERE membre_id = ".$ID_MEMBRE."");
		$query4->execute();
	}
}

$query->CloseCursor();
$query2->CloseCursor();

include("../Footer.php"); ?>