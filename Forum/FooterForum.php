<?php
echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/WhoOnLine.png" alt="Forum" /> Qui est en ligne ?</h2>';

echo '<div id="forumStats" class="bloc">';
	$countOnline = 0;
	
	//Nombre de messages
	$nbPosts = $baseDeDonnes->query('SELECT COUNT(*) AS nbr_posts FROM forum_post')->fetchColumn();
	$query->CloseCursor();
	
	//Nombre de sujets	
	$nbSujets = $baseDeDonnes->query('SELECT COUNT(*) AS nbr_topics FROM forum_topic')->fetchColumn();
	$query->CloseCursor();
	
	//Nombre de membres
	$nbMembres = $baseDeDonnes->query('SELECT COUNT(*) AS nbr_membres FROM membres')->fetchColumn();
	$query->CloseCursor();
	
	//Anniversaire des membres
	$timeA = time()+604800;
	$membresAnniv = $baseDeDonnes->query('SELECT membre_naissance, membre_id, membre_pseudo FROM membres WHERE membre_naissance < '. $timeA .' AND membre_naissance > '.time().' AND membre_naissance <> 0');
	$membresAnniv->setFetchMode(PDO::FETCH_ASSOC);
	$nbAnniv = 0;
	$textAnnivEnd = "";
	while($ligne = $membresAnniv->fetch()) {
		$nbAnniv++;
		$textAnnivEnd .= ' <a href="../Membres/Profil.php?id='.$ligne['membre_id'].'">'.stripslashes(utf8_encode($ligne['membre_pseudo'])).'</a> ('.date("d\/m", $ligne["membre_naissance"]).'),';
	}
	
	if($nbAnniv == 0) {
		$textAnnivDebut = "Membres dont c'est l'anniversaire dans moins de 7 jours : ";
		$textAnnivEnd = 'Aucun,';
	}
	if($nbAnniv == 1) {
		$textAnnivDebut = "Membre dont c'est l'anniversaire dans moins de 7 jours : ";
	}
	if($nbAnniv > 1) {
		$textAnnivDebut = "Membres dont c'est l'anniversaire dans moins de 7 jours : ";
	}

	$textAnniv = $textAnnivDebut.$textAnnivEnd;
	$query->CloseCursor();
	$textAnniv = substr($textAnniv, 0, -1);

	//Décompte des visiteurs
	$nbVisiteurs = $baseDeDonnes->query('SELECT COUNT(*) AS nbr_visiteurs FROM forum_whosonline WHERE online_id = 0')->fetchColumn();
	$query->CloseCursor();

	//Décompte des membres
	$texteToShow = "<br />Membres en ligne : ";
	$timeMax = time() - (60 * 5);
	$query = $baseDeDonnes->prepare('SELECT membre_id, membre_pseudo, online_id FROM forum_whosonline LEFT JOIN membres ON online_id = membre_id WHERE online_time > :timemax AND online_id <> 0 ORDER BY membre_pseudo ASC');
	$query->bindValue(':timemax', $timeMax, PDO::PARAM_INT);
	$query->execute();
	$countMembres = 0;
	
	while($data = $query->fetch()) {
		if($data["online_id"] != 011001110110111101101111011001110110110001100101) {
			$countMembres++;
			$texteToShow .= ' <a href="../Membres/Profil.php?id='.$data['membre_id'].'">'.stripslashes(utf8_encode($data['membre_pseudo'])).'</a>,';
		}
		else {
			$texteToShow .= ' <a href="http://www.google.fr/">GOOGLE BOT</a>,';
		}
	}
	$texteToShow = substr($texteToShow, 0, -1);
	
	$countOnline = $nbVisiteurs + $countMembres;
	if($countMembres == 0)  $texteToShow = ""; 
	
	if($countOnline == 0) 	$utilisateurs = 'Il n\'y a actuellement personne en ligne.';
	if($countOnline == 1) 	$utilisateurs = 'Il y a 1 utilisateur en ligne : '.$countMembres.' membre(s) et '.$nbVisiteurs.' visiteur(s).'.$texteToShow;
	if($countOnline > 1)    $utilisateurs = 'Il y a '.$countOnline.' utilisateurs en ligne : '.$countMembres.' membre(s) et '.$nbVisiteurs.' visiteur(s).'.$texteToShow;
	
	echo '<p>';
		echo '<img src="../Images/Forum/StatsMembres.png" alt="" /> ';
		echo '<span>';
			echo $utilisateurs;
		echo '</span>';
	echo '</p>';
	echo '<p>';
		echo '<img src="../Images/Forum/StatsConnexion.png" alt="" /> ';
		echo '<span>';
			echo 'Le forum compte '.$nbPosts.' messages répartis dans '.$nbSujets.' sujets.<br />'.$nbMembres.' membres sont inscrits sur le site et le forum.';
		echo '</span>';
	echo '</p>';
	echo '<p>';
		echo '<img src="../Images/Forum/StatsAnniversaire.png" alt="" /> ';
		echo '<span>';
			echo $textAnniv;
		echo '</span>';
	echo '</p>';
	$query->CloseCursor();
	
	echo '<div class="legendeForumGlobal">';
		echo '<div class="legendeForum"><img src="../Images/Forum/MessageLu.png" alt="Message" /> <span>Messages Lus</span></div>';
		echo '<div class="legendeForum"><img src="../Images/Forum/MessageNonLu.png" alt="Message" /> <span>Messages Non Lus</span></div>';
		echo '<div class="legendeForum"><img src="../Images/Forum/Annonce.png" alt="Message" /> <span>Annonce</span></div>';
		echo '<div class="legendeForum"><img src="../Images/Forum/MessageLock.png" alt="Message" /> <span>Sujet verrouillé</span></div>';
	echo '</div>';
	
echo '</div>';
?>