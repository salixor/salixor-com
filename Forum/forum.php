<?php
include("../Includes/Fonctions.php");
include("../Includes/Config.php");

include("OnlineForum.php");


$forum = (int) $_GET['f'];

$nbTopics = $baseDeDonnes->query('SELECT COUNT(*) AS nbr_topics FROM forum_topic WHERE forum_id = '.$forum.'')->fetchColumn();
$query->CloseCursor();

$query = $baseDeDonnes->prepare('SELECT forum_name, forum_topic, auth_view, auth_topic, auth_annonce FROM forum_forum WHERE forum_id = :forum');
$query->bindValue(':forum', $forum, PDO::PARAM_INT);
$query->execute();
$data = $query->fetch();

$auth_topic = $data["auth_topic"];
$auth_annonce = $data["auth_annonce"];
$auth_view = $data["auth_view"];
$forumName = stripslashes($data['forum_name']);

$titre_page_nom = $forumName;
include("../Header.php");

mysql_close($connect_sql);

if(!verifAuthForum($auth_view)) {
	erreur(ERR_AUTH_VIEW);
}

$nombreDeMessagesParPage = 20;
$nombreDePages = ceil($nbTopics / $nombreDeMessagesParPage);

echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Forum.png" alt="Forum" /> '.$forumName.'</h2>';

echo '<p class="Arianne">> <a href="index.php">Accueil du Forum</a> > <a href="forum.php?f='.$forum.'">'.$forumName.'</a>';

$page = (isset($_GET['page']))?intval($_GET['page']):1;

echo '<ul class="navigationPage">';
if(isset($ID_MEMBRE)) {
	if(verifAuthForum($auth_topic))   echo '<a href="poster.php?action=nouveautopic&amp;f='.$forum.'"><li><img src="../Images/Forum/NewSujet.png" alt="" /> Sujet</li></a>';
	if(verifAuthForum($auth_annonce))   echo '<a href="poster.php?action=annonce&amp;f='.$forum.'"><li><img src="../Images/Forum/NewAnnonce.png" alt="" /> Annonce</li></a>';
}
echo '</ul>';

if($nombreDePages > 1) {
	echo '<p class="pagination" align="right">'.pagination($page, $nombreDePages, "?f=".$forum."&amp;page=%d").'</p>';
}

$premierMessageAafficher = ($page - 1) * $nombreDeMessagesParPage;

$query->CloseCursor();

$add1='';
$add2='';
if(isset($ID_MEMBRE)) {
	$add1 = 'tv_id, tv_post_id, tv_poste,'; 
	$add2 = 'LEFT JOIN forum_topic_view 
	ON forum_topic.topic_id = forum_topic_view.tv_topic_id AND forum_topic_view.tv_id = :id';
}

$query = $baseDeDonnes->prepare('SELECT forum_topic.topic_id, topic_titre, topic_createur, topic_post, topic_time, topic_last_post, topic_locked, topic_descri,
Mb.membre_pseudo AS membre_pseudo_createur, post_createur, post_time, Ma.membre_pseudo AS membre_pseudo_last_posteur, Ma.membre_id AS membre_id_last_posteur,
'.$add1.' post_id FROM forum_topic 
LEFT JOIN membres Mb ON Mb.membre_id = forum_topic.topic_createur
LEFT JOIN forum_post ON forum_topic.topic_last_post = forum_post.post_id
LEFT JOIN membres Ma ON Ma.membre_id = forum_post.post_createur
'.$add2.'
WHERE forum_topic.forum_id = :forum AND topic_genre = "Annonce"
ORDER BY topic_last_post DESC
LIMIT :premier, :nombre');
$query->bindValue(':forum',$forum,PDO::PARAM_INT);
if(isset($ID_MEMBRE)) {
	$query->bindValue(':id',$ID_MEMBRE,PDO::PARAM_INT);
}
$query->bindValue(':forum', $forum, PDO::PARAM_INT);
$query->bindValue(':premier', (int) $premierMessageAafficher, PDO::PARAM_INT);
$query->bindValue(':nombre', (int) $nombreDeMessagesParPage, PDO::PARAM_INT);
$query->execute();

if($query->rowCount() > 0) {   
	echo '<table>';

	echo '
	<tr>
		<th colspan="2" style="width:430px;">Annonce</th>             
		<th style="width:40px;">Messages</th>
		<th style="width:200px;">Dernier message</th>
	</tr>';

	while($data = $query->fetch()) {
		echo '<tr>';
			
			if(isset($ID_MEMBRE)) {
				if($data['tv_id'] == $ID_MEMBRE) {
					if($data['tv_poste'] == '0') {
						if($data['tv_post_id'] == $data['topic_last_post']) {
							$messageIcone = 'Annonce.png';
						}
						else {
							$messageIcone = 'AnnonceNonLue.png';
						}
					}
					else {
						if($data['tv_post_id'] == $data['topic_last_post']) {
							$messageIcone = 'Annonce.png';
						}
						else {
							$messageIcone = 'AnnonceNonLue.png';
						}
					}
				}
				else {
					$messageIcone = 'AnnonceNonLue.png';
				}
			}
			else {
				$messageIcone = 'Annonce.png';
			}	
			
			if($data["topic_locked"] == 1) {
				$messageIcone = 'MessageLock.png';
			}
			
			echo '
			<td align="center" width="40"><img src="../Images/Forum/'.$messageIcone.'" alt="Message" /></td>
			<td style="font-size:13px; vertical-align:top;">
				<a href="voirsujet.php?t='.$data['topic_id'].'">'.stripslashes(htmlspecialchars($data['topic_titre'])).'</a><span style="font-size:12px; color:#333;"> par '.stripslashes(utf8_encode(htmlspecialchars($data['membre_pseudo_createur']))).'</span>
				<br /><span style="font-size:12px;">'.stripslashes(htmlspecialchars($data['topic_descri'])).'</span>
			</td>
			<td align="center" style="font-size:13px;">'.$data['topic_post'].'</td>
			';

		$nombreDeMessagesParPage = 10;
		$nbr_post = $data['topic_post'];
		$page = ceil($nbr_post / $nombreDeMessagesParPage);

		echo '
		<td style="font-size:13px;">
			<a href="voirsujet.php?t='.$data['topic_id'].'&amp;page='.$page.'#Post-'.$data['post_id'].'">
				<img src="../Images/Forum/PageGo.png" alt="Go" style="margin:2px 0 -2px;" /> '.stripslashes($data['membre_pseudo_last_posteur']).'
			</a><br />
			'.afficher_date_difference($data["post_time"]).'
		</td>';
		
		echo '</tr>';
	}
	
	echo '</table>';
}

$query->CloseCursor();


$query = $baseDeDonnes->prepare('SELECT forum_topic.topic_id, topic_titre, topic_createur, topic_post, topic_time, topic_last_post, topic_locked, topic_descri,
Mb.membre_pseudo AS membre_pseudo_createur, post_createur, post_time, Ma.membre_pseudo AS membre_pseudo_last_posteur, Ma.membre_id AS membre_id_last_posteur,
'.$add1.' post_id FROM forum_topic 
LEFT JOIN membres Mb ON Mb.membre_id = forum_topic.topic_createur
LEFT JOIN forum_post ON forum_topic.topic_last_post = forum_post.post_id
LEFT JOIN membres Ma ON Ma.membre_id = forum_post.post_createur
'.$add2.'
WHERE forum_topic.forum_id = :forum AND topic_genre <> "Annonce"
ORDER BY topic_last_post DESC
LIMIT :premier, :nombre');
$query->bindValue(':forum',$forum,PDO::PARAM_INT);
if(isset($ID_MEMBRE)) {
	$query->bindValue(':id',$ID_MEMBRE,PDO::PARAM_INT);
}
$query->bindValue(':forum', $forum, PDO::PARAM_INT);
$query->bindValue(':premier', (int) $premierMessageAafficher, PDO::PARAM_INT);
$query->bindValue(':nombre', (int) $nombreDeMessagesParPage, PDO::PARAM_INT);
$query->execute();

echo '<table>';

echo '
<tr>
	<th colspan="2" style="width:420px;">Sujet</th>             
	<th style="width:40px;">Messages</th>
	<th style="width:200px;">Dernier message</th>
</tr>';

if($query->rowCount() > 0) {  
	while($data = $query->fetch()) {
		echo '<tr>';
		
			if(isset($ID_MEMBRE)) {
				if($data['tv_id'] == $ID_MEMBRE) {
					if($data['tv_poste'] == '0') {
						if($data['tv_post_id'] == $data['topic_last_post']) {
							$messageIcone = 'MessageLu.png';
						}
						else {
							$messageIcone = 'MessageNonLu.png';
						}
					}
					else {
						if($data['tv_post_id'] == $data['topic_last_post']) {
							$messageIcone = 'MessageLu.png';
						}
						else {
							$messageIcone = 'MessageNonLu.png';
						}
					}
				}
				else {
					$messageIcone = 'MessageNonLu.png';
				}
			}
			else {
				$messageIcone = 'MessageLu.png';
			}	
			
			if($data["topic_locked"] == 1) {
				$messageIcone = 'MessageLock.png';
			}
			
			echo '
			<td align="center" width="40"><img src="../Images/Forum/'.$messageIcone.'" alt="Message" /></td>
			<td style="font-size:13px; vertical-align:top;">
				<a href="voirsujet.php?t='.$data['topic_id'].'">'.stripslashes(htmlspecialchars($data['topic_titre'])).'</a><span style="font-size:12px; color:#333;"> par '.stripslashes(utf8_encode(htmlspecialchars($data['membre_pseudo_createur']))).'</span>
				<br /><span style="font-size:12px;">'.stripslashes(htmlspecialchars($data['topic_descri'])).'</span>
			</td>
			<td align="center" style="font-size:13px;">'.$data['topic_post'].'</td>
			';

		$nombreDeMessagesParPage = 10;
		$nbr_post = $data['topic_post'];
		$page = ceil($nbr_post / $nombreDeMessagesParPage);

		echo '
		<td style="font-size:13px;">
			<a href="voirsujet.php?t='.$data['topic_id'].'&amp;page='.$page.'#Post-'.$data['post_id'].'">
				<img src="../Images/Forum/PageGo.png" alt="Go" style="margin:2px 0 -2px;" /> '.stripslashes($data['membre_pseudo_last_posteur']).'
			</a><br />
			'.afficher_date_difference($data["post_time"]).'
		</td>';
		
		echo '</tr>';
	}
}
else {
	echo '
	<tr>
		<td colspan="5" align="center" style="padding:7px; font-size:13px;">Ce forum ne contient actuellement aucun sujet.</td>
	</tr>';
}
	
echo '</table>';

echo '<ul class="navigationPage">';
if(isset($ID_MEMBRE)) {
	if(verifAuthForum($auth_topic))   echo '<a href="poster.php?action=nouveautopic&amp;f='.$forum.'"><li><img src="../Images/Forum/NewSujet.png" alt="" /> Sujet</li></a>';
	if(verifAuthForum($auth_annonce))   echo '<a href="poster.php?action=annonce&amp;f='.$forum.'"><li><img src="../Images/Forum/NewAnnonce.png" alt="" /> Annonce</li></a>';
}
echo '</ul>';

if($nombreDePages > 1) {
	echo '<p class="pagination" align="right">'.pagination($page, $nombreDePages, "?f=".$forum."&amp;page=%d").'</p>';
}

echo '<div style="clear:left;"></div>';

$query->CloseCursor();

include("FooterForum.php");

include("../Footer.php"); ?>