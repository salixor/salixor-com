<?php 
$titre_page_nom = "Les Dossiers";
include("../Header.php");
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Dossiers.png" alt="" /> Dossiers</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH; ?>">Accueil</a> > Dossiers</p>

<p class="encadreInfos"><span class="right">Voici une liste de dossiers sur des points divers du jeu.</span></p>

<ul class="navigationPage">
	<a href="Elevage.php"><li><img src="<?php echo ABSPATH; ?>Images/icones_pages/LittlesIcones/Dragodinde.png" alt="" /> L'élevage de Dragodindes</li></a>
	<li class="disabled"><img src="<?php echo ABSPATH; ?>Images/icones_pages/LittlesIcones/Ogrine.png" alt="" /> Les Ogrines<span></span></li>
</ul>

<?php include("../Footer.php"); ?>