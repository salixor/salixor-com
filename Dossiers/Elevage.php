<?php
$titre_page_nom = "Dossiers - L'élevage";
include("../Header.php");
 
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Dragodindes.png" alt="Dragodinde" /> Élevage de Dragodindes</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH; ?>">Accueil</a> > <a href="./">Dossiers</a> > Élevage de Dragodindes</p>

	<p class="encadreInfos">
		<span class="right">
			L'élevage de dragodindes est une des sources les plus rentables de revenus sur Dofus. Il faut cependant du temps avant de pouvoir se faire pas mal de Kamas, mais aussi bien comprendre le mécanisme de cette spécialité.<br />
			Ces montures ne peuvent pas être équipées avec un familier, mais sont plus pratiques grâce à de meilleurs bonus.<br />
			Partons à la rencontre de ce type de montures dès maintenant !
		</span>
	</p>
		
	<h3 class="troisieme_titre">Deux types de dragodindes</h3>
	
		<p class="encadreInfos">
			<span class="right">
				Sur Dofus, nous pouvons retrouver deux types de dragodindes. Voici un petit tableau récapitulatif de leurs différences.
			</span>
		</p>
			
		<table>
			<tr>
				<th width="50%">Dragodinde Sauvage</th>
				<th width="50%">Dragodinde Mature</th>
			</tr>
			<tr>
				<td>Ne peut pas devenir <span class="montable">Montable</span></td>
				<td>Peut devenir <span class="montable">Montable</span></td>
			</tr>
			<tr>
				<td>Ne peut pas devenir <span class="feconde">Féconde</span> plus d'une fois</td>
				<td>Peut devenir <span class="feconde">Féconde</span> (20 fois)</td>
			</tr>
			<tr>
				<td>Relachée une fois accouplée</td>
				<td><span class="sterile">Stérile</span> une fois accouplée 20 fois</td>
			</tr>
			<tr>
				<td>Ne donne pas de bonus</td>
				<td>Donne des bonus</td>
			</tr>
			<tr>
				<td>Ne peut pas avoir de capacité(s)</td>
				<td>Peut avoir de 0 à 2 capacités</td>
			</tr>
			<tr>
				<td>S'obtient sauvagement</td>
				<td>S'obtient suite à un accouplement</td>
			</tr>
		</table>
			
		<p class="encadreInfos">
			<span class="right">
				Sur les serveurs les plus récents, il faudra commencer par capturer deux dragodindes sauvages (ou plus selon leurs sexes) pour commencer votre élevage, à moins d'attendre et de perdre votre chance de devenir un éleveur très prisé.
			</span>
		</p>
		
	<h3 class="troisieme_titre">La Capture</h3>
	
		<p class="dossierImage">
			<img src="Elevage/kaliptus.jpg" alt="" />
		</p>
		
		<p class="encadreInfos">
			<span class="right">
					Pour capturer une dragodinde, il vous faut d'abord un niveau assez soutenu (vers le niveau 70) afin de réussir le Donjon du Koulosse. Avant de vous lancer dans le donjon, il vous faut vous procurer 50x Fleurs de Kaliptus et 50x Feuilles de Kaliptus, que vous échangerez à la fin du donjon à Oshar Marif contre le sort <b>Apprivoisement de Monture</b>.
			</span>
		</p>
		
		<p class="dossierImage">
			<img src="Elevage/sort.png" alt="" />
		</p>
		
		<p class="encadreInfos">
			<span class="right">
				Il va ensuite vous falloir acheter des <b>Filets simple de Capture</b> en [-17, 0] puis en équiper un, et veiller à ne pas avoir de dragodinde déjà équipée, sinon la capture échoue.
			</span>
		</p>
		
		<p class="dossierImage">
			<img src="Elevage/filet.png" alt="" />
		</p>
		
		<p class="encadreInfos">
			<span class="right">
				Nous pouvons passer au vif du sujet : La Capture ! Les Dragodindes se trouvent autour du Village des Éleveurs de la Montagne des Koalaks, et sont de races Amande, Rousse et Dorée (rare). Lancez le combat contre un groupe de Dragodindes pour commencer.
				<br />
				Durant le combat, vous devez d'abord vous assurer de garder une seule Dragodinde pour mieux assurer votre capture (cette dragodinde ne sera pas forcément celle que vous capturerez). Avant d'achever la pauvre bête, lancez votre sort d'Apprivoisement de Monture puis achevez la Dragodinde avant la fin de son effet.<br />
				A partir de là, avec de la chance, vous avez peut être capturé votre première Dragodinde ! Il faut maintenant la placer en enclos.
			</span>
		</p>
		
	<h3 class="troisieme_titre">Les Enclos</h3>

		<p class="encadreInfos">
			<span class="right">
				Les Enclos sont votre inventaire à Dragodindes. Vous avez 100 places pour avoir un élevage assez important. Cependant, il y a deux enclos différents. Voici un tableau comparatif :
			</span>
		</p>

			<table>
				<tr>
					<th width="50%">Enclos Public</th>
					<th width="50%">Enclos Privé (de guilde)</th>
				</tr>
				<tr>
					<td>Les objets d'élevage sont fixes</td>
					<td>Les objets d'élevage peuvent être gérés si vous avez le droit</td>
				</tr>
				<tr>
					<td>Les objets d'élevage sont peu puissants</td>
					<td>Les objets d'élevage peuvent être puissants selon votre budget</td>
				</tr>
				<tr>
					<td>Vous ne pouvez pas quitter la map sans que votre Dragodinde rentre dans l'étable</td>
					<td>Vous pouvez quitter la map et passer votre élevage en mode Passif</td>
				</tr>
				<tr>
					<td>Ne requiert pas de guilde</td>
					<td>Requiert une guilde et les droits</td>
				</tr>
			</table>
		
		<p class="encadreInfos">
			<span class="right">
				Les Enclos publics sont gratuits, mais sont bien moins efficaces que votre enclos personnel, et ils sont en plus très contraignants.<br />
				Une guilde peut acheter un enclos au bout de 2 mois et en ayant le niveau 10 minimum. Les enclos suivants peuvent êtres achetés à 1 mois et 10 niveaux d'intervalle.
			</span>
		</p>
		
		<p class="dossierImage">
			<img src="Elevage/enclos.jpg" alt="" />
		</p>
		
		<p class="encadreInfos">
			<span class="right">
				Grâce à l'interface de l'enclos, vous pouvez passer vos dragodindes à travers 4 modes :<br />
				<b>Étable : </b>Le stock de vos dragodindes, limité à 100 places.<br />
				<b>Enclos : </b>Les dragodindes dans l'enclos peuvent être élevées.<br />
				<b>Certificat : </b>Permet l'échange de vos dragodindes (valable 20 jours avant la perte de la dragodinde).<br />
				<b>Monture équipée : </b>Permet de monter une dragodinde.<br />
			</span>
		</p>
		
	<h3 class="troisieme_titre">Déplacement dans l'Enclos</h3>

		<p class="encadreInfos">
			<span class="right">
				Vos dragodindes se déplacent seules dans l'enclos. Mais parfois, il vous faut les guider pour les placer à un endroit particulier, ou tout simplement accélérer leurs activations d'objets d'élevage.<br />
				C'est alors qu'interviennent les <b>Emotes</b> :
			</span>
		</p>
		
			<table>
				<tr>
					<th></th>
					<th width="50%">Emote</th>
					<th width="50%">Effet</th>
					<th width="50%">Distance</th>
				</tr>
				<tr>
					<td><img src="../Images/Emotes/Bisous.png" alt="" /></td>
					<td>Bisou</td>
					<td>Avancée</td>
					<td>3 cases et +</td>
				</tr>
				<tr>
					<td><img src="../Images/Emotes/Applaudir.png" alt="" /></td>
					<td>Applaudir</td>
					<td>Avancée</td>
					<td>2 cases</td>
				</tr>
				<tr>
					<td><img src="../Images/Emotes/Signe.png" alt="" /></td>
					<td>Signe de la Main</td>
					<td>Avancée</td>
					<td>1 case</td>
				</tr>
				<tr>
					<td><img src="../Images/Emotes/Arme.png" alt="" /></td>
					<td>Montrer son Arme</td>
					<td>Repoussée</td>
					<td>3 cases et +</td>
				</tr>
				<tr>
					<td><img src="../Images/Emotes/Colere.png" alt="" /></td>
					<td>Se mettre en colère</td>
					<td>Repoussée</td>
					<td>2 cases</td>
				</tr>
				<tr>
					<td><img src="../Images/Emotes/Pet.png" alt="" /></td>
					<td>Lacher les Gazs</td>
					<td>Repoussée</td>
					<td>1 case</td>
				</tr>
			</table>
		
	<h3 class="troisieme_titre">Fécondité</h3>

		<p class="encadreInfos">
			<span class="right">
				Maintenant que vous connaissez les bases, il est l'heure de vous apprendre à rendre une dragodinde féconde, pour qu'elle puisse ensuite s'accoupler.<br />
				Il va falloir monter votre Dragodinde à 7500 et + en Amour et en Endurance, ainsi que la maturité au maximum. Si votre dragodinde n'est pas sauvage, il vous faudre en plus la monter au niveau 5. Ca vous paraît simple pour le moment, mais il faut prendre en compte un autre facteur : L'état de la dragodinde, sereine ou agressive.<br />
			</span>
		</p>
		
		<p class="dossierImage">
			<img src="Elevage/barreEtat.png" alt="" />
		</p>
		
		<p class="encadreInfos">
			<span class="right">
				Pour monter l'<b>Amour</b>, la barre d'état doit se trouver entre 1 et 10.000.<br />
				Pour monter la <b>Maturité</b>, la barre d'état doit se trouver entre -2000 et 2000.<br />
				Pour monter l'<b>Endurance</b>, la barre d'état doit se trouver entre -10.000 et -1.<br />
				Ainsi, dans l'exemple, la dragodinde est <b>agressive</b> et peut gagner en Maturité et Endurance.
			</span>
		</p>
		
		<p class="dossierImage">
			<img src="Elevage/dragoFeconde.png" alt="" />
		</p>
		
		<p class="encadreInfos">
			<span class="right">
				Si tous les prérequis sont remplis, votre Dragodinde doit obtenir le message "<span class="feconde">Féconde</span>".
			</span>
		</p>

	<h3 class="troisieme_titre">Accouplement</h3>

		<p class="encadreInfos">
			<span class="right">
				Nous voilà enfin à la partie finale. Une fois votre mâle et votre femelle féconds, placez les dans l'enclos, et laissez les agir.<br />
				Si votre mâle est sauvage, il sera tout de suite relaché. Quand à la femelle, si elle est sauvage, elle sera relachée après l'accouchement.<br />
				Une fois la scène passée, vous pouvez récupérer vos dragodindes.
			</span>
		</p>
		
		<p class="dossierImage">
			<img src="Elevage/dragoFecondee.png" alt="" />
		</p>
		
		<p class="encadreInfos">
			<span class="right">
				Votre dragodinde femelle est devenu "<span class="fecondee">Fécondée</span>" et en passant sur ce titre, vous connaisez la durée entre son accouplement et maintenant (utile pour savoir quand elle va accoucher).<br />
				La dragodinde accouchera selon son temps de gestation allant de 2 jours à 1 semaine. La dragodinde est toujours montable (si non sauvage) mais perd 7500 en Amour et Endurance.
			</span>
		</p>
		
	<h3 class="troisieme_titre">Accouchement</h3>
	
		<p class="encadreInfos">
			<span class="right">
				Pour que votre dragodinde accouche, il faut tout d'abord qu'elle se trouve dans l'étable. Il faut aussi que son temps de gestation soit arrivé à terme.<br />
				Une fois qu'elle accouche, un message apparait en rouge dans le Chat pour vous en avertir, avec le nombres de nouveaux nés affiché. Ce nombre va de 1 à 3, voire 4 avec une capacité spéciale.<br />
				Les bébés pourront être de la race du père, de la mère, du grand-père, de la grand-mère ou <b>bicolore</b> (Deux races mélangées). La race des bébés dépend de l'arbre généalogique des parents.<br /><br />
				
				Si vous accouplez 2 races pures (Par exemple, une dragodinde Amande et une dragodinde Rousse), vous aurez :<br />
				 - 10% de chance d'obtenir une bicolore (Ex: Amande-Rousse)<br />
				 - 45% de chance d'obtenir la même race que le père<br />
				 - 45% de chance d'obtenir la même race que la mère<br />
				Le bébé a aussi 1 chance sur 20 d'obtenir une capacité spéciale, et 1 chance sur 10 d'hériter d'une capacité spéciale venant des parents.
			</span>
		</p>
		
	<h3 class="troisieme_titre">Monter les Statistiques</h3>
	
		<p class="encadreInfos">
			<span class="right">
				Je vous ai parlé de l'Amour, de la Maturité, de l'Endurance, de la Sereinité et de l'Agressivité d'une dragodindes. Les dragodindes disposent aussi de l'Énergie.<br />
				Cependant, vous ne savez pas comment les faire progresser. C'est là qu'interviennent les <b>Objets d'élevage</b>. Voici les types d'objets disponibles :
			</span>
		</p>
		
			<table>
				<tr>
					<th width="33%">Type</th>
					<th width="33%">Effet</th>
					<th width="33%">Etat</th>
				</tr>
				<tr>
					<td>Baffeur</td>
					<td>Augmente l'Agressivité</td>
					<td>Aucun</td>
				</tr>
				<tr>
					<td>Caresseur</td>
					<td>Augmente la Sereinité</td>
					<td>Aucun</td>
				</tr>
				<tr>
					<td>Dragofesse</td>
					<td>Augmente l'Amour</td>
					<td>Sereine</td>
				</tr>
				<tr>
					<td>Foudroyeur</td>
					<td>Augmente l'Endurance</td>
					<td>Agressive</td>
				</tr>
				<tr>
					<td>Abreuvoir</td>
					<td>Augmente la Maturité</td>
					<td>Neutre</td>
				</tr>
				<tr>
					<td>Mangeoire</td>
					<td>Augmente l'Énergie</td>
					<td>Aucun</td>
				</tr>
			</table>
			
		<p class="encadreInfos">
			<span class="right">
				L'augmentation d'une caractéristiques dépend de la résistance maximale de l'objet et de la vitesse d'apprentissage de la Dragodinde.<br />
				Aussi, les mâles gagnent un peu en agressivité à chaque activation d'objet et les femelles gagnent un peu en sereinité.<br />
				L'énergie sert à pouvoir monter la dragodinde, et la maturité aussi, si la dragodinde n'est pas sauvage.
			</span>
		</p>
		
	<h3 class="troisieme_titre">Dragodinde épuisée ?</h3>
	
		<p class="encadreInfos">
			<span class="right">
				Les dragodindes deviennent comme tout le monde fatiguées au bout d'un long effort. Mais sur les dragodindes, la <b>Fatigue</b> est indiquée à l'aide d'une barre, et voici ce qu'il se passe selon la fatigue :
			</span>
		</p>
		
			<table>
				<tr>
					<th width="33%">Fatigue</th>
					<th width="67%">Influence</th>
				</tr>
				<tr>
					<td>0 à 160</td>
					<td>Bonus d'objet : 100%</td>
				</tr>
				<tr>
					<td>161 à 170</td>
					<td>Bonus d'objet : 115%</td>
				</tr>
				<tr>
					<td>171 à 180</td>
					<td>Bonus d'objet : 130%</td>
				</tr>
				<tr>
					<td>181 à 200</td>
					<td>Bonus d'objet : 150%</td>
				</tr>
				<tr>
					<td>201 à 210</td>
					<td>Bonus d'objet : 180%</td>
				</tr>
				<tr>
					<td>211 à 220</td>
					<td>Bonus d'objet : 210%</td>
				</tr>
				<tr>
					<td>221 à 230</td>
					<td>Bonus d'objet : 250%</td>
				</tr>
				<tr>
					<td>231 à 240</td>
					<td>Bonus d'objet : 300%</td>
				</tr>
				<tr>
					<td>240</td>
					<td>Votre dragodinde ne gagne plus rien</td>
				</tr>
			</table>
			
		<p class="encadreInfos">
			<span class="right">
				Exemple : Si un foudroyeur vous donne +10 lorsque votre dinde l'active, si elle a 235 de fatigue elle ne prendra pas +10 mais +30.<br />
				Pour que la fatigue redescende il faut la laisser tranquille, dans l'étable, dans l'enclos ou équipée. La fatigue redescend de 10 points par heure.
			</span>
		</p>
		
	<h3 class="troisieme_titre">Capacités Spéciales</h3>
	
		<p class="encadreInfos">
			<span class="right">
				Les dragodindes peuvent acquérir des capacités spéciales lors de leur naissance, comme nous l'avons vu dans le chapitre Accouchement. Voici toutes ces capacités :
			</span>
		</p>
		
			<table>
				<tr>
					<th width="33%">Capacité</th>
					<th width="67%">Effet</th>
				</tr>
				<tr>
					<td>Précoce</td>
					<td>Double les points gagnés en Maturité</td>
				</tr>
				<tr>
					<td>Amoureuse</td>
					<td>Double les points gagnés en Amour</td>
				</tr>
				<tr>
					<td>Endurante</td>
					<td>Double les points gagnés en Endurance</td>
				</tr>
				<tr>
					<td>Sage</td>
					<td>Double les points d'XP gagnés à la monture</td>
				</tr>
				<tr>
					<td>Reproducteur</td>
					<td>Donne 1 bébé de plus à l'accouchement</td>
				</tr>
				<tr>
					<td>Porteur</td>
					<td>Double le nombre de Pods de l'inventaire de la monture</td>
				</tr>
				<tr>
					<td>Infatigable</td>
					<td>Double les points donnés en énergie</td>
				</tr>
				<tr>
					<td>Prédisposition</td>
					<td>Double l'influence de cette dragodinde dans les croisements</td>
				</tr>
				<tr>
					<td>Caméléone</td>
					<td>La dragodinde prend vos couleurs</td>
				</tr>
			</table>
			
		<p class="encadreInfos">
			<span class="right">
				Une dragodinde peut très bien avoir deux de ces capacités.
			</span>
		</p>

	<h3 class="troisieme_titre">XP et bonus</h3>
	
		<p class="encadreInfos">
			<span class="right">
				Une dragodinde ne vous donnera pas de bonus si elle reste niveau 1 : il va vous falloir l'xp !<br />
				Chaque race de dragodinde vous confère des bonus différents, dont la quantité augmente selon le niveau.<br />
				Pour monter le niveau de la dragodinde, il faut XP et donner un pourcentage à la dragodinde, comme pour une guilde.
			</span>
		</p>
		
		<p class="dossierImage">
			<img src="Elevage/xp.png" alt="" />
		</p>
		
		<p class="encadreInfos">
			<span class="right">
				La dragodinde se sert en premier, puis la guilde et enfin vous sur votre XP de combat. Sachez qu'une dragodinde, même à 0%, gagne 20% d'XP supplémentaires calculé sur votre XP de combat mais non retiré de cette XP.
			</span>
		</p>
		
	<h3 class="troisieme_titre">Conclusion</h3>
		
		<p class="encadreInfos">
			<span class="right">
			Vous connaissez désormais les grandes bases de l'élevage.<br />
			Je vais vous donner quelques secrets pour la réussite de vos ventes : Rendez votre dragodinde montable avant de la vendre, et essayez de l'XP un maximum.<br />
			Je vous souhaite un bon élevage et à bientôt pour un autre article !
			</span>
		</p>
		
	<p class="encadreInfos">
		<span class="right">
			Article rédigé par <a href="../Membres/Profil.php?id=1">Salixor</a>. Mis à jour le <?php echo date("d ", getlastmod()); echo affiche_mois(date("F ", getlastmod())); echo date(" Y", getlastmod());  ?>.
			<span style="float:right;">Votez ! (Bientôt)</span>
		</span>
	</p>
	
<?php include("../Footer.php"); ?>