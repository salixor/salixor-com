<?php 
require_once("Includes/Fonctions.php");
require_once("Includes/Config.php");
require_once('Includes/Class/news.class.php');

if(isset($_GET["Classe"]) AND isset($_GET["Page"])) {
	if(is_numeric($_GET["Page"]) AND !empty($_GET["Classe"])) {
		$news = new AfficherNews(10, NULL, $_GET["Classe"], $_GET["Page"]);
		$titre_page_nom = "News classées sous '".$news->classe."' - Page ".$news->page;
	}
	else {
		$news = new AfficherNews(10);
		$news->hasErreur("Vide");
		$titre_page_nom = "Erreur";
	}
}
elseif(isset($_GET["Page"])) {
	if(is_numeric($_GET["Page"])) {
		$news = new AfficherNews(10, NULL, NULL, $_GET["Page"]);
		$titre_page_nom = "Les News - Page ".$news->page;
	}
	else {
		$news = new AfficherNews(10);
		$news->hasErreur("NotNumber");
		$titre_page_nom = "Erreur";
	}
}
elseif(isset($_GET["Classe"])) {
	if(!empty($_GET["Classe"])) {
		$news = new AfficherNews(10, NULL, $_GET["Classe"]);
		$titre_page_nom = "News classées sous '".stripslashes(stripslashes($news->classe))."'";
	}
	else {
		$news = new AfficherNews(10);
		$news->hasErreur("Vide");
		$titre_page_nom = "Erreur";
	}
}
elseif(isset($_GET["News"]) AND isset($_GET["Titre"])) {
	if(is_numeric($_GET["News"]) AND !empty($_GET["Titre"])) {
		$news = new AfficherNews(1, $_GET["News"], NULL);
		$titre_page_nom = $news->titreNews;
	}
	else {
		$news = new AfficherNews(10);
		$news->hasErreur("Vide");
		$titre_page_nom = "Erreur";
	}
}
else {
	$news = new AfficherNews(5);
	$titre_page_nom = "Accueil";
}

if(!empty($_POST)) {
	$_SESSION['commentaire_save'] = $_POST["commentaire"];
	
	if(!isset($PSEUDO_MEMBRE)) {
		if(!empty($_POST['userCode']) && !empty($_POST['commentaire']) && $_POST['commentaire'] != "Tapez ici votre commentaire."  && !empty($_POST['pseudo']) && !empty($_POST['id_news'])) {
			$userCode = strtoupper($_POST['userCode']);
	
			if(md5($userCode) == $_SESSION['captcha']) {
				$requete = $baseDeDonnes->prepare(
				"SELECT * FROM membres WHERE membre_pseudo = :pseudo");
				$requete->bindValue(":pseudo", $_POST["pseudo"], PDO::PARAM_STR);
				$requete->execute();
				
				if($requete->rowCount() > 0) {
					array_push($erreurs, "Un membre possède déjà ce pseudo.");
				}
				else {
					if(strlen($_POST["pseudo"]) >= 30) {
						array_push($erreurs, "Votre pseudo fait plus de 30 caractères.");
					}
					else {
						$requete = $baseDeDonnes->prepare(
						"INSERT INTO commentaires (pseudo, commentaire, id_reponse, id_membre, id_news) VALUES (:pseudo, :commentaire, :reponse, :membre, :news)");
						$requete->bindValue(":pseudo", $_POST["pseudo"], PDO::PARAM_STR);
						$requete->bindValue(":commentaire", $_POST["commentaire"], PDO::PARAM_STR);
						$requete->bindValue(":reponse", $_POST["reponse"], PDO::PARAM_INT);
						$requete->bindValue(":membre", $_POST["pseudo_membre"], PDO::PARAM_INT);
						$requete->bindValue(":news", $_POST["id_news"], PDO::PARAM_INT);
						$requete->execute();
								  
						$arrayValides[] = "Votre commentaire est enregistré";
							  
						unset($_SESSION['commentaire_save']);
					}
				}
			}
			else {
				$arrayErreurs[] = "Vous n'avez pas recopié le code correctement.";
			}
		}
		else {
			$arrayErreurs[] = "Un ou plusieurs des champs n'est ou ne sont pas rempli(s).";
		}
	}
	else {
		if(!empty($_POST['commentaire']) && $_POST['commentaire'] != "Tapez ici votre commentaire." && !empty($_POST['pseudo']) && !empty($_POST['id_news'])) {
			$requete = $baseDeDonnes->prepare(
			"INSERT INTO commentaires (pseudo, commentaire, id_reponse, id_membre, id_news) VALUES (:pseudo, :commentaire, :reponse, :membre, :news)");
			$requete->bindValue(":pseudo", $_POST["pseudo"], PDO::PARAM_STR);
			$requete->bindValue(":commentaire", $_POST["commentaire"], PDO::PARAM_STR);
			$requete->bindValue(":reponse", $_POST["reponse"], PDO::PARAM_INT);
			$requete->bindValue(":membre", $_POST["pseudo_membre"], PDO::PARAM_INT);
			$requete->bindValue(":news", $_POST["id_news"], PDO::PARAM_INT);
			$requete->execute();
			$requete->closeCursor();

			$requete = $baseDeDonnes->prepare("UPDATE membres SET membre_commentaires = membre_commentaires + 1 WHERE membre_id = :id");
			$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
			$requete->execute();
			$requete->closeCursor();
			
			$arrayValides[] = "Votre commentaire a été enregistré";
				  
			unset($_SESSION['commentaire_save']);
		}
		else {
			$arrayErreurs[] = "Un ou plusieurs des champs n'est ou ne sont pas remplis.";
		}
	}
}

include("Header.php");

echo '<div id="News"></div>';
$news->showHead();
$news->showErreurs();
$news->showNews();

if($titre_page_nom != "Accueil")           $news->showPagination();
else                                       echo '<p align="center" style="width:760px;"><a href="?Page=1" class="allNews"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/News.png" alt="" /> Toutes les news</a></p>';

if(isset($_GET["News"]) AND isset($_GET["Titre"])) {
	if(is_numeric($_GET["News"]) AND !empty($_GET["Titre"])) {
		if(isset($_GET['PageCommentaire']) AND is_numeric($_GET['PageCommentaire']))    $page = $_GET['PageCommentaire'];
		else                                                                            $page = 1;

		$pagination = 10;
		$limit_start = ($page - 1) * $pagination;

		$requete = $baseDeDonnes->prepare("SELECT COUNT(*) AS nb_total FROM commentaires WHERE id_news = :id AND id_reponse = 0");
		$requete->bindValue(":id", $_GET['News'], PDO::PARAM_INT);
		$requete->execute();
		
		$nb_total = $requete->fetch();
		$nb_total = $nb_total['nb_total'];
		$nb_pages = ceil($nb_total / $pagination);
				
		echo '<h2 class="titreSecond" id="Commentaires"><img src="'.ABSPATH.'Images/icones_pages/Commentaire.png" alt="Commentaire" /> Les Commentaires</h2>';
		
		if($nb_total == 0) {
			echo '<p class="encadreInfos"><span class="right">Aucun commentaire n\'a encore été posté. Soyez le premier !</span></p>';
		}
		else {
			echo '<div class="chatMessages commentaires">';
				afficheCommentaireFils(0, $_GET["News"], $limit_start, $pagination);
			echo '</div>';
		}
		
		if($nb_pages > 1) {
			echo '<p class="pagination" align="center" style="margin:10px 0;">' . pagination($page, $nb_pages, "".ABSPATH."?News=".$_GET["News"]."&amp;Titre=".optimiser_titre_et_classe($news->titreNews)."&amp;PageCommentaire=%d") . '</p>';
		}
		?>
		
		<h2 class="titreSecond" id="AjouterCommentaire"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Commentaire_add.png" alt="CommentaireAdd" /> Ajouter un commentaire</h2>
		
		<div class="bloc">
			<form method="post" action="#AjoutConfirmation">
				<input type="hidden" name="id_news" value="<?php echo $_GET['News']; ?>" />
				<input type="hidden" name="reponse" id="reponse" value="0" />
				
				<?php
				if(isset($ID_MEMBRE)) {
					echo '<input type="hidden" name="pseudo_membre" id="reponse" value="'.$ID_MEMBRE.'" />';
					echo '<p><label for="pseudo">Pseudo : </label><input type="hidden" maxlength="30" size="40" name="pseudo" id="pseudo" value="'.$PSEUDO_MEMBRE.'" />'.$PSEUDO_MEMBRE.'. <a href="Membres/Deconnexion.php">Se déconnecter</a>.</p>';
				}
				else {
					echo '<input type="hidden" name="pseudo_membre" id="reponse" value="" />';
					echo '<p><label for="pseudo">Pseudo : </label><input type="text" maxlength="30" size="40" name="pseudo" id="pseudo" /><a href="javascript:connexion();" style="margin:0 5px;">Se connecter</a></p>';
				}

				echo '<div id="infoReply" style="display:none;"></div>';
				
				afficher_smileys('commentaire');
				$FormatagePanel = new FormatagePanel(true, true, true, 'commentaire');
				$FormatagePanel->showFormatagePanel();
				?>

				<p><textarea name="commentaire" id="commentaire" style="height:250px;" onfocus="if(this.value=='Tapez ici votre commentaire.') this.value=''"><?php if((isset($_SESSION['commentaire_save'])) AND (!empty($_SESSION['commentaire_save']))) { echo $_SESSION['commentaire_save']; } else { echo 'Tapez ici votre commentaire.'; } ?></textarea></p>
				
				<?php if(!isset($PSEUDO_MEMBRE)) { ?>
					<p><label for="userCode">Entrez le code anti-spam : </label><input name="userCode" id="userCode" type="text" /></p>
					
					<p class="captcha">
						<img src="<?php echo ABSPATH; ?>captcha.php" alt="Captcha" /><br />
						<a href="javascript:void(0);" id="changerCaptcha">Nouvelle Image</a>
					</p>
					<div class="clear"></div>
				<?php } ?>
				
				<p align="center">
					<input type="button" id="previsualiser_bouton" value="Prévisualiser" onclick="previsualiser('commentaire','previsualiser_commentaire','AJAX_news.php');" />
					<input type="reset" value="Réinitialiser" />
					<input type="submit" value="Envoyer" name="envoyer" />
				</p>
			</form>
		</div>
						
		<p align="center" id="previsualiser_zone">
			<div class="preview" id="previsualiser_commentaire"></div>
		</p>
<?php	}
}
include("Footer.php"); ?>
