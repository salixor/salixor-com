<?php
$titres = array("404" => "Page Introuvable", "500" => "Problème Serveur");
$descriptions = array("404" => '
La page à laquelle vous avez voulu accéder n\'est pas trouvable.<br />
Si toutefois vous êtes tombés sur cette page à cause d\'un lien du site, vous pouvez contacter un administrateur pour lui reporter ce problème.<br />
Retournez à <a href="/">l\'accueil</a> pour essayez de retrouver ce que vous cherchiez.
', "500" => "fsefsefsfesfsefe");

if(isset($_GET["Erreur"])) {
	if($_GET["Erreur"] == "404") {
		$titre_page_nom = "Erreur 404";
		$erreur = "404";
	}
	elseif($_GET["Erreur"] == "500") {
		$titre_page_nom = "Erreur 500";
		$erreur = "500";
	}
	else {
		$titre_page_nom = "Erreur";
		$erreur = "0";
	}
}
include("../Header.php");

echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Erreur.png" alt="404" /> '.$titres[$erreur].'</h2>';

echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > '.$titres[$erreur].'</p>';

echo '
<div class="bloc">
	'.$descriptions[$erreur].'
</div>';

include("../Footer.php");
?>