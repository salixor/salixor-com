<?php
$titre_page_nom = "Maintenance";

//Les fichiers utiles
require_once('../Includes/Fonctions.php');
require_once('../Includes/Config.php');

//Si le site est en maintenance
if(!defined('MAINTENANCE')) {
	header("Location: ../");
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Maintenance du Site</title>

	<meta http-equiv="Content-Type" content="charset=UTF-8" />
	<meta http-equiv="Content-language" content="fr" />
	<meta name="robots" content="follow, index, all" />
	<meta name="description" content="Salixor.com, c'est une encyclopédie crée spécialement pour les Dofusiens avide d'informations sur le jeu. Retrouvez un tas de ressources sur le jeu aisi qu'une partie communautaire que vous pouvez rejoindre. Des outils sont disponibles sur le site, pour améliorer votre expérience globale du jeu." />
	<meta name="keywords" content="dofus, salixor, ankama, mmorpg, communauté, aide, astuces, wakfu, donjons, espace membre" />

	<link href="<?php echo ABSPATH; ?>Erreurs/StyleMaintenance.css" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo ABSPATH; ?>Images/Favicon.ico" />
	<link rel="icon" type="image/png" href="<?php echo ABSPATH; ?>Images/Favicon.png" />
	<link rel="apple-touch-icon-precomposed" href="<?php echo ABSPATH; ?>Images/Apple_icon-precomposed.png"/>

	<script type="text/javascript" src="<?php echo ABSPATH; ?>Scripts/jquery-1.5.1.min.js"></script>
</head>

<body>
	<?php
	echo '<div class="informations">Le site est en maintenance ! Essayez de revenir dans quelques minutes.</span></div>';
	?>
</body>

</html>