<?php
include_once('../Includes/Fonctions.php');
include_once('../Includes/Config.php');

if(isset($_POST["nbIncrease"]) AND isset($_POST["idMembre"]) AND isset($_POST["nbAffiches"]) AND isset($_POST["pageDuMembre"])) {
	$nbIncrease = $_POST["nbIncrease"];
	$idMembre = $_POST["idMembre"];
	$nbAffiches = $_POST["nbAffiches"] - 5;
	$pageDuMembre = $_POST["pageDuMembre"];
}

$requete = $baseDeDonnes->prepare("
SELECT news.*, commentaires.*, commentaires.id AS id_comm FROM commentaires
LEFT JOIN news ON news.id = commentaires.id_news
WHERE commentaires.id_membre = :id
ORDER BY commentaires.id DESC
LIMIT ".$nbAffiches.", ".$nbIncrease);
$requete->bindValue(':id', $idMembre, PDO::PARAM_INT);

$requete2 = $baseDeDonnes->prepare("
SELECT forum_post.*, forum_topic.* FROM forum_post
LEFT JOIN forum_topic ON forum_post.topic_id = forum_topic.topic_id
WHERE forum_post.post_createur = :id
ORDER BY forum_post.post_id DESC
LIMIT ".$nbAffiches.", ".$nbIncrease);
$requete2->bindValue(':id', $idMembre, PDO::PARAM_INT);

$i = 0;

$requete->execute();

echo '<ul class="navigationPage left">';
if($requete->rowCount() > 0) {
	while($data = $requete->fetch()) {
		if($i <= 4) {
			echo '
			<a href="'.ABSPATH.'?News='.$data["id_news"].'&amp;Titre='.optimiser_titre_et_classe(utf8_encode($data["titre"])).'#Commentaire'.$data["id_comm"].'">
				<li>
					'.utf8_encode(stripslashes($data["titre"]));
					if($pageDuMembre == 'true') {
						echo '
						<a href="'.ABSPATH.'Modifier_commentaire.php?id='.$data["id_comm"].'" title="Modifier le commentaire" style="float:right;">
							<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Modifier.png" />
						</a>
						<a href="'.ABSPATH.'Supprimer_commentaire.php?id='.$data["id_comm"].'" title="Supprimer le commentaire" style="float:right; margin-right:4px;">
							<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Supprimer.png" />
						</a>';
					}
				echo '</li>
			</a>';
		}
		$i++;
	}
}
echo '</ul>';

$i = 0;

$requete2->execute();

echo '<ul class="navigationPage right">';
if($requete2->rowCount() > 0) {
	while($data2 = $requete2->fetch()) {
		if($i <= 4) {
			$nombreDeMessagesParPage = 15;
			$nbPost = $data2['topic_post'];
			$page = ceil($nbPost / $nombreDeMessagesParPage);
		
			echo '
			<a href="'.ABSPATH.'Forum/voirsujet.php?t='.$data2["topic_id"].'&amp;page='.$page.'#Post-'.$data2["post_id"].'">
				<li>
					'.stripslashes($data2["topic_titre"]);
					if($pageDuMembre == 'true') {
						echo '
						<a href="'.ABSPATH.'Forum/poster.php?p='.$data2["post_id"].'&amp;action=edit" title="Modifier le post" style="float:right;">
							<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Modifier.png" />
						</a>
						<a href="'.ABSPATH.'Forum/poster.php?p='.$data2["post_id"].'&amp;action=delete" title="Supprimer le post" style="float:right; margin-right:4px;">
							<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Supprimer.png" />
						</a>';
					}
				echo '</li>
			</a>';
		}
		$i++;
	}
}
echo '</ul>';

echo '<div style="clear:left;"></div>';
?>