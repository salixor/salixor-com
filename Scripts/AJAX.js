var host = "http://localhost/Salixor/";
//var host = "http://www.salixor.com/";

/*Changement de personnage*/
var membrePersonnages = new Array();
var id;
var idMembre;
var classe;
var niveau;
var image;
var pseudo;

$(document).ready(function() {
	jQuery.fn.sort = function() {  
	   return this.pushStack( [].sort.apply( this, arguments ), []);  
	 };  
	  
	function sortAlpha(a,b){  
		return a.innerHTML < b.innerHTML ? 1 : -1;  
	};  

	$('.selectionPersonnage .abso a').not('.active').not('.on').not('.deactivate').click(function() {
		$('.selectionPersonnage .abso a').removeClass("deactivate");
		
		//On définit l'endroit de retour
		var actual = $('.selectionPersonnage .abso a').not('.active').not('.on').eq(0);
		actual.addClass("deactivate");
		
		//Variables sources
		id = $(this).attr("rel");
		idMembre = $(this).attr("rel-membre");
		classe = $(this).attr("rel-classe");
		niveau = $(this).attr("rel-niveau");
		image = $(this).attr("rel-image");
		pseudo = $(this).attr("rel-pseudo");
		
		//On place le retour sur la source
		$(this).attr({ rel : actual.attr("rel") });
		$(this).attr({ "rel-membre" : actual.attr("rel-membre") });
		$(this).attr({ "rel-classe" : actual.attr("rel-classe") });
		$(this).attr({ "rel-niveau" : actual.attr("rel-niveau") });
		$(this).attr({ "rel-image" : actual.attr("rel-image") });
		$(this).attr({ "rel-pseudo" : actual.attr("rel-pseudo") });
		
		//On affiche la source
		$(this).children('li').children('.image').children('img').attr({ src : $(this).attr("rel-image") });
		$(this).children('li').children('.pseudo').text( $(this).attr("rel-pseudo") );
		$(this).children('li').children('.little').text( $(this).attr("rel-classe") + " de niveau " + $(this).attr("rel-niveau") );

		//On place la source sur le retour
		actual.attr({rel: id});
		actual.attr({"rel-membre": idMembre});
		actual.attr({"rel-classe": classe});
		actual.attr({"rel-niveau": niveau});
		actual.attr({"rel-image": image});
		actual.attr({"rel-pseudo": pseudo});
		
		//On affiche le retour
		actual.children('li').children('.image').children('img').attr({ src : image});
		actual.children('li').children('.pseudo').text(pseudo);
		actual.children('li').children('.little').text(classe+" de niveau "+niveau);

		//On trie les personnages
		$('.selectionPersonnage .abso a').not('.active').not('.on').not('.deactivate').sort(sortAlpha).appendTo('.selectionPersonnage .abso');  
		
		//Récupération du personnage
		if(membrePersonnages[id]) {
			$('div.new-perso').html(membrePersonnages[id]);
		}
		else {
			$.ajax({
				type: "GET",
				url: host+"Membres/changementPerso.ajax.php",
				data: "id="+id+"&id_membre="+idMembre,
				success:function(msg) {
					$('div.new-perso').empty();
					$(msg).appendTo('div.new-perso');
					membrePersonnages[id] = msg;
				}
			});
		}
	});
});

/*Balise équipement*/

var showItem_liste = new Array();

function showItem(item_name, e) {
	var targ;
	
	if(!e) { var e = window.event; }
	if(e.target) { targ = e.target; }
	else if(e.srcElement) { targ = e.srcElement; }
	if(targ.nodeType == 3) { targ = targ.parentNode; }
	
	if(showItem_liste[item_name]) {
        var item = showItem_liste[item_name];
		
		positionnerItem(targ);
		traitementItem(item);
		positionnerItem(targ);
    }
	else {
		$(".itemShower").empty();
		$(".itemShower").append('<div class="contenu"><div class="loading"><span>Chargement ...</span></div></div>');
		$(".itemShower").append('<div class="bottom"></div>');
		
		positionnerItem(targ);
		requete(item_name, targ);
		positionnerItem(targ);
    }	
	
	positionnerItem(targ);

	return false;
}

function traitementItem(item) {
	$(".itemShower").empty()

	$(item).find('item').each(function() {
		var item_id_xml = $(this).attr("id");
		var item_type_xml = $(this).attr("type");
		var item_base_xml = $(this).find("type").text();
		var item_name_xml = $(this).find("name").text();
		var item_niveau_xml = $(this).find("niveau").text();
		var item_image_xml = $(this).find("image").text();
		var item_craft_xml = $(this).find("craft").text();
		var item_effets_xml = $(this).find("effets").text();
		var item_lien_xml = $(this).find("lien").text();
		var item_panoplie_xml = $(this).find("panoplie").text();
		var item_panoplie_lien_xml = $(this).find("lienPanoplie").text();
		
		$(".itemShower").append('<div class="contenu"><div class="head"><span class="titre">'+item_name_xml+'</span><span class="niveau">'+item_base_xml+' niveau '+item_niveau_xml+'</span></div><div class="image"><img src="'+item_image_xml+'" alt="" /></div><div class="craft">'+item_craft_xml+'</div><div class="effets">'+item_effets_xml+'</div><div style="clear:both;"></div><div class="foot"><a href="'+item_lien_xml+'" class="lien-fiche-item">Voir la fiche de l\'équipement</a><a href="'+item_panoplie_lien_xml+'" class="lien-panoplie-fiche-item">'+item_panoplie_xml+'</a></div></div>');
	});
	
	$(item).find('erreur').each(function() {
		var erreur = $(this).text();
		
		$(".itemShower").append('<div class="contenu"><div class="erreur"><span>'+erreur+'</span></div></div>');
	});
	
	$(".itemShower").append('<div class="bottom"></div>');
}

function positionnerItem(target) {
	var pos = $(target).offset(); 
	var height = $(target).height();  
	var heightThis = $(".itemShower").height();  

	$(".itemShower").css( { "left": pos.left + "px", "top": pos.top - heightThis + "px" } );

	showItemFiche();
}

function requete(item_name, target) {
	$.ajax({
		type: "GET",
		url: host+"Scripts/getItem.php",
		data: "item_name="+item_name,
		dataType: "xml",
		success:function(xml) {
			showItem_liste[item_name] = xml;
			traitementItem(showItem_liste[item_name]);
			positionnerItem(target);
		}
	});
}

function showItemFiche() { $(".itemShower").show(); }

function hideItem() { $(".itemShower").hide(); }

$(".receive-item-fiche").live("mouseover", function(e) { showItem($(this).attr("rel"), e); });
$(".itemShower").live("mouseover", function() { showItemFiche(); });
$(".itemShower, .receive-item-fiche").live("mouseout", function() { hideItem(); });
$('.deleteEquipement').live("mouseover", function() { hideItem(); });

/*Messagerie*/

$(document).ready(function() {
	$("#pseudoDestinataire").keyup(function() {
		var zone = document.getElementById("pseudoDestinataire");
		var searchMembre = $(this).val();
		
		if(zone.value.length <= 0) {
			$("#resultas").fadeOut(150);
		}
		if(zone.value.length > 0) {
			$.ajax({
				type: "POST",
				url: host+"Membres/searchMembre.ajax.php",
				data: "search="+searchMembre,
				success:function(msg) {
					$("#resultas").html(msg).fadeIn(150);
				}
			});
		}
	});

	$(".delete_value_champ").click(function() {
		$("#pseudoDestinataire").val("");
	});

	$("body").click(function() {
		$("#resultas").fadeOut(150);
	});

	$(".recherche-result").live("click", function() {
		$("#pseudoDestinataire").val( $(this).children("li").children(".pseudo").text() );
	});
	
	$(".read-messages").click(function() {
		$("td:contains(Non lu)").text("Lu");
		$('.read-messages-ligne').remove();
		
		$(".infoMessages").hide();
		$(this).hide();
		$('.navigationGenerale .menuMessages a.rouge').removeClass("rouge");
		$('.navigationGenerale .bouton.messages.newMessages').removeClass("newMessages");
		
		$.ajax({
			url: host+"Membres/defineRead.ajax.php"
		});
	});
});

/*Recherche d'équipements*/
$(document).ready(function() {
	$("#Nom").keyup(function() {
		var champ = document.getElementById("Nom");

		if(champ.value.length <= 3) {
			$("#results").fadeOut(150);
		}
		if(champ.value.length > 3) {
			var search = $(this).val();
			var search_type = $(this).attr("rel");
			$.ajax({
				type: "POST",
				url: host+"Equipements/Ajax_search_item.php",
				data: "search="+search+"&search_type="+search_type,
				success:function(msg) {
					$("#results").html(msg).fadeIn(150);
				}
			});
		}
	});

	$(".delete_value_champ").click(function() {
		$("#Nom").val("");
	});

	$("body").click(function() {
		$("#results").fadeOut(150);
	});

	$(".recherche-result").live("click", function() {
		$("#Nom").val( $(this).children("li").children(".pseudo").text() );
	});
});

/*Afficher Plus*/

var nbAffiches = 5;
var i = 0;
function afficherPlus(nbIncrease, total, membre, pageDuMembre) {
	nbAffiches += nbIncrease;
	
	if(nbAffiches > total + 5) {
		nbAffiches = total;
		$(".afficherPlus").remove();
	}
	else {
		$.ajax({
			type: "POST",
			url: host+"Scripts/afficherPlus.php",
			data: "nbIncrease="+nbIncrease+"&idMembre="+membre+"&nbAffiches="+nbAffiches+"&pageDuMembre="+pageDuMembre,
			success:function(msg) {
				$("#afficherPlusResultats"+i).append(msg);
			},
		});
		
		i++;
		if(i != 1) {
			$(".afficherPlus").attr({ href: "#afficherPlusResultats"+i });
			$('<div id="afficherPlusResultats'+i+'"></div>').insertBefore('p.afficherPlusParagraphe');
		}

		$(".afficherPlus").ajaxStart(function(){
			$(".afficherPlusChargement").show();
			$(".afficherPlus").hide();
		});
		$(".afficherPlus").ajaxStop(function(){
			$(".afficherPlusChargement").hide();
			$(".afficherPlus").show();
		});
	}
};

function previsualiser(champ, retour, fichier) {
	var contenu = $("#"+champ).val();
	
	if(contenu.length > 0) {
		$.post(
			fichier,
			{ contenu : contenu },
			function(data) {
				$("#"+retour).hide(0, function() {
					$(this).html(data);
				}).fadeIn(500);
			}
		);
	}
	else {
		$("#"+retour).fadeOut(200, function() {
			$(this).empty();
		});
	}
};

function AJAX_maintenance() {
	var active = $("#send").attr("name");

	if($("#send").attr("name") == "Active") {
		active = "Inactive";
		$("#send").attr({name : "Inactive" });
		$(".contenu#base").empty().append("Le mode maintenance va être <b>désactivé</b>.");
	}
	else {
		active = "Active";
		$("#send").attr({name : "Active" });
		$(".contenu#base").empty().append("Le mode maintenance va être <b>activé</b>.");
	}
	
	$.ajax({
		type: "POST",
		data: "boolean="+active,
		url: host+"Admin/Admin_maintenance.php",
		success:function(msg) {
			$(".contenu#base").hide();
			$(".contenu#callback").empty().append(msg).show();
			$("#return").empty().append("Retour en arrière ?");
			if($("#notification_maintenance").attr("src") == host+'Images/icones_pages/LittlesIcones/MaintenanceDesactive.png') {
				$("#notification_maintenance").attr({src : host+'Images/icones_pages/LittlesIcones/MaintenanceActive.png' });
				$('.texte-actuel-maintenance').html("Désactiver");
			}
			else {
				$("#notification_maintenance").attr({src : host+'Images/icones_pages/LittlesIcones/MaintenanceDesactive.png' });
				$('.texte-actuel-maintenance').html("Activer");
			}
		}
	});
};

$(document).ready(function() {
	$('.searchNews,  #rechercheNews').live("mouseover", function() {
		$("#rechercheNews").show();
		$('.searchNews').addClass("on");
	});
	
	$('.searchNews, #rechercheNews').live("mouseout", function() {
		$("#rechercheNews").hide();
		$('.searchNews').removeClass("on");
	});
});

function rechercheNews() {	
	var zone = document.getElementById("recherche_champ");
	
	if(zone.value.length <= 3) {
		$("#accueil_titre, .accueil, .news, .pagination, #News h2").fadeIn(500);
		$("#resultats").hide();
		$("#News").next("h2").html('<img src="'+host+'Images/icones_pages/News.png" alt="" /> Les News <a class="searchNews">Faire une recherche</a>');				
	}
	else {
		var recherche;
		var filtre;
		var filtre_contenu;
		
		recherche = $("#recherche_champ").val();
		filtre = $('input[type=radio][name=filtre]:checked').attr('value');
		$.cookie("filtre_moderation", $("input[type=radio][name=filtre]:checked").attr("value"), { expires: 30, path: '/', domain: 'www.salixor.com' });
		
		filtre_contenu = $('input[type=radio][name=filtre_contenu]:checked').attr('value');
		$.cookie("filtre_contenu", $("input[type=radio][name=filtre_contenu]:checked").attr("value"), { expires: 30, path: '/', domain: 'www.salixor.com' });
		
		$.ajax({
			type: "POST",
			url: host+"AJAX_search.php",
			data: "recherche="+recherche+"&filtre="+filtre+"&filtre_contenu="+filtre_contenu,
			success:function(msg) {
				$("#accueil_titre, .accueil, .news, .pagination, #News h2").hide();
				$("#resultats").html(msg).show();				
				$("#News").next("h2").html('<img src="'+host+'Images/icones_pages/Recherche.png" alt="" /> Recherche <a class="searchNews">Faire une recherche</a>');				
			}
		});
		
		$("#loading").ajaxStart(function(){ $("#loading").addClass("on"); });
		$("#loading").ajaxStop(function(){ $("#loading").removeClass("on");	});
	}
};

/*Calculateur de Dégats*/


function calculDegats(event) {
	event.preventDefault();
	
	var base1_neutre = $("#base0").val();
	var base2_neutre = $("#base0_2").val();
	
	var base1_terre = $("#base1").val();
	var base2_terre = $("#base1_2").val();
	var terre = $("#terre").val();
	
	var base1_feu = $("#base2").val();
	var base2_feu = $("#base2_2").val();
	var feu = $("#feu").val();
	
	var base1_eau = $("#base3").val();
	var base2_eau = $("#base3_2").val();
	var eau = $("#eau").val();
	
	var base1_air = $("#base4").val();
	var base2_air = $("#base4_2").val();
	var air = $("#air").val();
	
	var pourcent_dommages = $("#pourcent_dommage").val();
	var dommages_fixes = $("#dommages_fixes").val();
	
	$.ajax({
		type: "POST",
		url: host+"Aides/AJAX_calcul.php",
		data: "envoyer=true&base1(neutre)="+base1_neutre+"&base2(neutre)="+base2_neutre+"&base1(terre)="+base1_terre+"&base2(terre)="+base2_terre+"&terre="+terre+"&base1(feu)="+base1_feu+"&base2(feu)="+base2_feu+"&feu="+feu+"&base1(eau)="+base1_eau+"&base2(eau)="+base2_eau+"&eau="+eau+"&base1(air)="+base1_air+"&base2(air)="+base2_air+"&air="+air+"&pourcent_dommage="+pourcent_dommages+"&dommages_fixes="+dommages_fixes,
		success:function(msg) {
			$("#resultats").hide().html(msg).fadeIn(400);
		}
	});
};

function reset() {
	$("#recherche_champ").val("");
	tester_recherche();
};

function ajaxLoader(loader) {
	$(loader).ajaxStart(function(){
		$(this).show();
	});
	$(loader).ajaxStop(function(){
		$(this).hide();
	});
};

$(document).ready(function() {
	ajaxLoader(".loader");
});