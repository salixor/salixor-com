<?php
require("../Includes/Fonctions.php");
require("../Includes/Config.php");

header("Content-Type: text/xml; charset=UTF-8"); 

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; 

echo '<racine>';


$_GET["item_name"] = utf8_decode(addslashes($_GET["item_name"]));

$sql = '
SELECT equipements.*, panoplies.*, equipements.id AS id_equi
FROM equipements
LEFT JOIN panoplies
ON panoplies.id = equipements.panoplie
WHERE equipements.nom COLLATE utf8_unicode_ci = "'.$_GET["item_name"].'"';

$req = query($sql);

if(mysql_num_rows($req) > 0) {
	$listeTypes = array('chapeaux' => 'Chapeau', 'capes' => 'Cape', 'sacs' => 'Sac', 'anneaux' => 'Anneau', 'amulettes' => 'Amulette', 'bottes' => 'Botte', 'ceintures' => 'Ceinture', 'dofus' => 'Dofus', 'boucliers' => 'Bouclier', 'arcs' => "Arc", "baguettes" => "Baguette", "batons" => "Baton", "dagues" => "Dague", "epees" => "Epee", "marteaux" => "Marteau", "pelles" => "Pelle", "haches" => "Hache", "pioches" => "Pioche", "faux" => "Faux", "familiers" => "Familier");

	$data = mysql_fetch_assoc($req);
	$id = intval($data["id_equi"]);
	$type = $data["type"];
	$typeBase = $listeTypes[$type];
	$nom = utf8_encode(stripslashes($data["nom"]));
	$niveau = intval($data["niveau"]);
	$image = ABSPATH.lienImageEquipement(ucfirst($data["type"]), $data["nom"]);
	$craft = nl2br(changerCraft(stripslashes(utf8_encode($data['craft']))));
	$craft = preg_replace("#<br />#isU", "<![CDATA[<br />]]>", $craft);
	$effets = nl2br(colorationDommages(stripslashes(utf8_encode($data['boost']))));
	$effets = preg_replace("#<br />#isU", "<![CDATA[<br />]]>", $effets);
	$effets = preg_replace("#<span class=\"(.+)\">#isU", "<![CDATA[<span class=\"$1\">]]>", $effets);
	$effets = preg_replace("#</span>#isU", "<![CDATA[</span>]]>", $effets);
	$lien = ABSPATH.'Equipements/?Page='.$listeTypes[$data["type"]].'#Item-'.$id;
	$panoplie = utf8_encode($data["nom_panoplie"]);
	$panoplieLien = ABSPATH.'Equipements/Panoplies.php?Panoplie='.$data["id"];

	echo '
	<item id="'.$id.'" type="'.$type.'">
		<type>'.$typeBase.'</type>
		<name>'.$nom.'</name>
		<niveau>'.$niveau.'</niveau>
		<image>'.$image.'</image>
		<craft>'.$craft.'</craft>
		<effets>'.$effets.'</effets>
		<lien>'.$lien.'</lien>
		<panoplie>'.$panoplie.'</panoplie>
		<lienPanoplie>'.$panoplieLien.'</lienPanoplie>
	</item>';
}
else {
	echo '<erreur>Équipement introuvable</erreur>';
}

echo '</racine>';
?>