//Fonctions utiles
function trim(myString) {
	return myString.replace(/^\s+/g,'').replace(/\s+$/g,'')
} 

function ucfirst(str) {
	if(str.length > 0) {
		return str[0].toUpperCase() + str.substring(1);
	}
	else {
		return str;
	}
}

function in_array(array, p_val) {
    var l = array.length;
    for(var i = 0; i < l; i++) {
        if(array[i] == p_val) {
            rowid = i;
            return true;
        }
    }
    return false;
}

function afficherDifferenceTemps(timestamp) {
	if(timestamp == 0) { return "Jamais"; }
	
	timestamp = new Date(timestamp);
	var timestampTime = timestamp.getTime();
	timestampTime = Math.floor(timestampTime / 1000);
	
	var tampon = new Date();
	var tamponTime = tampon.getTime();
	tamponTime = Math.floor(tamponTime / 1000);
	
	var diff = tamponTime - timestampTime;
	
	var dateDay = timestamp.getDay();
	var tamponDay = tampon.getDay();
	var diffDay = tamponDay - dateDay;
	
	var heureTimestamp = timestamp.getHours();
	var minuteTimestamp = timestamp.getMinutes();
	var jourTimestamp = timestamp.getDate();
	var moisTimestamp = timestamp.getMonth() + 1;
	var anneeTimestamp = timestamp.getFullYear();
	
	if(diff < 60 && diffDay == 0) {
		return 'Il y a ' + diff + 's';
	}
	else if(diff < 600 && diffDay == 0) {
		return 'Il y a ' + Math.floor(diff / 60) + 'mn et ' + Math.floor(diff % 60) + 's';
	}
	else if(diff < 3600 && diffDay == 0) {
		return 'Il y a ' + Math.floor(diff / 60) + 'mn';
	}
	else if(diff < 7200 && diffDay == 0) {
		return 'Il y a ' + Math.floor(diff / 3600) + 'h et ' + Math.floor((diff % 3600) / 60) + 'mn';
	}
	else if(diff < 24 * 3600 && diffDay == 0) {
		return 'Aujourd\'hui à ' + heureTimestamp + 'h' + minuteTimestamp;
	}
	else if(diff < 48 * 3600 && diffDay == 1) {
		return 'Hier à ' + heureTimestamp + 'h' + minuteTimestamp;
	}
	else {
		return 'Le ' + jourTimestamp + '/' + moisTimestamp + '/' + anneeTimestamp + ' à ' + heureTimestamp + 'h' + minuteTimestamp;
	}
}

//Guildes
$(document).ready(function() {
	$('.guildes .listeGuildes').hide();
	$('.guildes .boutonGuilde').removeClass("on");
	
	var idGuilde;
	
	$('.guildes .boutonGuilde').click(function() {
		idGuilde = $(this).attr("rel");
		
		$('.guildes .listeGuildes#openGuilde-' + idGuilde).toggle();
		$(this).toggleClass("on");
	});
});

//Tweets
$(document).ready(function() {
	$('#TweetsReceiver h5 img').attr({"src":"http://a3.twimg.com/profile_images/1270881566/Apple_icon-precomposed_reasonably_small.png"});
	
	var tweets;
	var finalAffichage = '';
	
	$.getJSON("http://twitter.com/statuses/user_timeline/Salixor.json?count=3&callback=?", function(data){
        tweets = data;

		$(tweets).each(function(indice, donnees) {
			showTweet(donnees);
		});
		
		$('#TweetsReceiver div.listeTweets').html(finalAffichage);
	});
	
	function showTweet(donnees) {
		var tweet = convertTweet(donnees.text);
		finalAffichage += '<p class="aTweet">' + tweet + '<span class="date">' + afficherDifferenceTemps(donnees.created_at) + '</span></p>';
	}
	
    function convertTweet(tweet) {
        var regexLien = /(http:\/\/(?:.[^\s]*))/ig;
        var links = tweet.match(regexLien);
        if(regexLien.test(tweet)){
            for(var i = 0; i < links.length; i++){
                var link = '<a href="'+links[i]+'">'+links[i]+'</a>';
                tweet = tweet.replace(links[i], link);
            }
        }
		
		tweet = tweet.replace(/(^|\s)@(\w+)/g, '$1<a href="http://www.twitter.com/$2">@$2</a>');
		tweet = tweet.replace(/(^|\s)#(\w+)/g, '$1<a href="http://search.twitter.com/search?q=%23$2">#$2</a>');
		return tweet;
    }
});

//LE GOULTARMINATOR

function FitToContent(id, maxHeight) {
	var text = id && id.style ? id : document.getElementById(id);
	if(!text)    return;

	var adjustedHeight = text.clientHeight;
	if(!maxHeight || maxHeight > adjustedHeight) {
		adjustedHeight = Math.max(text.scrollHeight, adjustedHeight);
		if(maxHeight)                            adjustedHeight = Math.min(maxHeight, adjustedHeight);
		if(adjustedHeight > text.clientHeight)   text.style.height = adjustedHeight + "px";
	}
}

$(document).ready(function() {
	var maxLength = 500;
	
	$("#textarea-commentaire-goultarminator").keyup(function() {
		FitToContent(document.getElementById("textarea-commentaire-goultarminator"), 500);
		var lengthAct = document.getElementById("textarea-commentaire-goultarminator").value.length;
		var restant = maxLength - lengthAct;
		
		if(restant >= 0) {
			$('.countCommGoulta').html("Il vous reste <b>"+restant+"</b> caractères.");
		}
		if(restant < 0) {
			var restantNew = restant * (-1);
			$('.countCommGoulta').html("Vous avez <b>"+restantNew+"</b> caractères en trop.");
		}
    });
		
	$('.countCommGoulta').show();
	$('.countCommGoulta').html("Il vous reste <b>"+maxLength+"</b> caractères.");
	
	$(".goultarminatorCommentaire.send input[type=submit]").click(function(e) {
		e.preventDefault();
		
		var actualVid = $('#actual-vid').text();
		var comm = $('#textarea-commentaire-goultarminator').val();
		var membre_id = $('#MEMBRE_ID').text();
		var membre_pseudo = $('#MEMBRE_PSEUDO').text();
		var date = new Date();
		var mois = new Array('Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre ','Octobre','Novembre','Décembre');
		
		$.post(
			host+"Goultarminator/Videos.php?Video="+actualVid, { commentaire : comm },
			function(data) {
				$('<div class="goultarminatorCommentaire" id="just-posted-'+comm+'"><div class="fleche"></div><div class="commentaire">'+comm+'<div class="infos">Écrit par <a href='+host+'Membres/Profil.php?id='+membre_id+'>'+membre_pseudo+'</a> le '+date.getDate()+' '+mois[date.getMonth()]+' '+date.getFullYear()+'</div></div></div>').insertAfter('.goultarminatorCommentaire.send');
				$('#just-posted-'+comm+' *').hide();
				$('#just-posted-'+comm+' *').fadeIn(300);
				$('#textarea-commentaire-goultarminator').val("");
			}
		);
    });
	
	var startActualPageComm = $('.pagination#pagination-goulta span.current').text();
	var listePagesComms = new Array();
	
	$('.pagination#pagination-goulta a').live("click", function(e) {
		e.preventDefault();
		var actualPage = $(this).text();
		var scrollTableComm = $('.goultarminatorCommentaire.send').offset().top;
		scrollTableComm = scrollTableComm + $('.goultarminatorCommentaire.send').height();
		var link = $(this).attr("href");
		
		$('body').animate({"scrollTop":( scrollTableComm - 30 )}, 400);
		
		if(listePagesComms[actualPage]) {
			$('#receiver-page-comm').html(listePagesComms[actualPage]);
		}
		else {
			$.ajax({
				type: "GET",
				url: link,
				data: "OnlyNav",
				success:function(msg) {
					$('#receiver-page-comm').html(msg);
					listePagesComms[actualPage] = msg;
				}
			});
		}
	});
});

//Administration des Panoplies

$(document).ready(function() {
	var actualNbEquipements = $('.bloc .second-step-add-pano p[id]:last').attr("id");
	if( $('.bloc .second-step-add-pano p[id]').length > 0) {
		actualNbEquipements = trim(actualNbEquipements.replace(new RegExp("[^(0-9)]", "g"), ''));
	}

	$('.bloc .second-step-add-pano').hide();
	$('.bloc .first-step-add-pano, .bloc .second-step-add-pano').css({"position":"relative"});
	
	$('.bloc#ajouter-pano input[type=submit].next-step').live("click", function(e) {
		e.preventDefault();

		$('.bloc div').toggle();
		$(this).val("Valider").removeClass("next-step");
		
		var scrollVal = $('#ajouter-pano').offset().top;
		$('body').scrollTop(scrollVal-30);
	});
	
	$('.bloc#ajouter-pano a.allNews:not(.delete-thing)').click(function(e) {
		if($('.bloc#ajouter-pano a.allNews.delete-thing').is(":hidden")) {
			$('.bloc#ajouter-pano a.allNews.delete-thing').show();
		}
		
		actualNbEquipements++;
		
		$(this).parent("p").before('<p id="zone-'+actualNbEquipements+'equipements"><label for="'+actualNbEquipements+'equipements" class="panoplie-nb-equipement">'+actualNbEquipements+' Équipements</label><textarea name="'+actualNbEquipements+'equipements" id="'+actualNbEquipements+'equipements" style="width:500px; min-width:500px; max-width:500px; height:100px; margin-left:10px;"></textarea></p>');
			
		if(actualNbEquipements > 9) {
			$(this).animate({"marginLeft":"-1000px"}, 500, function() {
				$(this).hide(250);
			});
		}
	});
	
	$('.bloc#ajouter-pano a.allNews.delete-thing').click(function(e) {		
		actualNbEquipements--;
		
		$('.bloc .second-step-add-pano p#zone-'+ (actualNbEquipements + 1) +'equipements').remove();
			
		if(actualNbEquipements == 2) {
			$(this).hide();
		}
	});
});


//Liste des panoplies

$(document).ready(function() {
	var startActualPage = $('.pagination#pagination-pano span.current').text();
	var listePagesPanos = new Array();
	
	$('.pagination#pagination-pano a').live("click", function(e) {
		e.preventDefault();
		var actualPage = $(this).text();
		var scrollTablePano = $('#pano-table-first-line').offset().top;
		var link = $(this).attr("href");
		
		$('body').animate({"scrollTop":( scrollTablePano - 30 )}, 400);
		
		if(listePagesPanos[actualPage]) {
			$('#receiver-pano-page').html(listePagesPanos[actualPage]);
		}
		else {
			$.ajax({
				type: "GET",
				url: link,
				data: "OnlyNav",
				success:function(msg) {
					$('#receiver-pano-page').html(msg);
					listePagesPanos[actualPage] = msg;
				}
			});
		}
	});
});


//Changement de Personnage

$(document).ready(function() {
	$('.selectionPersonnage div.abso').hide();
	
	$('.selectionPersonnage a.active').click(function(e) {
		e.stopPropagation();
		$('.selectionPersonnage div.abso').toggle();
		$(this).toggleClass("on");
	});
		
	$('.selectionPersonnage div.abso').click(function(e) {
		e.stopPropagation();
		$(this).show();
	});
	
	$('body').click(function() {
		$('.selectionPersonnage a.on, .selectionPersonnage a.active').removeClass("on").removeClass("active").addClass("active");
		$('.selectionPersonnage div.abso').hide();
	});	
	
});

function showPersonnages(e) {
	e.stopPropagation();
	$('.selectionPersonnage a.on, .selectionPersonnage a.active').removeClass("on").removeClass("active").addClass("on");
	$('.selectionPersonnage div.abso').show();
}

//Sidebar

$(document).ready(function() {
	$('.sidebarCommentaires').append('<div class="boutonCaroussel vert"><ul><li class="on"><a>o</a></li><li><a>o</a></li><li><a>o</a></li><li><a>o</a></li><div class="clear"></div></ul></div>');
	$('.sidebarTweets').append('<div class="boutonCaroussel orange"><ul><li class="on"><a>o</a></li><li><a>o</a></li><li><a>o</a></li><div class="clear"></div></ul></div>');
	
	var Cpt = 0;

	$(".boutonCaroussel.vert ul li").click(function() {
		Cpt = $(this).index();
		
		if($(this).hasClass("on")) {
		}
		else {
			$(".boutonCaroussel.vert ul li").removeClass("on");
			$(this).addClass("on");

			$(".blocCommentaires").hide();
			$(".blocCommentaires:eq("+Cpt+")").fadeIn(500);
		}
	});
	
	$(".boutonCaroussel.orange ul li").click(function() {
		Cpt = $(this).index();
		
		if($(this).hasClass("on")) {
		}
		else {
			$(".boutonCaroussel.orange ul li").removeClass("on");
			$(this).addClass("on");

			$(".blocTweets").hide();
			$(".blocTweets:eq("+Cpt+")").fadeIn(500);
		}
	});
	
	$('#StickEquipements .contenustick').append('<span class="refresh">Charger un nouvel objet</span><div class="clear"></div>');
	
	$('#StickEquipements .contenustick .refresh').live("click", function() {
		$.ajax({
			url: host+"Includes/itemSidebar.php",
			success:function(msg) {
				$('#StickEquipements .contenustick .sideEqui').animate({"margin-left":"-500px"}, 500, function() {
					$(this).css({"margin-left":"500px"}).animate({"margin-left":"0"}, 500).html(msg);
				});
			}
		});
	});
});


//La "Top-Bar"

$(document).ready(function() {
	$(window).scroll(function(){
		var scrollTop = $(window).scrollTop();
		var posItem = $(".itemShower").css("top");
		var posAcutelleItem = posItem.replace("px", "");
		
		if(scrollTop < ($(".header").height()+30)) {
			$('.navigationGenerale a[href="#Haut"]').stop().animate({'opacity':'0'}, 200).css("cursor", "default");
		}
		else {
			$('.navigationGenerale a[href="#Haut"]').stop().animate({'opacity':'1'}, 200).css("cursor", "pointer");
		}
		
		if(scrollTop > ($(document).height()-$(window).height()-100)) {
			$('.navigationGenerale a[href="#Bas"]').stop().animate({'opacity':'0'}, 200).css("cursor", "default");
		}
		else {
			$('.navigationGenerale a[href="#Bas"]').stop().animate({'opacity':'1'}, 200).css("cursor", "pointer");
		}
	});
	
	//Menu du membre
	$('.deroulerMenu').click(function(e) {
		e.stopPropagation();
		$(this).toggleClass("on");
		$('.menuProfil').toggle();
		
		$('.menuStyle, .menuMessages').hide();
		$('.selectStyle, .navigationGenerale .bouton.messages').removeClass("on");
		
		if( $('.deroulerMenu').hasClass("on") || $('.selectStyle').hasClass("on") || $('.navigationGenerale .bouton.messages').hasClass("on") ) {
			$('.fullBlackNaviGenerale').show();
		}
		else {
			$('.fullBlackNaviGenerale').hide();
		}
	});
	
	$('.menuProfil').click(function(e) {
		e.stopPropagation();
		$('.deroulerMenu').addClass("on");
		$('.menuProfil, .fullBlackNaviGenerale').show();
	});
	
	//Menu du style
	$('.selectStyle').click(function(e) {
		e.stopPropagation();
		$(this).toggleClass("on");
		$('.menuStyle').toggle();
		
		$('.menuProfil, .menuMessages').hide();
		$('.deroulerMenu, .navigationGenerale .bouton.messages').removeClass("on");
		
		if( $('.deroulerMenu').hasClass("on") || $('.selectStyle').hasClass("on") || $('.navigationGenerale .bouton.messages').hasClass("on") ) {
			$('.fullBlackNaviGenerale').show();
		}
		else {
			$('.fullBlackNaviGenerale').hide();
		}
	});
	
	$('.menuStyle').click(function(e) {
		e.stopPropagation();
		$('.selectStyle').addClass("on");
		$('.menuStyle, .fullBlackNaviGenerale').show();
	});
	
	//Menu des messages
	$('.navigationGenerale .bouton.messages').click(function(e) {
		e.stopPropagation();
		$(this).toggleClass("on");
		$('.menuMessages').toggle();
		
		$('.menuProfil, .menuStyle').hide();
		$('.deroulerMenu, .selectStyle').removeClass("on");
		
		if( $('.deroulerMenu').hasClass("on") || $('.selectStyle').hasClass("on") || $('.navigationGenerale .bouton.messages').hasClass("on") ) {
			$('.fullBlackNaviGenerale').show();
		}
		else {
			$('.fullBlackNaviGenerale').hide();
		}
	});
	
	$('.menuMessages').click(function(e) {
		e.stopPropagation();
		$('.navigationGenerale .bouton.messages').addClass("on");
		$('.menuMessages, .fullBlackNaviGenerale').show();
	});
	
	var pseudoMembre = $('#MEMBRE_PSEUDO').text();
	var arrayThemes = new Array("Aucun", "Normal", "Ete", "Noel", "Ballotin", pseudoMembre);
	var imageTheme;
	
	$('.menuStyle a').not('.selected').not('.selectThemePerso').click(function() {
		var actualRel   =  $(this).attr("rel");
		var actualHtml  =  $(this).html();
		var actualActiveRel   =  $('.menuStyle a.selected').attr("rel");
		var actualActiveHtml  =  $('.menuStyle a.selected').html();
		
		$(this).attr({ rel : actualActiveRel });
		$(this).html(actualActiveHtml);
	
		$('.menuStyle a.selected').attr({ rel : actualRel });
		$('.menuStyle a.selected').html(actualHtml);
		
		if(in_array(arrayThemes, actualRel)) {
			imageTheme = host+'Images/Themes/'+actualRel+'.png';
			
			$.ajax({
				type: "POST",
				url: host+"Membres/Modifier_Profil.php?Action=Style",
				data: "change_style="+actualRel,
				success:function(msg) {
					$('.header .background').attr('style', 'background:url('+imageTheme+') no-repeat;');
					$('img#imageThemeActuelModificationTheme').attr('src', imageTheme);
					
					$('.notification').html('<img src="'+host+'Images/Notifications/Validation.jpg" />Le thème a correctement été sélectionné.').fadeIn(300).delay(2000).fadeOut(200);
					
					$('.deroulerMenu, .selectStyle, .navigationGenerale .bouton.messages').removeClass("on");
					$('.menuProfil, .menuStyle, .menuMessages, .fullBlackNaviGenerale').hide();
					
					if($("div.header div.headerContent").hasClass("ballotin") && actualRel != "Ballotin")  { $("div.header div.headerContent").removeClass("ballotin");  }
					else if($("div.header div.headerContent") && actualRel == "Ballotin")                  { $("div.header div.headerContent").addClass("ballotin");     }
				}
			});
		}
		else {
			$('.notification').html('<img src="'+host+'Images/Notifications/Erreur.jpg" />Le thème choisi n\'existe pas.').fadeIn(300).delay(2000).fadeOut(200);
		}
	});
	
	//Clic hors de ces deux menus
	$('body').click(function() {
		$('.deroulerMenu, .selectStyle, .navigationGenerale .bouton.messages').removeClass("on");
		$('.menuProfil, .menuStyle, .menuMessages, .fullBlackNaviGenerale').hide();
	});	
});

function connexion() {
	$('.deroulerMenu').addClass("on");
	$('.menuProfil, .fullBlackNaviGenerale').show();
	$('.menuProfil input[type=text]').focus();
}

$(document).ready(function() {
	$("#quickReponse").draggable({ containment: 'body', handle: '.head' });
});

function showQuickReponse(vitesse) {
	if($("#quickReponse").css("display") == "none") {
		$("#quickReponse").fadeIn(vitesse);
		$(".show-fast-reponse").empty().append('<img src="../Images/Forum/Masquer.png" alt="" /> Masquer');
		$('.full_black').fadeIn(vitesse);
	}
	else {
		closeQuickReponse(200);
	}
}

function closeQuickReponse(vitesse) {
	$("#quickReponse").fadeOut(vitesse);
	$(".show-fast-reponse").empty().append('<img src="../Images/Forum/RepondreSujet.png" alt="" /> Réponse Rapide');
	$('.full_black').fadeOut(vitesse);
}

function sendQuickReponse() {
	verifQuickReponse();
	if(verifQuickReponse() == true) {
		javascript:document.quickReponseForm.submit();
	}
	else {
		vibreQuickReponse();
	}
}

function verifQuickReponse() {
	var messageZone = document.getElementById("messageQuickReponse");
	
	if(messageZone.value.length > 10) {
		$("#informationQuickReponse").show().addClass("valide").removeClass("error").text("Valide");
		return true;
	}
	if(messageZone.value.length < 10) {
		$("#informationQuickReponse").show().addClass("error").removeClass("valide").text("Trop court");
		return false;
	}
}

function vibreQuickReponse() {
	$("#quickReponse").fadeTo(200, 0.5);
	$("#quickReponse").fadeTo(100, 1);
}

$(document).ready(function() {
	$("#menu .openMenu a").attr({title:"Afficher le sous-menu"});
	
	$("#menu .openMenu").click(function(event) {
		openMenu(event, this);
	});
});

function openMenu(event, zone) {
	if($(zone).next("ul.sousMenu:visible").length != 0) {
		$(zone).next("ul.sousMenu").slideUp(500);
		$(zone).attr({title:"Afficher le sous-menu"});
	}
	else {
		$("#menu .openMenu").next("ul").slideUp(500);
		$(zone).next("ul").slideDown(500).addClass("menuOpen");
		$("#menu .openMenu").attr({title:"Afficher le sous-menu"});
		$(zone).attr({title:"Masquer le sous-menu"});
	}

	event.preventDefault();
}

$(document).ready(function(){
	$(".fancybox, .fancybox-news, .zoomMonstre, .zoomDonjon").fancybox({
		'speedIn'		:	600, 
		'speedOut'		:	300, 
		'titlePosition' :  'over',
		'overlayOpacity':  '0.3',
		'overlayColor'  :  '#000',
		'transitionIn'  :  'elastic',
		'transitionOut' :  'elastic',
		'easingIn'      :  'swing',
		'easingOut'     :  'swing'
	});
	
	$(".fancybox-classique").fancybox({
		'speedIn'		:	600, 
		'speedOut'		:	300, 
		'overlayOpacity':  '0.3',
		'overlayColor'  :  '#000',
		'easingIn'      :  'swing',
		'easingOut'     :  'swing'
	});
});

$(document).ready(function() {
	var paras = $(".jauge .fill");
	var i = 0;
	
	$(".jauge .fill").css({"width" : "0%"});
	
	(function() {  
		$(paras[i]).animate({"width" : $(paras[i]).attr("rel")+"%" }, 300, arguments.callee);
		i++;
	})();  
});

//Validations des divers "form"
$(document).ready(function() {
	$("#contactForm").validate({
		rules: {
			"contactname": {
				required: true,
				maxlength:20,
				minlength:6
			},
			"emailContact": {
				required: true,
				"regex": /^[a-zA-Z0-9\-_]+[a-zA-Z0-9\.\-_]*@[a-zA-Z0-9\-_]+\.[a-zA-Z\.\-_]{1,}[a-zA-Z\-_]+/i,
				maxlength:255
			}
		}
	});
	
	jQuery.extend(jQuery.validator.messages, {
		required: "Champ requis",
		remote: "votre message",
		email: "votre message",
		url: "votre message",
		date: "votre message",
		dateISO: "votre message",
		number: "votre message",
		digits: "votre message",
		creditcard: "votre message",
		equalTo: "votre message",
		accept: "votre message",
		maxlength: jQuery.validator.format("Le champ fait plus de {0} caractères"),
		minlength: jQuery.validator.format("Le champ fait moins de {0} caractères"),
		rangelength: jQuery.validator.format("votre message&nbsp; entre {0} et {1} caractéres."),
		range: jQuery.validator.format("votre message&nbsp; entre {0} et {1}."),
		max: jQuery.validator.format("Trop long"),
		min: jQuery.validator.format("Trop court")
	});
	
	jQuery.validator.addMethod(
		"regex",
		function(value, element, regexp) {
			if (regexp.constructor != RegExp)
				regexp = new RegExp(regexp);
			else if (regexp.global)
				regexp.lastIndex = 0;
			return this.optional(element) || regexp.test(value);
		}, "La forme n'est pas valide"
	);
});

//SCROLL DE LA PAGE//

$(document).ready(function() {
	var scrollElement = $("html");
	var scrollValue = scrollElement.scrollTop();
	if (scrollElement.scrollTop(scrollValue + 1).scrollTop() == scrollValue) {
		scrollElement = $("body");
	}
	else {
		scrollElement.scrollTop(scrollValue);
	}
	$("a[href*=#]").click(function() {
		if(location.hostname+location.pathname+location.search == this.hostname+this.pathname.replace(/^([^\/].*)/, "/$1")+this.search) {
			var hash = "#"+$(this).attr("href").split("#")[1];
			var target = $(hash == "#" ? body : hash);
			if (target.length) {
				var id = target.attr("id");
				target.attr("id", "");
				location.hash = hash;
				target.attr("id", id);
				var to = target.offset().top - 30;
				scrollElement.animate({ scrollTop: to }, {
				duration:Math.abs($(window).scrollTop() - to) * 0.15,
				complete:function() { target.focus(); 
			}
		});
		return false;
		}
	}
	});
});

$(document).ready(function() {
	$("div#messageMonstres").append('<p class="encadreInfos"><span class="left help">Aide</span><span class="right">Appuyez sur le nom d\'un monstre pour dérouler sa fiche, et recliquez pour la remonter.</span></p>');
	
	$("p.nomMonstre").append('<div class="moreMonstre">+</div>');
	$("div.contentFicheMonstre").hide();
	
	$("div.openFicheMonstre").click(function() {
		$('div.openFicheMonstre').removeClass('on');
		$(".contentFicheMonstre").slideUp(500);
		$('div.openFicheMonstre').children(".nomMonstre").children(".moreMonstre").text("+");
		
		if($(this).next().is(':hidden') == true) {
			$(this).addClass('on');
			$(this).next().slideDown(500);
		} 
	});
	
	$('.captcha #changerCaptcha').click(function() {
		$(".captcha img").attr({ src: 'captcha.php?id='+Math.round(Math.random(0)*1000)+1 });
		$(".captcha img").hide().fadeIn(200);
	});
});

/*AJOUT DE TAGS*/

	/*PARTIE LIEN*/

	$("#lien-text-pop-up").live("keyup", function() { testerValiditeLabel(); });
	
	$("#lien-url-pop-up").live("keyup", function() { testerValiditeLien(); });
	
	function testerValiditeLabel() {
		$("#information-text-pop-up").show();
		var labelZone = document.getElementById('lien-text-pop-up');
		
		if(labelZone.value.length > 50) {
			$("#information-text-pop-up").show().addClass("error").removeClass("valide").text("Trop grand");
			pop_up_erreur();
			return false;
		}
		if(labelZone.value.length < 5 && labelZone.value.length != 0) {
			$("#information-text-pop-up").show().addClass("error").removeClass("valide").text("Trop court");
			pop_up_erreur();
			return false;
		}
		if(labelZone.value.length >= 5 && labelZone.value.length <= 50 || labelZone.value.length == 0) {
			$("#information-text-pop-up").show().addClass("valide").text("Valide");
			pop_up_normale();
			return true;
		}
	}
	
	function testerValiditeLien() {
		$("#information-url-pop-up").show();
		var URLZone = document.getElementById('lien-url-pop-up');
		var regex = new RegExp();
		regex.compile("^[A-Za-z]+://[A-Za-z0-9-_]+\\.[A-Za-z0-9-_%&\?\/.=]+$"); 
		
		if(!regex.test(URLZone.value)) {
			$("#information-url-pop-up").show().addClass("error").removeClass("valide").text("Invalide");
			pop_up_erreur();
			return false;
		}
		else {
			$("#information-url-pop-up").show().removeClass("error").addClass("valide").text("Valide");
			pop_up_normale();
			return true;
		}
	}

	$("#lien-send-pop-up").live("click", function() {
		testerValiditeLabel();
		testerValiditeLien();

		if(testerValiditeLien() == true && testerValiditeLabel() == true) {
			pop_up_normale();
			close_popup(200);
			
			var URL = $("#lien-url-pop-up").val();
			var label = $("#lien-text-pop-up").val();
			var zone = $(".pop_up").attr("rel");
			
			if(label == "") {
				debut_tag = "<lien url="+URL+">";
			}
			else {
				debut_tag = "<lien url="+URL+">"+label;
			}
			
			fin_tag = "</lien>";
			
			add_tag(debut_tag, fin_tag, zone);
			$("#lien-url-pop-up").val("");
			$("#lien-text-pop-up").val("");
			$("#information-text-pop-up").show().removeClass("error").removeClass("valide").text("Facultatif");
			$("#information-url-pop-up").show().removeClass("error").removeClass("valide").text("http://www.exemple.fr");
		}
		else {
			pop_up_vibre(200);
		}
	});
	
	/*QUOTE*/

	$("#quote-author-pop-up").live("keyup", function() { testerValiditeAuteur(); });
	
	$("#quote-text-pop-up").live("keyup", function() { testerValiditeQuote(); });
	
	function testerValiditeAuteur() {
		$("#information-author-pop-up").show();
		var authorZone = document.getElementById('quote-author-pop-up');
		
		if(authorZone.value.length >= 2 || authorZone.value.length == 0) {
			$("#information-author-pop-up").show().removeClass("error").addClass("valide").text("Valide");
			pop_up_normale();
			return true;
		}
		if(authorZone.value.length <= 2 && authorZone.value.length != 0) {
			$("#information-author-pop-up").show().addClass("error").removeClass("valide").text("Trop court");
			pop_up_erreur();
			return false;
		}
	}
	
	function testerValiditeQuote() {
		$("#information-quote-pop-up").show();
		var quoteZone = document.getElementById('quote-text-pop-up');
		
		if(quoteZone.value.length <= 4) {
			$("#information-quote-pop-up").show().addClass("error").removeClass("valide").text("Invalide");
			pop_up_erreur();
			return false;
		}
		if(quoteZone.value.length >= 4) {
			$("#information-quote-pop-up").show().removeClass("error").addClass("valide").text("Valide");
			pop_up_normale();
			return true;
		}
	}

	$("#quote-send-pop-up").live("click", function() {
		testerValiditeAuteur();
		testerValiditeQuote();

		if(testerValiditeAuteur() == true && testerValiditeQuote() == true) {
			pop_up_normale();
			close_popup(200);
			
			var auteur = $("#quote-author-pop-up").val();
			var text = $("#quote-text-pop-up").val();
			var zone = $(".pop_up").attr("rel");
			
			if(auteur == "") {
				debut_tag = "<citation>"+text;
			}
			else {
				debut_tag = "<citation auteur="+auteur+">"+text;
			}
			
			fin_tag = "</citation>";
			
			add_tag(debut_tag, fin_tag, zone);
			$("#quote-author-pop-up").val("");
			$("#quote-text-pop-up").val("");
			$("#information-author-pop-up").show().removeClass("error").removeClass("valide").text("Facultatif");
			$("#information-quote-pop-up").show().removeClass("error").removeClass("valide").text("+ de 4 caractères");
		}
		else {
			pop_up_vibre(200);
		}
	});
	
	/*IMAGES*/

	$("#image-url-pop-up").live("keyup", function() { testerValiditeImage(); });
	
	$("#image-titre-pop-up").live("keyup", function() { testerValiditeTitreImage(); });
	
	function testerValiditeImage() {
		$("#information-url-image-pop-up").show();
		var URLZone = document.getElementById('image-url-pop-up');
		var regex = new RegExp();
		regex.compile("^[A-Za-z]+://[A-Za-z0-9-_]+\\.[A-Za-z0-9-_%&\?\/.=]+$"); 
		
		if(!regex.test(URLZone.value)) {
			$("#information-url-image-pop-up").show().addClass("error").removeClass("valide").text("Invalide");
			pop_up_erreur();
			return false;
		}
		else {
			$("#information-url-image-pop-up").show().removeClass("error").addClass("valide").text("Valide");
			pop_up_normale();
			return true;
		}
	}
	
	function testerValiditeTitreImage() {
		$("#information-image-pop-up").show();
		var titreZone = document.getElementById('image-titre-pop-up');
		
		if(titreZone.value.length < 4 && titreZone.value.length != 0) {
			$("#information-image-pop-up").show().addClass("error").removeClass("valide").text("Invalide");
			pop_up_erreur();
			return false;
		}
		if(titreZone.value.length > 4 || titreZone.value.length == 0) {
			$("#information-image-pop-up").show().removeClass("error").addClass("valide").text("Valide");
			pop_up_normale();
			return true;
		}
	}

	$("#image-send-pop-up").live("click", function() {
		testerValiditeImage();
		testerValiditeTitreImage();

		if(testerValiditeImage() == true && testerValiditeTitreImage() == true) {
			pop_up_normale();
			close_popup(200);
			
			var image = $("#image-url-pop-up").val();
			var titre = $("#image-titre-pop-up").val();
			var zone = $(".pop_up").attr("rel");
			
			if(titre == "") {
				debut_tag = "<image>"+image;
			}
			else {
				debut_tag = "<image titre="+titre+">"+image;
			}
			
			fin_tag = "</image>";
			
			add_tag(debut_tag, fin_tag, zone);
			
			$("#image-url-pop-up").val("");
			$("#image-titre-pop-up").val("");
			$("#information-image-pop-up").show().removeClass("error").removeClass("valide").text("Facultatif");
			$("#information-url-image-pop-up").show().removeClass("error").removeClass("valide").text("http://www.exemple.fr");
		}
		else {
			pop_up_vibre(200);
		}
	});

function add_tag(debut_tag, fin_tag, zone, typeTag) {
	var zone = document.getElementById(zone);
	zone.focus();

	if(window.ActiveXObject) {
		var textRange = document.selection.createRange();
		var currentSelection = textRange.text;
	}
	else {
		var startSelection = zone.value.substring(0, zone.selectionStart);
		var currentSelection = zone.value.substring(zone.selectionStart, zone.selectionEnd);
		var endSelection = zone.value.substring(zone.selectionEnd);
	}
	
	if($('#lien-text-pop-up').length > 0) {
		$('#lien-text-pop-up').val(currentSelection);
		testerValiditeLabel();
	}
	
	if(typeTag) {
		switch(typeTag) {
			case "lien":
				popup(300, "pop-up-lien");
			break;
			
			case "citation":
				popup(300, "pop-up-quote");
			break;
			
			case "image":
				popup(300, "pop-up-image");
			break;

			case "liste":
				fin_tag = "</liste>";
				
				var texte = '';
				
				while(tmp = prompt('Saisissez le contenu d\'une puce (Pour arrêter, cliquez sur Annuler)')) {
					if(tmp != "") {
						texte += '<puce>'+tmp+'</puce>';
						debut_tag = "<liste>"+texte;
					}
				}
			break;
		}
    }

	if(window.ActiveXObject) {
		textRange.text = debut_tag + currentSelection + fin_tag;
		textRange.moveStart('character', -fin_tag.length-currentSelection.length);
		textRange.moveEnd('character', -fin_tag.length);
		textRange.select();
	}
	else {
		zone.value = startSelection + debut_tag + currentSelection + fin_tag + endSelection;
		zone.focus();
		zone.setSelectionRange(startSelection.length + debut_tag.length, startSelection.length + debut_tag.length + currentSelection.length);
	} 
};

$(document).ready(function() {
	$(".image_preview").toggle(function() {
			$(this).parent(".equipement_membre").children(".effets").slideDown(500);
			$(this).addClass("on");
			$(this).parent(".equipement_membre").addClass("on");
			$(this).attr({title:"Masquer les effets"});
		}, function() {
			$(this).parent(".equipement_membre").children(".effets").slideUp(300);
			$(this).removeClass("on");
			$(this).parent(".equipement_membre").removeClass("on");
			$(this).attr({title:"Voir les effets"});
	});

	//REPONSE A UN COMMENTAIRE//
	
	$(".buttons.buttonReply").click(function() {
		var id = $(this).attr("rel");
		var pseudo = $(this).attr("rel_pseudo");
		
		$("#reponse").val(id);
		
		$("#infoReply").html('<p><label>En réponse à : </label>'+pseudo+'. <a href="javascript:void(0);" onclick="resetReply();">Annuler</a>.</p>').show();
	});
});

function resetReply() {
	$("#reponse").val("0");
	$("#infoReply").hide();
};

function popup(vitesse, id) {
	if(id) {
		$(".full_black").fadeIn(vitesse);
		$(".pop_up#"+id).show();
		$(".pop_up#"+id).animate({top: "30px"}, 500 );
	}
	else {
		$(".full_black").fadeIn(vitesse);
		$(".pop_up").show();
		$(".pop_up").animate({top: "30px"}, 500 );
	}
};

function pop_up_vibre(vitesse) {
	$(".pop_up").fadeTo(vitesse, 0.5);
	$(".pop_up").fadeTo(vitesse*2, 1);
	pop_up_erreur();
};

function pop_up_erreur() {
	$(".pop_up").css({"box-shadow" : "rgba(204, 0, 0, 0.5) 0px 0px 10px 0px"});
};

function pop_up_normale() {
	$(".pop_up").css({"box-shadow" : "#333 0px 0px 10px 0px"});
};

function close_popup(vitesse) {
	$(".full_black").fadeOut(vitesse);
	$(".pop_up").animate({top: "-1000px"}, 500 );
	$(".pop_up").hide(1);
	$(".contenu#base").show();
	$(".contenu#callback").empty().hide();
};

//Analytics

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-18203478-1']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();