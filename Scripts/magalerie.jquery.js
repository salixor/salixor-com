$(document).ready(function() {
	var actualImage;
	var listeImages = new Array();
	var groupeImages = new Array();
	var firstImage = 0;
	var lastImage = $('a.maGalerieLink').length - 1;
	var i = 0;
	
	$('a.maGalerieLink').each(function() {
		groupeImages[i] = $(this).attr("href");
		i++;
	});
	
	if($('a.maGalerieLink').length <= 1) {
		$('.maGalerie .next').remove();
		$('.maGalerie .prev').remove();
		$('.maGalerie .close').css({"right":"15px"});
	}

	$('.maGalerie .close').live("click", function() {
		$('.full_black, .maGalerie').fadeOut(300);
		$('body').css({"overflow":"auto"});
	});

	$('a.maGalerieLink').click(function(e) {
		var scrollVal = $('#conteneur').offset().top;

		if($('body').scrollTop() < scrollVal) {
			$('body').animate({ "scrollTop" : scrollVal+"px" }, 400, function() {
				$('.full_black, .maGalerie').fadeIn(300);
			});
		}
		else {
			$('.full_black, .maGalerie').fadeIn(300);
		}
	
		$('body').css({"overflow":"hidden"});
		e.preventDefault();
		actualImage = $('a.maGalerieLink').index( $(this)[0] );
		
		recupImage();

		$(window).keydown(function(event){
			if(event.keyCode == '39') {
				actualImage++;
				if(actualImage > lastImage) {
					actualImage = firstImage;
				}
			
				recupImage();
			}
			if(event.keyCode == '37'){
				actualImage--;
				if(actualImage < firstImage) {
					actualImage = lastImage;
				}
			
				recupImage();
			}
		});

		$('.maGalerie .prev').click(function() {
			actualImage--;
			if(actualImage < firstImage) {
				actualImage = lastImage;
			}
		
			recupImage();
		});

		$('.maGalerie .next').click(function() {
			actualImage++;
			if(actualImage > lastImage) {
				actualImage = firstImage;
			}
		
			recupImage();
		});
	});

	function recupImage() {
		if(listeImages[actualImage]) {
			$('.maGalerie .bigImage').html(listeImages[actualImage]);
		}
		else {
			$('.maGalerie .bigImage').html('<div><img src="'+groupeImages[actualImage]+'" alt="" /></div>');
			listeImages[actualImage] = $('.maGalerie .bigImage').html();
		}
	}
});