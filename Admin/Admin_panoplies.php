<?php 
$titre_page_nom = "Administration des Panoplies";
include("../Header.php");

if(isset($ID_MEMBRE)) {
	if($NIVEAU_MEMBRE == 5) {
		if(isset($_GET['Action'])) {
			switch($_GET['Action']) {
				case "Ajouter":
					echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Ajout d\'une Panoplie</h2>';
					
					echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > <a href="Admin_panoplies.php">Administration des Panoplies</a> > Ajout</p>';
				
					if(isset($_POST['submit'])) {
						if(!is_numeric($_POST['niveau']) OR empty($_POST['nom']) OR empty($_POST['craft']) OR empty($_POST['boost']) OR empty($_POST['2equipements'])) {
							echo '<p class="encadreInfos">';
								echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
								echo '<span class="right">Un ou plusieurs des champs n\'ont pas été remplis.</span>';
							echo '</p>';
						}
						else {
							$bonusParEquipements = "";
							
							for($i = 2; $i <= 10; $i++) {
								if(isset($_POST[$i."equipements"])) {
									$bonusParEquipements .= $i." Équipements;".$_POST[$i."equipements"].";";
								}
							}
							
							$bonusParEquipements = substr($bonusParEquipements, 0, -1);
							
							$requete = $baseDeDonnes->prepare("
							INSERT INTO panoplies(niveau_panoplie, nom_panoplie, recette_complete, bonus_complets, bonus_items) VALUES (:niveau, :nom, :craft, :boost, :items)");
							$requete->bindValue(":niveau", $_POST["niveau"], PDO::PARAM_INT);
							$requete->bindValue(":nom", utf8_decode($_POST["nom"]), PDO::PARAM_STR);
							$requete->bindValue(":craft", utf8_decode($_POST["craft"]), PDO::PARAM_STR);
							$requete->bindValue(":boost", utf8_decode($_POST["boost"]), PDO::PARAM_STR);
							$requete->bindValue(":items", utf8_decode($bonusParEquipements), PDO::PARAM_STR);
							$requete->execute();

							echo '<p class="encadreInfos">';
								echo '<span class="left valide"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Valide.png" alt="Validation" /> Confirmation</span>';
								echo '<span class="right">La panoplie a bien été ajoutée.</span>';
							echo '</p>';
						}
					}
					else { ?>
						<div class="bloc" id="ajouter-pano">
							<form method="post" action="Admin_panoplies.php?Action=Ajouter">
								<div class="first-step-add-pano move-pano">
									<p>
										<label for="niveau">Niveau : </label>
										<input type="text" name="niveau" id="niveau" style="width:150px;" />
									</p>
									<p>
										<label for="nom">Nom : </label>
										<input type="text" name="nom" id="nom" style="width:250px;" />
									</p>
									<p>
										<label for="craft">Craft complet : </label>
										<textarea name="craft" id="craft" style="width:500px; min-width:500px; max-width:500px; height:200px; margin-left:0;"></textarea>
									</p>
									<p>
										<label for="boost">Bonus complet : </label>
										<textarea name="boost" id="boost" style="width:500px; min-width:500px; max-width:500px; height:200px; margin-left:0;"></textarea>
									</p>
								</div>
								<div class="second-step-add-pano">
									<p id="zone-2equipements">
										<label for="2equipements" class="panoplie-nb-equipement">2 Équipements</label>
										<textarea name="2equipements" id="2equipements" style="width:500px; min-width:500px; max-width:500px; height:100px; margin-left:10px;"></textarea>
									</p>
									<p align="center" style="width:746px;">
										<a class="allNews" href="javascript:void(0);"><img src="<?php echo ABSPATH ?>Images/icones_pages/LittlesIcones/Ajouter.png" alt="" /> Rajouter</a>
										<a class="allNews delete-thing" href="javascript:void(0);" style="display:none;"><img src="<?php echo ABSPATH ?>Images/icones_pages/LittlesIcones/Supprimer.png" alt="" /> Supprimer</a>
									</p>
								</div>
								
								<div style="clear:left;"></div>
								
								<p align="center">
									<input type="submit" name="submit" value="Suite" class="next-step" />
								</p>
							</form>
						</div>
					<?php
					}
				break;

				case "Gerer":
					if(isset($_GET['Page']) && is_numeric($_GET['Page']))    $page = $_GET['Page'];
					else                                                     $page = 1;

					$pagination = 25;
					$limit_start = ($page - 1) * $pagination;

					$requete = $baseDeDonnes->prepare("SELECT COUNT(*) AS nb_total FROM panoplies");
					$requete->execute();
					
					$nb_total = $requete->fetch();
					$nb_total = $nb_total['nb_total'];
					$nb_pages = ceil($nb_total / $pagination);
										
					$requete = $baseDeDonnes->prepare(
					'SELECT equipements.*, panoplies.*
					FROM equipements
					LEFT JOIN panoplies
					ON panoplies.id = equipements.panoplie
					WHERE equipements.panoplie != 0 AND equipements.equipement_ajoute = "2"
					GROUP BY panoplies.id
					ORDER BY panoplies.niveau_panoplie, panoplies.id ASC
					LIMIT '.$limit_start.', '.$pagination);
					$requete->execute();

					echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Gestion des Panoplies</h2>';
					
					echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > <a href="Admin_panoplies.php">Administration des Panoplies</a> > Gestion</p>';
					
					echo '
					<table>
						<tr>
							<th></th>
							<th width="560">Nom de la Panoplie</th>
							<th width="200">Niveau</th>
							<th>Modifier</th>
							<th>Supprimer</th>
						</tr>';
						
					while($data = $requete->fetch()) {
						$image = lienImageEquipement(ucfirst($data["type"]), $data['nom']);
						
						if(!file_exists('../'.$image))     $image = 'Images/Items/Unknown.png';

						echo '
						<tr>
							<td><img src="'.ABSPATH.$image.'" alt="" height="30" style="background:#FFF" /></td>
							<td><a href="'.ABSPATH.'Equipements/Panoplies.php?Panoplie='.$data["id"].'">'.utf8_encode(stripslashes($data["nom_panoplie"])).'</a></td>
							<td align="center">'.$data["niveau_panoplie"].'</td>
							<td align="center">
								<a href="Admin_panoplies.php?Action=Modifier&amp;id='.$data['id'].'">
									Modifier
								</a>
							</td>
							<td align="center">
								<a href="Admin_panoplies.php?Action=Supprimer&amp;id='.$data['id'].'">
									Supprimer
								</a>
							</td>
						</tr>';
					}
					
					echo '</table>';

					echo '<p class="pagination" align="center" style="margin:15px 0 0;">'.pagination($page, $nb_pages, "?Action=Gerer&amp;Page=%d").'</p>';
				break;

				case "Modifier":
					echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Modification d\'une Panoplie</h2>';
					
					echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > <a href="Admin_panoplies.php">Administration des Panoplies</a> > Modification</p>';
				
					if(isset($_GET['id']) AND is_numeric($_GET['id'])) {
						$id = $_GET['id'];
						
						if(isset($_POST['submit'])) {
							if(!is_numeric($_POST['niveau']) OR empty($_POST['nom']) OR empty($_POST['craft']) OR empty($_POST['boost']) OR empty($_POST['2equipements'])) {
								echo '<p class="encadreInfos">';
									echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
									echo '<span class="right">Un ou plusieurs des champs n\'ont pas été remplis.</span>';
								echo '</p>';
							}
							else {
								$bonusParEquipements = "";
								
								for($i = 2; $i <= 10; $i++) {
									if(isset($_POST[$i."equipements"])) {
										$bonusParEquipements .= $i." Équipements;".$_POST[$i."equipements"].";";
									}
								}
								
								$bonusParEquipements = substr($bonusParEquipements, 0, -1);
								
								$requete = $baseDeDonnes->prepare("
								UPDATE panoplies SET niveau_panoplie = :niveau, nom_panoplie = :nom, recette_complete = :craft, bonus_complets = :boost, bonus_items = :items
								WHERE id = :id");
								$requete->bindValue(":niveau", $_POST["niveau"], PDO::PARAM_INT);
								$requete->bindValue(":nom", utf8_decode($_POST["nom"]), PDO::PARAM_STR);
								$requete->bindValue(":craft", utf8_decode($_POST["craft"]), PDO::PARAM_STR);
								$requete->bindValue(":boost", utf8_decode($_POST["boost"]), PDO::PARAM_STR);
								$requete->bindValue(":items", utf8_decode($bonusParEquipements), PDO::PARAM_STR);
								$requete->bindValue(":id", $id, PDO::PARAM_INT);
								$requete->execute();

								echo '<p class="encadreInfos">';
									echo '<span class="left valide"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Valide.png" alt="Validation" /> Confirmation</span>';
									echo '<span class="right">La panoplie a bien été modifiée.</span>';
								echo '</p>';
							}
						}
						else {
							$requete = $baseDeDonnes->prepare("SELECT * FROM panoplies WHERE id = :id");
							$requete->bindValue(":id", $id, PDO::PARAM_INT);
							$requete->execute();
							
							if($requete->rowCount() > 0) {
								$data = $requete->fetch(); ?>
								
								<div class="bloc" id="ajouter-pano">
									<form method="post" action="Admin_panoplies.php?Action=Modifier&amp;id=<?php echo $id ?>">
										<div class="first-step-add-pano move-pano">
											<p>
												<label for="niveau">Niveau : </label>
												<input type="text" name="niveau" id="niveau" style="width:150px;" value="<?php echo $data["niveau_panoplie"]; ?>" />
											</p>
											<p>
												<label for="nom">Nom : </label>
												<input type="text" name="nom" id="nom" style="width:250px;" value="<?php echo stripslashes(utf8_encode($data["nom_panoplie"])); ?>" />
											</p>
											<p>
												<label for="craft">Craft complet : </label>
												<textarea name="craft" id="craft" style="width:500px; min-width:500px; max-width:500px; height:200px; margin-left:0;"><?php echo stripslashes(utf8_encode($data["recette_complete"])); ?></textarea>
											</p>
											<p>
												<label for="boost">Bonus complet : </label>
												<textarea name="boost" id="boost" style="width:500px; min-width:500px; max-width:500px; height:200px; margin-left:0;"><?php echo stripslashes(utf8_encode($data["bonus_complets"])); ?></textarea>
											</p>
										</div>
										
										<div class="second-step-add-pano">
											<?php
											$a = 2;
											$i = 0;
											$j = 1;
											$listeBonus = explode(";", stripslashes(utf8_encode($data["bonus_items"])));
											
											while($j < count($listeBonus)) {	
												echo '<p id="zone-'.$a.'equipements">';
													echo '<label for="'.$a.'equipements" class="panoplie-nb-equipement">'.$listeBonus[$i].'</label>';
													echo '<textarea name="'.$a.'equipements" id="'.$a.'equipements" style="width:500px; min-width:500px; max-width:500px; height:100px; margin-left:10px;">'.$listeBonus[$j].'</textarea>';
													echo '<div style="clear:left;"></div>';
												echo '</p>';
												
												$a++;
												$i += 2;
												$j += 2;
											}
											?>
											
											<p align="center" style="width:746px;">
												<a class="allNews" href="javascript:void(0);"><img src="<?php echo ABSPATH ?>Images/icones_pages/LittlesIcones/Ajouter.png" alt="" /> Rajouter</a>
												<a class="allNews delete-thing" href="javascript:void(0);"><img src="<?php echo ABSPATH ?>Images/icones_pages/LittlesIcones/Supprimer.png" alt="" /> Supprimer</a>
											</p>
										</div>
										
										<div style="clear:left;"></div>
										
										<p align="center">
											<input type="submit" name="submit" value="Suite" class="next-step" />
										</p>
									</form>
								</div>
							<?php }
							else {
								echo '<p class="encadreInfos">';
									echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
									echo '<span class="right">Cette panoplie n\'existe pas ou plus.</span>';
								echo '</p>';
							}
						}
					}
					else {
						echo '<p class="encadreInfos">';
							echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
							echo '<span class="right">Aucune panoplie n\'est définie.</span>';
						echo '</p>';
					}
				break;

				case "Supprimer":
					echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Suppression d\'une Panoplie</h2>';
					
					echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > <a href="Admin_panoplies.php">Administration des Panoplies</a> > Suppression</p>';
					
					if(isset($_GET['id']) AND is_numeric($_GET['id'])) {
						$id = $_GET['id'];
														
						$requete = $baseDeDonnes->prepare("SELECT * FROM panoplies WHERE id = :id");
						$requete->bindValue(":id", $id, PDO::PARAM_INT);
						$requete->execute();

						if($requete->rowCount() == 0) {
							echo '<p class="encadreInfos">';
								echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
								echo '<span class="right">Cette panoplie n\'existe pas ou plus.</span>';
							echo '</p>';
						}
						else {
							if(isset($_GET["Ok"])) {
								$requete = $baseDeDonnes->prepare("DELETE FROM panoplies WHERE id = :id");
								$requete->bindValue(":id", $id, PDO::PARAM_INT);
								$requete->execute();
								
								$requete = $baseDeDonnes->prepare("UPDATE equipements SET panoplie = 0 WHERE panoplie = :id");
								$requete->bindValue(":id", $id, PDO::PARAM_INT);
								$requete->execute();
								
								echo '<p class="encadreInfos">';
									echo '<span class="left valide"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Valide.png" alt="Validation" /> Confirmation</span>';
									echo '<span class="right">La panoplie a bien été supprimée. Les équipements qui y étaient liés ont été déliés.</span>';
								echo '</p>';
							}
							else {									
								echo '
								<p class="ForumSend">
									<span class="titreSend"><img src="../Images/Forum/Erreur.png" /> Attention</span>
									<span class="contenuSend">
										<span id="a">
											Vous allez supprimer une panoplie. Seules les données de cette panoplie sont supprimées, les équipements qui y sont liés seront juste déliés.<br />
											Cette action est irréversible. Voulez-vous vraiment continuer ?
										</span>
										<span id="b" align="center"><a href="?Action=Supprimer&amp;id='.$id.'&amp;Ok" class="choice">Oui</a><a href="?Action=Gerer" class="choice">Non</a></span>
									</span>
								</p>';
							}
						}
					}
					else {
						echo '<p class="encadreInfos">';
							echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
							echo '<span class="right">Aucune panoplie n\'est définie.</span>';
						echo '</p>';
					}
				break;

				default:
					echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Administration des Panoplies</h2>';
							
					echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > <a href="Admin_panoplies.php">Administration des Panoplies</a></p>';
					
					echo '<p class="encadreInfos">';
						echo '<span class="left information"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Information.png" alt="Information" /> Information</span>';
						echo '<span class="right">Cette page n\'existe pas.</span>';
					echo '</p>';
				break;
			}
		}
		else {
			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Administration des Panoplies</h2>';
					
			echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > Administration des Panoplies</p>';
			
			echo'
			<ul class="navigationPage">
				<a href="Admin_panoplies.php?Action=Ajouter"><li><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Ajouter.png" alt="" /> Ajouter une Panoplie</li></a>
				<a href="Admin_panoplies.php?Action=Gerer"><li><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Gerer.png" alt="" /> Gérer les Panoplies</li></a>
			</ul>';
		}
	}
	else {
		echo '<p class="encadreInfos"><span class="left">Information</span><span class="right">Vous n\'avez pas l\'autorisation de voir cette page.</span></p>';
	}
}
else {
	echo '<p class="encadreInfos"><span class="left">Information</span><span class="right">Vous n\'avez pas l\'autorisation de voir cette page.</span></p>';
}

include('../Footer.php');
?>