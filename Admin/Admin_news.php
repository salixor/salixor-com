<?php
$titre_page_nom = "Administration des News";
include("../Header.php");

if(isset($ID_MEMBRE)) {
	if($NIVEAU_MEMBRE == 5) {
		if(isset($_GET['Action'])) {
			if($_GET['Action'] == "Creer" OR $_GET['Action'] == "Modifier" OR $_GET['Action'] == "Supprimer" OR $_GET['Action'] == "Gerer") {
				$Action = $_GET['Action'];
			 
				switch($Action) {
					case "Creer":
						echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Création d\'une News</h2>';
						
						echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > <a href="Admin_news.php">Administration des News</a> > Création</p>';
					
						if(!empty($_POST['titre']) AND !empty($_POST['contenu']) AND !empty($_POST['classe'])) {
							$ListeExtension = array('jpg' => 'image/jpeg', 'jpeg' => 'image/jpeg', 'png' => 'image/png', 'gif' => 'image/gif');
							$ListeExtensionIE = array('jpg' => 'image/pjpg', 'jpeg'=>'image/pjpeg');
							$NouvelleLargeur = 220;
							
							if(!empty($_FILES['ImageNews'])) {
								if($_FILES['ImageNews']['error'] <= 0) {
									if($_FILES['ImageNews']['size'] <= $_POST["MAX_FILE_SIZE"]) {
										$ImageNews = $_FILES['ImageNews']['name'];
										
										$ExtensionPresumee = explode('.', $ImageNews);
										$ExtensionPresumee = strtolower($ExtensionPresumee[count($ExtensionPresumee)-1]);
										if($ExtensionPresumee == 'jpg' || $ExtensionPresumee == 'jpeg' || $ExtensionPresumee == 'pjpg' || $ExtensionPresumee == 'pjpeg' || $ExtensionPresumee == 'gif' || $ExtensionPresumee == 'png') {
											$ImageNews = getimagesize($_FILES['ImageNews']['tmp_name']);
											
											if($ImageNews['mime'] == $ListeExtension[$ExtensionPresumee]  || $ImageNews['mime'] == $ListeExtensionIE[$ExtensionPresumee]) {
												if($ExtensionPresumee == 'jpg' || $ExtensionPresumee == 'jpeg' || $ExtensionPresumee == 'pjpg') {
													$ImageChoisie = imagecreatefromjpeg($_FILES['ImageNews']['tmp_name']);
												}
												if($ExtensionPresumee == 'gif')   $ImageChoisie = imagecreatefromgif($_FILES['ImageNews']['tmp_name']);
												if($ExtensionPresumee == 'png')   $ImageChoisie = imagecreatefrompng($_FILES['ImageNews']['tmp_name']);

												$TailleImageChoisie = getimagesize($_FILES['ImageNews']['tmp_name']);
												
												$Reduction = ( ($NouvelleLargeur * 100)/$TailleImageChoisie[0] );
												$NouvelleHauteur = ( ($TailleImageChoisie[1] * $Reduction)/100 );
												
												if($NouvelleHauteur < 150) {
													$NouvelleHauteur = 150;
													$Reduction = ( ($NouvelleHauteur * 100)/$TailleImageChoisie[1] );
													$NouvelleLargeur = ( ($TailleImageChoisie[0] * $Reduction)/100 );
												}
												
												$NouvelleImage = imagecreatetruecolor($NouvelleLargeur , $NouvelleHauteur) or die ("Erreur");
												imagecopyresampled($NouvelleImage, $ImageChoisie, 0, 0, 0, 0, $NouvelleLargeur, $NouvelleHauteur, $TailleImageChoisie[0], $TailleImageChoisie[1]);
												
												$NouvelleImageBis = imagecreatetruecolor(220, 150) or die ("Erreur");
												imagecopy($NouvelleImageBis, $NouvelleImage, 0, 0, 0, 0, 220, 150);
												
												$ImageNews = $_FILES['ImageNews']['name'];
												$NomImageChoisie = explode('.', $ImageNews);
												$NomImageExploitable = time();
												$LienBig = '../Images/News/'.$NomImageExploitable.'-big.'.$ExtensionPresumee;
												$LienMini = '../Images/News/'.$NomImageExploitable.'-mini.'.$ExtensionPresumee;
												$LienBigClear = 'Images/News/'.$NomImageExploitable.'-big.'.$ExtensionPresumee;
												$LienMiniClear = 'Images/News/'.$NomImageExploitable.'-mini.'.$ExtensionPresumee;
												imagejpeg($NouvelleImageBis, $LienMini, 100);
												imagejpeg($ImageChoisie, $LienBig, 100);
												
												imagedestroy($ImageChoisie);
											}
										}
									}
								}
							}
							
							$requete = $baseDeDonnes->prepare('
							INSERT INTO news(titre, contenu, classe, images) VALUES (:titre, :contenu, :classe, :images)');
							$requete->bindValue(":titre", utf8_decode($_POST["titre"]), PDO::PARAM_STR);
							$requete->bindValue(":contenu", $_POST["contenu"], PDO::PARAM_STR);
							$requete->bindValue(":classe", $_POST["classe"], PDO::PARAM_STR);
							$requete->bindValue(":images", $LienMiniClear.';'.$LienBigClear, PDO::PARAM_STR);
							$requete->execute();
							
							echo '<p class="encadreInfos">';
								echo '<span class="left valide"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Valide.png" alt="Validation" /> Confirmation</span>';
								echo '<span class="right">La news a bien été créée.</span>';
							echo '</p>';
							
							$fp = fopen("CreerRSS.php", 'r+');
							fclose($fp);
						}
						else { ?>
							<div class="bloc">
								<form method="post" action="Admin_news.php?Action=Creer" enctype="multipart/form-data">
								
								<p><label for="titre">Titre : </label><input type="text" id="titre" name="titre" style="width:300px;" /></p>
								<p><label for="classe">Classe : </label><input type="text" id="classe" name="classe" style="width:300px;" /></p>
								
								<?php 
								afficher_smileys('contenu');
								$FormatagePanel = new FormatagePanel(true, true, true, 'contenu');
								$FormatagePanel->showFormatagePanel();
								?>
								
								<p><textarea name="contenu" style="width:700px; height:300px;" id="contenu"></textarea></p>
								
								<p>
									<label for="image">Image : </label>
									<input type="hidden" name="MAX_FILE_SIZE" value="2097152" />
									<input type="file" name="ImageNews" id="image" />
								</p>
								
								<p align="center">
									<input type="button" id="previsualiser_bouton" value="Prévisualiser" onclick="previsualiser('contenu', 'previsualiser_news', '../AJAX_news.php');" />
									<input type="submit" value="Créer"/>
								</p>
								
								</form>
							</div>
							
							<p align="center" id="previsualiser_zone">
								<div class="preview" id="previsualiser_news"></div>
							</p>
						<?php }
					break;
					
					case "Gerer":						
						if(isset($_GET['Page']) && is_numeric($_GET['Page']))    $page = $_GET['Page'];
						else                                                     $page = 1;
						
						$pagination = 20;
						$limit_start = ($page - 1) * $pagination;

						$requete = $baseDeDonnes->prepare("SELECT COUNT(*) AS nb_total FROM news");
						$requete->execute();
						
						$nb_total = $requete->fetch();
						$nb_total = $nb_total['nb_total'];
						$nb_pages = ceil($nb_total / $pagination);
						
						$requete = $baseDeDonnes->prepare("SELECT * FROM news ORDER BY id DESC LIMIT $limit_start, $pagination");
						$requete->execute();
						
						echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Administration des News</h2>';
						
						echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > <a href="Admin_news.php">Administration des News</a> > Gestion</p>';
						
						echo '
						<table>
							<tr>
								<th width="200">Titre</th>
								<th width="100">Classe</th>
								<th width="130">Date</th>
								<th width="45">Modifier</th>
								<th width="45">Supprimer</th>
							</tr>'; 
							
							while($data = $requete->fetch()) {
								echo '
								<tr>
									<td>'.stripslashes(utf8_encode($data['titre'])).'</td>
									<td>'.stripslashes($data['classe']).'</td>
									<td>'.afficher_date_difference(strtotime($data['date'])).'</td>
									<td align="center"><a href="Admin_news.php?Action=Modifier&amp;id='.$data['id'].'">Modifier</a></td>
									<td align="center"><a href="Admin_news.php?Action=Supprimer&amp;id='.$data['id'].'">Supprimer</a></td>
								</tr>';
							}
					
						echo '</table>';
						
						echo '<p class="pagination" align="center" style="margin:15px 0 0;">'.pagination($page, $nb_pages, "?Action=Gerer&amp;Page=%d").'</p><br />';
					break;
				 
					case "Modifier":
						echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Modification d\'une News</h2>';
						
						echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > <a href="Admin_news.php">Administration des News</a> > Modification</p>';
						
						if(isset($_GET['id'])) {
							$id = intval($_GET['id']);
					 
							if(!empty($_POST['titre']) AND !empty($_POST['contenu']) AND !empty($_POST['classe'])) {
								$ListeExtension = array('jpg' => 'image/jpeg', 'jpeg' => 'image/jpeg', 'png' => 'image/png', 'gif' => 'image/gif');
								$ListeExtensionIE = array('jpg' => 'image/pjpg', 'jpeg'=>'image/pjpeg');
								$NouvelleLargeur = 220;
								
								$requete = $baseDeDonnes->prepare("SELECT * FROM news WHERE id = :id");
								$requete->bindValue(":id", $id, PDO::PARAM_INT);
								$requete->execute();
								
								$data = $requete->fetch();
								
								$add = '';
								if(!empty($_FILES['ImageNews'])) {
									if($_FILES['ImageNews']['error'] <= 0) {
										if($_FILES['ImageNews']['size'] <= $_POST["MAX_FILE_SIZE"]) {
											$ImageNews = $_FILES['ImageNews']['name'];
											
											$ExtensionPresumee = explode('.', $ImageNews);
											$ExtensionPresumee = strtolower($ExtensionPresumee[count($ExtensionPresumee)-1]);
											if($ExtensionPresumee == 'jpg' || $ExtensionPresumee == 'jpeg' || $ExtensionPresumee == 'pjpg' || $ExtensionPresumee == 'pjpeg' || $ExtensionPresumee == 'gif' || $ExtensionPresumee == 'png') {
												$ImageNews = getimagesize($_FILES['ImageNews']['tmp_name']);
												
												if($ImageNews['mime'] == $ListeExtension[$ExtensionPresumee]  || $ImageNews['mime'] == $ListeExtensionIE[$ExtensionPresumee]) {
													if($ExtensionPresumee == 'jpg' || $ExtensionPresumee == 'jpeg' || $ExtensionPresumee == 'pjpg') {
														$ImageChoisie = imagecreatefromjpeg($_FILES['ImageNews']['tmp_name']);
													}
													if($ExtensionPresumee == 'gif')   $ImageChoisie = imagecreatefromgif($_FILES['ImageNews']['tmp_name']);
													if($ExtensionPresumee == 'png')   $ImageChoisie = imagecreatefrompng($_FILES['ImageNews']['tmp_name']);

													$TailleImageChoisie = getimagesize($_FILES['ImageNews']['tmp_name']);
													
													$Reduction = ( ($NouvelleLargeur * 100)/$TailleImageChoisie[0] );
													$NouvelleHauteur = ( ($TailleImageChoisie[1] * $Reduction)/100 );
													
													if($NouvelleHauteur < 150) {
														$NouvelleHauteur = 150;
														$Reduction = ( ($NouvelleHauteur * 100)/$TailleImageChoisie[1] );
														$NouvelleLargeur = ( ($TailleImageChoisie[0] * $Reduction)/100 );
													}
													
													$NouvelleImage = imagecreatetruecolor($NouvelleLargeur , $NouvelleHauteur) or die ("Erreur");
													imagecopyresampled($NouvelleImage, $ImageChoisie, 0, 0, 0, 0, $NouvelleLargeur, $NouvelleHauteur, $TailleImageChoisie[0], $TailleImageChoisie[1]);
													
													$NouvelleImageBis = imagecreatetruecolor(220, 150) or die ("Erreur");
													imagecopy($NouvelleImageBis, $NouvelleImage, 0, 0, 0, 0, 220, 150);
													
													$ImageNews = $_FILES['ImageNews']['name'];
													$NomImageChoisie = explode('.', $ImageNews);
													$NomImageExploitable = time();
													$LienBig = '../Images/News/'.$NomImageExploitable.'-big.'.$ExtensionPresumee;
													$LienMini = '../Images/News/'.$NomImageExploitable.'-mini.'.$ExtensionPresumee;
													$LienBigClear = 'Images/News/'.$NomImageExploitable.'-big.'.$ExtensionPresumee;
													$LienMiniClear = 'Images/News/'.$NomImageExploitable.'-mini.'.$ExtensionPresumee;
													imagejpeg($NouvelleImageBis, $LienMini, 100);
													imagejpeg($ImageChoisie, $LienBig, 100);
													
													imagedestroy($ImageChoisie);
													
													if(!empty($data["images"])) {
														$images = explode(";", $data["images"]);
														unlink("../".$images[0]);
														unlink("../".$images[1]);
													}
													
													$add = ', images = :images';
												}
											}
										}
									}
								}
								
								$requete = $baseDeDonnes->prepare('
								UPDATE news SET titre = :titre, contenu = :contenu, classe = :classe '.$add.', edit_date = :edit WHERE id = :id');
								$requete->bindValue(":titre", utf8_decode($_POST["titre"]), PDO::PARAM_STR);
								$requete->bindValue(":contenu", $_POST["contenu"], PDO::PARAM_STR);
								$requete->bindValue(":classe", $_POST["classe"], PDO::PARAM_STR);
								if(!empty($add))        $requete->bindValue(":images", $LienMiniClear.';'.$LienBigClear, PDO::PARAM_STR);
								$requete->bindValue(":edit", time(), PDO::PARAM_INT);
								$requete->bindValue(":id", $id, PDO::PARAM_INT);
								$requete->execute();
								
								echo '<p class="encadreInfos">';
									echo '<span class="left valide"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Valide.png" alt="Validation" /> Confirmation</span>';
									echo '<span class="right">La news a bien été modifiée.</span>';
								echo '</p>';
								
								$fp = fopen("CreerRSS.php", 'r+');
								fclose($fp);
							}
							else {
								$requete = $baseDeDonnes->prepare("SELECT * FROM news WHERE id = :id");
								$requete->bindValue(":id", $id, PDO::PARAM_INT);
								$requete->execute();
								
								$data = $requete->fetch();
								
								if($requete->rowCount() > 0) { ?>
									<div class="bloc">
										<form method="post" action="Admin_news.php?Action=Modifier&amp;id=<?php echo $data["id"]; ?>" enctype="multipart/form-data">
										
										<p><label for="titre">Titre : </label><input type="text" id="titre" name="titre" style="width:300px;" value="<?php echo stripslashes(utf8_encode($data['titre'])); ?>" /></p>
										<p><label for="classe">Classe : </label><input type="text" id="classe" name="classe" style="width:300px;" value="<?php echo $data['classe'] ?>" /></p>
										
										<?php 
										afficher_smileys('contenu');
										$FormatagePanel = new FormatagePanel(true, true, true, 'contenu');
										$FormatagePanel->showFormatagePanel();
										?>
										
										<p><textarea name="contenu" style="width:700px; height:300px;" id="contenu"><?php echo stripslashes($data['contenu']); ?></textarea></p>
										
										<p>
											<label for="image">Nouvelle Image : </label>
											<input type="hidden" name="MAX_FILE_SIZE" value="2097152" />
											<input type="file" name="ImageNews" id="image" />
										</p>
										
										<p align="center">
											<input type="button" id="previsualiser_bouton" value="Prévisualiser" onclick="previsualiser('contenu', 'previsualiser_news', '../AJAX_news.php');" />
											<input type="submit" value="Modifier"/>
										</p>
										
										</form>
									</div>
									
									<p align="center" id="previsualiser_zone">
										<div class="preview" id="previsualiser_news"></div>
									</p>
							<?php
								}
								else {
									echo '<p class="encadreInfos">';
										echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
										echo '<span class="right">Cette news n\'existe pas ou a été supprimée.</span>';
									echo '</p>';
								}
							}
						}
						else {
							echo '<p class="encadreInfos">';
								echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
								echo '<span class="right">Aucune news n\'est définie.</span>';
							echo '</p>';
						}
					break;
					
					case "Supprimer":
						echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Suppression d\'une News</h2>';
						
						echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > <a href="Admin_news.php">Administration des News</a> > Suppression</p>';
						
						if(isset($_GET['id']) AND is_numeric($_GET['id'])) {
							$id = $_GET['id'];
															
							$requete = $baseDeDonnes->prepare("SELECT * FROM news WHERE id = :id");
							$requete->bindValue(":id", $id, PDO::PARAM_INT);
							$requete->execute();

							if($requete->rowCount() == 0) {
								echo '<p class="encadreInfos">';
									echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
										echo '<span class="right">Cette news n\'existe pas ou a été supprimée.</span>';
								echo '</p>';
							}
							else {
								if(isset($_GET["Ok"])) {
									$data = $requete->fetch();
					
									if(!empty($data["images"])) {
										$images = explode(";", $data["images"]);
										unlink("../".$images[0]);
										unlink("../".$images[1]);
									}
					
									$requete = $baseDeDonnes->prepare("DELETE FROM news WHERE id = :id");
									$requete->bindValue(":id", $id, PDO::PARAM_INT);
									$requete->execute();
									
									echo '<p class="encadreInfos">';
										echo '<span class="left valide"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Valide.png" alt="Validation" /> Confirmation</span>';
										echo '<span class="right">La news a bien été supprimée.</span>';
									echo '</p>';
								}
								else {									
									echo '
									<p class="ForumSend">
										<span class="titreSend"><img src="../Images/Forum/Erreur.png" /> Attention</span>
										<span class="contenuSend">
											<span id="a">Vous allez supprimer une news. Cette action est irréversible. Voulez-vous vraiment continuer ?</span>
											<span id="b" align="center"><a href="?Action=Supprimer&amp;id='.$id.'&amp;Ok" class="choice">Oui</a><a href="?Action=Gerer" class="choice">Non</a></span>
										</span>
									</p>';
								}
							}
						}
						else {
							echo '<p class="encadreInfos">';
								echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
								echo '<span class="right">Aucune news n\'est définie.</span>';
							echo '</p>';
						}
					break;
				 
				}
			}
		}
		else { 
			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Administration des News</h2>';
			
			echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > Administration des News</p>';

			echo '
			<ul class="navigationPage">
				<a href="Admin_news.php?Action=Creer"><li><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Ajouter.png" alt="" /> Créer une News</li></a>
				<a href="Admin_news.php?Action=Gerer"><li><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Gerer.png" alt="" /> Gérer les News</li></a>
			</ul>';
		}
	}
	else {
		echo '<p class="encadreInfos"><span class="left">Information</span><span class="right">Vous n\'avez pas l\'autorisation de voir cette page.</span></p>';
	}
}
else {
	echo '<p class="encadreInfos"><span class="left">Information</span><span class="right">Vous n\'avez pas l\'autorisation de voir cette page.</span></p>';
}

include("../Footer.php"); ?>