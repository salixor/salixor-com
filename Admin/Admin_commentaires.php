<?php
$titre_page_nom = "Administration des Commentaires";
include("../Header.php");


if(isset($ID_MEMBRE)) {
	if($NIVEAU_MEMBRE == 5) {
		if(isset($_GET['Action'])) {
		if($_GET['Action'] == "Modifier" OR $_GET['Action'] == "Supprimer" OR $_GET['Action'] == "Gerer") {
			$Action = addslashes(utf8_encode($_GET['Action']));
		 
			switch($Action) {
			 
				case "Gerer":
					
					if(isset($_GET['page']) && is_numeric($_GET['page']))
						$page = $_GET['page'];
					else {
						$page = 1;
					}
					
					$pagination = 10;
					$limit_start = ($page - 1) * $pagination;
					
					$nb_total = query("SELECT COUNT(*) AS nb_total FROM commentaires");
					$nb_total = mysql_fetch_array($nb_total);
					$nb_total = $nb_total['nb_total'];
					$nb_pages = ceil($nb_total / $pagination);
					
					$req = query("SELECT * FROM commentaires ORDER BY id DESC LIMIT $limit_start, $pagination"); ?>
					
					<h2 class="titreSecond"><img src="../Images/icones_pages/Administration.png" alt="BDD" /> Gestion des Commentaires</h2>
					
					<table>
					<tr>
					<th class="titre" width="200px">Pseudo</th>
					<th class="titre" width="200px">Date</th>
					<th class="titre" width="45px" align="center">Modi.</th>
					<th class="titre" width="45px" align="center">Supp.</th>
					</tr>	 
					
					<?php while($data = mysql_fetch_assoc($req)) { ?>
						
						<tr>
						<td><?php echo stripslashes(utf8_encode($data['pseudo'])) ?></td>
						<td><?php echo date("d/m/Y", strtotime($data['date'])) .' à '. date("G:i", strtotime($data['date'])) ?></td>
						<td align="center"><a href="Admin_commentaires.php?Action=Modifier&amp;id=<?php echo $data['id'] ?>"><img src="../Images/icones_pages/Editer_Commentaire.png" alt="Modifier" /></a></td>
						<td align="center"><a href="Admin_commentaires.php?Action=Supprimer&amp;id=<?php echo $data['id'] ?>"><img src="../Images/icones_pages/Supprimer_Commentaire.png" alt="Supprimer" /></a></td>
						</tr>
				 
					<?php } ?>
				
					</table>
					<br />
					
					<?php if(isset ($_GET['page']) AND ($_GET['page'] > $nb_pages)) { ?>
						<p align="center">Cette page n'existe pas ... <a href="http://www.salixor.com/">Retour à l'accueil</a></p><br />");
					<?php }
					else { 
						echo "<p class=\"pagination\" align=\"center\"> ". pagination($page, $nb_pages, "?Action=Gerer&page=%d") ." </p><br />";
					}
			
				break;
			 
				case "Modifier":
			 
					if(isset($_GET['id'])) {
						$id = intval(utf8_encode($_GET['id']));
				 
						if(!empty($_POST['pseudo']) AND !empty($_POST['commentaire']) AND !empty($_POST['id_news'])) {
					 
							extract($_POST);
								$pseudo = addslashes($pseudo);
								$commentaire = addslashes($commentaire);
								$id_news = addslashes($id_news);
						 
							$sql = 'UPDATE commentaires SET pseudo="'.$pseudo.'", commentaire="'.$commentaire.'", id_news="'.$id_news.'" WHERE id='.$id.'';
							$req = query($sql); ?>
						 
							<h2 class="titreSecond"><img src="../Images/icones_pages/Administration.png" alt="BDD" /> Modification du Commentaire de "<?php echo $pseudo ?>"</h2>
							<div class="encadre_infos">
							<p>Modification du Commentaire de "<?php echo $pseudo ?>" effectuée avec succès !</p>
							<p><a href="Admin_commentaires.php?Action=Gerer">Retour</a> à l'Administration des Commentaires.</p>
							</div>
					 
						<?php }
					 
						else {
					 
							$req = query('SELECT * FROM commentaires WHERE id='.$id.'');
							$data = mysql_fetch_assoc($req); ?>
							
							<h2 class="titreSecond"><img src="../Images/icones_pages/Administration.png" alt="BDD" /> Modification d'un Commentaire</h2>
									
							<div align="center">
							<form method="post" action="Admin_commentaires.php?Action=Modifier&amp;id=<?php echo $data['id'];?>">
						 
							<p><input type="text" value="<?php echo stripslashes(utf8_encode($data['pseudo'])); ?>" name="pseudo" style="width:250px;" /></p>
							<p><textarea name="commentaire" style="width:700px; height:300px;"><?php echo stripslashes($data['commentaire']); ?></textarea></p>
							<p><input type="text" value="<?php echo $data['id_news'] ?>" name="id_news" /></p>
							<p><input type="submit" value="Valider"/></p>
							</form>
							</div>
					
						<?php }
					}
				 
				break;
			 
				case "Supprimer":
			 
					if(isset($_GET['id'])) {
						$id = intval(utf8_encode($_GET['id']));
					 
						$sql_pseudo = 'SELECT pseudo FROM commentaires WHERE id='.$id.'';
						$sql = 'DELETE FROM commentaires WHERE id='.$id.'';
						
						$req_pseudo = query($sql_pseudo);
						query($sql);
						
						$data_pseudo = mysql_fetch_assoc($req_pseudo);
						$pseudo = $data_pseudo['pseudo']; ?>
						
						<h2 class="titreSecond"><img src="../Images/icones_pages/Administration.png" alt="BDD" /> Suppresion du Commentaire de "<?php echo $pseudo ?>"</h2>
						<div class="encadre_infos">
						<p>Suppression du Commentaire effectuée avec succès !</p>
						<p><a href="Admin_commentaires.php?Action=Gerer">Retour</a> à l'Administration des Commentaires.</p>
						</div>
				 
					<?php }
					
				break;
			 
			}
		}
		}
	}
	else {
		echo '<p class="encadreInfos"><span class="left">Information</span><span class="right">Vous n\'avez pas l\'autorisation de voir cette page.</span></p>';
	}
}
else {
	echo '<p class="encadreInfos"><span class="left">Information</span><span class="right">Vous n\'avez pas l\'autorisation de voir cette page.</span></p>';
}

include("../Footer.php"); ?>