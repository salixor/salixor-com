<?php
$titre_page_nom = "Administration";
include("../Header.php");

echo '<h2 class="titreSecond"><img src="../Images/icones_pages/Administration.png" alt="BDD" /> Administration</h2>';
			
echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > Administration</p>';

if(isset($ID_MEMBRE)) {
	if($NIVEAU_MEMBRE == 5) {
		if(defined('MAINTENANCE')) { $image = '<img id="notification_maintenance" src="'.ABSPATH.'Images/icones_pages/LittlesIcones/MaintenanceActive.png" /> <span class="texte-actuel-maintenance">Désactiver</span>'; }
		else { $image = '<img id="notification_maintenance" src="'.ABSPATH.'Images/icones_pages/LittlesIcones/MaintenanceDesactive.png" /> <span class="texte-actuel-maintenance">Activer</span>'; }
?>

		<p class="encadreInfos">
			<span class="right">Voici l'administration du site.</span>
		</p>

		<ul class="navigationPage">
			<a href="Admin_news.php"><li><img src="<?php echo ABSPATH; ?>Images/icones_pages/LittlesIcones/News.png" alt="" /> Administration des News</li></a>
			<a href="Admin_commentaires.php?Action=Gerer"><li><img src="<?php echo ABSPATH; ?>Images/icones_pages/LittlesIcones/Commentaires.png" alt="" /> Administration des Commentaires</li></a>
		</ul>

		<ul class="navigationPage">
			<a href="Admin_sondages.php"><li><img src="<?php echo ABSPATH; ?>Images/icones_pages/LittlesIcones/Sondages.png" alt="" /> Administration des Sondages</li></a>
			<a href="Admin_partenaires.php"><li><img src="<?php echo ABSPATH; ?>Images/icones_pages/LittlesIcones/Partenaires.png" alt="" /> Administration des Partenaires</li></a>
			<a href="javascript:popup(200);" id="click_maintenance"><li><?php echo $image; ?> la Maintenance</li></a>
		</ul>

		<ul class="navigationPage">
			<a href="Admin_equipements.php"><li><img src="<?php echo ABSPATH; ?>Images/icones_pages/LittlesIcones/Equipements.png" alt="" /> Administration des Équipements</li></a>
			<a href="Admin_panoplies.php"><li><img src="<?php echo ABSPATH; ?>Images/icones_pages/LittlesIcones/Equipements.png" alt="" /> Administration des Panoplies</li></a>
		</ul>

			
		<div class="pop_up">
			<div class="titre">
				<div class="annuler" onclick="close_popup(200);"></div>
				<h3 class="troisieme_titre h3_titre">Mode Maintenance</h3>
			</div>
			<div class="contenu" id="base">
				<?php 
				if(defined('MAINTENANCE')) {
					echo "Le mode maintenance va être <b>désactivé</b>.";
					}
				else {
					echo "Le mode maintenance va être <b>activé</b>.";
				} ?>
			</div>
			<div class="contenu" id="callback" style="display:none;">
			</div>
			<div class="footer_pop">
				<a onclick="AJAX_maintenance();" id="send" name="<?php if(defined('MAINTENANCE')) { echo "Active"; } else { echo "Inactive"; } ?>" href="javascript:void(0);">Oui</a>
				<a onclick="close_popup(200);" id="send" name="<?php if(defined('MAINTENANCE')) { echo "Active"; } else { echo "Inactive"; } ?>" href="javascript:void(0);">Non</a>
				<div style="clear:right;"></div>
			</div>
		</div>
		<div class="full_black" onclick="close_popup(200);"></div>

<?php
	}
	else {
		echo '<p class="encadreInfos">';
			echo '<span class="left information"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Information.png" alt="Information" /> Information</span>';
			echo '<span class="right">Vous n\'avez pas l\'autorisation de voir cette page.</span>';
		echo '</p>';
	}
}
else {
	echo '<p class="encadreInfos">';
		echo '<span class="left information"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Information.png" alt="Information" /> Information</span>';
		echo '<span class="right">Vous n\'avez pas l\'autorisation de voir cette page.</span>';
	echo '</p>';
}

include("../Footer.php");
?>