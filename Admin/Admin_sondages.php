<?php 
$titre_page_nom = "Administration des Sondages";
include("../Header.php");

if(isset($ID_MEMBRE)) {
	if($NIVEAU_MEMBRE == 5) {
		if(isset($_GET['Action'])) {
			if($_GET['Action'] == "Creer" OR $_GET['Action'] == "Modifier" OR $_GET['Action'] == "Supprimer" OR $_GET['Action'] == "Gerer") {
				$Action = $_GET['Action'];
			 
				switch($Action) {
					case "Creer":
						if(!empty($_POST['question']) AND !empty($_POST['reponses']) AND !empty($_POST['date']) AND !empty($_POST['id'])) {
					 
							extract($_POST);
								$question = addslashes($question);
								$id = $id;
								$reponses = explode(";", $reponses);
								
								$datePartie0 = explode(" ", $date);					
								$datePartie1 = explode("-", $datePartie0[0]);
								$datePartie2 = explode(":", $datePartie0[1]);
								
								$timestamp = mktime($datePartie2[0], $datePartie2[1], $datePartie2[2], $datePartie1[1], $datePartie1[2], $datePartie1[0]); 
							
							foreach($reponses as $reponse) {
								$sql2 = 'INSERT INTO sondage_reponses(id_sondage, reponse, nb_reponses) VALUES ('.$id.', "'.$reponse.'", 0)';
								$req2 = query($sql2); 
							} 
							
							$sql = 'INSERT INTO sondage_questions(id, question, date) VALUES ('.$id.', "'.$question.'", '.$timestamp.')';
							$req = query($sql);?>
							
							<h2 class="titreSecond"><img src="../Images/icones_pages/Administration.png" alt="BDD" /> Création du Sondage "<?php echo stripslashes($question) ?>"</h2>
							<div class="encadre_infos">
							<p>Création du Sondage "<?php echo $question ?>" effectuée avec succès !</p>
							<p><a href="Admin_sondages.php">Retour</a> à l'Administration des Sondages.</p>
							</div>
					 
						<?php }
						else { ?>
						
							<h2 class="titreSecond"><img src="../Images/icones_pages/Administration.png" alt="BDD" /> Création d'un Sondage</h2>
							
							<div align="center">
							<form method="post" action="Admin_sondages.php?Action=Creer">
							
							<p><input type="text" value="Id du sondage" name="id" style="width:700px;" onfocus="if(this.value=='Id du sondage')this.value=''" onblur="if(this.value=='')this.value='Id du sondage'" /></p>
							<p><input type="text" value="Question" name="question" style="width:700px;" onfocus="if(this.value=='Question')this.value=''" onblur="if(this.value=='')this.value='Question'" /></p>
							<p><textarea name="reponses" style="width:700px; height:100px;"
							onfocus="if(this.value=='Réponses séparées par un ;')this.value=''" onblur="if(this.value=='')this.value='Réponses séparées par un ;'">Réponses séparées par un ;</textarea></p>
							<p><input type="text" value="Date (AAAA-MM-JJ HH:MM:SS)" name="date" style="width:700px;" onfocus="if(this.value=='Date (AAAA-MM-JJ HH:MM:SS)')this.value='AAAA-MM-JJ HH:MM:SS'" onblur="if(this.value=='AAAA-MM-JJ HH:MM:SS')this.value='Date (AAAA-MM-JJ HH:MM:SS)'" />
							<p><input type="submit" value="Créer"/></p>
							</form>
							</div>
							
						<?php }
					break;
					
					case "Gerer":
						if(isset($_GET['page']) && is_numeric($_GET['page']))         $page = $_GET['page'];
						else                                                          $page = 1;
						
						$pagination = 10;
						$limit_start = ($page - 1) * $pagination;
						
						$nb_total = query("SELECT COUNT(*) AS nb_total FROM sondage_questions");
						$nb_total = mysql_fetch_array($nb_total);
						$nb_total = $nb_total['nb_total'];
						$nb_pages = ceil($nb_total / $pagination);
						
						$req = query("SELECT * FROM sondage_questions ORDER BY id DESC LIMIT $limit_start, $pagination"); ?>
						
						<h2 class="titreSecond"><img src="../Images/icones_pages/Administration.png" alt="BDD" /> Gestion des Sondages</h2>
						
						<table>
						<tr>
						<th class="titre" width="250px">Question</th>
						<th class="titre" width="150px">Date de fin</th>
						<th class="titre" width="45px" align="center">Supp.</th>
						</tr>	 
						
						<?php while($data = mysql_fetch_assoc($req)) { ?>
							
							<tr>
							<td><?php echo stripslashes($data['question']) ?></td>
							<td><?php $jours = array('Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi');
									  $affiche_jour = date("w", strtotime($data['date']));
									  echo ''. $jours[$affiche_jour] .'
											'. date("j", strtotime($data['date'])) .'
											'.affiche_mois($data['date']).'
											'. date("Y", strtotime($data['date'])) .' 
											à '. date("G", strtotime($data['date'])) .'h'. date("i", strtotime($data['date'])) .''; ?></td>
							<td align="center"><a href="Admin_sondages.php?Action=Supprimer&amp;id=<?php echo $data['id'] ?>"><img src="../Images/icones_pages/Supprimer_Sondage.png" alt="Supprimer" /></a></td>
							</tr>
					 
						<?php } ?>
					
						</table>
						<br />
						
						<?php if(isset ($_GET['page']) AND ($_GET['page'] > $nb_pages)) { ?>
							<p align="center">Cette page n'existe pas ... <a href="http://www.salixor.com/">Retour à l'accueil</a></p><br />");
						<?php }
						else { 
							echo "<p class=\"pagination\" align=\"center\"> ". pagination($page, $nb_pages, "?Action=Gerer&page=%d") ." </p><br />";
						}
				
					break;
					 
					case "Supprimer":
				 
						if(isset($_GET['id'])) {
							$id = intval($_GET['id']);
						 
							$sql_question = 'SELECT question FROM sondage_questions WHERE id='.$id.'';
							$sql = 'DELETE FROM sondage_questions WHERE id='.$id.'';
							$sql2 = 'DELETE FROM sondage_reponses WHERE id_sondage='.$id.'';
							
							$req_question = query($sql_question);
							query($sql);
							query($sql2);
							
							$data_question = mysql_fetch_assoc($req_question);
							$question = $data_question['question']; ?>
							
							<h2 class="titreSecond"><img src="../Images/icones_pages/Administration.png" alt="BDD" /> Suppresion du Sondage "<?php echo stripslashes($question) ?>"</h2>
							<div class="encadre_infos">
							<p>Suppression du Sondage "<?php echo $question ?>" effectuée avec succès !</p>
							<p><a href="Admin_sondages.php">Retour</a> à l'Administration des Sondages.</p>
							</div>
					 
						<?php }
						
					break;
				 
				}
			}
		}
		else { ?>

			<h2 class="titreSecond"><img src="../Images/icones_pages/Administration.png" alt="BDD" /> Administration des Sondages</h2>
			
			<div class="lienscubes">
			<a href="Admin_sondages.php?Action=Creer">Créer un Sondage</a>
			<a href="Admin_sondages.php?Action=Gerer">Gérer les Sondages</a>
			</div>

		<?php }
	}
	else {
		echo '<p class="encadreInfos"><span class="left">Information</span><span class="right">Vous n\'avez pas l\'autorisation de voir cette page.</span></p>';
	}
}
else {
	echo '<p class="encadreInfos"><span class="left">Information</span><span class="right">Vous n\'avez pas l\'autorisation de voir cette page.</span></p>';
}

include("../Footer.php"); ?>