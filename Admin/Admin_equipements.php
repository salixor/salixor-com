<?php 
include("../Includes/Fonctions.php");
include("../Includes/Config.php");

$arrayPluriels = array("Familier" => "familiers", "Bouclier" => "boucliers", "Cape" => "capes", "Chapeau" => "chapeaux", "Amulette" => "amulettes", "Anneau" => "anneaux", "Dofus" => "dofus", "Ceinture" => "ceintures", "Botte" => "bottes", "Sac" => "sacs", "Arc" => "arcs", "Baguette" => "baguettes", "Baton" => "batons", "Dague" => "dagues", "Epee" => "epees", "Marteau" => "marteaux", "Pelle" => "pelles", "Hache" => "haches", "Pioche" => "pioches", "Faux" => "faux");

$arraySinguliers = array("familiers" => "Familier", "capes" => "Cape", "chapeaux" => "Chapeau", "amulettes" => "Amulette", "anneaux" => "Anneau", "dofus" => "Dofus", "ceintures" => "Ceinture", "bottes" => "Botte", "sacs" => "Sac", "boucliers" => "Bouclier", "arcs" => "Arc", "baguettes" => "Baguette", "batons" => "Baton", "dagues" => "Dague", "epees" => "Epee", "marteaux" => "Marteau", "pelles" => "Pelle", "haches" => "Hache", "pioches" => "Pioche", "faux" => "Faux");

$armes = array("Arc", "Baguette", "Baton", "Dague", "Epee", "Marteau", "Pelle", "Hache", "Pioche", "Faux");

function pluriel() {
	global $arrayPluriels;
	return $arrayPluriels[NOM_EQUIPEMENT];
}

if(isset($NIVEAU_MEMBRE)) {
	if(isset($_GET['Type'])) {
		if(in_array($_GET['Type'], $arraySinguliers)) {
			define('NOM_EQUIPEMENT', $_GET['Type']);

			if(isset($_GET['Action'])) {
				switch($_GET["Action"]) {
					case "Ajouter":
						$titre_page_nom = "Ajout des ".ucfirst(pluriel());
					break;
					case "Gerer":
						$titre_page_nom = "Gestion des ".ucfirst(pluriel());
					break;
					case "Modifier":
						$titre_page_nom = "Modification des ".ucfirst(pluriel());
					break;
					case "Supprimer":
						$titre_page_nom = "Suppression des ".ucfirst(pluriel());
					break;
					default:
						$titre_page_nom = "Introuvable";
					break;
				}
			}
			else {
				$titre_page_nom = "Administration des ".ucfirst(pluriel());
			}

			include("../Header.php");
			
			if(isset($_GET['Action'])) {
				switch($_GET['Action']) {
					case "Ajouter":
						echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Ajout d\'un Équipement</h2>';
						
						if($NIVEAU_MEMBRE >= 5) {
							echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > <a href="Admin_equipements.php">Administration des Équipements</a> > <a href="?Type='.NOM_EQUIPEMENT.'">Administration des '.ucfirst(pluriel()).'</a> > Ajout</p>';
						}
						else {
							echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="'.ABSPATH.'Membres">Membres</a> > Ajouter un équipement</p>';
						}
					
						if(isset($_POST['submit'])) {
							if(!is_numeric($_POST['niveau']) OR empty($_POST['nom']) OR empty($_POST['craft']) OR empty($_POST['boost']) OR empty($_POST['description'])) {
								echo '<p class="encadreInfos">';
									echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
									echo '<span class="right">Un ou plusieurs des champs n\'ont pas été remplis.</span>';
								echo '</p>';
							}
							else {
								$nom = utf8_decode($_POST["nom"]);
								$craft = utf8_decode($_POST["craft"]);
								$boost = utf8_decode($_POST["boost"]);
								$caracteristiques = utf8_decode($_POST["caracteristiques"]);
								$description = utf8_decode($_POST["description"]);
								$conditions = utf8_decode($_POST["conditions"]);
								
								$bonus = utf8_decode($_POST["bonus_predilections"]);
								$intervalle = utf8_decode($_POST["intervalle"]);
								$pdv = $_POST["points_de_vie"];
								$zones = utf8_decode($_POST["zones_predilections"]);
								
								if($NIVEAU_MEMBRE < 5)     $level = '1';
								if($NIVEAU_MEMBRE >= 5)    $level = '2';
								
								$requete = $baseDeDonnes->prepare("
								INSERT INTO equipements(niveau, nom, craft, boost, caracteristiques, description, conditions, type, panoplie, bonus_predilections, zones_predilections, points_de_vie, intervalle, id_ajouteur, date_ajout, equipement_ajoute) VALUES (:niveau, :nom, :craft, :boost, :caracteristiques, :description, :conditions, :type, :panoplie, :bonus, :zones, :pdv, :intervalle, :membre, :time, :level)");
								$requete->bindValue(":niveau", $_POST["niveau"], PDO::PARAM_INT);
								$requete->bindValue(":nom", $nom, PDO::PARAM_STR);
								$requete->bindValue(":craft", $craft, PDO::PARAM_STR);
								$requete->bindValue(":boost", $boost, PDO::PARAM_STR);
								$requete->bindValue(":caracteristiques", $caracteristiques, PDO::PARAM_STR);
								$requete->bindValue(":description", $description, PDO::PARAM_STR);
								$requete->bindValue(":conditions", $conditions, PDO::PARAM_STR);
								$requete->bindValue(":type", pluriel(), PDO::PARAM_STR);
								$requete->bindValue(":panoplie", $_POST["panoplie"], PDO::PARAM_INT);
								$requete->bindValue(":bonus", $bonus, PDO::PARAM_STR);
								$requete->bindValue(":zones", $zones, PDO::PARAM_STR);
								$requete->bindValue(":pdv", $pdv, PDO::PARAM_INT);
								$requete->bindValue(":intervalle", $intervalle, PDO::PARAM_STR);
								$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
								$requete->bindValue(":time", time(), PDO::PARAM_INT);
								$requete->bindValue(":level", $level, PDO::PARAM_STR);
								$requete->execute();

								echo '<p class="encadreInfos">';
									echo '<span class="left valide"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Valide.png" alt="Validation" /> Confirmation</span>';
									echo '<span class="right">L\'équipement a bien été ajouté.</span>';
								echo '</p>';
							}
						}
						else { 
							echo '
							<p class="encadreInfos">
								<span class="right">
									Avant de commencer, merci de lire <a href="http://www.salixor.com/Forum/voirsujet.php?t=23" target="_blank">ce sujet</a>.
									<b align="center">Merci de l\'aide que vous apportez au site !</b>
								</span>
							</p>';
							
							if($_GET['Type'] != "Familier")     $displayFamilier = 'style="display:none;"';
							else                                $displayFamilier = '';
							
							if(!in_array(NOM_EQUIPEMENT, $armes))    $display = 'display:none;';
							else                                     $display = '';
							
							if($NIVEAU_MEMBRE < 5)              $displayPano = 'style="display:none;"';
							else                                $displayPano = ''; ?>
							
							<div class="bloc">
								<form method="post" action="Admin_equipements.php?Type=<?php echo NOM_EQUIPEMENT; ?>&amp;Action=Ajouter">
									<p>
										<label for="niveau">Niveau : </label>
										<input type="text" name="niveau" id="niveau" style="width:150px;" />
									</p>
									<p>
										<label for="nom">Nom : </label>
										<input type="text" name="nom" id="nom" style="width:250px;" />
									</p>
									<p>
										<label for="craft">Craft : </label>
										<textarea name="craft" id="craft" style="width:500px; min-width:500px; max-width:500px; height:150px; margin-left:0;"></textarea>
									</p>
									<p>
										<label for="boost">Effets : </label>
										<textarea name="boost" id="boost" style="width:500px; min-width:500px; max-width:500px; height:150px; margin-left:0;"></textarea>
									</p>
									<p>
										<label for="caracteristiques" style="<?php echo $display; ?>">Caractéristiques : </label>
										<textarea name="caracteristiques" id="caracteristiques" style="width:500px; min-width:500px; max-width:500px; height:150px; margin-left:0; <?php echo $display; ?>"></textarea>
									</p>
									<p <?php echo $displayFamilier; ?>>
										<label for="intervalle">Intervalle : </label>
										<input type="text" name="intervalle" id="intervalle" style="width:500px;" />
									</p>
									<p <?php echo $displayFamilier; ?>>
										<label for="points_de_vie">PDV : </label>
										<input type="text" name="points_de_vie" id="points_de_vie" style="width:500px;" />
									</p>
									<p <?php echo $displayFamilier; ?>>
										<label for="zones_predilections">Zone de Prédilection : </label>
										<input type="text" name="zones_predilections" id="zones_predilections" style="width:500px;" />
									</p>
									<p <?php echo $displayFamilier; ?>>
										<label for="bonus_predilections">Bonus de Prédilection : </label>
										<input type="text" name="bonus_predilections" id="bonus_predilections" style="width:500px;" />
									</p>
									<p>
										<label for="description">Description : </label>
										<textarea name="description" id="description" style="width:500px; min-width:500px; max-width:500px; height:100px; margin-left:0;"></textarea>
									</p>
									<p>
										<label for="conditions">Conditions : </label>
										<textarea name="conditions" id="conditions" style="width:500px; min-width:500px; max-width:500px; height:50px; margin-left:0;"></textarea>
									</p>
									<p <?php echo $displayPano; ?>>
										<label for="panoplie">Numéro de la panoplie : </label>
										<input type="text" name="panoplie" id="panoplie" style="width:150px;" />
									</p>
									<p align="center">
										<input type="submit" name="submit" value="Valider"/>
									</p>
								</form>
							</div>
						<?php
						}
					break;

					case "Gerer":
						if($NIVEAU_MEMBRE >= 5) {
							if(isset($_GET['Page']) && is_numeric($_GET['Page']))    $page = $_GET['Page'];
							else                                                     $page = 1;

							$pagination = 25;
							$limit_start = ($page - 1) * $pagination;

							$requete = $baseDeDonnes->prepare("SELECT COUNT(*) AS nb_total FROM equipements WHERE type = :type AND equipement_ajoute = '2'");
							$requete->bindValue(":type", pluriel(), PDO::PARAM_STR);
							$requete->execute();
							
							$nb_total = $requete->fetch();
							$nb_total = $nb_total['nb_total'];
							$nb_pages = ceil($nb_total / $pagination);
							
							$requete = $baseDeDonnes->prepare("SELECT * FROM equipements WHERE type = :type AND equipement_ajoute = '2' ORDER BY niveau, id ASC LIMIT $limit_start, $pagination");
							$requete->bindValue(":type", pluriel(), PDO::PARAM_STR);
							$requete->execute();

							echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Gestion des '.ucfirst(pluriel()).'</h2>';
							
							echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > <a href="Admin_equipements.php">Administration des Équipements</a> > <a href="?Type='.NOM_EQUIPEMENT.'">Administration des '.ucfirst(pluriel()).'</a> > Gestion</p>';
							
							echo'
							<table>
								<tr>
									<th width="70px">Niveau</th>
									<th width="500px">Nom</th>
									<th>Modifier</th>
									<th>Supprimer</th>
								</tr>'; 

								while($data = $requete->fetch()) {
									echo '
									<tr>
										<td align="center">'.stripslashes(utf8_encode($data['niveau'])).'</td>
										<td>'.stripslashes(utf8_encode($data['nom'])).'</td>
										<td align="center">
											<a href="Admin_equipements.php?Type='.NOM_EQUIPEMENT.'&amp;Action=Modifier&amp;id='.$data['id'].'">
												Modifier
											</a>
										</td>
										<td align="center">
											<a href="Admin_equipements.php?Type='.NOM_EQUIPEMENT.'&amp;Action=Supprimer&amp;id='.$data['id'].'">
												Supprimer
											</a>
										</td>
									</tr>';
								} 
							
							echo '</table>';

							echo '<p class="pagination" align="center" style="margin:15px 0 0;">'.pagination($page, $nb_pages, "?Type=".NOM_EQUIPEMENT."&amp;Action=Gerer&amp;Page=%d").'</p>';
						}
						else {
							echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Administration des Équipements</h2>';
							
							echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > Administration des Équipements</p>';
							
							echo '<p class="encadreInfos">';
								echo '<span class="left information"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Information.png" alt="Information" /> Information</span>';
								echo '<span class="right">Vous n\'avez pas l\'autorisation de voir cette page.</span>';
							echo '</p>';
						}
					break;

					case "Modifier":
						echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Modification d\'un Équipement</h2>';
						
						echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > <a href="Admin_equipements.php">Administration des Équipements</a> > <a href="?Type='.NOM_EQUIPEMENT.'">Administration des '.ucfirst(pluriel()).'</a> > Modification</p>';
					
						if(isset($_GET['id']) AND is_numeric($_GET['id'])) {
							$id = $_GET['id'];
							
							if(isset($_POST['submit'])) {
								if(!is_numeric($_POST['niveau']) OR empty($_POST['nom']) OR empty($_POST['craft']) OR empty($_POST['boost']) OR empty($_POST['description'])) {
									echo '<p class="encadreInfos">';
										echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
										echo '<span class="right">Un ou plusieurs des champs n\'ont pas été remplis.</span>';
									echo '</p>';
								}
								else {
									$nom = utf8_decode($_POST["nom"]);
									$craft = utf8_decode($_POST["craft"]);
									$boost = utf8_decode($_POST["boost"]);
									$caracteristiques = utf8_decode($_POST["caracteristiques"]);
									$description = utf8_decode($_POST["description"]);
									$conditions = utf8_decode($_POST["conditions"]);
									
									$bonus = utf8_decode($_POST["bonus_predilections"]);
									$intervalle = utf8_decode($_POST["intervalle"]);
									$pdv = $_POST["points_de_vie"];
									$zones = utf8_decode($_POST["zones_predilections"]);
									
									if($NIVEAU_MEMBRE < 5)     $level = '1';
									if($NIVEAU_MEMBRE >= 5)    $level = '2';
									
									$requete = $baseDeDonnes->prepare("
									UPDATE equipements
									SET niveau = :niveau, nom = :nom, craft = :craft, boost = :boost, caracteristiques = :caracteristiques, description = :description, conditions = :conditions, bonus_predilections = :bonus, zones_predilections = :zones, points_de_vie = :pdv, intervalle = :intervalle, panoplie = :panoplie, date_ajout = :time, equipement_ajoute = :level
									WHERE type = :type AND id = :id");
									$requete->bindValue(":niveau", $_POST["niveau"], PDO::PARAM_INT);
									$requete->bindValue(":nom", $nom, PDO::PARAM_STR);
									$requete->bindValue(":craft", $craft, PDO::PARAM_STR);
									$requete->bindValue(":boost", $boost, PDO::PARAM_STR);
									$requete->bindValue(":caracteristiques", $caracteristiques, PDO::PARAM_STR);
									$requete->bindValue(":description", $description, PDO::PARAM_STR);
									$requete->bindValue(":conditions", $conditions, PDO::PARAM_STR);
									$requete->bindValue(":pdv", $pdv, PDO::PARAM_INT);
									$requete->bindValue(":intervalle", $intervalle, PDO::PARAM_STR);
									$requete->bindValue(":bonus", $bonus, PDO::PARAM_STR);
									$requete->bindValue(":zones", $zones, PDO::PARAM_STR);
									$requete->bindValue(":panoplie", $_POST["panoplie"], PDO::PARAM_INT);
									$requete->bindValue(":id", $id, PDO::PARAM_INT);
									$requete->bindValue(":type", pluriel(), PDO::PARAM_STR);
									$requete->bindValue(":time", time(), PDO::PARAM_INT);
									$requete->bindValue(":level", $level, PDO::PARAM_STR);
									$requete->execute();

									echo '<p class="encadreInfos">';
										echo '<span class="left valide"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Valide.png" alt="Validation" /> Confirmation</span>';
										echo '<span class="right">L\'équipement a bien été modifié.</span>';
									echo '</p>';
								}
							}
							else {
								if($NIVEAU_MEMBRE < 5)      $sqlAjout = "AND equipement_ajoute = '1' OR equipement_ajoute = '0' AND id_ajouteur = :membre";
								else                        $sqlAjout = "";
								
								$requete = $baseDeDonnes->prepare("SELECT * FROM equipements WHERE id = :id AND type = :type ".$sqlAjout);
								$requete->bindValue(":id", $id, PDO::PARAM_INT);
								$requete->bindValue(":type", pluriel(), PDO::PARAM_STR);
								if($NIVEAU_MEMBRE < 5)      $requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_STR);
								$requete->execute();
								
								if($requete->rowCount() > 0) {
									$data = $requete->fetch();
									?>
									
									<div class="bloc">
										<form method="post" action="Admin_equipements.php?Type=<?php echo NOM_EQUIPEMENT; ?>&amp;Action=Modifier&amp;id=<?php echo $id; ?>">
											<p>
												<label for="niveau">Niveau : </label>
												<input type="text" value="<?php echo stripslashes($data['niveau']); ?>" name="niveau" id="niveau" style="width:150px;" />
											</p>
											<p>
												<label for="nom">Nom : </label>
												<input type="text" value="<?php echo stripslashes(utf8_encode($data['nom'])); ?>" name="nom" id="nom" style="width:250px;" />
											</p>
											<p>
												<label for="craft">Craft : </label>
												<textarea name="craft" id="craft" style="width:500px; min-width:500px; max-width:500px; height:150px; margin-left:0;"><?php echo stripslashes(utf8_encode($data['craft'])); ?></textarea>
											</p>
											<p>
												<label for="boost">Effets : </label>
												<textarea name="boost" id="boost" style="width:500px; min-width:500px; max-width:500px; height:150px; margin-left:0;"><?php echo stripslashes(utf8_encode($data['boost'])); ?></textarea>
											</p>
											<?php
											if(!in_array(NOM_EQUIPEMENT, $armes))    $hidden = 'display:none;';
											else                                     $hidden = '';
											?>
											<p>
												<label for="caracteristiques" style="<?php echo $hidden; ?>">Caractéristiques : </label>
												<textarea name="caracteristiques" id="caracteristiques" style="width:500px; min-width:500px; max-width:500px; height:150px; margin-left:0; <?php echo $hidden; ?>"><?php echo stripslashes(utf8_encode($data['caracteristiques'])); ?></textarea>
											</p>
											<?php
											if(NOM_EQUIPEMENT != "Familier")         $displayFamilier = 'style="display:none;"';
											else                                     $displayFamilier = '';
											?>
											<p <?php echo $displayFamilier; ?>>
												<label for="intervalle">Intervalle : </label>
												<input type="text" name="intervalle" id="intervalle" style="width:500px;" value="<?php echo stripslashes(utf8_encode($data["intervalle"])); ?>" />
											</p>
											<p <?php echo $displayFamilier; ?>>
												<label for="points_de_vie">PDV : </label>
												<input type="text" name="points_de_vie" id="points_de_vie" style="width:500px;" value="<?php echo $data["points_de_vie"]; ?>" />
											</p>
											<p <?php echo $displayFamilier; ?>>
												<label for="zones_predilections">Zone de Prédilection : </label>
												<input type="text" name="zones_predilections" id="zones_predilections" style="width:500px;" value="<?php echo stripslashes(utf8_encode($data["zones_predilections"])); ?>" />
											</p>
											<p <?php echo $displayFamilier; ?>>
												<label for="bonus_predilections">Bonus de Prédilection : </label>
												<input type="text" name="bonus_predilections" id="bonus_predilections" style="width:500px;" value="<?php echo stripslashes(utf8_encode($data["bonus_predilections"])); ?>" />
											</p>
											<p>
												<label for="description">Description : </label>
												<textarea name="description" id="description" style="width:500px; min-width:500px; max-width:500px; height:100px; margin-left:0;"><?php echo stripslashes(utf8_encode($data['description'])); ?></textarea>
											</p>
											<p>
												<label for="conditions">Conditions : </label>
												<textarea name="conditions" id="conditions" style="width:500px; min-width:500px; max-width:500px; height:50px; margin-left:0;"><?php echo stripslashes(utf8_encode(nl2br($data['conditions']))); ?></textarea>
											</p>
											<?php
											if($NIVEAU_MEMBRE < 5)              $displayPano = 'style="display:none;"';
											else                                $displayPano = '';
											?>
											<p <?php echo $displayPano; ?>>
												<label for="panoplie">Numéro de la panoplie : </label>
												<input type="text" value="<?php echo stripslashes($data['panoplie']); ?>" name="panoplie" id="panoplie" style="width:150px;" />
											</p>
											<p align="center">
												<input type="submit" name="submit" value="Valider"/>
											</p>
										</form>
									</div>

							<?php
								}
								else {
									echo '<p class="encadreInfos">';
										echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
										echo '<span class="right">Cet équipement n\'existe pas, n\'a pas été ajouté par vous ou ne peut plus être modifié.</span>';
									echo '</p>';
								}
							}
						}
						else {
							echo '<p class="encadreInfos">';
								echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
								echo '<span class="right">Aucun équipement n\'est défini.</span>';
							echo '</p>';
						}
					break;

					case "Supprimer":
						if($NIVEAU_MEMBRE >= 5) {
							echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Suppression d\'un Équipement</h2>';
							
							echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > <a href="Admin_equipements.php">Administration des Équipements</a> > <a href="?Type='.NOM_EQUIPEMENT.'">Administration des '.ucfirst(pluriel()).'</a> > Suppression</p>';
							
							if(isset($_GET['id']) AND is_numeric($_GET['id'])) {
								$id = $_GET['id'];
																
								$requete = $baseDeDonnes->prepare("SELECT * FROM equipements WHERE id = :id AND type = :type");
								$requete->bindValue(":id", $id, PDO::PARAM_INT);
								$requete->bindValue(":type", pluriel(), PDO::PARAM_STR);
								$requete->execute();

								if($requete->rowCount() == 0) {
									echo '<p class="encadreInfos">';
										echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
										echo '<span class="right">Cet équipement n\'existe pas ou plus.</span>';
									echo '</p>';
								}
								else {
									if(isset($_GET["Ok"])) {
										$requete = $baseDeDonnes->prepare("DELETE FROM equipements WHERE id = :id AND type = :type");
										$requete->bindValue(":id", $id, PDO::PARAM_INT);
										$requete->bindValue(":type", pluriel(), PDO::PARAM_STR);
										$requete->execute();
										
										echo '<p class="encadreInfos">';
											echo '<span class="left valide"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Valide.png" alt="Validation" /> Confirmation</span>';
											echo '<span class="right">L\'équipement a bien été supprimé.</span>';
										echo '</p>';
									}
									else {									
										echo '
										<p class="ForumSend">
											<span class="titreSend"><img src="../Images/Forum/Erreur.png" /> Attention</span>
											<span class="contenuSend">
												<span id="a">Vous allez supprimer un équipement. Cette action est irréversible. Voulez-vous vraiment continuer ?</span>
												<span id="b" align="center"><a href="?Type='.NOM_EQUIPEMENT.'&amp;Action=Supprimer&amp;id='.$id.'&amp;Ok" class="choice">Oui</a><a href="?Type='.NOM_EQUIPEMENT.'&amp;Action=Gerer" class="choice">Non</a></span>
											</span>
										</p>';
									}
								}
							}
							else {
								echo '<p class="encadreInfos">';
									echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
									echo '<span class="right">Aucun équipement n\'est défini.</span>';
								echo '</p>';
							}
						}
						else {
							echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Administration des Équipements</h2>';
							
							echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > Administration des Équipements</p>';
							
							echo '<p class="encadreInfos">';
								echo '<span class="left information"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Information.png" alt="Information" /> Information</span>';
								echo '<span class="right">Vous n\'avez pas l\'autorisation de voir cette page.</span>';
							echo '</p>';
						}
					break;

					default:
						echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Administration des Équipements</h2>';
								
						echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > <a href="Admin_equipements.php">Administration des Équipements</a> > <a href="?Type='.NOM_EQUIPEMENT.'">Administration des '.ucfirst(pluriel()).'</a></p>';
						
						echo '<p class="encadreInfos">';
							echo '<span class="left information"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Information.png" alt="Information" /> Information</span>';
							echo '<span class="right">Cette page n\'existe pas.</span>';
						echo '</p>';
					break;
				}
			}
			else {
				if($NIVEAU_MEMBRE >= 5) { ?>
					<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Administration.png" alt="BDD" /> Administration des <?php echo ucfirst(pluriel()); ?></h2>
				
					<p class="Arianne">> <a href="<?php echo ABSPATH; ?>">Accueil</a> > <a href="./">Administration</a> > <a href="?Page=Accueil">Administration des Équipements</a> > Administration des <?php echo ucfirst(pluriel()); ?></p>
					
					<ul class="navigationPage">
						<a href="Admin_equipements.php?Type=<?php echo NOM_EQUIPEMENT; ?>&amp;Action=Ajouter"><li><img src="<?php echo ABSPATH; ?>Images/icones_pages/LittlesIcones/Ajouter.png" alt="" /> Ajouter un Équipement</li></a>
						<a href="Admin_equipements.php?Type=<?php echo NOM_EQUIPEMENT; ?>&amp;Action=Gerer"><li><img src="<?php echo ABSPATH; ?>Images/icones_pages/LittlesIcones/Gerer.png" alt="" /> Gérer les Équipements</li></a>
					</ul>
			<?php
				}
				else {
					echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Administration des Équipements</h2>';
					
					echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > Administration des Équipements</p>';
					
					echo '<p class="encadreInfos">';
						echo '<span class="left information"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Information.png" alt="Information" /> Information</span>';
						echo '<span class="right">Vous n\'avez pas l\'autorisation de voir cette page.</span>';
					echo '</p>';
				}
			}

			include("../Footer.php"); 
		}
		else {
			$titre_page_nom = "Administration des Équipements";
			include("../Header.php");
			
			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Administration des Équipements</h2>';
					
			echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > <a href="Admin_equipements.php">Administration des Équipements</a></p>';
			
			echo '<p class="encadreInfos">';
				echo '<span class="left information"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Information.png" alt="Information" /> Information</span>';
				echo '<span class="right">Cette page n\'existe pas.</span>';
			echo '</p>';
			
			include("../Footer.php");
		}
	}
	elseif(isset($_GET["Action"]) && !isset($_GET["Type"])) {
		$titre_page_nom = "Gérer les contributions";

		include("../Header.php");
		
		if($_GET["Action"] == "Contributions") {
			if($NIVEAU_MEMBRE >= 5) {
				echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Gestion des Contributions</h2>';
				
				if(isset($_GET['id']) AND is_numeric($_GET['id']) AND isset($_GET["Visionner"])) {
					echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > <a href="Admin_equipements.php">Administration des Équipements</a> > <a href="?Action=Contributions">Gestion des Contributions</a> > Visionner</p>';
					
					$requete = $baseDeDonnes->prepare("
					SELECT equipements.id AS equi_id, equipements.*, panoplies.*
					FROM equipements
					LEFT JOIN panoplies
					ON panoplies.id = equipements.panoplie
					WHERE equipements.id = :id
					ORDER BY equipements.niveau, equipements.id ASC");
					$requete->bindValue(":id", $_GET["id"], PDO::PARAM_INT);
					$requete->execute();
					
					$data = $requete->fetch();
					
					$data["nom_panoplie"] = utf8_encode($data["nom_panoplie"]);
					$niveau = $data['niveau'];
					$image = imageEquipement(ucfirst($data["type"]), $data['nom']);
					$nom = tronquer(stripslashes(utf8_encode($data['nom'])), 28, " ...");
				
					$craft = nl2br(changerCraft(stripslashes(utf8_encode($data['craft']))));
					$boost = nl2br(utf8_encode(formatage($data['boost'])));
					
					$intervalle = nl2br(stripslashes(utf8_encode(formatage($data['intervalle']))));
					$pdv = nl2br(stripslashes(utf8_encode(formatage($data['points_de_vie']))));
					$zonePredilection = nl2br(stripslashes(utf8_encode(formatage($data['zones_predilections']))));
					$bonusPredilection = nl2br(stripslashes(utf8_encode(formatage($data['bonus_predilections']))));
					
					$caracteristiques = nl2br(utf8_encode($data['caracteristiques']));
					$equipementDescription = nl2br(utf8_encode($data['description']));
					$equipementConditions = nl2br(utf8_encode($data['conditions']));
					$type = array('Chapeaux' => 'Chapeau', 'Capes' => 'Cape', 'Sacs' => 'Sac', 'Anneaux' => 'Anneau', 'Amulettes' => 'Amulette', 'Bottes' => 'Botte', 'Ceintures' => 'Ceinture', 'Dofus' => 'Dofus', 'Boucliers' => 'Bouclier');
					
					if($data["type"] != "familiers") { ?>
				
						<table class="equipementTable" rel="<?php echo $data["type"] ?>" id="Item-<?php echo $data['equi_id']; ?>">
							<tr class="equipementInformations">
								<th>Niveau <?php echo $niveau ?></th>
								<th colspan="2"><?php echo $nom; ?>
								</th>
							</tr>
							
							<tr class="equipementTitres">
								<td class="screen"></td>
								<td width="320">Obtention</td>
								<td width="320">Effets</td>
							</tr>
							<?php if(empty($caracteristiques)) { ?>
								<tr>
									<td class="screen"><?php echo $image; ?></td>         
									<td class="obtention"><?php echo stripslashes($craft); ?></td>
									<td class="effets"><?php echo stripslashes($boost); ?></td>
								</tr>
							<?php }
							else { ?>					
								<tr>
									<td class="screen" rowspan="3"><?php echo $image; ?></td>         
									<td class="obtention" style="height:50%;"><?php echo stripslashes($craft); ?></td>
									<td class="effets" rowspan="3"><?php echo colorationDommages(stripslashes($boost)); ?></td>
								</tr>
								
								<tr class="equipementTitres">
									<td width="320">Caractéristiques</td>
								</tr>
							
								<tr>
									<td class="obtention" style="height:50%;"><?php echo stripslashes($caracteristiques); ?></td>
								</tr>
							<?php }

								echo '
								<tr class="panoplie">
									<td colspan="3">
										<span>Description, Conditions :</span>
									</td>
								</tr>';
							?>
							
							<tr>
								<td colspan="3" class="description"><b>&laquo;</b> <?php echo stripslashes($equipementDescription); ?> <b>&raquo;</b></td>
							</tr>
							
							<?php if(!empty($equipementConditions)) { ?>
								<tr>
									<td colspan="3" class="conditions"><b>Conditions :</b> <?php echo $equipementConditions ?></td>
								</tr>
							<?php } ?>
							
						</table>
					
					<?php
					}
					else {
					?>
				
						<table class="equipementTable">
							<tr class="equipementInformations">
								<th>
									<a href="<?php echo ABSPATH; ?>Equipements?Page=<?php echo $type[ucfirst($data["type"])]; ?>#Item-<?php echo $data['equi_id']; ?>" title="Lien vers cet équipement." class="lienEquipement">#</a>
									Niveau <?php echo $niveau ?></th>
								<th colspan="2">
									<?php echo $nom; ?>
								</th>
							</tr>
							
							<tr class="equipementTitres">
								<td class="screen"></td>
								<td width="320">Obtention</td>
								<td width="320">Nourriture</td>
							</tr>
							
							<tr>
								<td class="screen big"><?php echo $image; ?></td>         
								<td class="obtention" style="width:220px;"><?php echo stripslashes($craft); ?></td>
								<td class="effets"><?php echo stripslashes($boost); ?></td>
							</tr>
						
							<?php
							if(!empty($intervalle)) {
								echo '
								<tr class="clair">
									<td>Intervalle entre 2 repas</td>
									<td class="right" colspan="2">'.$intervalle.'</td>
								</tr>';
							}
							if(!empty($pdv)) {
								echo '
								<tr class="clair left">
									<td>Points de vie</td>
									<td class="right" colspan="2">'.$pdv.' PDV</td>
								</tr>';
							}
							if(!empty($zonePredilection)) {
								echo '
								<tr class="clair">
									<td>Zone(s) de prédilection</td>
									<td class="right" colspan="2">'.$zonePredilection.'</td>
								</tr>';
							}
							if(!empty($bonusPredilection)) {
								echo '
								<tr class="clair left">
									<td>Bonus de prédilection</td>
									<td class="right" colspan="2">'.$bonusPredilection.'</td>
								</tr>
								';
							}
							?>
							
							<tr>
								<td colspan="3" class="description"><b>&laquo;</b> <?php echo stripslashes($equipementDescription); ?> <b>&raquo;</b></td>
							</tr>
							
							<?php if(!empty($equipementConditions)) { ?>
								<tr>
									<td colspan="3" class="conditions"><b>Conditions :</b> <?php echo $equipementConditions ?></td>
								</tr>
							<?php } ?>
							
						</table>
				<?php
					}
				}
				else {
					if(isset($_GET["Refuser"]) AND isset($_GET["id"])) {
						$requete = $baseDeDonnes->prepare("UPDATE equipements SET equipement_ajoute = '0' WHERE id = :id");
						$requete->bindValue(":id", $_GET["id"], PDO::PARAM_INT);
						$requete->execute();
					}
					elseif(isset($_GET["Accepter"]) AND isset($_GET["id"])) {
						$requete = $baseDeDonnes->prepare("UPDATE equipements SET equipement_ajoute = '2' WHERE id = :id");
						$requete->bindValue(":id", $_GET["id"], PDO::PARAM_INT);
						$requete->execute();
					}
					
					echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > <a href="Admin_equipements.php">Administration des Équipements</a> > Gestion des Contributions</p>';
					
					if(isset($_GET['Page']) && is_numeric($_GET['Page']))    $page = $_GET['Page'];
					else                                                     $page = 1;

					$pagination = 50;
					$limit_start = ($page - 1) * $pagination;

					$requete = $baseDeDonnes->prepare("SELECT COUNT(*) AS nb_total FROM equipements WHERE equipements.id_ajouteur <> 1");
					$requete->execute();

					$nb_total = $requete->fetch();
					$nb_total = $nb_total['nb_total'];
					$nb_pages = ceil($nb_total / $pagination);

					$requete = $baseDeDonnes->prepare("
					SELECT * FROM equipements
					WHERE id_ajouteur <> 1
					ORDER BY equipement_ajoute DESC, date_ajout DESC, equipements.niveau DESC, equipements.id ASC
					LIMIT $limit_start, $pagination");
					$requete->execute();

					$requete->setFetchMode(PDO::FETCH_OBJ);
					
					echo '
					<table>
						<tr>
							<th width="30"></th>
							<th width="50">Niveau</th>
							<th width="200">Nom</th>
							<th width="100">Type</th>
							<th width="90">État</th>
							<th width="70">Modifier</th>
							<th width="70">Refuser</th>
							<th width="70">Accepter</th>
						</tr>';
						
						if($requete->rowCount() > 0) {
							while($data = $requete->fetch()) {
								$image = lienImageEquipement(ucfirst($data->type), $data->nom);
								if(!file_exists('../'.$image))    $image = 'Images/Items/Unknown.png';
																
								$lien = utf8_encode(stripslashes($data->nom));
								
								if($data->equipement_ajoute == '0')        $etat = '<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/BulletRed.png" alt="" style="margin:3px 0 -3px;" /> Refusé';
								if($data->equipement_ajoute == '1')        $etat = '<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/BulletYellow.png" alt="" style="margin:3px 0 -3px;" /> En attente';
								if($data->equipement_ajoute == '2')        $etat = '<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/BulletGreen.png" alt="" style="margin:3px 0 -3px;" /> Accepté';
								
								echo '
								<tr>
									<td align="center"><img src="'.ABSPATH.$image.'" height="30" style="background:#FFF;" alt="" /></td>
									<td align="center">'.$data->niveau.'</td>
									<td><a href="'.ABSPATH.'Admin/Admin_equipements.php?Action=Contributions&amp;Visionner&amp;id='.$data->id.'">'.$lien.'</a></td>
									<td>'.$arraySinguliers[$data->type].'</td>
									<td>'.$etat.'</td>
									<td align="center"><a href="'.ABSPATH.'Admin/Admin_equipements.php?Type='.$arraySinguliers[$data->type].'&amp;Action=Modifier&amp;id='.$data->id.'">Modifier</a></td>
									<td align="center"><a href="'.ABSPATH.'Admin/Admin_equipements.php?Action=Contributions&amp;Refuser&amp;id='.$data->id.'">Refuser</a></td>
									<td align="center"><a href="'.ABSPATH.'Admin/Admin_equipements.php?Action=Contributions&amp;Accepter&amp;id='.$data->id.'">Accepter</a></td>
								</tr>';
							}
						}
						else {
							echo '<tr><td colspan="8" align="center" style="padding:7px;">Aucun équipement n\'a encore été proposé par un membre.</td></tr>';
						}
										
					echo '</table>';
					
					if($nb_pages > 1)              echo '<p class="pagination" align="center" style="margin:15px 0 0; width:760px;">'.pagination($page, $nb_pages, "?Page=%d").'</p>';
				}
			}
			else {
				echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Administration des Équipements</h2>';
				
				echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > Administration des Équipements</p>';
				
				echo '<p class="encadreInfos">';
					echo '<span class="left information"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Information.png" alt="Information" /> Information</span>';
					echo '<span class="right">Vous n\'avez pas l\'autorisation de voir cette page.</span>';
				echo '</p>';
			}
		}
		else {
			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Administration des Équipements</h2>';
			
			echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > Administration des Équipements</p>';
			
			echo '<p class="encadreInfos">';
				echo '<span class="left information"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Information.png" alt="Information" /> Information</span>';
				echo '<span class="right">Vous n\'avez pas l\'autorisation de voir cette page.</span>';
			echo '</p>';
		}
			
		include("../Footer.php");
	}
	else {
		if($NIVEAU_MEMBRE >= 5) {
			$titre_page_nom = "Administration des Équipements";
			include("../Header.php");

			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Administration des Équipements</h2>';
			
			echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > Administration des Équipements</p>';
			
			$i = 0;
			
			sort($arrayPluriels);
			foreach($arrayPluriels as $equipement) {
				if($i == 0)                           echo '<ul class="navigationPage left">';
				if($i == count($arrayPluriels)/2)     echo '</ul><ul class="navigationPage right">';
				
				echo '<a href="?Type='.$arraySinguliers[$equipement].'"><li>'.ucfirst($equipement).'</li></a>';
				
				if($i == count($arrayPluriels)-1)     echo '</ul>';
				
				$i++;
			}
			
			echo '
			<div style="clear:left;"></div>
			
			<ul class="navigationPage">
				<a href="Admin_equipements.php?Action=Contributions"><li><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Contribuer.png" alt="" /> Gérer les Contributions</li></a>
			</ul>';
		}
		else {
			$titre_page_nom = "Ajouter un Équipement";
			include("../Header.php");

			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Ajouter un Équipement</h2>';
			
			echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="'.ABSPATH.'Membres">Membres</a> > Ajouter un équipement</p>';
			
			$i = 0;
			
			sort($arrayPluriels);
			foreach($arrayPluriels as $equipement) {
				if($i == 0)                           echo '<ul class="navigationPage left">';
				if($i == count($arrayPluriels)/2)     echo '</ul><ul class="navigationPage right">';
				echo '<a href="?Type='.$arraySinguliers[$equipement].'&amp;Action=Ajouter"><li>'.ucfirst($equipement).'</li></a>';
				if($i == count($arrayPluriels)-1)     echo '</ul>';
				
				$i++;
			}
		}
			
		include("../Footer.php");
	}
}
else {
	$titre_page_nom = "Administration des Équipements";
	include("../Header.php");

	echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" alt="BDD" /> Administration des Équipements</h2>';
	
	echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Administration</a> > Administration des Équipements</p>';
	
	echo '<p class="encadreInfos">';
		echo '<span class="left information"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Information.png" alt="Information" /> Information</span>';
		echo '<span class="right">Vous n\'avez pas l\'autorisation de voir cette page.</span>';
	echo '</p>';
	
	include("../Footer.php");
}
?>