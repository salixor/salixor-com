<?php
if($_SERVER['SERVER_NAME'] == 'localhost') {
	define('ABSPATH', 'http://localhost/Salixor/');
}
else {
	define('ABSPATH', 'http://www.salixor.com/');
}

include('../Fonctions.php');

if(isset($_POST["contenu"])) {
	$contenu = $_POST["contenu"];
	$contenu = smileys($contenu);
	$contenu = formatage($contenu);
	$contenu = nl2br($contenu);
	$contenu = stripslashes($contenu);
	echo $contenu;
}
?>