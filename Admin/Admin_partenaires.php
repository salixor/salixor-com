<?php
$titre_page_nom = "Administration des Partenaires";
include("../Header.php");


if(isset($ID_MEMBRE)) {
	if($NIVEAU_MEMBRE == 5) {

		if(isset($_GET['Action'])) {
		if($_GET['Action'] == "Ajouter" OR $_GET['Action'] == "Modifier" OR $_GET['Action'] == "Supprimer" OR $_GET['Action'] == "Gerer") {
			$Action = $_GET['Action'];
		 
			switch($Action) {
			 
				case "Ajouter":
				
					if(!empty($_POST['titre']) AND !empty($_POST['image']) AND !empty($_POST['description']) AND !empty($_POST['lien'])) {
				 
						extract($_POST);
							$titre = $titre;
							$image = $image;
							$description = addslashes($description);
							$lien = $lien;
						
						$sql = 'INSERT INTO partenaires(titre, image, description, lien) VALUES ("'.$titre.'", "'.$image.'", "'.$description.'", "'.$lien.'")';
						$req = query($sql); ?>
						
						<h2 class="titreSecond"><img src="../Images/icones_pages/Administration.png" alt="BDD" /> Ajout du Partenaire "<?php echo stripslashes($titre) ?>"</h2>
						<div class="encadre_infos">
						<p>Ajout du Partenaire "<?php echo $titre ?>" effectué avec succès !</p>
						<p><a href="Admin_partenaires.php">Retour</a> à l'Administration des Partenaires.</p>
						</div>
				 
					<?php }
					else { ?>
					
						<h2 class="titreSecond"><img src="../Images/icones_pages/Administration.png" alt="BDD" /> Ajout d'un Partenaire</h2>
						
						<div align="center">
						<form method="post" action="Admin_partenaires.php?Action=Ajouter">
					 
						<p><input type="text" value="Titre" name="titre" style="width:700px;" onfocus="if(this.value=='Titre')this.value=''" onblur="if(this.value=='')this.value='Titre'" /></p>
						<p><input type="text" value="Image" name="image" style="width:700px;" onfocus="if(this.value=='Image')this.value=''" onblur="if(this.value=='')this.value='Image'" /></p>
						<p><textarea name="description" style="width:700px; height:200px;"
						onfocus="if(this.value=='Description')this.value=''" onblur="if(this.value=='')this.value='Description'">Description</textarea></p>
						<p><input type="text" value="Lien" name="lien" style="width:700px;" onfocus="if(this.value=='Lien')this.value=''" onblur="if(this.value=='')this.value='Lien'" /></p>
						<p><input type="submit" value="Ajouter"/></p>
						</form>
						</div>
						
					<?php }
					
				break;
				
				case "Gerer":
					
					if(isset($_GET['page']) && is_numeric($_GET['page']))
						$page = $_GET['page'];
					else {
						$page = 1;
					}
					
					$pagination = 10;
					$limit_start = ($page - 1) * $pagination;
					
					$nb_total = query("SELECT COUNT(*) AS nb_total FROM partenaires");
					$nb_total = mysql_fetch_array($nb_total);
					$nb_total = $nb_total['nb_total'];
					$nb_pages = ceil($nb_total / $pagination);
					
					$req = query("SELECT * FROM partenaires ORDER BY id DESC LIMIT $limit_start, $pagination"); ?>
					
					<h2 class="titreSecond"><img src="../Images/icones_pages/Administration.png" alt="BDD" /> Gestion des Partenaires</h2>
					
					<table>
					<tr>
					<th class="titre" width="250px">Titre</th>
					<th class="titre" width="160px">Lien</th>
					<th class="titre" width="45px" align="center">Modi.</th>
					<th class="titre" width="45px" align="center">Supp.</th>
					</tr>	 
					
					<?php while($data = mysql_fetch_assoc($req)) { ?>
						
						<tr>
						<td><?php echo stripslashes($data['titre']) ?></td>
						<td><?php echo stripslashes($data['lien']) ?></td>
						<td align="center"><a href="Admin_partenaires.php?Action=Modifier&amp;id=<?php echo $data['id'] ?>"><img src="../Images/icones_pages/Editer_Partenaire.png" alt="Modifier" /></a></td>
						<td align="center"><a href="Admin_partenaires.php?Action=Supprimer&amp;id=<?php echo $data['id'] ?>"><img src="../Images/icones_pages/Supprimer_Partenaire.png" alt="Supprimer" /></a></td>
						</tr>
				 
					<?php } ?>
				
					</table>
					<br />
					
					<?php if(isset ($_GET['page']) AND ($_GET['page'] > $nb_pages)) { ?>
						<p align="center">Cette page n'existe pas ... <a href="http://www.salixor.com/">Retour à l'accueil</a></p><br />");
					<?php }
					else { 
						echo "<p class=\"pagination\" align=\"center\"> ". pagination($page, $nb_pages, "?Action=Gerer&page=%d") ." </p><br />";
					}
			
				break;
			 
				case "Modifier":
			 
					if(isset($_GET['id'])) {
						$id = intval($_GET['id']);
				 
						if(!empty($_POST['titre']) AND !empty($_POST['image']) AND !empty($_POST['description']) AND !empty($_POST['lien'])) {
					 
							extract($_POST);
								$titre = $titre;
								$image = $image;
								$description = addslashes($description);
								$lien = $lien;
							 
							$sql = 'UPDATE partenaires SET titre="'.$titre.'", image="'.$image.'", description="'.$description.'", lien="'.$lien.'" WHERE id='.$id.'';
							$req = query($sql); ?>
						 
							<h2 class="titreSecond"><img src="../Images/icones_pages/Administration.png" alt="BDD" /> Modification du Partenaire "<?php echo stripslashes($titre) ?>"</h2>
							<div class="encadre_infos">
							<p>Modification du Partenaire "<?php echo stripslashes($titre) ?>" effectuée avec succès !</p>
							<p><a href="Admin_partenaires.php">Retour</a> à l'Administration des Partenaires.</p>
							</div>
					 
						<?php }
					 
						else {
					 
							$req = query('SELECT * FROM partenaires WHERE id='.$id.'');
							$data = mysql_fetch_assoc($req); ?>
							
							<h2 class="titreSecond"><img src="../Images/icones_pages/Administration.png" alt="BDD" /> Modification d'un Partenaire</h2>
									
						<div align="center">
						<form method="post" action="Admin_partenaires.php?Action=Modifier&amp;id=<?php echo $id; ?>">
					 
						<p><input type="text" value="<?php echo $data['titre'] ?>" name="titre" style="width:700px;" /></p>
						<p><input type="text" value="<?php echo $data['image'] ?>" name="image" style="width:700px;" /></p>
						<p><textarea name="description" style="width:700px; height:200px;"><?php echo $data['description'] ?></textarea></p>
						<p><input type="text" value="<?php echo $data['lien'] ?>" name="lien" style="width:700px;" /></p>
						<p><input type="submit" value="Modifier"/></p>
						</form>
						</div>
					
						<?php }
					}
				 
				break;
			 
				case "Supprimer":
			 
					if(isset($_GET['id'])) {
						$id = intval($_GET['id']);
					 
						$sql_titre = 'SELECT titre FROM partenaires WHERE id='.$id.'';
						$sql = 'DELETE FROM partenaires WHERE id='.$id.'';
						
						$req_titre = query($sql_titre);
						query($sql);
						
						$data_titre = mysql_fetch_assoc($req_titre);
						$titre = $data_titre['titre']; ?>
						
						<h2 class="titreSecond"><img src="../Images/icones_pages/Administration.png" alt="BDD" /> Suppresion du Partenaire "<?php echo stripslashes($titre) ?>"</h2>
						<div class="encadre_infos">
						<p>Suppression du Partenaire "<?php echo $titre ?>" effectuée avec succès !</p>
						<p><a href="Admin_partenaires.php">Retour</a> à l'Administration des Partenaires.</p>
						</div>
				 
					<?php }
					
				break;
			 
			}
		}
		}
		else { ?>

			<h2 class="titreSecond"><img src="../Images/icones_pages/Administration.png" alt="BDD" /> Administration des Partenaires</h2>
			
			<div class="lienscubes">
			<a href="Admin_partenaires.php?Action=Ajouter">Ajouter un Partenaire</a>
			<a href="Admin_partenaires.php?Action=Gerer">Gérer les Partenaires</a>
			</div>

		<?php }
	}
	else {
		echo '<p class="encadreInfos"><span class="left">Information</span><span class="right">Vous n\'avez pas l\'autorisation de voir cette page.</span></p>';
	}
}
else {
	echo '<p class="encadreInfos"><span class="left">Information</span><span class="right">Vous n\'avez pas l\'autorisation de voir cette page.</span></p>';
}

include("../Footer.php"); ?>