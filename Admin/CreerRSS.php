<?php
$xml = '<?xml version="1.0" encoding="UTF-8"?>';
$xml .= '<rss version="2.0">'; 
$xml .= '<channel>'; 
$xml .= '<title>Flux RSS de Salixor.com</title>';
$xml .= '<link>http://www.salixor.com</link>';
$xml .= '<description>Suivez grâce à ce flux l\'actualité du site mais aussi l\'actualité Dofusienne sans accéder directement au site.</description>';
$xml .= '<copyright>© 2010 - 2011, Salixor.com - Les illustrations sont la propriété de leurs auteurs respectifs.</copyright>';
$xml .= '<language>fr</language>';
$xml .= '<image>';
	$xml .= '<title>Flux RSS de Salixor.com</title>';
	$xml .= '<url>http://www.salixor.com/Images/General/FluxRSS.jpg</url>';
	$xml .= '<link>http://www.salixor.com</link>';
$xml .= '</image>';
$today = date("D, d M Y H:i:s +0100");
$xml .= '<pubDate>'.$today.'</pubDate>';

require_once('../Includes/Fonctions.php'); 
require_once('../Includes/Config.php'); 

$resultat_requete = mysql_query("SELECT * FROM news ORDER BY id DESC LIMIT 0, 10");

while($data = mysql_fetch_assoc($resultat_requete)) { 
	$titre = stripslashes(utf8_encode($data["titre"]));
	$adresse = 'http://www.salixor.com?News='.$data["id"].'&amp;Titre='.optimiser_titre_et_classe($titre).'';
	$contenu = formatage(smileys(nl2br(stripslashes($data["contenu"]))));
	$date = $data["date"];
	$datephp = date("D, d M Y H:i:s +0100", strtotime($date));
	
	$xml .= '<item>';
		$xml .= '<title><![CDATA['.$titre.']]></title>';
		$xml .= '<link>'.$adresse.'</link>';
		$xml .= '<guid>'.$adresse.'</guid>';
		$xml .= '<pubDate>'.$datephp.'</pubDate>'; 
		$xml .= '<description><![CDATA['.$contenu.']]></description>';
	$xml .= '</item>'; 
}

$xml .= '</channel>';
$xml .= '</rss>';

$fp = fopen("../Includes/RSS.xml", 'w+');
fputs($fp, $xml);
fclose($fp);
?>