<?php $titre_page_nom = "Partenaires";
include("Header.php");
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Partenaires.png" alt="Partenaires" /> Partenaires</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH ?>">Accueil</a> > Les Partenaires</p>

<p class="encadreInfos"><span class="right">
Cette page regroupe les partenaires du site. Je vous invite à aller visiter leurs sites, vous ne regretterez pas le détour.
Si vous souhaite devenir partenaire, n'hésitez pas à passer par le formulaire de <a href="Contact.php">Contact</a>.</span></p>

<?php
$requete = $baseDeDonnes->query("SELECT * FROM partenaires ORDER BY id ASC");
$requete->setFetchMode(PDO::FETCH_OBJ);

while($data = $requete->fetch()) {
	$titre = $data->titre;
	$imageBig = utf8_encode($data->image); 
	$imageMini = utf8_encode($data->image_mini); 
	$description = stripslashes($data->description); 
	$lien = utf8_encode($data->lien);
	
	echo '<div class="news">';
		echo '
		<div class="imageLeft">
			<a href="'.$imageBig.'" class="fancybox-classique"><img src="'.$imageMini.'" alt="" /></a>
		</div>';
		
		echo '<div class="head">';
			echo '<h4><a href="'.$lien.'">'.$titre.'</a></h4>';
			echo '<div class="infos">';
				echo '
				<span class="date"><img src="Images/icones_pages/LittlesIcones/France.png" alt="" /> Français</span>
				<span class="tag"><a href="'.$lien.'"><img src="Images/icones_pages/LittlesIcones/TitreEdit.png" alt="" /> Voir le site</a></span>';
			echo '</div>';
		echo '</div>';
		
		echo '<div class="contenu">';
			echo $description;
		echo '</div>';

		echo '<div style="clear:left;"></div>';
	echo '</div>';
}

include("Footer.php");
?>