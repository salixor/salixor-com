<div class="sidebar">  

<?php
if(!defined('MAINTENANCE')) {
	$afficher = true;
}
else {
	if(isset($NIVEAU_MEMBRE)) {
		if($NIVEAU_MEMBRE >= 5) {
			$afficher = true;
		}
		else {
			$afficher = false;
		}
	}
	else {
		$afficher = false;
	}
}

if($afficher == true) {
?>
<div class="stick" id="stickMenu">
	<h3 class="titlestick" id="titlestickMenu"><img src="<?php echo ABSPATH; ?>Images/General/MenuSide.png" alt="" />Menu</h3>
	<div class="contenustick" style="padding:0;">
		<ul id="menu">
			<li class="titreMenu" style="margin-top:7px;">Général</li>
				<li><a href="<?php echo ABSPATH; ?>">Accueil</a></li>
				<li><a href="<?php echo ABSPATH; ?>Forum">Forum</a></li>
				<li><a href="<?php echo ABSPATH; ?>Chat">Chat</a></li>

			<li class="titreMenu">Encyclopédie</li>
				<li><a href="<?php echo ABSPATH; ?>Guides/Donjons/">Les Donjons</a></li>
				<li><a href="<?php echo ABSPATH; ?>Guides/Parchemins">Les Parchemins</a></li>
		
			<li class="titreMenu">Équipements</a>
			
				<li class="openMenu"><a href="<?php echo ABSPATH; ?>Equipements">Les Items</a></li>
					<ul class="sousMenu">
						<a href="<?php echo ABSPATH; ?>Equipements?Page=Amulette&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Amulettes</li></a>		
						<a href="<?php echo ABSPATH; ?>Equipements?Page=Anneau&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Anneaux</li></a>
						<a href="<?php echo ABSPATH; ?>Equipements?Page=Botte&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Bottes</li></a>
						<a href="<?php echo ABSPATH; ?>Equipements?Page=Cape&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Capes</li></a>
						<a href="<?php echo ABSPATH; ?>Equipements?Page=Ceinture&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Ceintures</li></a>
						<a href="<?php echo ABSPATH; ?>Equipements?Page=Chapeau&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Chapeaux</li></a>
						<a href="<?php echo ABSPATH; ?>Equipements?Page=Sac"><li>Sacs à Dos</li></a>
						<a href="<?php echo ABSPATH; ?>Equipements?Page=Bouclier"><li>Boucliers</li></a>
					</ul>
					
				<li class="openMenu"><a href="<?php echo ABSPATH; ?>Equipements">Les Armes</a></li>
					<ul class="sousMenu">
						<a href="<?php echo ABSPATH; ?>Equipements?Page=Arc&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Arcs</li></a>			
						<a href="<?php echo ABSPATH; ?>Equipements?Page=Baguette&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Baguettes</li></a>			
						<a href="<?php echo ABSPATH; ?>Equipements?Page=Baton&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Batons</li></a>		
						<a href="<?php echo ABSPATH; ?>Equipements?Page=Dague&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Dagues</li></a>		
						<a href="<?php echo ABSPATH; ?>Equipements?Page=Epee&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Épées</li></a>		
						<a href="<?php echo ABSPATH; ?>Equipements?Page=Marteau&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Marteaux</li></a>			
						<a href="<?php echo ABSPATH; ?>Equipements?Page=Pelle&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Pelles</li></a>			
						<a href="<?php echo ABSPATH; ?>Equipements?Page=Hache&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Haches</li></a>		
						<a href="<?php echo ABSPATH; ?>Equipements?Page=Pioche"><li>Pioches</li></a>			
						<a href="<?php echo ABSPATH; ?>Equipements?Page=Faux"><li>Faux</li></a>	
					</ul>
					
				<li><a href="<?php echo ABSPATH; ?>Equipements?Page=Dofus">Les Dofus</a></li>
				<li><a href="<?php echo ABSPATH; ?>Equipements/Familiers.php">Les Familiers</a></li>
				<li><a href="<?php echo ABSPATH; ?>Equipements/Panoplies.php">Les Panoplies</a></li>
			
			<li class="titreMenu">Dossiers et Outils</li>
				<li><a href="<?php echo ABSPATH; ?>Dossiers/Elevage.php">L'élevage de Dragodindes</a></li>
				<li><a href="<?php echo ABSPATH; ?>Aides/Calculateur.php">Calculateur de dégats</a></li>
				
			<li class="titreMenu">Membres</li>
				<li><a href="<?php echo ABSPATH; ?>Membres/Membres.php">Liste des Membres</a></li>
				<li><a href="<?php echo ABSPATH; ?>Membres/Personnages.php">Liste des Personnages</a></li>
				<li><a href="<?php echo ABSPATH; ?>Membres/Guildes">Liste des Guildes</a></li>
				
			<?php
			if(isset($ID_MEMBRE)) {
				echo'
				<li class="titreMenu">Mes Participations</li>
					<li><a href="'.ABSPATH.'Membres/ListeEquipements.php">Mes équipements ajoutés</a></li>
					<li><a href="'.ABSPATH.'Admin/Admin_equipements.php">Ajouter un équipement</a></li>';
			}
			?>
				
			<li class="titreMenu">Divers</li>
				<li><a href="<?php echo ABSPATH; ?>Sondage.php?Action=Question">Les Sondages</a></li>
				<li><a href="<?php echo ABSPATH; ?>Partenaires.php">Les Partenaires</a></li>
				<li><a href="<?php echo ABSPATH; ?>LivreOr.php">Le Livre d'Or</a></li>
		</ul>
	</div>
</div>
<?php 
}

if(!defined('Afficher_Equipement') && $afficher == true) { ?>
	<div class="stick" id="StickEquipements">
		<h3 class="titlestick" id="titlestick_items"><img src="<?php echo ABSPATH; ?>Images/General/Rand.png" alt="" />Connaissez-vous ?</h3>
		<div class="contenustick">
			<?php
			include('Includes/itemSidebar.php');
			?>
			
		</div>
</div>	
<?php }

/*


///////////////////////////////COMMENTAIRES


<div class="stick">
	<div id="StickCommentaires">
		<h3 class="titlestick" id="titlestick_commentaires"><img src="<?php echo ABSPATH; ?>Images/General/Comment.png" alt="" />Commentaires</h3>
		<div class="contenustick">
			<div class="sidebarCommentaires">
			<?php
			$nbCommentaires = 16;
			
			$requete = $baseDeDonnes->prepare('
			SELECT commentaires.*, membres.*, commentaires.id AS comm_id, commentaires_reponse.pseudo AS membre_pseudo_reponse, news.*
			FROM commentaires
			LEFT JOIN commentaires AS commentaires_reponse ON commentaires_reponse.id = commentaires.id_reponse
			LEFT JOIN membres ON commentaires.id_membre = membres.membre_id
			LEFT JOIN news ON news.id = commentaires.id_news
			ORDER BY comm_id DESC
			LIMIT 0, :nb');
			$requete->bindValue(':nb', $nbCommentaires, PDO::PARAM_INT);
			$requete->execute();
			
			$i = 0;
			while($data = $requete->fetch()) {
				if($i == 0) { echo '<div class="blocCommentaires">'; }
				if($i == 4 || $i == 8 || $i == 12) { echo '<div class="blocCommentaires" style="display:none;">'; }
				
				if($data["id_reponse"] != 0) {
					$pseudoReponse  =  stripslashes($data['membre_pseudo_reponse']);
					$idReponse      =  intval($data['comm_id']);
					$titreReponse   =  optimiser_titre_et_classe(utf8_encode(stripslashes($data['titre'])));
					$idNewsReponse  =  intval($data['id_news']); 

					$reponse        = '<br />&raquo; En réponse à <a href="'.ABSPATH.'?News='.$idNewsReponse.'&amp;Titre='.$titreReponse.'#Commentaire'.$idReponse.'">'.$pseudoReponse.'</a>';
				}
				else {
					$reponse        = '';
				}
				
				$idNews      =  intval($data['id_news']); 
				$idComm      =  intval($data['comm_id']);
				$titreNews   =  utf8_encode(stripslashes($data['titre']));
				$titreNewsFinal   =  utf8_encode(stripslashes($data['titre']));
				$pseudo      =  stripslashes($data['pseudo']);
				$date        =  afficherDateDifferenceNoTime(strtotime($data["date"]));
				
				if(strlen($titreNewsFinal) > 30) {
					$titreNewsFinal = substr($titreNewsFinal, 0, 30); 
					$titreNewsFinal .= "...";
				}

				echo '<div class="stickComm">';	  
				echo '<p><a href="'.ABSPATH.'?News='.$idNews.'&amp;Titre='.optimiser_titre_et_classe($titreNews).'">'.$titreNewsFinal.'</a></p>';
				echo '<span class="informationComm">'.$date.' par <a href="'.ABSPATH.'?News='.$idNews.'&amp;Titre='.optimiser_titre_et_classe($titreNews).'#Commentaire'.$idComm.'">'.$pseudo.'</a>'.$reponse.'</span>';
				echo '</div>';
				
				if($i == 3 || $i == 7 || $i == 11 || $i == 15) { echo '</div>'; }

				$i++;
			}
			?>   
			</div>
		</div>
	</div>
</div>	
*/ ?>

</div>