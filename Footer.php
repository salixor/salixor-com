	<!--Inclusions de tous les scripts!-->
	<script type="text/javascript" src="<?php echo ABSPATH; ?>Scripts/jquery-cookie.js"></script>
	<script type="text/javascript" src="<?php echo ABSPATH; ?>Scripts/jquery.tipsy.js"></script>
	<script type="text/javascript" src="<?php echo ABSPATH; ?>Scripts/jquery.validate.js"></script>


	<!--Script!-->
	<script type="text/javascript">
	$(".delete").tipsy({title: 'title', gravity: 's'});
	$(".click_ajouter_equipement").tipsy({title: 'title', gravity: 'w'});
	$('.commentaire_front a[title*="En réponse"], .commentaire_front a[title*="Répondre"], .commentaire_front a[title*="News en relation avec ce commentaire"], .commentaire_front a[title*="Vous répondez à"]').tipsy({title: 'title', gravity: 'e'});
	$(".suiteEquipement").tipsy({title: 'title', gravity: 'w'});
	$(".chatMessages .message .buttons").tipsy({title: 'title', gravity: 's'});
	</script>

	<div class="clear"></div>

	<div class="rcdBanner">
		<?php
			if(!defined('RCDofus')) {
				echo '<script type="text/javascript" src="http://script.rcdofus.co.cc/nabspcjs.php?id=85"></script>';
				echo '<script type="text/javascript">OA_show(154);</script>';
				echo '<noscript>
					<a target="_blank" href="http://script.rcdofus.co.cc/ck.php?n=44c6a4a">
						<img alt="" src="http://script.rcdofus.co.cc/avw.php?zoneid=154&amp;n=44c6a4a" />
					</a>
				</noscript>';
			}
			/*else {
				echo '<span class="erreur">Bannière temporairement désactivée</span>';
			}*/
		?>
	</div>

	<?php include_once("Sidebar.php"); ?>
	
</div>  <!--Fin du conteneur!-->

<div class="clear"></div>

<div class="footer" id="Bas">
	<div class="conteneurPrincipal">
		<div class="contenuPrincipal">
		
			<div class="blocFooter">
				<h5>Suivez-Nous</h5>					
					<div class="boutons">
						<a href="http://www.twitter.com/Salixor"><div class="icone twitter"></div></a>      <div class="texte"><span class="fleche"></span>Suivez-nous sur Twitter.</div>
					</div>
					<span class="clear"></span>
					
					<div class="boutons">
						<a href="http://salixor.com/Includes/RSS.xml"><div class="icone rss"></div></a>     <div class="texte"><span class="fleche"></span>Abonnez-vous au flux RSS.</div>
					</div>
					<span class="clear"></span>
			</div>
			
			<div class="blocFooter" id="TweetsReceiver">
				<h5>Tweets récents</h3>
				<div class="listeTweets">
					<p class="aTweet">
						<img src="<?php echo ABSPATH ?>Images/icones_pages/LittlesIcones/Erreur.png" alt="" style="margin:3px 0 -3px;" /> Système indisponible.
						<span class="date">Javascript est requis.</span>
					</p>
				</div>
			</div>
			
			<div class="blocFooter">
				<h5>A propos</h5>
				<div class="infosSite">
					<img src="<?php echo ABSPATH; ?>Images/General/Logo.png" alt="" />
					Salixor.com est un fansite sur le jeu Dofus.<br />Retrouvez des <b>outils</b>, des <b>aides</b>, une <b>communauté</b> et une <b>base de données</b> assez riche sur le jeu.
					<span class="infosSupp">
					<?php 
						define('TEMPS_FIN', microtime(true));
						$TIME_GENERATE = round(TEMPS_FIN - TEMPS_DEBUT, 3);
						
						echo "Page générée en ".$TIME_GENERATE." secondes.<br />";
						
						$nbRequetes = $baseDeDonnes->count() + $nb_requetes;
						
						if($nbRequetes == 0)       echo "Aucune requête exécutée.";
						elseif($nbRequetes == 1)   echo "1 requête exécutée."; 
						else                       echo $nbRequetes." requêtes exécutées.";
						echo "<br />Version ".VERSION." - ".$GLOBALS["TABLEAU_VERSIONS"][VERSION];
					?>
					</span>
				</div>
			</div>
			
			<div class="clear"></div>
		</div>
	</div>
	
	<div class="contenuSecondaire">
		<span class="left">Salixor © 2010 - 2012. Site créé en intégralité par MrSalixor.</span><span class="right">Les illustrations sont la propriété de leurs auteurs respectifs.</span>
	</div>
</div>

</body>
</html>