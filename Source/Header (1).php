<?php
//define('MAINTENANCE', true);
define('TEMPS_DEBUT', microtime(true));
//define('RCDofus', 'ok');
define('VERSION', '2.2');

//Les fichiers utiles
require_once('Includes/Fonctions.php');
require_once('Includes/Config.php');

//Les variables utiles un peu partout
$MEMBRES_RANGS = array(0 => "Membre Banni", 1 => "Non Membre", 2 => "Membre", 3 => "Béta Testeur", 4 => "Modérateur", 5 => "Administrateur");
$MEMBRES_RANGS_IMAGES = array(0 => "Banni", 2 => "Membre", 3 => "BetaTesteur", 4 => "Moderateur", 5 => "Administrateur");
$GLOBALS["TABLEAU_VERSIONS"] = array("1.0" => "&quot;Commencement&quot;",
									 "1.1" => "Goultarminator II",
									 "1.2" => "Les News",
									 "1.3" => "&quot;Nowel&quot;",
									 "1.4" => "&quot;Fonte des Glaces&quot;",
									 "1.5" => "Refonte Graphique",
									 "1.6" => "AJAXisation et modifications PHP",
									 "1.6.1" => "Amélioration de la version 1.6",
									 "1.7.a01" => "Espace Membre - Version Alpha 01",
									 "1.7.a02" => "Espace Membre - Version Alpha 02",
									 "1.7.a03" => "Espace Membre - Version Alpha 03",
									 "1.7.b01" => "Espace Membre - Version Beta 01",
									 "1.7.b02" => "Espace Membre - Beta 02",
									 "1.7" => "Espace Membre",
									 "1.8" => "Pâques !",
									 "1.8.1" => "Panoplies",
									 "1.8.2" => "Le Chat",
									 "1.8.3" => "Nouveau Menu",
									 "1.9" => "Les Dossiers",
									 "1.10.b00" => "Le Forum - Premier Test",
									 "1.10.b01" => "Le Forum - Béta Test 01",
									 "1.10" => "Le Forum",
									 "1.10.1" => "&quot;Salix'kode Complete&quot;",
									 "2.0" => "&quot;Happy Birthday Salixor.com&quot;",
									 "2.1" => "Le Goultarminator",
									 "2.2" => "Les Guildes");

//Si le site est en maintenance
if(defined('MAINTENANCE')) {
	if(isset($NIVEAU_MEMBRE)) {
		if($NIVEAU_MEMBRE < 5) {
			if(!stripos($_SERVER['SCRIPT_NAME'], "Admin")) {
				if($titre_page_nom != "Maintenance" && ABSPATH == 'http://localhost/Salixor/')     header("Location: http://localhost/Salixor/Erreurs/Maintenance.php");
				if($titre_page_nom != "Maintenance" && ABSPATH == 'http://www.salixor.com/')       header("Location: http://www.salixor.com/Erreurs/Maintenance.php");
			}
		}
	}
	elseif(!isset($NIVEAU_MEMBRE)) {
		if(!stripos($_SERVER['SCRIPT_NAME'], "Admin")) {
			if($titre_page_nom != "Maintenance" && ABSPATH == 'http://localhost/Salixor/')         header("Location: http://localhost/Salixor/Erreurs/Maintenance.php");
			if($titre_page_nom != "Maintenance" && ABSPATH == 'http://www.salixor.com/')           header("Location: http://www.salixor.com/Erreurs/Maintenance.php");
		}
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title><?php echo $titre_page_nom." - Salixor.com"; ?></title>

<meta name="description" content="" />
<meta name="keywords" content="" />

<meta http-equiv="Content-Type" content="charset=UTF-8" />
<meta http-equiv="Content-language" content="fr" />
<meta name="robots" content="follow, index, all" />
<meta name="description" content="Salixor.com, c'est une encyclopédie crée spécialement pour les Dofusiens avide d'informations sur le jeu. Retrouvez un tas de ressources sur le jeu aisi qu'une partie communautaire que vous pouvez rejoindre. Des outils sont disponibles sur le site, pour améliorer votre expérience globale du jeu." />
<meta name="keywords" content="dofus, salixor, ankama, mmorpg, communauté, aide, astuces, wakfu, donjons, espace membre" />

<link href="<?php echo ABSPATH; ?>Styles.css" rel="stylesheet" type="text/css" />
<?php
//temp
//echo '<link href="'.ABSPATH.'StylesEte.css" rel="stylesheet" type="text/css" />';

if(isset($ID_MEMBRE)) {
	$requete = $baseDeDonnes->prepare("SELECT membre_style, membre_avatar FROM membres WHERE membre_id = :id");
	$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
	$requete->execute();
	
	$dataStyle = $requete->fetch();

	$style = $dataStyle["membre_style"];	
	
	if($style == "Noel")          echo '<link href="'.ABSPATH.'StylesNoel.css" rel="stylesheet" type="text/css" />';
	if($style == "Ete")    		  echo '<link href="'.ABSPATH.'StylesEte.css" rel="stylesheet" type="text/css" />';
}
?>

<link rel="shortcut icon" type="image/x-icon" href="<?php echo ABSPATH; ?>Images/Favicon.ico" />
<link rel="icon" type="image/png" href="<?php echo ABSPATH; ?>Images/Favicon.png" />
<link rel="apple-touch-icon-precomposed" href="<?php echo ABSPATH; ?>Images/Apple_icon-precomposed.png"/>

<link href="<?php echo ABSPATH; ?>Scripts/tipsy.css" rel="stylesheet" type="text/css" />
<link href="<?php echo ABSPATH; ?>Scripts/Fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" />

<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php echo ABSPATH; ?>Includes/RSS.xml" />

<script type="text/javascript" src="<?php echo ABSPATH; ?>Scripts/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="<?php echo ABSPATH; ?>Scripts/Fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="<?php echo ABSPATH; ?>Scripts/AJAX.js"></script>
<script type="text/javascript" src="<?php echo ABSPATH; ?>Scripts/general.js"></script>
<script type="text/javascript" src="<?php echo ABSPATH; ?>Scripts/jquery-ui-1.8.12.custom.min.js"></script>
<script type="text/javascript" src="<?php echo ABSPATH; ?>Membres/ModificationEquipementPersonnage.jquery.ajax.js"></script>
<script type="text/javascript" src="<?php echo ABSPATH; ?>Scripts/magalerie.jquery.js"></script>

</head>

<body>

<?php
//echo '<iframe src="http://www.dofus.com/fr/perso/mylaise/salixor-319033300026" style="display:none;">Non pris en compte</iframe>';

if(isset($ID_MEMBRE)) {
	$requete = $baseDeDonnes->prepare("SELECT COUNT(*) AS nb_messages_non_lus FROM messagerie WHERE destinataire = :id AND vu = '0'");
	$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
	$requete->execute();
	
	$data = $requete->fetch();
	
	$nbNonLus = '';
	$nbMessages = '';
	if($data["nb_messages_non_lus"] > 0) {
		if($data["nb_messages_non_lus"] == 1) {
			$nbMessages = $data["nb_messages_non_lus"].' message';
			$nbNonLus = '<div class="infoMessages" title="1 message non lu">1</div>';
		}
		if($data["nb_messages_non_lus"] > 1) {
			$nbMessages = $data["nb_messages_non_lus"].' messages';
			$nbNonLus = '<div class="infoMessages" title="'.$data["nb_messages_non_lus"].' messages non lus">'.$data["nb_messages_non_lus"].'</div>';
		}
	}
	
	$requete->closeCursor();

	echo '<span style="display:none;" id="MEMBRE_ID">'.$ID_MEMBRE.'</span>';
	echo '<span style="display:none;" id="MEMBRE_PSEUDO">'.$PSEUDO_MEMBRE.'</span>';
	
	if(!empty($dataStyle["membre_avatar"]))      $avatar = ABSPATH."Images/Avatars/".$dataStyle["membre_avatar"];
	else                                         $avatar = ABSPATH."Images/Avatars/avatar_normal.png";
	
	$avatar = '<img src='.$avatar.' alt="" class="avatarMembre" />';
	
	if($NIVEAU_MEMBRE >= 5)     $administration = '<a href="'.ABSPATH.'Admin" class="admin">Administration</a>';
	else                        $administration = '';

	echo '
	<div class="navigationGenerale">
		<span class="left">
			<span class="deroulerMenu">
				'.$avatar.' '.$PSEUDO_MEMBRE.'
				<span class="fleche"></span>
			</span>
			
			<span class="bouton messages">
				'.$nbNonLus.' Mes Messages
				<span class="fleche"></span>
				
				<ul class="menuMessages">';
									
					$requete = $baseDeDonnes->prepare("
					SELECT * FROM messagerie
					LEFT JOIN membres ON messagerie.expediteur = membres.membre_id
					WHERE destinataire = :id ORDER BY id DESC LIMIT 0, 5");
					$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
					$requete->execute();
					
					if($requete->rowCount() > 0) {
						while($data = $requete->fetch()) {
							if(!empty($data["membre_avatar"]))      $avatar = ABSPATH."Images/Avatars/".$data["membre_avatar"];
							else                                    $avatar = ABSPATH."Images/Avatars/avatar_normal.png";
							
							$avatar = '<img class="avatar" src='.$avatar.' alt="" />';
							
							if($data["vu"] == '1') $rouge = '';
							else                   $rouge = 'class="rouge"';

							echo '
							<a href="'.ABSPATH.'Membres/Messagerie.php?Action=Voir&amp;Message='.$data["id"].'" '.$rouge.'>
								<li>
									'.$avatar.'
									<div class="infosMessage">
										<div class="pseudo">'.stripslashes($data["membre_pseudo"]).'<span class="subject">'.stripslashes($data["sujet"]).'</span></div>
										<div class="message">'.smileys(tronquer(stripslashes($data["message"]), 50)).'</div>
									</div>
									<div class="clear"></div>
								</li>
							</a>';
						}
					}
					else {
						echo '
						<a href="'.ABSPATH.'Membres/Messagerie.php?Action=Envoyer">
							<li align="center">
								Aucun message. Cliquez pour en composer un.
							</li>
						</a>';
					}
					
					echo '<div class="others">';
						if(!empty($nbMessages))      echo '<a href="javascript:void(0)" class="read-messages admin">Marquer tout comme lu</a>';
						echo '
						<a href="'.ABSPATH.'Membres/Messagerie.php" class="green">Ma Messagerie</a>
						<div class="clear"></div>
					</div>
				</ul>
			</span>
			
			<span class="selectStyle" href="'.ABSPATH.'Membres/Membres.php">
				Changer de thème
				<span class="fleche"></span>
				
				<ul class="menuStyle">';
					$arrayRels = array("Normal", "Ete", "Noel");
					$arrayThemes = array("Thème Normal", "Thème d'Été", "Thème de Noël");
					$i = 0;
					$final = '';
					
					foreach($arrayRels as $rel) {
						$theme = $arrayThemes[$i];
						
						if($style == $rel) {
							$final = '
							<a href="javascript:void(0);" rel="'.$rel.'" class="active">
								<li>
									'.$theme.'
									<span class="fleche"></span>
								</li>
							</a>';
						}
						else {
							echo '
							<a href="javascript:void(0);" rel="'.$rel.'">
								<li>
									'.$theme.'
									<span class="fleche"></span>
								</li>
							</a>';
						}
						
						$i++;
					}
					
					echo $final.'
				</ul>
			</span>
			
			<span class="leftInterne">
				<a class="bouton" href="'.ABSPATH.'Membres/Profil.php?id='.$ID_MEMBRE.'">Mon Profil</a>
				<a class="bouton" href="'.ABSPATH.'Membres/Membres.php">Les Membres</a>
				<a class="bouton" href="'.ABSPATH.'Membres/Personnages.php">Les Personnages</a>
				<a class="bouton" href="'.ABSPATH.'Membres/Guildes">Les Guildes</a>
			</span>
		</span>
		
		<span class="right">
			<a href="#Haut" class="bouton" style="opacity:0; cursor:default;">Haut</a>
			<a href="#Bas" class="bouton">Bas</a>
		</span>
		
		<ul class="menuProfil">
			<a href="'.ABSPATH.'Membres/Modifier_Profil.php?Action=MotDePasse">
				<li>
					<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/MotDePasse.png" alt="" /> Modifier mon Mot de Passe
					<span class="fleche"></span>
				</li>
			</a>
			<a href="'.ABSPATH.'Membres/Modifier_Profil.php?Action=Email">
				<li>
					<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/EmailEdit.png" alt="" /> Modifier mon adresse Mail
					<span class="fleche"></span>
				</li>
			</a>
			<a href="'.ABSPATH.'Membres/Modifier_Profil.php?Action=Titre">
				<li>
					<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/TitreEdit.png" alt="" /> Modifier le Titre
					<span class="fleche"></span>
				</li>
			</a>
			<a href="'.ABSPATH.'Membres/Modifier_Profil.php?Action=Signature">
				<li>
					<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Signature.png" alt="" /> Modifier ma Signature
					<span class="fleche"></span>
				</li>
			</a>
			<a href="'.ABSPATH.'Membres/Modifier_Profil.php?Action=Citation">
				<li>
					<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Citation.png" alt="" /> Modifier ma Citation
					<span class="fleche"></span>
				</li>
			</a>
			<a href="'.ABSPATH.'Membres/Modifier_Profil.php?Action=Dofus">
				<li>
					<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Personnages.png" alt="" /> Gérer mes Personnages
					<span class="fleche"></span>
				</li>
			</a>
			<a href="'.ABSPATH.'Membres/Modifier_Profil.php?Action=Publiques">
				<li>
					<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Publiques.png" alt="" /> Gérer mes informations publiques
					<span class="fleche"></span>
				</li>
			</a>
			<a href="'.ABSPATH.'Membres/Modifier_Profil.php?Action=Avatar">
				<li>
					<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Avatar.png" alt="" /> Modifier mon Avatar
					<span class="fleche"></span>
				</li>
			</a>
			
			<div class="others">
				'.$administration.'
				<a href="'.ABSPATH.'Membres/Deconnexion.php">Déconnexion</a>
				<div class="clear"></div>
			</div>
		</ul>
	</div>';
}
else {	
	$avatar = ABSPATH."Images/Avatars/avatar_normal.png";
	
	echo '
	<div class="navigationGenerale">
		<span class="left">
			<span class="deroulerMenu">
				<img src='.$avatar.' alt="" class="avatarMembre" /> Connexion
				<span class="fleche"></span>
			</span>
			
			<span class="leftInterne">
				<a class="bouton" href="'.ABSPATH.'Membres/Membres.php">Les Membres</a>
				<a class="bouton" href="'.ABSPATH.'Membres/Personnages.php">Les Personnages</a>
				<a class="bouton" href="'.ABSPATH.'Membres/Guildes">Les Guildes</a>
			</span>
		</span>
		
		<span class="right">
			<a href="#Haut" class="bouton" style="opacity:0; cursor:default;">Haut</a>
			<a href="#Bas" class="bouton">Bas</a>
		</span>

		<ul class="menuProfil">
			<div class="others bottom">
				<a href="'.ABSPATH.'Membres/Inscription.php" class="admin">Inscription</a>
				<span class="border" style="left:120px;"></span>
				<a href="'.ABSPATH.'Membres/Modifier_Profil.php?Action=Restauration">Mot de Passe oublié</a>
				<div class="clear"></div>
			</div>
			<form action="'.ABSPATH.'Membres/Connexion.php" method="post">
				<p><label for="pseudo-topbar">Pseudo</label> <input type="text" name="pseudo" id="pseudo-topbar" autocomplete="off" /></p>
				<p><label for="pasword-topbar">Mot de passe</label> <input type="password" name="password" id="pasword-topbar" autocomplete="off" /></p>
				<p><label for="save_pseudo" title="Permet d\'être connecté à chaque visite">Automatique</label> <input type="checkbox" name="save_pseudo" id="save_pseudo" /></p>
				<input type="text" name="adresse_redirection" style="display:none;" value="'.$_SERVER["HTTP_REFERER"].'" />
				<p align="center"><input type="submit" name="submit" value="Connexion"  /></p>
			</form>
		</ul>
	</div>';
}

echo '<div class="fullBlackNaviGenerale"></div>';
?>

<div class="header" id="Haut">
	<div class="headerContent">
		<?php
		if(stripos($_SERVER['SCRIPT_NAME'], "Forum"))       echo '<a href="'.ABSPATH.'Forum" class="retourAccueil"></a>';
		if(!stripos($_SERVER['SCRIPT_NAME'], "Forum"))      echo '<a href="'.ABSPATH.'" class="retourAccueil"></a>';
		?>

		<noscript class="javascriptDisabled">
		Activez JavaScript pour une navigation optimale.
		</noscript>
		
		<!--<div class="twitter">
			<div class="infos">
				<h5><a href="http://twitter.com/Salixor"><img src="<?php //echo ABSPATH ?>Images/General/Twitter.png" alt="" /></a> Twitter</h5>
			</div>
			<div class="content">
				<noscript>Vous devez activer Javascript pour accéder aux tweets.</noscript>
			</div>
		</div>!-->
	</div>
</div>

<div id="menuHeader">
	<ul class="content">
		<a href="<?php echo ABSPATH; ?>"><li>Accueil</li></a>
		<a href="<?php echo ABSPATH; ?>Forum"><li>Forum</li></a>
		<a href="<?php echo ABSPATH; ?>Equipements"><li>Équipements</li></a>
		<a href="<?php echo ABSPATH; ?>Guides/Donjons"><li>Donjons</li></a>
		<a href="<?php echo ABSPATH; ?>Membres/Guildes" class="new"><li>Guildes</li></a>
		<a href="<?php echo ABSPATH; ?>Membres"><li>Membres</li></a>
		<a href="<?php echo ABSPATH; ?>Chat"><li>Chat</li></a>
		<div class="clear"></div>
	</ul>
</div>

<div class="clear"></div>

<span class="loader"><img src="<?php echo ABSPATH; ?>Images/LoaderGeneral.gif" alt="" /> Chargement</span>

<div id="conteneur">

<?php
	include_once("Sidebar.php");
	
	echo '<div class="itemShower"><div class="bottom"></div></div>';
	echo '
	<div class="ajouter_equipement" id="information_ajouter_equipement">
		<span class="delete_value_champ" style="margin:0; top:12px; right:12px; background:none;" onclick="$(\'#information_ajouter_equipement\').fadeOut(300);">×</span>
				
		<div class="conteneur">
			<div class="contenu"></div>
			<div class="validation">
				<p class="Arianne" style="text-align:left; margin:0; padding:5px;" id="info-equipement"></p>
				
				<div class="choixPersonnageInfos validate">
					<span class="changerPerso"><a href="javascript:void(0);">Changer de personnage</a></span>
				
					<div class="image">-</div>
					<div class="pseudo">-</div>
					<div class="little">-</div>
					<div class="clear"></div>
				</div>
				
				<div class="fleche"></div>
				
				<div class="choixPersonnageInfos equipement">
					<div class="image">-</div>
					<div class="pseudo">-</div>
					<div class="little">-</div>
				</div>

				<p><input type="submit" value="Confirmer" id="sendPersonnage" /></p>
			</div>
		</div>
	</div>';
	echo '<div class="notification"></div>';
	echo '
	<div class="maGalerie">
		<div class="bigImage"></div>
		<div class="prev"><span></span></div>
		<div class="next"><span></span></div>
		<div class="close"></div>
		<div class="full_black" style="background:rgba(0,0,0,0.2)"></div>
	</div>';
?>