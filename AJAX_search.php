<?php
include('Includes/Fonctions.php');
include('Includes/Config.php');

if(isset($_POST['recherche']) AND isset($_POST['filtre']) AND isset($_POST['filtre_contenu'])) {
	$recherche = $_POST['recherche']; 
	$filtre = $_POST['filtre'];
	$filtre_contenu = $_POST['filtre_contenu'];
	
	$requete1 = "SELECT news.*, COUNT(commentaires.id) AS nb_commentaires
				 FROM news 
				 LEFT JOIN commentaires 
				 ON news.id = commentaires.id_news";
	
	if(($filtre_contenu == "titre" OR $filtre_contenu == "contenu" OR $filtre_contenu == "classe") AND ($filtre == "1" OR $filtre == "2")) {
		$s = explode(" ", $recherche);
		$i = 0;
						
		foreach($s as $mot) {
			if((count($s) == 1) AND (strlen($mot) <= 3)) {
				$erreur = 'Le mot que vous avez recherché fait moins de 3 caractères.';
			}
			elseif((count($s) <= 4) AND (strlen($mot) > 3)) {
				if($i <= 4) {
					if($i == 0) {
						$requete1 .= " WHERE";
					}
					else {
						if($filtre == "1") {
							$requete1 .= " AND";
						}
						if($filtre == "2") {
							$requete1 .= " OR";
						}
					}
					$requete1 .= " ".$filtre_contenu." COLLATE utf8_unicode_ci LIKE '%".mysql_real_escape_string($mot)."%'";
					$i++;
				}
			}
			elseif(count($s) > 4) {
				$erreur = 'Vous recherchez plus de 4 mots-clefs.';
			}
		}
	}
	elseif($filtre_contenu != "titre" AND $filtre_contenu != "contenu" AND $filtre_contenu != "classe") {
		$erreur = "Ce filtre de contenu n'existe pas.";
	}
	elseif($filtre != "1" AND $filtre != "2") {
		$erreur = "Ce filtre de modération n'existe pas.";
	}
	
	if($filtre_contenu == "classe" AND ($filtre_contenu != "titre" OR $filtre_contenu != "contenu") AND ($filtre == "1" OR $filtre == "2")) {
		$requete1 .= " GROUP BY news.id DESC";
		$requete1 .= " ORDER BY news.classe DESC";
	}
	elseif(($filtre_contenu != "titre" OR $filtre_contenu != "contenu" OR $filtre_contenu != "classe") AND ($filtre != "1" OR $filtre != "2")) {
		$requete1 .= " GROUP BY news.id DESC";
	}
	elseif(($filtre_contenu == "titre" OR $filtre_contenu == "contenu") AND ($filtre == "1" OR $filtre == "2")) {
		$requete1 .= " GROUP BY news.id DESC";
		$requete1 .= " ORDER BY news.id DESC";
	}
	
	$resultat = query($requete1); 
	$nb_results_news_search = mysql_num_rows($resultat);

	if($nb_results_news_search > 1 AND !isset($erreur)) {	
        $h2 = '
		<p class="encadreInfos">
			<span class="left">Recherche</span>
			<span class="right">'.$nb_results_news_search.' résultats pour cette recherche.</span>
		</p>';
	}
	elseif($nb_results_news_search == 0 AND !isset($erreur)) { 
		$erreur = "Aucune news ne correspond à la recherche.";
	}
	elseif($nb_results_news_search == 1 AND !isset($erreur)) {
        $h2 = '
		<p class="encadreInfos">
			<span class="left">Recherche</span>
			<span class="right">1 résultat pour cette recherche.</span>
		</p>';
	}
}

if(isset($h2)) {
	echo $h2;
}
if(isset($erreur)) {
	echo '<div class="encadre_infos"><p><img src="'.ABSPATH.'Images/erreur.png" alt="Erreur" align="top" /> '.$erreur.'</p></div>';
}

if($nb_results_news_search > 0) {
	while($data = mysql_fetch_assoc($resultat)) {
		$titreNews     =   stripslashes(utf8_encode($data['titre']));
		$dateEdit      =   $data['edit_date']; 
		$dateNews      =   $data['date']; 
		$classeNews    =   stripslashes($data['classe']);
		$contenuNews   =   formatage(smileys(nl2br(stripslashes($data['contenu']))));
		$editNews      =   formatage(smileys(nl2br(stripslashes($data['edit']))));
		$idNews        =   intval($data['id']);

		$images        =   $data["images"];
		$imageExplode  =   explode(";", $images);
		if(isset($imageExplode[0])) {
		$imageMini     =   $imageExplode[0];
		}
		if(isset($imageExplode[1])) {
		$imageBig      =   $imageExplode[1];
		}

		$nombreComms   =   $data['nb_commentaires']." commentaires";

		switch($data['nb_commentaires']) {
		  case "0":
			  $nombreComms = "Aucun commentaire";
		  break;
		  
		  case "1":
			  $nombreComms = "1 commentaire";
		  break;
		}

		if(!empty($editNews)) {
		  $contenuNews .= $editNews;
		}
		$contenuNews = limiternews($contenuNews, 200);
		$date = afficherDate($dateNews);
		
		echo '<div class="news" id="News'.$data["id"].'">';
			if(!empty($imageMini) && !empty($imageBig)) {
				if(file_exists($imageMini) && file_exists($imageBig)) {
					echo '
					<div class="imageLeft">
						<a href="'.ABSPATH.$imageBig.'" class="fancybox-news" title="'.$titreNews.'"><img src="'.ABSPATH.$imageMini.'" alt="" /></a>
					</div>';
				}
				else {
					echo '<div class="imageLeft"><img src="'.ABSPATH.'Images/News/Default-mini.png" alt="" /></div>';
				}
			}
			else {
				echo '<div class="imageLeft"><img src="'.ABSPATH.'Images/News/Default-mini.png" alt="" /></div>';
			}
			
			echo '<div class="head">';
				echo '<h4><a href="'.ABSPATH.'?News='.$idNews.'&amp;Titre='.optimiser_titre_et_classe($titreNews).'#News">'.$titreNews.'</a></h4>';
				echo '<div class="infos">';
					echo '
					<span class="date"><img src="'.ABSPATH.'Images/Forum/Time.png" alt="" /> Écrite le '.$date.'</span>
					<span class="tag"><a href="'.ABSPATH.'?Classe='.$classeNews.'#News"><img src="'.ABSPATH.'Images/Tag.png" alt="" /> '.$classeNews.'</a></span>
					<span class="comm"><a href="'.ABSPATH.'?News='.$idNews.'&amp;Titre='.optimiser_titre_et_classe($titreNews).'#Commentaires"><img src="'.ABSPATH.'Images/CommentaireInfos.png" alt="" /> '.$nombreComms.'</a></span>';
				echo '</div>';
			echo '</div>';
			
			echo '<div class="contenu">';
				echo $contenuNews;
			echo '</div>';
			
			echo '<a class="readRest" href="'.ABSPATH.'?News='.$idNews.'&amp;Titre='.optimiser_titre_et_classe($titreNews).'#News">Lire la suite</a>';

			echo '<div style="clear:left;"></div>';
		echo '</div>';
	}
}

?>