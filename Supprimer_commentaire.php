<?php 
$titre_page_nom = "Supprimer un commentaire";

require_once('Includes/Fonctions.php');
require_once('Includes/Config.php');

if(isset($_GET["Ok"]) && isset($_GET["Redirection"])) {
	$requete = $baseDeDonnes->prepare('UPDATE commentaires SET id_reponse = "0" FROM commentaires WHERE id_reponse = :reponse');
	$requete->bindValue(':reponse', $_GET["id"], PDO::PARAM_STR);
	$requete->execute();
	
	$requete = $baseDeDonnes->prepare('DELETE FROM commentaires WHERE id = :id');
	$requete->bindValue(':id', $_GET["id"], PDO::PARAM_STR);
	$requete->execute();
	
	$requete = $baseDeDonnes->prepare('UPDATE membres SET membre_commentaires = membre_commentaires - 1 WHERE membre_id = :id');
	$requete->bindValue(':id', $ID_MEMBRE, PDO::PARAM_STR);
	$requete->execute();
	
	$valide = "Le commentaire a été supprimé. <a href=\"".ABSPATH."?Page=1\">Retourner</a> aux news.";
}

include("Header.php");
 

echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Supprimer_commentaire.png" alt="Supprimer_commentaire" /> Supprimer un Commentaire</h2>';

echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="'.ABSPATH.'?Page=1">Les News</a> > Supprimer un Commentaire</p>';

if(isset($erreur)) {
	echo '<p class="encadreInfos">';
		echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
		echo '<span class="right">'.$erreur.'</span>';
	echo '</p>';
}
if(isset($valide)) {
	echo '<p class="encadreInfos">';
		echo '<span class="left valide"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Valide.png" alt="Confirmation" /> Confirmation</span>';
		echo '<span class="right">'.$valide.'</span>';
	echo '</p>';
}

if(isset($_GET["id"]) AND !isset($valide)) { 
	$requete = $baseDeDonnes->prepare('SELECT * FROM commentaires WHERE id = :id');
	$requete->bindValue(':id', $_GET["id"], PDO::PARAM_INT);
	$requete->execute();

	$data = $requete->fetch();
	
	if($requete->rowCount() > 0) { 
		if($ID_MEMBRE == $data["id_membre"] || $NIVEAU_MEMBRE == 5) { 
			echo '
			<p class="ForumSend">
				<span class="titreSend"><img src="Images/Forum/Erreur.png" /> Attention</span>
				<span class="contenuSend">
					<span id="a">Vous allez supprimer un commentaire. Cette action est irréversible. Voulez-vous vraiment continuer ?</span>
					<span id="b" align="center"><a href="?id='.$_GET["id"].'&amp;Ok&amp;Redirection='.$_SERVER["HTTP_REFERER"].'" class="choice">Oui</a><a href="'.$_SERVER["HTTP_REFERER"].'" class="choice">Non</a></span>
				</span>
			</p>';
		}
		else {
			echo '<p class="encadreInfos">';
				echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
				echo '<span class="right">Ce commentaire ne vous appartient pas.</span>';
			echo '</p>';
		}
	}
	else {
		echo '<p class="encadreInfos">';
			echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
			echo '<span class="right">Ce commentaire n\'existe pas.</span>';
		echo '</p>';
	}
}
elseif(!isset($_GET["id"])) {
	echo '<p class="encadreInfos">';
		echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
		echo '<span class="right">Aucun commentaire n\'est défini.</span>';
	echo '</p>';
}

include("Footer.php"); ?>
