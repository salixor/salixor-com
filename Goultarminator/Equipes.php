<?php 
$titre_page_nom = "Les Équipes";
include("../Header.php");

if(isset($_GET["Equipe"])) {
	$requete = $baseDeDonnes->prepare('
	SELECT * FROM goultarminator_equipes
	LEFT JOIN goultarminator_joueurs ON goultarminator_joueurs.equipe = goultarminator_equipes.id
	WHERE goultarminator_equipes.id = :id');
	$requete->bindValue(":id", $_GET["Equipe"], PDO::PARAM_INT);
	$requete->execute();
	
	if($requete->rowCount() > 0) { 
		$personnages = array();
		while($data = $requete->fetch()) {
			$serveur = stripslashes($data["serveur"]);
			$nom = stripslashes($data["nom"]);
			$points = $data["points"];
			$position = $data["position"];
			
			$imageServeurPage = strtr($serveur, $GLOBALS['normalizeChars']);
			$imageServeurPage = preg_replace("#\s#", "", $serveur);

			if($position == 1)   $position .= "<sup>ère</sup>";
			if($position > 1)   $position .= "<sup>ème</sup>";
			
			array_push($personnages, $data);
		}
		?>
	
		<h2 class="titreSecond"><img src="../Images/Serveurs/<?php echo $imageServeurPage; ?>.png" alt="" /> <?php echo $nom; ?></h2>

		<p class="Arianne">> <a href="<?php echo ABSPATH; ?>">Accueil</a> > <a href="./">Goultarminator</a> > <a href="Equipes.php">Les Équipes</a> > <?php echo $nom; ?></p>

		<p class="encadreInfos"><span class="right">Voici la page de l'équipe <?php echo $nom; ?> du serveur <?php echo $serveur; ?></span></p>
		
		<h3 class="troisieme_titre">Informations</h3>
		
		<p class="encadreInfos"><span class="right">L'équipe dispose actuellement de <?php echo $points; ?> points ce qui la situe en <?php echo $position; ?> place dans le classement des équipes.</span></p>
		
		<?php
		echo '<h3 class="troisieme_titre">Membres de l\'Équipe</h3>';
			
		echo '<div class="personnagesGoultarminator">';
			if(count($personnages) > 1) {
				$i = 0;
				foreach($personnages as $personnage) {
					$add = '<span>';
					if(strlen($personnage["pseudo"]) > 9)   $add = '<span title="'.stripslashes($personnage["pseudo"]).'">';
					
					$pseudo = tronquer(stripslashes($personnage["pseudo"]), 9, "..");
					$classe = stripslashes($personnage["classe"]);
					$niveau = $personnage["niveau"];
					$sexe = stripslashes($personnage["sexe"]);
				
					$sexeB = strtolower(substr($sexe, 0, 1));
					$classeB = strtolower(strtr(utf8_encode($classe), $GLOBALS['normalizeChars']));
					$image = strtolower($classeB).'-'.$sexeB.'.png';
					
					echo '
					<div class="fichePersoGoultarminator">
						<div class="image">
							<img src="'.ABSPATH.'Images/Personnages/'.$image.'" alt="" />
						</div>
						<span class="niveau" title="Niveau '.$niveau.'">'.$niveau.'</span>
						<span class="pseudo">'.$add.$pseudo.'</span></span>
					</div>';
					if($i == 3)   echo '<div style="clear:left;"></div>';
					
					$i++;
				}
				echo '<div style="clear:left;"></div>';
			}
			else {
				for($i = 0; $i < 4; $i++) {
					echo '
					<div class="fichePersoGoultarminator">
						<div class="image">
							<img src="'.ABSPATH.'Images/Personnages/inconnu.png" alt="" />
						</div>
						<span class="pseudo">A venir</span></span>';
					echo '</div>';
					if($i == 3)   echo '<div style="clear:left;"></div>';
				}
				echo '<div style="clear:left;"></div>';
			}
		echo '</div>';
		?>
			
		<h3 class="troisieme_titre">Matchs de l'Équipe</h3>
		
			<?php
				$requete = $baseDeDonnes->prepare('
				SELECT *, (SELECT COUNT(*) FROM goultarminator_matchs WHERE equipe_gagnante = :id) AS nb_gagnes, (SELECT COUNT(*) FROM goultarminator_matchs WHERE equipe_a = :id OR equipe_b = :id) AS nb_matchs
				FROM goultarminator_matchs');
				$requete->bindValue(":id", $_GET["Equipe"], PDO::PARAM_INT);
				$requete->execute();
				
				$data = $requete->fetch();
				$gagnes = $data["nb_gagnes"];
				$matchs = $data["nb_matchs"];
			
				echo '<p class="encadreInfos"><span class="right">L\'Équipe a participé à '.$matchs.' match(s) dont '.$gagnes.' victoire(s).</span></p>';
			
				$requete = $baseDeDonnes->prepare('
				SELECT goultarminator_matchs.*, equipe_a.nom AS equipe_a_nom, equipe_b.nom AS equipe_b_nom, equipe_a.id AS equipe_a_id, equipe_b.id AS equipe_b_id, equipe_a.serveur AS equipe_a_serveur, equipe_b.serveur AS equipe_b_serveur, goultarminator_videos.filmeur FROM goultarminator_matchs
				LEFT JOIN goultarminator_equipes AS equipe_a ON goultarminator_matchs.equipe_a = equipe_a.id
				LEFT JOIN goultarminator_equipes AS equipe_b ON goultarminator_matchs.equipe_b = equipe_b.id
				LEFT JOIN goultarminator_videos ON goultarminator_matchs.id = goultarminator_videos.id_match
				WHERE goultarminator_matchs.equipe_a = :id OR goultarminator_matchs.equipe_b = :id');
				$requete->bindValue(":id", $_GET["Equipe"], PDO::PARAM_INT);
				$requete->execute();
			
				echo '
				<table>
					<tr>
						<th width="100">Date</th>
						<th width="250">Match contre</th>
						<th width="300">Équipe Gagante</th>
						<th width="200">Carte</th>
						<th width="200">Vidéo</th>
						<th width="200">Filmeur</th>
					</tr>
					<tbody>';
					if($requete->rowCount() > 0) {
						while($data = $requete->fetch()) {
							$nomA = stripslashes($data["equipe_a_nom"]);
							$nomB = stripslashes($data["equipe_b_nom"]);
							$raison = stripslashes($data["raison"]);
							
							$serveurA = stripslashes($data["equipe_a_serveur"]);
							$serveurB = stripslashes($data["equipe_b_serveur"]);
							
							$imageServeurA = strtr($serveurA, $GLOBALS['normalizeChars']);
							$imageServeurA = preg_replace("#\s#", "", $serveurA);
							
							$imageServeurB = strtr($serveurB, $GLOBALS['normalizeChars']);
							$imageServeurB = preg_replace("#\s#", "", $serveurB);

							echo '
							<tr>
								<td align="center">'.date("d\/m\/Y", $data["date"]).'</td>';

								if($data["equipe_a_nom"] == $nom) {
									echo '
									<td style="padding:7px 5px 3px;">
										<img src="../Images/Serveurs/'.$imageServeurB.'.png" alt="" height="30" style="background:#FFF; vertical-align:middle; position:relative; top:-2px; bottom:-2px; margin-right:2px;" /> <a href="?Equipe='.$data["equipe_b_id"].'">'.$nomB.'</a>
									</td>';
								}
								elseif($data["equipe_b_nom"] == $nom) {
									echo '
									<td style="padding:7px 5px 3px;">
										<img src="../Images/Serveurs/'.$imageServeurA.'.png" alt="" height="30" style="background:#FFF; vertical-align:middle; position:relative; top:-2px; bottom:-2px; margin-right:2px;" /> <a href="?Equipe='.$data["equipe_a_id"].'">'.$nomA.'</a>
									</td>';
								}
								
								if($data["equipe_gagnante"] == $data["equipe_b_id"]) {
									echo '
									<td style="padding:7px 5px 3px;">
										<img src="../Images/Serveurs/'.$imageServeurB.'.png" alt="" height="30" style="background:#FFF; vertical-align:middle; position:relative; top:-2px; bottom:-2px; margin-right:2px;" /> <a href="?Equipe='.$data["equipe_b_id"].'">'.$nomB.'</a> - '.$raison.'
									</td>';
								}
								if($data["equipe_gagnante"] == $data["equipe_a_id"]) {
									echo '
									<td style="padding:7px 5px 3px;">
										<img src="../Images/Serveurs/'.$imageServeurA.'.png" alt="" height="30" style="background:#FFF; vertical-align:middle; position:relative; top:-2px; bottom:-2px; margin-right:2px;" /> <a href="?Equipe='.$data["equipe_a_id"].'">'.$nomA.'</a> - '.$raison.'
									</td>';
								}
								
								echo '
								<td align="center"><a href="'.ABSPATH.'Goultarminator/Images/Map-'.$data["image"].'.jpg" class="maGalerieLink">Voir la carte</a></td>
								<td align="center"><a href="#fsefsefse">Voir la vidéo</a></td>
								<td>'.$data["filmeur"].'</td>
							</tr>';
						}
					}
					else {
						echo '<tr><td colspan="6" align="center" style="padding:7px;">Cette équipe n\'a encore pas combattu.</td></tr>';
					}
				echo '</tbody>
				</table>';
			?>
		
<?php
	}
}
else { ?>
	<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Goultarminator.png" alt="" /> Les Équipes</h2>

	<p class="Arianne">> <a href="<?php echo ABSPATH; ?>">Accueil</a> > <a href="./">Goultarminator</a> > Les Équipes</p>

	<p class="encadreInfos"><span class="right">Retrouvez-ici les équipes qui vont se battre cet Été lors du Goultarminator.</span></p>

	<?php
	if(isset($_GET['Page']) && is_numeric($_GET['Page']))    $page = $_GET['Page'];
	else                                                     $page = 1;

	$pagination = 20;
	$limit_start = ($page - 1) * $pagination;

	$requete = $baseDeDonnes->prepare("SELECT COUNT(*) AS nb_total FROM goultarminator_equipes");
	$requete->execute();

	$nb_total = $requete->fetch();
	$nb_total = $nb_total['nb_total'];
	$nb_pages = ceil($nb_total / $pagination);

	$requete = $baseDeDonnes->prepare('SELECT * FROM goultarminator_equipes ORDER BY position, points, id DESC, nom ASC LIMIT '.$limit_start.', '.$pagination);
	$requete->execute();

	if($nb_total > 0) {
		echo '
		<table>
			<tr>
				<th></th>
				<th width="250">Équipe</th>
				<th width="250">Serveur</th>
				<th width="200">Points</th>
				<th width="200">Position <span class="rouge">*</span></th>
			</tr>
			<tbody>';
				while($data = $requete->fetch()) {
					$serveur = stripslashes(utf8_encode($data["serveur"]));
					$nom = stripslashes(utf8_encode($data["nom"]));
					
					$imageServeur = strtr($data["serveur"], $GLOBALS['normalizeChars']);
					$imageServeur = preg_replace("#ï#", "i", $serveur);
					$imageServeur = preg_replace("#\s#", "", $imageServeur);

					echo '
					<tr>
						<td><img src="../Images/Serveurs/'.$imageServeur.'.png" alt="" height="30" style="background:#FFF" /></td>
						<td><a href="?Equipe='.$data["id"].'">'.$nom.'</a></td>
						<td>'.$serveur.'</td>
						<td align="center">'.$data["points"].'</td>
						<td align="center">'.$data["position"].' sur '.$nb_total.'</td>
					</tr>';
				}
		echo '</tbody>
		</table>';
		
		if($nb_pages > 1)      echo '<p class="pagination" align="center" style="margin:15px 0;">'.pagination($page, $nb_pages, "?Page=%d").'</p>';
	}
	
	echo '<p class="encadreInfos"><span class="right"><span class="rouge">*</span> Le classement dépend du nombre de points de l\'équipe.</span></p>';
}

include("../Footer.php"); ?>