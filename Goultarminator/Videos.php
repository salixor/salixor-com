<?php 
if(isset($_GET["Video"])) {	
	require_once("../Includes/Fonctions.php");
	require_once("../Includes/Config.php");

	$requete = $baseDeDonnes->prepare("
	SELECT *, equipe_a.nom AS equipe_a_nom, equipe_b.nom AS equipe_b_nom FROM goultarminator_videos
	LEFT JOIN goultarminator_matchs ON goultarminator_videos.id_match = goultarminator_matchs.id
	LEFT JOIN goultarminator_equipes AS equipe_a ON goultarminator_matchs.equipe_a = equipe_a.id
	LEFT JOIN goultarminator_equipes AS equipe_b ON goultarminator_matchs.equipe_b = equipe_b.id
	WHERE goultarminator_videos.id = :id");
	$requete->bindValue(":id", $_GET["Video"], PDO::PARAM_INT);
	$requete->execute();

	if($requete->rowCount() > 0) {
		if(isset($_POST["commentaire"])) {
			$requete = $baseDeDonnes->prepare("
			INSERT INTO goultarminator_commentaires(id_video, id_membre, commentaire, date)
			VALUES(:id_video, :id_membre, :commentaire, :date)");
			$requete->bindValue(":id_video", $_GET["Video"], PDO::PARAM_INT);
			$requete->bindValue(":id_membre", $ID_MEMBRE, PDO::PARAM_INT);
			$requete->bindValue(":commentaire", $_POST["commentaire"], PDO::PARAM_STR);
			$requete->bindValue(":date", time(), PDO::PARAM_INT);
			$requete->execute();
		}
		
		if(!isset($_GET["OnlyNav"])) {
			$data = $requete->fetch();
			$equipe_a = $data["equipe_a_nom"];
			$equipe_b = $data["equipe_b_nom"];
			$filmeur = utf8_encode($data["filmeur"]);
			$lienVideo = $data["lien_video"];
			
			if(!empty($equipe_a))  $titre_page_nom = 'Vidéo - '.$equipe_a.' contre '.$equipe_b;
			else                   $titre_page_nom = 'Finale du Goultarminator II';

			include("../Header.php");
			
			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Goultarminator.png" alt="" /> Visionner un Match</h2>';
			
			if(!empty($equipe_a))  echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Goultarminator</a> > <a href="Videos.php">Les Vidéos</a> > '.$equipe_a.' contre '.$equipe_b.'</p>';
			else                   echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Goultarminator</a> > <a href="Videos.php">Les Vidéos</a> > Finale du Goultarminator II</p>';
			
			if(!empty($equipe_a))  echo '<p class="encadreInfos"><span class="right">Retrouvez le match de '.$equipe_a.' VS '.$equipe_b.', filmé par '.$filmeur.'.</span></p>';
			else                   echo '<p class="encadreInfos"><span class="right">Retrouvez la Finale du Goultarminator II, filmée par '.$filmeur.'.</span></p>';
		
			echo '
			<p class="goultarminatorVideo" style="position:relative; z-index:10;">
				<iframe width="760" height="460" src="http://www.youtube.com/embed/'.$lienVideo.'" frameborder="0" allowfullscreen></iframe>
			</p>';

			if(isset($ID_MEMBRE)) {
				echo '<div class="goultarminatorCommentaire send">';
					echo '
					<div class="commentaire">
						<h5>Commentez la vidéo !</h5>
						<form method="post">
							<textarea name="commentaire" id="textarea-commentaire-goultarminator"></textarea>
							<p class="input">
								<input type="submit" name="submit" value="Envoyer" />
								<div class="countCommGoulta" style="display:none;"></div>
								<span id="actual-vid" style="display:none;">'.$_GET["Video"].'</span>
							</p>
						</form>
					</div>';
				echo '</div>';
			}
			else {
				echo '<div class="goultarminatorCommentaire send">';
					echo '
					<div class="commentaire">
						<a href="javascript:connexion();">Connectez-vous</a> pour commenter la vidéo.
					</div>';
				echo '</div>';
			}
		}

		if(isset($_GET['Page']) && is_numeric($_GET['Page']))    $page = $_GET['Page'];
		else                                                     $page = 1;

		$pagination = 10;
		$limit_start = ($page - 1) * $pagination;
		
		$requete = $baseDeDonnes->prepare("
		SELECT COUNT(*) AS nb_total FROM goultarminator_commentaires WHERE id_video = :id");
		$requete->bindValue(":id", $_GET["Video"], PDO::PARAM_INT);
		$requete->execute();
		
		$nb_total = $requete->fetch();
		$nb_total = $nb_total['nb_total'];
		$nb_pages = ceil($nb_total / $pagination);
		
		$requete = $baseDeDonnes->prepare('
		SELECT goultarminator_commentaires.*, membres.*, goultarminator_commentaires.id AS comm_id
		FROM goultarminator_commentaires
		LEFT JOIN membres ON goultarminator_commentaires.id_membre = membres.membre_id
		WHERE goultarminator_commentaires.id_video = :id
		ORDER BY goultarminator_commentaires.id DESC
		LIMIT '.$limit_start.', '.$pagination);
		$requete->bindValue(":id", $_GET["Video"], PDO::PARAM_INT);
		$requete->execute();
		
		echo '<div id="receiver-page-comm">';
			while($data = $requete->fetch()) {
				echo '<div class="goultarminatorCommentaire">';
					echo '<div class="fleche"></div>';
					echo '
					<div class="commentaire">
						'.smileys(nl2br(stripslashes(formatage($data['commentaire'])))).'
						<div class="infos">Écrit par <a href='.ABSPATH.'Membres/Profil.php?id='.$data["membre_id"].'>'.$data["membre_pseudo"].'</a> le '.convertirTimestamp($data["date"]).'</div>
					</div>';
				echo '</div>';
			}
			
			if($nb_pages > 1)   echo '<p class="pagination black" align="center" id="pagination-goulta">'.pagination($page, $nb_pages, "?Video=".$_GET["Video"]."&amp;Page=%d").'</p>';
	}
}
else {
	$titre_page_nom = "Les Vidéos";
	include("../Header.php");

	echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Goultarminator.png" alt="" /> Les Vidéos</h2>';

	echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Goultarminator</a> > Les Vidéos</p>';

	echo '<p class="encadreInfos"><span class="right">Vous pouvez ici retrouver certaines des vidéos du Goultarminator, et les commenter.</span></p>';
	
	$requete = $baseDeDonnes->prepare("
	SELECT *, equipe_a.nom AS equipe_a_nom, equipe_b.nom AS equipe_b_nom, goultarminator_videos.id AS id_video, (SELECT COUNT(*) FROM goultarminator_commentaires WHERE id_video = goultarminator_videos.id) AS nb_comms FROM goultarminator_videos
	LEFT JOIN goultarminator_matchs ON goultarminator_videos.id_match = goultarminator_matchs.id
	LEFT JOIN goultarminator_equipes AS equipe_a ON goultarminator_matchs.equipe_a = equipe_a.id
	LEFT JOIN goultarminator_equipes AS equipe_b ON goultarminator_matchs.equipe_b = equipe_b.id
	ORDER BY goultarminator_videos.id ASC LIMIT 0, 10");
	$requete->execute();
	
	echo '<div class="listeVideosGoulta">';
		while($data = $requete->fetch()) {
			echo '<a href="?Video='.$data["id_video"].'">';
				echo '<div class="goultaVideoMini">';
					echo '<div class="image">';
						echo '<img src="http://i1.ytimg.com/vi/'.$data["lien_video"].'/hqdefault.jpg" />';
					echo '</div>';
					echo '<div class="infos">';
						if(!empty($data["equipe_a_nom"])) {
							echo $data["equipe_a_nom"].' contre '.$data["equipe_b_nom"];
						}
						else {
							echo 'Finale du Goultarminator II';
						}
						echo '<span class="pseudo">par '.utf8_encode($data["filmeur"]).'<span class="count">'.$data["nb_comms"].' commentaire(s)</span></span>';
					echo '</div>';
				echo '</div>';
			echo '</a>';
		}
		echo '<div style="clear:left;"></div>';
	echo '</div>';
}

if(!isset($_GET["OnlyNav"])) {
	include("../Footer.php");
}
?>