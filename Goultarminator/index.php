<?php 
$titre_page_nom = "Le Goultarminator";
include("../Header.php");
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Goultarminator.png" alt="" /> Goultarminator</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH; ?>">Accueil</a> > Goultarminator</p>

<p class="encadreInfos"><span class="right">Bienvenue sur la partie Goultarminator du site. Suivez ici l'actualité du Goultarminator et retrouvez les équipes et autre.</span></p>

<ul class="navigationPage">
	<a href="Actu.php"><li><img src="<?php echo ABSPATH; ?>Images/icones_pages/LittlesIcones/News.png" alt="" /> Actualité</li></a>
	<a href="Equipes.php"><li><img src="<?php echo ABSPATH; ?>Images/icones_pages/LittlesIcones/Teams.png" alt="" /> Équipes</li></a>
	<a href="Matchs.php"><li><img src="<?php echo ABSPATH; ?>Images/icones_pages/LittlesIcones/Matchs.png" alt="" /> Matchs</li></a>
	<a href="Videos.php"><li><img src="<?php echo ABSPATH; ?>Images/icones_pages/LittlesIcones/Videos.png" alt="" /> Vidéos</li></a>
</ul>

<?php include("../Footer.php"); ?>