<?php 
$titre_page_nom = "Les Matchs";
include("../Header.php");
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Goultarminator.png" alt="" /> Les Matchs</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH; ?>">Accueil</a> > <a href="./">Goultarminator</a> > Les Matchs</p>

<p class="encadreInfos"><span class="right">Vous pouvez ici consulter la liste des matchs, mais aussi rechercher des matchs par serveur ou équipe.</span></p>

<div class="bloc">
Vous cherchez un match particulier ?
<form method="post" name="searchMatch">
	<p>
		<label for="serveur">Chercher par serveur :</label> 
		<select id="serveur" name="serveur">
			<?php
			$requete = $baseDeDonnes->prepare('SELECT serveur FROM goultarminator_equipes GROUP BY serveur ASC');
			$requete->execute();
			
			echo '<option value="">Serveurs</option>';
			while($data = $requete->fetch()) {
				echo '<option value="'.$data["serveur"].'">'.$data["serveur"].'</option>';
			}
			?>
		</select>
		<b> OU </b>
		<label for="equipe">Chercher par équipe :</label> 
		<select id="equipe" name="equipe">
			<?php
			$requete = $baseDeDonnes->prepare('SELECT nom FROM goultarminator_equipes GROUP BY nom ASC');
			$requete->execute();
			
			echo '<option value="">Équipes</option>';
			while($data = $requete->fetch()) {
				echo '<option value="'.$data["nom"].'">'.$data["nom"].'</option>';
			}
			?>
		</select>
	</p>
	<p align="center">
		<input type="submit" value="Chercher" />
	</p>
</form>
</div>

<?php
$pagination = 20;

if(isset($_GET['PageVenir']) && is_numeric($_GET['PageVenir']))    $page = $_GET['PageVenir'];
else                                                               $page = 1;

$limit_start_venir = ($page - 1) * $pagination;

//Définition des variables en cas de recherche
$add1 = "";
$add2 = $add1;
$add3 = "WHERE date > ".time()." LIMIT ".$limit_start_venir.", ".$pagination;
$add4 = "WHERE date > ".time();
if(isset($_POST["serveur"])) {
	if(!empty($_POST["serveur"])) {
		$message = "Vous consultez les matchs du serveur '".$_POST["serveur"]."'.";
		$add1 = "WHERE equipe_a.serveur = :serveur OR equipe_b.serveur = :serveur";
		$add3 = " AND date > ".time();
	}
}

if(isset($_POST["equipe"])) {
	if(!empty($_POST["equipe"])) {
		$message = "Vous consultez les matchs de l'équipe '".$_POST["equipe"]."'.";
		$add2 = "WHERE equipe_a.nom = :equipe OR equipe_b.nom = :equipe";
		$add3 = " AND date > ".time();
	}
}

//Message des matchs à venir
if(isset($message))            echo '<p class="encadreInfos"><span class="right">'.$message.' Voici les matchs à venir.</span></p>';
else                           echo '<p class="encadreInfos"><span class="right">Voici les matchs à venir.</span></p>';

//Pagination des matchs à venir
if(!isset($_POST["serveur"])) {
	$requete = $baseDeDonnes->prepare("
	SELECT COUNT(*) AS nb_total FROM goultarminator_matchs ".$add4);
	$requete->execute();

	$nb_total = $requete->fetch();
	$nb_total = $nb_total['nb_total'];
	$nb_pages_venir = ceil($nb_total / $pagination);
}

//Requête des matchs à venir
$requete = $baseDeDonnes->prepare('
SELECT goultarminator_matchs.*, equipe_a.nom AS equipe_a_nom, equipe_b.nom AS equipe_b_nom, equipe_a.id AS equipe_a_id, equipe_b.id AS equipe_b_id, equipe_a.serveur AS equipe_a_serveur, equipe_b.serveur AS equipe_b_serveur, goultarminator_videos.filmeur FROM goultarminator_matchs
LEFT JOIN goultarminator_equipes AS equipe_a ON goultarminator_matchs.equipe_a = equipe_a.id
LEFT JOIN goultarminator_equipes AS equipe_b ON goultarminator_matchs.equipe_b = equipe_b.id
LEFT JOIN goultarminator_videos ON goultarminator_matchs.id = goultarminator_videos.id_match
'.$add1.$add2.$add3);
if(!empty($add1))     $requete->bindValue(":serveur", $_POST["serveur"], PDO::PARAM_STR);
if(!empty($add2))     $requete->bindValue(":equipe", $_POST["equipe"], PDO::PARAM_STR);
$requete->execute();

//Affichage des matchs à venir
echo '
<table>
	<tr>
		<th width="200">Date</th>
		<th width="250">Équipe A</th>
		<th width="250">Équipe B</th>
		<th width="200">Carte</th>
	</tr>
	<tbody>';
	if($requete->rowCount() > 0) {
		while($data = $requete->fetch()) {
			$nomA = stripslashes(utf8_encode($data["equipe_a_nom"]));
			$nomB = stripslashes(utf8_encode($data["equipe_b_nom"]));
			
			$serveurA = stripslashes(utf8_encode($data["equipe_a_serveur"]));
			$serveurB = stripslashes(utf8_encode($data["equipe_b_serveur"]));
			
			$imageServeurA = strtr($serveurA, $GLOBALS['normalizeChars']);
			$imageServeurA = preg_replace("#ï#", "i", $serveurA);
			$imageServeurA = preg_replace("#\s#", "", $imageServeurA);
			
			$imageServeurB = strtr($serveurB, $GLOBALS['normalizeChars']);
			$imageServeurB = preg_replace("#ï#", "i", $imageServeurB);
			$imageServeurB = preg_replace("#\s#", "", $imageServeurB);

			echo '
			<tr>
				<td>'.date("d\/m\/Y", $data["date"]).' à '.date("H\:i", $data["date"]).'</td>
				<td style="padding:7px 5px 3px;">
					<img src="../Images/Serveurs/'.$imageServeurA.'.png" alt="" height="30" style="background:#FFF; vertical-align:middle; position:relative; top:-2px; bottom:-2px; margin-right:2px;" /> <a href="Equipes.php?Equipe='.$data["equipe_a_id"].'">'.$nomA.'</a>
				</td>
				<td style="padding:7px 5px 3px;">
					<img src="../Images/Serveurs/'.$imageServeurB.'.png" alt="" height="30" style="background:#FFF; vertical-align:middle; position:relative; top:-2px; bottom:-2px; margin-right:2px;" /> <a href="Equipes.php?Equipe='.$data["equipe_b_id"].'">'.$nomB.'</a>
				</td>
				<td align="center"><a href="'.ABSPATH.'Goultarminator/Images/Map-'.$data["image"].'.jpg" class="maGalerieLink">Voir la carte</a></td>
			</tr>';
		}
	}
	else {
		echo '<tr><td colspan="7" align="center" style="padding:7px;">Aucun match n\'a été trouvé.</td></tr>';
	}
echo '</tbody>
</table>';

//Affichage de la pagination des matchs à venir
if(!isset($_POST["serveur"])) {
	if($nb_pages_venir > 1) {
		if(isset($_GET["PageAncien"])) {
			echo '<p class="pagination" align="center">'.pagination($page, $nb_pages_venir, "?PageVenir=%d&amp;PageAncien=".$_GET["PageAncien"]).'</p>';
		}
		else {
			echo '<p class="pagination" align="center">'.pagination($page, $nb_pages_venir, "?PageVenir=%d").'</p>';
		}
	}
}


//Définition des variables en cas de recherche
if(isset($_GET['PageAncien']) && is_numeric($_GET['PageAncien']))    $page = $_GET['PageAncien'];
else                                                                 $page = 1;

$limit_start_ancien = ($page - 1) * $pagination;

$add1 = "";
$add2 = $add1;
$add3 = "WHERE goultarminator_matchs.date < ".time()." LIMIT ".$limit_start_ancien.", ".$pagination;
$add4 = "WHERE date < ".time();
if(isset($_POST["serveur"])) {
	if(!empty($_POST["serveur"])) {
		$message = "Vous consultez les matchs du serveur '".$_POST["serveur"]."'.";
		$add1 = "WHERE (equipe_a.serveur = :serveur OR equipe_b.serveur = :serveur)";
		$add3 = " AND goultarminator_matchs.date < ".time();
	}
}

if(isset($_POST["equipe"])) {
	if(!empty($_POST["equipe"])) {
		$message = "Vous consultez les matchs de l'équipe '".$_POST["equipe"]."'.";
		$add2 = "WHERE (equipe_a.nom = :equipe OR equipe_b.nom = :equipe)";
		$add3 = " AND goultarminator_matchs.date < ".time();
	}
}

//Message des matchs passés
if(isset($message))            echo '<p class="encadreInfos"><span class="right">'.$message.' Voici les matchs déjà passés.</span></p>';
else                           echo '<p class="encadreInfos"><span class="right">Voici les matchs déjà passés.</span></p>';

//Pagination des matchs passés
if(!isset($_POST["serveur"])) {
	$requete = $baseDeDonnes->prepare("
	SELECT COUNT(*) AS nb_total FROM goultarminator_matchs ".$add4);
	$requete->execute();

	$nb_total = $requete->fetch();
	$nb_total = $nb_total['nb_total'];
	$nb_pages_ancien = ceil($nb_total / $pagination);
}

//Requête des matchs passés
$requete = $baseDeDonnes->prepare('
SELECT goultarminator_matchs.*, equipe_a.nom AS equipe_a_nom, equipe_b.nom AS equipe_b_nom, equipe_a.id AS equipe_a_id, equipe_b.id AS equipe_b_id, equipe_a.serveur AS equipe_a_serveur, equipe_b.serveur AS equipe_b_serveur, goultarminator_videos.filmeur, goultarminator_videos.id AS id_video FROM goultarminator_matchs
LEFT JOIN goultarminator_equipes AS equipe_a ON goultarminator_matchs.equipe_a = equipe_a.id
LEFT JOIN goultarminator_equipes AS equipe_b ON goultarminator_matchs.equipe_b = equipe_b.id
LEFT JOIN goultarminator_videos ON goultarminator_matchs.id = goultarminator_videos.id_match
'.$add1.$add2.$add3);
if(!empty($add1))     $requete->bindValue(":serveur", $_POST["serveur"], PDO::PARAM_STR);
if(!empty($add2))     $requete->bindValue(":equipe", $_POST["equipe"], PDO::PARAM_STR);
$requete->execute();

//Affichage des matchs passés
echo '
<table>
	<tr>
		<th width="150">Date</th>
		<th width="300">Équipe A</th>
		<th width="300">Équipe B</th>
		<th width="400">Équipe Gagante</th>
		<th width="100">Carte</th>
		<th width="100">Vidéo</th>
	</tr>
	<tbody>';
	if($requete->rowCount() > 0) {
		while($data = $requete->fetch()) {
			$nomA = stripslashes(utf8_encode($data["equipe_a_nom"]));
			$nomB = stripslashes(utf8_encode($data["equipe_b_nom"]));
			
			$serveurA = stripslashes(utf8_encode($data["equipe_a_serveur"]));
			$serveurB = stripslashes(utf8_encode($data["equipe_b_serveur"]));
			
			$imageServeurA = strtr($serveurA, $GLOBALS['normalizeChars']);
			$imageServeurA = preg_replace("#ï#", "i", $serveurA);
			$imageServeurA = preg_replace("#\s#", "", $imageServeurA);
			
			$imageServeurB = strtr($serveurB, $GLOBALS['normalizeChars']);
			$imageServeurB = preg_replace("#ï#", "i", $imageServeurB);
			$imageServeurB = preg_replace("#\s#", "", $imageServeurB);

			$raison = stripslashes(utf8_encode($data["raison"]));

			echo '
			<tr>
				<td>'.date("d\/m\/Y", $data["date"]).'</td>
				<td style="padding:7px 5px 3px;">
					<img src="../Images/Serveurs/'.$imageServeurA.'.png" alt="" height="30" style="background:#FFF; vertical-align:middle; position:relative; top:-2px; bottom:-2px; margin-right:2px;" /> <a href="Equipes.php?Equipe='.$data["equipe_a_id"].'">'.$nomA.'</a>
				</td>
				<td style="padding:7px 5px 3px;">
					<img src="../Images/Serveurs/'.$imageServeurB.'.png" alt="" height="30" style="background:#FFF; vertical-align:middle; position:relative; top:-2px; bottom:-2px; margin-right:2px;" /> <a href="Equipes.php?Equipe='.$data["equipe_b_id"].'">'.$nomB.'</a>
				</td>';
				if($data["equipe_gagnante"] == $data["equipe_b_id"]) {
					echo '
					<td style="padding:7px 5px 3px;">
					<img src="../Images/Serveurs/'.$imageServeurB.'.png" alt="" height="30" style="background:#FFF; vertical-align:middle; position:relative; top:-2px; bottom:-2px; margin-right:2px;" /> <a href="Equipes.php?Equipe='.$data["equipe_b_id"].'">'.$nomB.'</a> - '.$raison.'
					</td>';
				}
				elseif($data["equipe_gagnante"] == $data["equipe_a_id"]) {
					echo '
					<td style="padding:7px 5px 3px;">
					<img src="../Images/Serveurs/'.$imageServeurA.'.png" alt="" height="30" style="background:#FFF; vertical-align:middle; position:relative; top:-2px; bottom:-2px; margin-right:2px;" /> <a href="Equipes.php?Equipe='.$data["equipe_a_id"].'">'.$nomA.'</a> - '.$raison.'
					</td>';
				}
				elseif($data["equipe_gagnante"] == 1111111111) {
					echo '<td>'.$raison.'</td>';
				}
				else {
					echo '<td>A venir</td>';
				}
				
				echo '<td align="center"><a href="'.ABSPATH.'Goultarminator/Images/Map-'.$data["image"].'.jpg" class="maGalerieLink">Voir</a></td>';

				if($data["id_video"] == NULL) {
					echo '<td align="center">A venir</td>';
				}
				else {
					echo '<td align="center"><a href="'.ABSPATH.'Goultarminator/Videos.php?id='.$data["id_video"].'">Voir</a></td>';
				}
			echo '</tr>';
		}
	}
	else {
		echo '<tr><td colspan="7" align="center" style="padding:7px;">Aucun match n\'a été trouvé.</td></tr>';
	}
echo '</tbody>
</table>';

//Affichage de la pagination des matchs passés
if(!isset($_POST["serveur"])) {
	if($nb_pages_ancien > 1) {
		if(isset($_GET["PageVenir"])) {
			echo '<p class="pagination" align="center">'.pagination($page, $nb_pages_ancien, "?PageAncien=%d&amp;PageVenir=".$_GET["PageVenir"]).'</p>';
		}
		else {
			echo '<p class="pagination" align="center">'.pagination($page, $nb_pages_ancien, "?PageAncien=%d").'</p>';
		}
	}
}

include("../Footer.php"); ?>