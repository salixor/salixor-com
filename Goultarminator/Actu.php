<?php 
$titre_page_nom = "Actualité du Goultarminator";
include("../Header.php");
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Goultarminator.png" alt="" /> Actualité</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH; ?>">Accueil</a> > <a href="./">Goultarminator</a> > Actualité</p>

<p class="encadreInfos"><span class="right">Suivez sur cette page toute l'actualité du Goultarminator.</span></p>

<?php
$requete = $baseDeDonnes->query("
SELECT news.*, COUNT(commentaires.id) AS nb_commentaires
FROM news 
LEFT JOIN commentaires ON news.id = commentaires.id_news
WHERE news.classe = 'Goultarminator III'
GROUP BY news.id
ORDER BY news.id DESC");
$requete->execute();

while($data = $requete->fetch()) {
	$titreNews = stripslashes(utf8_encode($data['titre']));
	$dateEdit = $data['edit_date']; 
	$dateNews = $data['date']; 
	$classeNews = stripslashes(utf8_encode($data['classe']));
	$contenuNews = formatage(smileys(nl2br(stripslashes($data['contenu']))));
	$contenuNews = strip_tags(limiternews(preg_replace("#<p>#", " ", $contenuNews), 250));
	$idNews = intval($data['id']);

	$images = $data["images"];
	$imageExplode = explode(";", $images);
	if(isset($imageExplode[0]))      $imageMini = $imageExplode[0];
	if(isset($imageExplode[1]))      $imageBig = $imageExplode[1];

	$nombreComms = $data['nb_commentaires']." commentaires";

	switch($data['nb_commentaires']) {
		case "0":
			$nombreComms = "Aucun commentaire";
		break;
		case "1":
			$nombreComms = "1 commentaire";
		break;
	}

	$date = afficherDate($dateNews);
		
	echo '<div class="news" id="News'.$data["id"].'">';
		if(!empty($imageMini) && !empty($imageBig)) {
			if(file_exists('../'.$imageMini) && file_exists('../'.$imageBig)) {
				echo '
				<div class="imageLeft">
					<a href="'.ABSPATH.$imageBig.'" class="fancybox-news" title="'.$titreNews.'"><img src="'.ABSPATH.$imageMini.'" alt="" /></a>
				</div>';
			}
			else {
				echo '<div class="imageLeft"><img src="'.ABSPATH.'Images/News/Default-mini.png" alt="" /></div>';
			}
		}
		else {
			echo '<div class="imageLeft"><img src="'.ABSPATH.'Images/News/Default-mini.png" alt="" /></div>';
		}
		
		echo '<div class="head">';
			echo '<h4><a href="'.ABSPATH.'?News='.$idNews.'&amp;Titre='.optimiser_titre_et_classe($titreNews).'#News">'.$titreNews.'</a></h4>';
			echo '<div class="infos">';
				if($dateEdit == 0)    echo '<span class="date"><img src="'.ABSPATH.'Images/Forum/Time.png" alt="" /> Écrite le '.$date.'</span>';
				else                  echo '<span class="date"><img src="'.ABSPATH.'Images/Forum/Edition.png" alt="" /> Édité le '.convertirTimestamp($dateEdit).'</span>';
				
				echo '
				<span class="tag"><a href="'.ABSPATH.'?Classe='.$classeNews.'#News"><img src="'.ABSPATH.'Images/Tag.png" alt="" /> '.$classeNews.'</a></span>
				<span class="comm"><a href="'.ABSPATH.'?News='.$idNews.'&amp;Titre='.optimiser_titre_et_classe($titreNews).'#Commentaires"><img src="'.ABSPATH.'Images/CommentaireInfos.png" alt="" /> '.$nombreComms.'</a></span>';
			echo '</div>';
		echo '</div>';
		
		echo '<div class="contenu">';
			echo $contenuNews;
		echo '</div>';
		
		echo '<a class="readRest" href="'.ABSPATH.'?News='.$idNews.'&amp;Titre='.optimiser_titre_et_classe($titreNews).'#News">Lire la suite</a>';

		echo '<div style="clear:left;"></div>';
	echo '</div>';
}

include("../Footer.php"); ?>