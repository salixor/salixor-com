<?php $titre_page_nom = "Les Parchemins";
include("../../Header.php");


$legendesTables = '<tr class="equipementTitres">
					   <td width="100">Image</td>
					   <td width="70">Taille</td>
					   <td width="70">Ajout</td>
					   <td width="150">Contraintes</td>
					   <td width="300">Ressources</td>
					   <td width="70">Position</td>
				   </tr>';
				   
$legendesTablesSpecial = '<tr class="equipementTitres">
					          <td>Image</td>
							  <td>Ajout</td>
					          <td>Taille</td>
							  <td>Ressources</td>
							  <td>Position</td>
						  </tr>';
				   
$legendesTablesExp = '<tr class="equipementTitres">
						   <td width="100">Image</td>
						   <td width="180">Effets</td>
						   <td width="180">Monstres</td>
						   <td width="180">Taux de drop</td>
						   <td width="180">Seuil de prospection</td>
				     </tr>';
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Parchemins.png" alt="Parchemins" /> Les Parchemins</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH; ?>">Accueil</a> > <a href="../">Encyclopédie</a> > Les Parchemins</p>

<p class="encadreInfos">
	<span class="right">
		Les parchemins sont très anciens et très puissants. Ils servent à vous améliorer ou à vous aider à progresser, et se trouvent aux quatres coins du monde des Douzes, en échange de ressources à des PNJs, ou bien directement sur des monstres.
	</span>
</p>

<ul class="navigationPage left">
	<a href="#Caracteristiques"><li>Parchemins de Caractéristiques</li></a>
</ul>

<ul class="navigationPage right">
	<a href="#Experience"><li>Parchemins d'Experience</li></a>
</ul>

<h3 class="troisieme_titre" id="Caracteristiques">Parchemins de Caractéristiques</h3>

<p class="encadreInfos">
	<span class="right">
		Les parchemins de caractéristiques vous permettent d'améliorer vos caractéristiques selon leur puissance, mais aussi vos caractéristiques de base.
	</span>
</p>

<table id="Agilite" class="parcheminTable">
	<tr>
		<th colspan="6">Agilité</th>
	</tr>

	<?php echo $legendesTables; ?>

	<tr>
		<td rowspan="6" class="screen"><img src="Images/agilite.png" alt="Image" /></td>
	</tr>

	<tr>
		<td>Petit</td>
		<td>1 point</td>
		<td>0 à 25 de base</td>
		<td>100 Anneaux Agilesques</td>
		<td>[0, 3]</td>
	</tr>

	<tr class="parent">
		<td rowspan="2">Normal</td>
		<td rowspan="2">1 point</td>
		<td rowspan="2">26 à 50 de base</td>
		<td>70 Glands</td>
		<td>[2, 17]</td>
	</tr>

	<tr class="parent">
		<td>70 Glands<br/>70 Langues de Pissenlits</td>
		<td>[8, -2]</td>
	</tr>
								  
	<tr>
		<td>Grand</td>
		<td>1 point</td>
		<td>51 à 80 de base</td>
		<td>75 Glands<br/>75 Langues de Pissenlits<br/>70 Pétales de Rose</td>
		<td>[3, 4]</td>
	</tr>

	<tr class="parent">
		<td>Puissant</td>
		<td>2 point</td>
		<td>81 à 100 de base</td>
		<td>80 Glands<br/>80 Langues de Pissenlits<br/>80 Pétales de Rose<br/>20 Sporme de Champ Champ</td>
		<td>[3, 4]</td>
	</tr>
</table>

<!-- Fin de l'agilité !-->

<table id="Chance" class="parcheminTable">

	<tr>
		<th colspan="6">Chance</th>
	</tr>

	<?php echo $legendesTables; ?>
	
	<tr>
		<td rowspan="7" class="screen"><img src="Images/chance.png" alt="Image" /></td>
	</tr>

	<tr>
		<td rowspan="2">Petit</td>
		<td rowspan="2">1 point</td>
		<td rowspan="2">0 à 25 de base</td>
		<td>80 Pattes d'Arakne</td>
		<td>[2, -2]</td>
	</tr>

	<tr>
		<td>1 Amulette du Bucheron</td>
		<td>[0, 2]</td>
	</tr>

	<tr class="parent">
		<td>Normal</td>
		<td>1 point</td>
		<td>26 à 50 de base</td>
		<td>90 Pattes d'Arakne<br/>65 Trèfles à 5 feuilles</td>
		<td>[4, 8]</td>
	</tr>
								  
	<tr>
		<td>Grand</td>
		<td>1 point</td>
		<td>51 à 80 de base</td>
		<td>40 Ongles Chevaucheurs de Karne<br/>75 Pattes d'Arakne<br/>60 Trèfles à 5 feuilles</td>
		<td>[-2, -4]</td>
	</tr>

	<tr class="parent">
		<td>Puissant</td>
		<td>2 point</td>
		<td>81 à 100 de base</td>
		<td>40 Ongles Chevaucheurs de Karne<br/>60 Pattes d'Arakne<br/>55 Trèfles à 5 feuilles<br/>15 Queues du Mulou</td>
		<td>[2, 5]</td>
	</tr>

</table>

<!-- Fin de la chance !-->

<table id="Force" class="parcheminTable">

	<tr>
		<th colspan="6">Force</th>
	</tr>

	<?php echo $legendesTables; ?>
	
	<tr>
		<td rowspan="8" class="screen"><img src="Images/force.png" alt="Image" /></td>
	</tr>

	<tr>
		<td rowspan="3">Petit</td>
		<td rowspan="3">1 point</td>
		<td rowspan="3">0 à 25 de base</td>
		<td>100 Epines de Champ Champ</td>
		<td>[1, 3]</td>
	</tr>

	<tr>
		<td>100 Peaux de Larve Bleue</td>
		<td>[2, 0]</td>
	</tr>

	<tr>
		<td>80 Pics de Prespic</td>
		<td>[1, -2]</td>
	</tr>

	<tr class="parent">
		<td>Normal</td>
		<td>1 point</td>
		<td>26 à 50 de base</td>
		<td>70 Pics de Prespic<br/>70 Pinces de Crabe</td>
		<td>[-2, -3]</td>
	</tr>
								  
	<tr>
		<td>Grand</td>
		<td>1 point</td>
		<td>51 à 80 de base</td>
		<td>40 Côtes de Rib<br/>70 Pics de Prespic<br/>75 Pinces de Crabe</td>
		<td>[8, -2]</td>
	</tr>

	<tr class="parent">
		<td>Puissant</td>
		<td>2 point</td>
		<td>81 à 100 de base</td>
		<td>45 Côtes de Rib<br/>75 Pics de Prespic<br/>75 Pinces de Crabe<br/>30 Silex</td>
		<td>[-2, -3]</td>
	</tr>

</table>

<!-- Fin de la force !-->

<table id="Intelligence" class="parcheminTable">

	<tr>
		<th colspan="6">Intelligence</th>
	</tr>

	<?php echo $legendesTables; ?>

	<tr>
		<td rowspan="7" class="screen"><img src="Images/intelligence.png" alt="Image" /></td>
	</tr>

	<tr>
		<td>Petit</td>
		<td>1 point</td>
		<td>0 à 25 de base</td>
		<td>100 Ceintures de Chance</td>
		<td>[7, 1]</td>
	</tr>

	<tr class="parent">
		<td>Normal</td>
		<td>1 point</td>
		<td>26 à 50 de base</td>
		<td>50 Peaux de Larve Bleue<br/>40 Peaux de Larve Orange<br/>40 Peaux de Larve Verte</td>
		<td>[0, 5]</td>
	</tr>
								  
	<tr>
		<td>Grand</td>
		<td>1 point</td>
		<td>51 à 80 de base</td>
		<td>30 Ailes de Tofu Maléfique<br/>45 Peaux de Larve Bleue<br/>45 Peaux de Larve Orange<br/>45 Peaux de Larve Verte</td>
		<td>[5, 4]</td>
	</tr>

	<tr class="parent">
		<td>Puissant</td>
		<td>2 point</td>
		<td>81 à 100 de base</td>
		<td>40 Ailes de Tofu Maléfique<br/>60 Peaux de Larve Bleue<br/>50 Peaux de Larve Orange<br/>50 Peaux de Larve Verte<br/>30 Sangs de Vampire</td>
		<td>[2, 2]</td>
	</tr>

</table>

<!-- Fin de l'intelligence !-->

<table id="Vitalite" class="parcheminTable">

	<tr>
		<th colspan="6">Vitalité</th>
	</tr>

	<?php echo $legendesTables; ?>

	<tr>
		<td rowspan="7" class="screen"><img src="Images/vitalite.png" alt="Image" /></td>
	</tr>

	<tr>
		<td rowspan="2">Petit</td>
		<td rowspan="2">1 point</td>
		<td rowspan="2">0 à 25 de base</td>
		<td>100 Défenses de Sanglier</td>
		<td>[12, 5]</td>
	</tr>

	<tr>
		<td>100 Gelées Bleutée</td>
		<td>[4, 5]</td>
	</tr>

	<tr class="parent">
		<td>Normal</td>
		<td>1 point</td>
		<td>26 à 50 de base</td>
		<td>80 Gelées Bleutée<br/>30 Gelées Menthe</td>
		<td>[3, -1]</td>
	</tr>
								  
	<tr>
		<td>Grand</td>
		<td>1 point</td>
		<td>51 à 80 de base</td>
		<td>60 Gelées Bleutée<br/>20 Gelées Menthe<br/>25 Gelées Fraise</td>
		<td>[3, 1]</td>
	</tr>

	<tr class="parent">
		<td>Puissant</td>
		<td>2 point</td>
		<td>81 à 100 de base</td>
		<td>60 Gelées Bleutée<br/>25 Gelées Menthe<br/>30 Gelées Fraise<br/>1 Gelée Bleutée Royale<br/>1 Gelée Fraise Royale</td>
		<td>[0, -2]</td>
	</tr>

</table>

<!-- Fin de la vitalité !-->

<table id="Sagesse" class="parcheminTable">

	<tr>
		<th colspan="6">Sagesse</th>
	</tr>

	<?php echo $legendesTables; ?>

	<tr>
		<td rowspan="7" class="screen"><img src="Images/sagesse.png" alt="Image" /></td>
	</tr>

	<tr>
		<td rowspan="2">Petit</td>
		<td rowspan="2">1 point</td>
		<td rowspan="2">0 à 25 de base</td>
		<td>100 Cornes de Bouftou</td>
		<td>[8, 2]</td>
	</tr>

	<tr>
		<td>100 Champignons</td>
		<td>[10, 3]</td>
	</tr>

	<tr class="parent">
		<td>Normal</td>
		<td>1 point</td>
		<td>26 à 50 de base</td>
		<td>80 Champignons<br/>60 Graines de Tournesol</td>
		<td>[10, 6]</td>
	</tr>
								  
	<tr>
		<td>Grand</td>
		<td>1 point</td>
		<td>51 à 80 de base</td>
		<td>90 Champignons<br/>70 Graines de Tournesol<br/>40 Graines de Chanvre</td>
		<td>[1, 1]</td>
	</tr>

	<tr class="parent">
		<td>Puissant</td>
		<td>2 point</td>
		<td>81 à 100 de base</td>
		<td>200 Ailes de Moskitos<br/>90 Champignons<br/>75 Graines de Tournesol<br/>60 Graines de Chanvre</td>
		<td>[-2, 14]</td>
	</tr>

</table>

<!-- Fin de la sagesse !-->

<table id="Sorts" class="parcheminTable">

	<tr>
		<th colspan="6">Point de Sorts</th>
	</tr>

	<?php echo $legendesTables; ?>

	<tr>
		<td rowspan="3" class="screen"><img src="Images/vitalite.png" alt="Image" /></td>
	</tr>

	<tr>
		<td>Normal</td>
		<td>1 point</td>
		<td>Illimité</td>
		<td>5 Diamants Polis</td>
		<td>[2, 0]</td>
	</tr>

</table>

<!-- Fin des sorts !-->

<h3 class="troisieme_titre" id="Experience">Parchemins d'Expérience</h3>

<p class="encadreInfos">
	<span class="right">
		Les Parchemins d'expérience vous permettent d'obtenir de l'expérience variant selon leur puissance.
	</span>
</p>

<table id="Blanc" class="parcheminTable">

	<tr>
		<th colspan="6">Parchemin Blanc</th>
	</tr>

	<?php echo $legendesTablesExp; ?>

	<tr>
		<td rowspan="3" class="screen"><img src="Images/vitalite.png" alt="Image" /></td>
	</tr>

	<tr>
		<td>+100 XP</td>
		<td>Tous les monstres</td>
		<td>2%</td>
		<td>150</td>
	</tr>

</table>

<!-- Fin du blanc !-->

<table id="Ivoire" class="parcheminTable">

	<tr>
		<th colspan="6">Parchemin d'Ivoire</th>
	</tr>

	<?php echo $legendesTablesExp; ?>

	<tr>
		<td rowspan="7" class="screen"><img src="Images/vitalite.png" alt="Image" /></td>
	</tr>

	<tr>
		<td>+1.000 XP</td>
		<td>Tous les monstres</td>
		<td>1,5%</td>
		<td>150</td>
	</tr>
	
	<tr class="parent">
		<td>+1.000 XP</td>
		<td>Mama Bwork</td>
		<td>24%</td>
		<td>300</td>
	</tr>
	
	<tr>
		<td>+1.000 XP</td>
		<td>Cybwork</td>
		<td>24%</td>
		<td>300</td>
	</tr>
	
	<tr class="parent">
		<td>+1.000 XP</td>
		<td>Bworkcelhaine</td>
		<td>24%</td>
		<td>300</td>
	</tr>
	
	<tr>
		<td>+1.000 XP</td>
		<td>Bworkette</td>
		<td>24%</td>
		<td>300</td>
	</tr>
	
	<tr class="parent">
		<td>+1.000 XP</td>
		<td>Kreuvète la Bwork inégnue</td>
		<td>24%</td>
		<td>300</td>
	</tr>
	
	<?php echo $legendesTablesSpecial; ?>
	
	<tr>
		<td rowspan="15" class="screen"><img src="Images/vitalite.png" alt="Image" /></td>
	</tr>
	
	<tr>
		<td>Moyenne</td>
		<td>+1.000 XP</td>
		<td>10 Ailes de Moskito</td>
		<td>[4, 12]</td>
	</tr>

</table>

<!-- Fin de l'ivoire !-->

<table id="Dore" class="parcheminTable">

	<tr>
		<th colspan="6">Parchemin Doré</th>
	</tr>

	<?php echo $legendesTablesExp; ?>

	<tr>
		<td rowspan="20" class="screen"><img src="Images/vitalite.png" alt="Image" /></td>
	</tr>

	<?php
	$i = 0;
	
	$monstresDore = array("Rat d'égoutant", "Rat de marais", "Rat Batteur", "Rat Klure", "Rat Basher", "Rat Noir", "Rat Blanc", "Rat Bajoie", "Rat Croc", "Sphincter Cell", "Pandawa Ivre", "Ratlbol l'aigri", "Pandouille le titubant", "Bworkette", "Mama Bwork", "Bworkcelhaine", "Cybwork", "Kreuvète la bwork ingénue");
	$monstresDoreDrop = array("0,1%", "0,1%", "0,1%", "0,1%", "0,1%", "0,1%", "0,1%", "0,1%", "0,1%", "0,1%", "0,1%", "0,2%", "0,2%", "8%", "8%", "8%", "9%", "9%");
	$monstresDoreProspe = array("0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "200", "0", "200", "300", "300", "300", "400", "400");

	while($i < count($monstresDore)) {
		if($i%2 == 1) {
			echo '
			<tr class="parent">
				<td>+10.000 XP</td>
				<td>'.$monstresDore[$i].'</td>
				<td>'.$monstresDoreDrop[$i].'</td>
				<td>'.$monstresDoreProspe[$i].'</td>
			</tr>';
		}
		else {
			echo '
			<tr>
				<td>+10.000 XP</td>
				<td>'.$monstresDore[$i].'</td>
				<td>'.$monstresDoreDrop[$i].'</td>
				<td>'.$monstresDoreProspe[$i].'</td>
			</tr>';
		}
		$i++;
	} ?>

</table>

<!--Fin du doré!-->

<a href="../">&laquo; Retour à l'Encyclopédie</a>

<?php include("../../Footer.php"); ?>