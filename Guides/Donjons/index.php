<?php $titre_page_nom = "Les Donjons";
include("../../Header.php");

if(isset($_GET['Donjon'])) { 

$sql_donjons = "SELECT infos_donjons.*, zones.nom AS zone_donjon
				FROM infos_donjons 
				LEFT JOIN zones 
		  		ON zones.id = infos_donjons.id_zone
				WHERE infos_donjons.id_donjon = ".$_GET['Donjon'].""; 

$requete_donjons = query($sql_donjons);

	$data_donjons = mysql_fetch_assoc($requete_donjons);
	$nom_donjon = utf8_encode($data_donjons['nom_donjon']);
	$zone = utf8_encode($data_donjons['zone_donjon']);
	$type = utf8_encode($data_donjons['type_donjon']);
	$position = $data_donjons['position_donjon'];
	$nb_salles = $data_donjons['salles_donjons'];
	$equipe = utf8_encode($data_donjons['equipe_donjon']);
	$temps = utf8_encode($data_donjons['temps_donjon']);
	$capture = utf8_encode($data_donjons['capture_donjon']);

if(mysql_num_rows($requete_donjons) == 0) {
    echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Erreur_404.png" alt="Donjons" /> Erreur</h2>';
    
	echo '<div class="encadre_infos"><p><img src="'.ABSPATH.'Images/erreur.png" alt="Erreur" align="top" /> Ce donjon n\'existe pas. <a href="./">Retour</a> aux donjons.</p></div>';
}
else { ?>	
	<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Donjons_view.png" alt="Donjons" /> <?php echo $nom_donjon; ?></h2>
	
	<p class="Arianne">> <a href="<?php echo ABSPATH; ?>">Accueil</a> > <a href="./">Encyclopédie</a> > <a href="index.php">Les Donjons</a> > <?php echo $nom_donjon; ?></p>
	
	<ul class="navigationPage left">
		<a href="#Commencer"><li>Avant de Commencer</li></a>
		<a href="#Salles"><li>Les Salles</li></a>
	</ul>
	
	<ul class="navigationPage right">
		<a href="#Monstres"><li>Les Monstres</li></a>
		<a href="#Conclusion"><li>Mot de la Fin</li></a>
	</ul>

	<table id="Informations">
		<tr>
			<th colspan="2">Informations sur le donjon</th>
		</tr>
		<tr>
			<td>Zone</td>
			<td><?php echo $zone; ?></td>
		</tr>
		<tr>
			<td>Position</td>
			<td><?php echo $position; ?></td>
		</tr>
		<tr>
			<td>Type</td>
			<td><?php echo $type; ?></td>
		</tr>
		<tr>
			<td>Nombre de salles</td>
			<td><?php echo $nb_salles; ?></td>
		</tr>
		<tr>
			<td>Équipe conseillée</td>
			<td><?php echo $equipe; ?></td>
		</tr>
		<tr>
			<td>Temps estimé</td>
			<td><?php echo $temps; ?></td>
		</tr>
		<tr>
			<td>Pierre de capture</td>
			<td><?php echo $capture; ?></td>
		</tr>
	</table>
	
	<?php
	$requete = $baseDeDonnes->prepare(
	"SELECT monstres_donjons.*
	FROM monstres_donjons
	LEFT JOIN infos_donjons
	ON monstres_donjons.id_donjon = infos_donjons.id_donjon
	WHERE infos_donjons.id_donjon = :id"); 
	$requete->bindValue(":id", $_GET['Donjon'], PDO::PARAM_INT);
	$requete->execute();
	
	echo '<h3 class="troisieme_titre" id="Monstres">Les Monstres</h3>';
	
	if($requete->rowCount() > 0) {
		echo '<div id="monstres">';
		echo '<div id="messageMonstres"></div>';
			
		while($data = $requete->fetch()) {
			$id = $data['id'];
			$nom = utf8_encode(stripslashes($data['nom']));
			$image = $data['image'];
			$description = utf8_encode(stripslashes(nl2br($data['description']))); ?>
		
			<div class="ficheMonstre" id="<?php echo $id ?>">
				<div class="openFicheMonstre">
					<p class="nomMonstre"><?php echo $nom; ?></p>
				</div>
				<div class="contentFicheMonstre">
					<p class="imageMonstre"><a class="zoomMonstre" href="<?php echo $image; ?>" title="<?php echo $nom; ?>" rel-fiche="Voir la fiche du monstre"><img src="<?php echo $image; ?>" alt="<?php echo $nom; ?>" /></a></p>
					<p class="descriptionMonstre"><?php echo $description; ?></p>
				</div>
			</div>
		
	<?php
		}
		
		echo '</div>';
	}
	else {
        echo '<p class="encadreInfos"><span class="left">Avertissement</span><span class="right">Les monstres de ce donjon seront ajoutés plus tard.</span></p>';
	}
	echo '<div style="clear:left;"></div>';
	
	$requete->closeCursor();

	$requete = $baseDeDonnes->prepare(
	"SELECT salles_donjons.*, infos_donjons.id_donjon
	FROM salles_donjons
	LEFT JOIN infos_donjons
	ON infos_donjons.id_donjon = salles_donjons.id_donjon
	WHERE salles_donjons.id_donjon = :id
	ORDER BY salles_donjons.image"); 
	$requete->bindValue(":id", $_GET['Donjon'], PDO::PARAM_INT);
	$requete->execute();

	while($data = $requete->fetch()) {
		$id_donjon = $data['id_donjon'];
		$salle = $data['numero_salle'];
		$image = $data['image'];
		$description = utf8_encode(stripslashes(nl2br($data['description']))); 
		
		if($salle == "Introduction") {
			echo '<h3 class="troisieme_titre" id="Commencer">Avant de commencer</h3>';
		}
		elseif($salle == "Conclusion") {
			echo '<h3 class="troisieme_titre" id="Conclusion">Mot de la Fin</h3>';
		}
		elseif($salle == 1) {
			echo '<h3 class="troisieme_titre" id="Salles">Les Salles</h3>';
		}
		elseif(!is_numeric($salle)) { 
			echo '<h3 class="troisieme_titre" id="Salles">'.utf8_encode($salle).'</h3>';
		}

		echo '<table class="donjonTable" id="Salle'.$salle.'">';
			if(intval($salle)) {
				echo '
				<tr>
					<th align="center">
						<a href="'.ABSPATH.'Guides/Donjons?Donjon='.$id_donjon.'#Salle'.$salle.'" class="lienEquipement">+</a> Salle '.$salle.'
					</th>
				</tr>';
			}
			else {
				echo '
				<tr>
					<th align="center">
						<a href="'.ABSPATH.'Guides/Donjons?Donjon='.$id_donjon.'#Salle'.$salle.'" class="lienEquipement">+</a> '.$salle.'
					</th>
				</tr>';
			}
			echo '
			<tr>
				<td class="center"><a href="'.$image.'" class="zoomDonjon"><img src="'.$image.'" alt="'.$salle.'" /></a></td>
			</tr>
			<tr class="description">
				<td>'.$description.'</td>
			</tr>
		</table>';
	} 
}

}

elseif(empty($_GET['Donjon'])) { ?> 
	
	<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Donjons.png" alt="Donjons" /> Les Donjons</h2>

	<p class="Arianne">> <a href="<?php echo ABSPATH; ?>">Accueil</a> > <a href="../">Encyclopédie</a> > Les Donjons</p>

	<p class="encadreInfos">
		<span class="right">
			Les donjons sont des lieux constitués de plusieurs salles dans lesquelles sont renfermées des monstres. On peut y récupérer des ressources rares, y découvrir de nouveaux monstres et de nombreux décors.
			<br />Plus vous montez en niveau, plus il faut relever de stratégie et de force pour vaincre les monstres enfermés dans les donjons.
		</span>
	</p>
	
	<table>

	<?php
	$requete = $baseDeDonnes->query("
	SELECT infos_donjons.*, zones.nom AS zone_donjon
	FROM infos_donjons 
	LEFT JOIN zones 
	ON zones.id = infos_donjons.id_zone
	ORDER BY zones.nom, id ASC"); 
	$requete->setFetchMode(PDO::FETCH_OBJ);
	
	$actualZone = NULL;
	while($data = $requete->fetch()) {
		$zone = utf8_encode($data->zone_donjon);
		$idDonjon = $data->id_donjon;
		$nomDonjon = utf8_encode($data->nom_donjon);
		$type = utf8_encode($data->type_donjon);
		$position = $data->position_donjon;
		$nbSalles = $data->salles_donjons; 
		
		if($zone != $actualZone) {
			echo '
			<tr>
				<th style="text-align:left; width:350px;">'.$zone.'</th>
				<th style=" width:150px;">Entrée</th>
				<th style=" width:150px;">Type</th>
				<th style=" width:110px;">Salles</th>
			</tr>';
			$actualZone = utf8_encode($data->zone_donjon);
		}

		echo '
		<td><a href="?Donjon='.$idDonjon.'">'.$nomDonjon.'</a></td>
			<td>'.$position.'</td>
			<td>'.$type.'</td>
			<td>'.$nbSalles.' salles</td>
		</tr>';
	}

	echo '</table>';
}

include("../../Footer.php");
?>