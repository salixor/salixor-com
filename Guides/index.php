<?php
$titre_page_nom = "Encyclopédie";
include("../Header.php");
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Encyclopedie.png" alt="Equipements" /> Encyclopédie</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH; ?>">Accueil</a> > Encyclopédie</p>

<p class="encadreInfos"><span class="right">L'Encyclopédie, c'est un peu le lieu ou vous pourrez découvrir des informations sur le jeu comme pour les Donjons, les parchemins, ou encore plus tard les métiers.</span></p>

<ul class="navigationPage">
	<a href="Donjons"><li><img src="<?php echo ABSPATH; ?>Images/icones_pages/LittlesIcones/Donjon.png" alt="" /> Les Donjons</li></a>
	<a href="Parchemins"><li><img src="<?php echo ABSPATH; ?>Images/icones_pages/LittlesIcones/Parchemin.png" alt="" /> Les Parchemins</li></a>
	<li class="disabled"><img src="<?php echo ABSPATH; ?>Images/icones_pages/LittlesIcones/Metier.png" alt="" /> Les Métiers<span></span></li>
</ul>

<?php include("../Footer.php"); ?>