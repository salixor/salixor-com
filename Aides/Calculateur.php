<?php 
$titre_page_nom = "Calculateur de Dégats";
include("../Header.php");

$classes = array("Crâ", "Ecaflip", "Eniripsa", "Enutrof", "Féca", "Iop", "Osamodas", "Pandawa", "Sacrieur", "Sadida", "Sram", "Xélor", "Roublard", "Zobal");
sort($classes);

$armes = array("Arc", "Baguette", "Bâton", "Dague", "Épée", "Hache", "Marteau", "Pelle");
sort($armes);

$maitrises = array("0", "1", "2", "3", "4", "5", "6");
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/calculateur.png" alt="" /> Calculateur de Dégats</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH; ?>">Accueil</a> > <a href="./">Outils</a> > Calculateur de Dégats</p>

<p class="encadreInfos"><span class="right">Voici le calculateur de Dégats du site.<br />Il s'agit ici de la v1, une v2 étant en préparation mais n'ayant pas de date de sortie de prévue.</span></p>

<div class="bloc">
	<form method="post">
		<!--<h4 class="quatrieme_titre">Général</h4>
			<p>
				<label for="classe">Classe <span class="rouge">*</span> : </label>
				<select name="classe" id="classe">
				<?php
				/*foreach($classes as $cle => $classe) {
					echo '<option value="'.$classe.'">'.$classe.'</option>';
				}*/
				?>
				</select>
			</p>
			<p>
				<label for="arme">Arme <span class="rouge">*</span> : </label>
				<select name="arme" id="arme">
				<?php
				/*foreach($armes as $cle => $arme) {
					echo '<option value="'.$arme.'">'.$arme.'</option>';
				}*/
				?>
				</select>
			</p>
			<p>
				<label for="maitrise">Niveau de la Maîtrise <span class="rouge">*</span> : </label>
				<select name="maitrise" id="maitrise">
				<?php
				/*foreach($maitrises as $cle => $maitrise) {
					echo '<option value="'.$maitrise.'">'.$maitrise.'</option>';
				}*/
				?>
				</select>
			</p>!-->
			
		<h4 class="quatrieme_titre">Vos Caractéristiques</h4>
			<p>
				<label for="terre">Force <span class="rouge">*</span> : </label>
				<input type="text" value="" name="terre" id="terre" />
			</p>
			<p>
				<label for="feu">Intelligence <span class="rouge">*</span> : </label>
				<input type="text" value="" name="feu" id="feu" />
			</p>
			<p>
				<label for="eau">Chance <span class="rouge">*</span> : </label>
				<input type="text" value="" name="eau" id="eau" />
			</p>
			<p>
				<label for="air">Agilité <span class="rouge">*</span> : </label>
				<input type="text" value="" name="air" id="air" />
			</p>
			<p>
				<label for="pourcent_dommage">% Dommages <span class="rouge">*</span> : </label>
				<input type="text" value="" name="pourcent_dommage" id="pourcent_dommage" />
			</p>
			<p>
				<label for="dommages_fixes">Dommages fixes <span class="rouge">*</span> : </label>
				<input type="text" value="" name="dommages_fixes" id="dommages_fixes" />
			</p>
			<!--<p>
				<label for="bonus_cc">Bonus CC <span class="rouge">*</span> : </label>
				<input type="text" value="" name="bonus_cc" id="bonus_cc" />
			</p>
			<p>
				<label for="soin">Soin <span class="rouge">*</span> : </label>
				<input type="text" value="" name="soin" id="soin" />
			</p>!-->
			
		<h4 class="quatrieme_titre">Vos Sorts</h4>
			<p>
				<label for="base0">Dégats Neutre : </label>
				<input type="text" style="width:50px;" name="base1(neutre)" id="base0" /> à
				<input type="text" style="width:50px;" name="base2(neutre)" id="base0_2" />
			</p>
			<p>
				<label for="base1">Dégats Terre : </label>
				<input type="text" style="width:50px;" name="base1(terre)" id="base1" /> à
				<input type="text" style="width:50px;" name="base2(terre)" id="base1_2" />
			</p>
			<p>
				<label for="base2">Dégats Feu : </label>
				<input type="text" style="width:50px;" name="base1(feu)" id="base2" /> à
				<input type="text" style="width:50px;" name="base2(feu)" id="base2_2" />
			</p>
			<p>
				<label for="base3">Dégats Eau : </label>
				<input type="text" style="width:50px;" name="base1(eau)" id="base3" /> à
				<input type="text" style="width:50px;" name="base2(eau)" id="base3_2" />
			</p>
			<p>
				<label for="base4">Dégats Air : </label>
				<input type="text" style="width:50px;" name="base1(air)" id="base4" /> à
				<input type="text" style="width:50px;" name="base2(air)" id="base4_2" />
			</p>
			
		<!--<h4 class="quatrieme_titre">Votre Arme</h4>
			<p>
				<label for="base0">Dégats Neutre : </label>
				<input type="text" style="width:50px;" name="base1(neutre)" id="base0" /> à
				<input type="text" style="width:50px;" name="base2(neutre)" id="base0_2" />
			</p>
			<p>
				<label for="base1">Dégats Terre : </label>
				<input type="text" style="width:50px;" name="base1(terre)" id="base1" /> à
				<input type="text" style="width:50px;" name="base2(terre)" id="base1_2" />
			</p>
			<p>
				<label for="base2">Dégats Feu : </label>
				<input type="text" style="width:50px;" name="base1(feu)" id="base2" /> à
				<input type="text" style="width:50px;" name="base2(feu)" id="base2_2" />
			</p>
			<p>
				<label for="base3">Dégats Eau : </label>
				<input type="text" style="width:50px;" name="base1(eau)" id="base3" /> à
				<input type="text" style="width:50px;" name="base2(eau)" id="base3_2" />
			</p>
			<p>
				<label for="base4">Dégats Air : </label>
				<input type="text" style="width:50px;" name="base1(air)" id="base4" /> à
				<input type="text" style="width:50px;" name="base2(air)" id="base4_2" />
			</p>!-->
			
		<p align="center"><input type="submit" onclick="calculDegats(event);" value="Envoyer" name="envoyer" /> </p>
	</form>
</div>

<div id="resultats">
<?php
if(isset($_POST["envoyer"])) {
	include("AJAX_calcul.php");
}

echo '</div>';

include("../Footer.php"); ?>