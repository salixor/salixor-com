<?php
include_once('../Includes/Fonctions.php');
include_once('../Includes/Config.php');

if(isset($_POST['envoyer'])) {
	$erreurs = array();
	
	if(empty($_POST['base1(neutre)']) && empty($_POST['base2(neutre)']) && empty($_POST['base1(terre)']) && empty($_POST['base2(terre)']) && empty($_POST['base1(feu)']) && empty($_POST['base2(feu)']) && empty($_POST['base1(eau)']) && empty($_POST['base2(eau)']) && empty($_POST['base1(air)']) && empty($_POST['base2(air)'])) {
		array_push($erreurs, "Aucun sort n'a été spécifié");
	}
	
	if(empty($_POST['base1(neutre)']) XOR empty($_POST['base2(neutre)'])) {
		array_push($erreurs, "Les dégats du sort d'élément Neutre ne sont pas complets.");
	}
	if(empty($_POST['base1(terre)']) XOR empty($_POST['base2(terre)'])) {
		array_push($erreurs, "Les dégats du sort d'élément Terre ne sont pas complets.");
	}
	if(empty($_POST['base1(feu)']) XOR empty($_POST['base2(feu)'])) {
		array_push($erreurs, "Les dégats du sort d'élément Feu ne sont pas complets.");
	}
	if(empty($_POST['base1(eau)']) XOR empty($_POST['base2(eau)'])) {
		array_push($erreurs, "Les dégats du sort d'élément Eau ne sont pas complets.");
	}
	if(empty($_POST['base1(air)']) XOR empty($_POST['base2(air)'])) {
		array_push($erreurs, "Les dégats du sort d'élément Air ne sont pas complets.");
	}
	
	if(!empty($_POST['base1(neutre)']) && !empty($_POST['base2(neutre)'])) {
		if($_POST['base1(neutre)'] > $_POST['base2(neutre)']) {
			array_push($erreurs, "Les dégats du sort d'élément Neutre sont incohérents.");
		}
		else {
			$NEUTRE_ARRAY = array("1" => "NEUTRE_ARRAY", "Base1" => $_POST['base1(neutre)'], "Base2" => $_POST['base2(neutre)'], "Element" => $_POST['terre']);
		}
	}
	if(!empty($_POST['base1(terre)']) && !empty($_POST['base2(terre)'])) {
		if($_POST['base1(terre)'] > $_POST['base2(terre)']) {
			array_push($erreurs, "Les dégats du sort d'élément Terre sont incohérents.");
		}
		else {
			$TERRE_ARRAY = array("1" => "TERRE_ARRAY", "Base1" => $_POST['base1(terre)'], "Base2" => $_POST['base2(terre)'], "Element" => $_POST['terre']);
		}
	}
	if(!empty($_POST['base1(feu)']) && !empty($_POST['base2(feu)'])) {
		if($_POST['base1(feu)'] > $_POST['base2(feu)']) {
			array_push($erreurs, "Les dégats du sort d'élément Feu sont incohérents.");
		}
		else {
			$FEU_ARRAY = array("1" => "FEU_ARRAY", "Base1" => $_POST['base1(feu)'], "Base2" => $_POST['base2(feu)'], "Element" => $_POST['feu']);
		}
	}
	if(!empty($_POST['base1(eau)']) && !empty($_POST['base2(eau)'])) {
		if($_POST['base1(eau)'] > $_POST['base2(eau)']) {
			array_push($erreurs, "Les dégats du sort d'élément Eau sont incohérents.");
		}
		else {
			$EAU_ARRAY = array("1" => "EAU_ARRAY", "Base1" => $_POST['base1(eau)'], "Base2" => $_POST['base2(eau)'], "Element" => $_POST['eau']);
		}
	}
	if(!empty($_POST['base1(air)']) && !empty($_POST['base2(air)'])) {
		if($_POST['base1(air)'] > $_POST['base2(air)']) {
			array_push($erreurs, "Les dégats du sort d'élément Air sont incohérents.");
		}
		else {
			$AIR_ARRAY = array("1" => "AIR_ARRAY", "Base1" => $_POST['base1(air)'], "Base2" => $_POST['base2(air)'], "Element" => $_POST['air']);
		}
	}

	if(isset($erreurs) && $erreurs != NULL) {
		if(count($erreurs) > 1) {
			echo '<p class="encadreInfos">';
				echo '<span class="left erreur block"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreurs</span>';
				foreach($erreurs as $erreur_array) {
					echo '<span class="right">';
						echo $erreur_array;
					echo '</span>';
				}
			echo '</p>';
		}
		else {
			echo '
			<p class="encadreInfos">
				<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>
				<span class="right">'.$erreurs[0].'</span>
			</p>';
		}
	}
	
	if(isset($NEUTRE_ARRAY) OR isset($TERRE_ARRAY) OR isset($FEU_ARRAY) OR isset($EAU_ARRAY) OR isset($AIR_ARRAY)) {	
		$NOM_CARAC_ARRAY = array("NEUTRE", "TERRE", "FEU", "EAU", "AIR");		
		$liste_caracs = array("terre", "terre", "feu", "eau", "air");
		$liste_base1 = array("base1(neutre)", "base1(terre)", "base1(feu)", "base1(eau)", "base1(air)");
		$liste_base2 = array("base2(neutre)", "base2(terre)", "base2(feu)", "base2(eau)", "base2(air)");
		$liste_images = array("neutre", "terre", "feu", "eau", "air");
		$SORTS_INSERES = array();
	
		for($i = 0; $i < 4; $i++) {
			$VAR_TEST = $NOM_CARAC_ARRAY[$i]."_ARRAY";

			if(isset($NEUTRE_ARRAY) OR isset($TERRE_ARRAY) OR isset($FEU_ARRAY) OR isset($EAU_ARRAY) OR isset($AIR_ARRAY)) {
				if(isset($NEUTRE_ARRAY)) {
					if($VAR_TEST == $NEUTRE_ARRAY["1"]) {
						array_push($SORTS_INSERES, 0);
					}
				}
				if(isset($TERRE_ARRAY)) {
					if($VAR_TEST == $TERRE_ARRAY["1"]) {
						array_push($SORTS_INSERES, 1);
					}
				}
				if(isset($FEU_ARRAY)) {
					if($VAR_TEST == $FEU_ARRAY["1"]) {
						array_push($SORTS_INSERES, 2);
					}
				}
				if(isset($EAU_ARRAY)) {
					if($VAR_TEST == $EAU_ARRAY["1"]) {
						array_push($SORTS_INSERES, 3);
					}
				}
				if(isset($AIR_ARRAY)) {
					if($VAR_TEST == $AIR_ARRAY["1"]) {
						array_push($SORTS_INSERES, 4);
					}
				}
			}
		}
		
		if(empty($_POST['pourcent_dommage'])) {
			$_POST['pourcent_dommage'] = 0;
		}
		if(empty($_POST['dommages_fixes'])) {
			$_POST['dommages_fixes'] = 0;
		}
		
		$DEGAT_TOTAL_1 = array();
		$DEGAT_TOTAL_2 = array();
		
		foreach($SORTS_INSERES as $sort_insere) {
			$i = $sort_insere;
			$degats1 = too_much_roxx($_POST[$liste_base1[$i]], 2, "<")*(100 + under_zero(too_much_stats($_POST[$liste_caracs[$i]], 2500)) + too_much_stats($_POST['pourcent_dommage'], 2000)) / 100 + too_much_stats($_POST['dommages_fixes'], 2000);
			
			$degats2 = too_much_roxx($_POST[$liste_base2[$i]], 100, ">")*(100 + under_zero(too_much_stats($_POST[$liste_caracs[$i]], 2500)) + too_much_stats($_POST['pourcent_dommage'], 2000)) / 100 + too_much_stats($_POST['dommages_fixes'], 2000);
				
			echo '
			<p class="encadreInfos">
				<span class="right">
					<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/'.$liste_images[$i].'.png" alt="'.$liste_images[$i].'" /> 
					Vous taperez '.floor($degats1).' à '.floor($degats2).' dans l\'élément '.ucfirst($liste_images[$i]).'.
				</span>
			</p>';
			array_push($DEGAT_TOTAL_1, $degats1);
			array_push($DEGAT_TOTAL_2, $degats2);
		}
		
		if(count($SORTS_INSERES) > 1) {
			$i = 0;
			
			$total_1 = 0;
			foreach($DEGAT_TOTAL_1 as $degat_total_n1) {
				$i++;
				$total_1 = $total_1+$degat_total_n1;
			}
			
			$total_2 = 0;
			foreach($DEGAT_TOTAL_2 as $degat_total_n1) {
				$i++;
				$total_2 = $total_2+$degat_total_n1;
			}
			
			echo '
			<p class="encadreInfos">
				<span class="right">
					<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/multi.png" alt="Multi" /> 
					Vous taperez '.floor($total_1).' à '.floor($total_2).' au total.
				</span>
			</p>';
		}
	}
} 
?>