<?php
require "Config.php";
$connectSql = mysql_connect(SERVEUR_SQL, UTILISATEUR_SQL, MDP_SQL);
mysql_select_db(BASE_SQL, $connectSql);
?>

<!DOCTYPE HTML>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Kévin Cocchi - Site web perso'</title>
<meta name="description" content="Bienvenue sur le site personnel de Kévin Cocchi, un passioné de développement informatique.">
<meta name="author" content="Kévin Cocchi">

<link href="Styles.css" rel="stylesheet" type="text/css" />
<link rel="icon" type="image/png" href="<?php echo ABSPATH; ?>Images/Favicon.png" />
<link rel="apple-touch-icon-precomposed" href="<?php echo ABSPATH; ?>Images/Apple_icon-precomposed.png"/>

<script src="../Scripts/jquery-1.5.1.min.js" type="text/javascript"></script>
<script src="<?php echo ABSPATH; ?>Scripts/jquery.validate.js" type="text/javascript"></script>
<script src="<?php echo ABSPATH; ?>Scripts/general.js" type="text/javascript"></script>

<!--[if IE]>
<script type="text/javascript">
	document.createElement("header"); 
	document.createElement("footer"); 
	document.createElement("section"); 
	document.createElement("aside"); 
	document.createElement("nav"); 
	document.createElement("article"); 
	document.createElement("figure"); 
	document.createElement("figcaption"); 
	document.createElement("hgroup"); 
	document.createElement("time");
</script>
<![endif]-->
</head>

<body>

<div class="loaderPage">Chargement ...</div>

<header role="banner">
    <div class="conteneurHeader" id="Haut">
        <a href="<?php echo ABSPATH; ?>"><div class="logo">Kévin Cocchi</div></a>
        <div class="logoHover">Kévin Cocchi</div>
        
        <div class="clear"></div>

        <nav role="navigation">
            <div class="left"></div>
            <div class="center">
                <li class="on" link-ajax="page.php" rel-id="1"></li>
                <li link-ajax="fzefsefse.php" rel-id="2"></li>
                <li link-ajax="test.php" rel-id="3"></li>
            </div>
            <div class="right"></div>
            <div class="clear"></div>
        </nav>
        
    </div>
</header>
    
<div class="wrapper">
    <div class="wrapperContent">
        Contenu par défaut
    </div>
</div>

<footer>
    Salut
</footer>

</body>
</html>