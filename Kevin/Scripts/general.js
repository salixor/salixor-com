var arrayPages = new Array();
var i;

$(document).ready(function() {
	$('.wrapper').hide();

	var actualMargin = $('.wrapperContent').css("margin-left");
	
	$('.wrapperContent').hide().css({"marginLeft":"-100000px"}).show();

	$('header').css({"marginTop":"-500px"}).animate({"marginTop":"0"}, 1000, function() {
		$('.wrapperContent').animate({"marginLeft":actualMargin}, 500);
		$('.wrapper').fadeIn(400);
	});
	
	$('footer').css({"marginBottom":"-500px"}).animate({"marginBottom":"0"}, 1000);	
	
	$('nav').css({"marginTop":"-500px"}).animate({"marginTop":"0"}, 1000);

	$("#contactform").validate();

	var scrollElement = $("html");
	var scrollValue = scrollElement.scrollTop();
	if(scrollElement.scrollTop(scrollValue + 1).scrollTop() == scrollValue) {
		scrollElement = $("body");
	}
	else {
		scrollElement.scrollTop(scrollValue);
	}
	$("a[href*=#]").click(function() {
		if(location.hostname+location.pathname+location.search == this.hostname+this.pathname.replace(/^([^\/].*)/, "/$1")+this.search) {
			var hash = "#"+$(this).attr("href").split("#")[1];
			var target = $(hash == "#" ? body : hash);
			if(target.length) {
				var id = target.attr("id");
				target.attr("id", "");
				location.hash = hash;
				target.attr("id", id);
				var to = target.offset().top;
				scrollElement.animate({ scrollTop: to }, {
				duration: Math.abs($(window).scrollTop() - to) * 0.7,
				complete: function() { target.focus(); }
				});
				return false;
			}
		}
	});
	
	$("div.infobulle").infobulle();
	
	$('*[link-ajax]').click(function() {
		if($(this).hasClass("on")) {
			var rajouterOn = true;
		}
		
		$('nav li').removeClass('on');
		
		if(rajouterOn == true) {
			$(this).addClass("on");
		}
		
		i = $(this).attr("rel-id");
	
		if(!arrayPages[i]) {
			if($(this).hasClass("on")) {
			}
			else {
				var lien = $(this).attr("link-ajax");
				$(this).addClass('on');
				loadPage(lien);
			}
		}
		else {
			if($(this).hasClass("on")) {
			}
			else {
				$(this).addClass('on');
				showPage(i);
			}
		}
	});
});

jQuery.fn.infobulle = function() {
	return this.each(function(){
		jQuery(this).attr({infobulle: jQuery(this).attr("title")}).removeAttr("title");
	});
};

function loadPage(page) {
	$.ajax({
		url: page,
		success:function(msg) {
			arrayPages[i] = msg;
			showPage(i);
		}
	});
	
	ajaxLoader('.loaderPage');
}

function ajaxLoader(loader) {
	$(loader).ajaxStart(function(){
		$(this).show();
	});
	$(loader).ajaxStop(function(){
		$(this).hide();
	});
}

function showPage(id) {
	$('.wrapperContent').fadeOut(200, function() {
		$('.wrapperContent').empty().append(arrayPages[i]).fadeIn(400);
	})
}