<?php
header("Location:v2.php");
//If the form is submitted
if(isset($_POST['submit'])) {

	//Check to make sure that the name field is not empty
	if(trim($_POST['contactname']) == '') {
		$hasError = true;
	} else {
		$name = trim($_POST['contactname']);
	}

	//Check to make sure that the subject field is not empty
	if(trim($_POST['subject']) == '') {
		$hasError = true;
	} else {
		$subject = trim($_POST['subject']);
	}

	//Check to make sure sure that a valid email address is submitted
	if(trim($_POST['email']) == '')  {
		$hasError = true;
	} else if (strpos("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim($_POST['email']))) {
		$hasError = true;
	} else {
		$email = trim($_POST['email']);
	}

	//Check to make sure comments were entered
	if(trim($_POST['message']) == '') {
		$hasError = true;
	} else {
		if(function_exists('stripslashes')) {
			$comments = stripslashes(trim($_POST['message']));
		} else {
			$comments = trim($_POST['message']);
		}
	}

	//If there is no error, send the email
	if(!isset($hasError)) {
		$emailTo = 'kevin@cocchi.fr'; //Put your own email address here
		$body = "Nom : $name \n\n Email: $email \n\n Sujet : $subject \n\n Message :\n $comments";
		$headers = 'Contact depuis mon site \r\n ' . 'Envoyé par : ' . $email;

		mail($emailTo, $subject, $body, $headers);
		$emailSent = true;
	}
}
?>
<!DOCTYPE HTML>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Kévin Cocchi - Développement Informatique</title>
<meta name="description" content="Bienvenue sur le site personnel de Kévin Cocchi, un passioné de développement informatique.">
<meta name="author" content="Kévin Cocchi">
<link href="Styles.css" rel="stylesheet" type="text/css" />
<link rel="icon" type="image/png" href="<?php echo ABSPATH; ?>Images/Favicon.png" />
<link rel="apple-touch-icon-precomposed" href="<?php echo ABSPATH; ?>Images/Apple_icon-precomposed.png"/>
<script type="text/javascript" src="../Scripts/jquery-1.5.1.min.js"></script>
<script src="Scripts/jquery.validate.js" type="text/javascript"></script>
<script src="Scripts/general.js" type="text/javascript"></script>

<?php
require "Config.php";
$connect_sql = mysql_connect(SERVEUR_SQL, UTILISATEUR_SQL, MDP_SQL);
mysql_select_db(BASE_SQL, $connect_sql);
?>
</head>

<body>
<div class="conteneur_header" id="Haut">
    <header>
        <div class="discover_header">
        	<?php $req = query("SELECT * FROM kevin_works ORDER BY rand() DESC LIMIT 1");
				  $data = mysql_fetch_assoc($req);
			      $titre = stripslashes($data['titre']);
			      $image = stripslashes($data['image']);
			      $description = stripslashes(utf8_encode(nl2br($data['description']))); 
			   	  $id = intval($data['id']);
			?>
            <h3 class="discover_title"><span>Découvrez ...</span> <?php echo $titre; ?></h3>
            <div class="img"><img src="<?php echo $image; ?>" alt="<?php echo $titre; ?> - Image en chargement ou corrompue" /></div>
            <p class="discover_header_infos"><?php echo $description; ?></p>
        </div>
        <div class="logo">Kévin Cocchi<span></span></div>
        <div class="infos_header"><h3>Bienvenue !</h3>
            <p>Je m'appelle Kévin Cocchi et je suis un adolescent passioné de développement informatique !</p>
            <p>Sur ce site, vous pouvez trouver ce que je sais faire, mes différentes réalisations, mon CV et vous pouvez aussi déposer un avis ou me contacter.</p>
            <p>Je vous souhaite donc la bonne visite sur le site.</p>
        </div>
        <div class="clear"></div>
        <div class="navigation">
        	<div class="nav">Menu de Navigation : </div>
        	<a href="<?php echo ABSPATH; ?>Kevin/#News" class="nav" id="news_nav" title="Se rendre aux Informations">Informations</a>
        	<a href="<?php echo ABSPATH; ?>Kevin/#Competences" class="nav" id="competences_nav" title="Se rendre à Mes Compétences">Mes Compétences</a>
        	<a href="<?php echo ABSPATH; ?>Kevin/#CV" class="nav" id="cv_nav" title="Se rendre à Mon CV">Mon CV</a>
        	<a href="<?php echo ABSPATH; ?>Kevin/#Realisations" class="nav" id="realisations_nav" title="Se rendre à Mes Réalisations">Mes Réalisations</a>
        	<a href="<?php echo ABSPATH; ?>Kevin/#Contact" class="nav" id="contact_nav" title="Me Contacter">Contact</a>
        </div>
    </header>  
</div>

<div class="conteneur_contenu">
    <section id="News">
        <h3 class="h3_contenu">Informations</h3>
        	<div class="section_contenu">
				<?php $req = query("SELECT * FROM kevin_news ORDER BY date DESC");
                      while($data = mysql_fetch_assoc($req)) {
						  $titre = stripslashes(utf8_encode($data['titre']));
						  $contenu = stripslashes(utf8_encode(nl2br($data['contenu'])));
						  $date = stripslashes(utf8_encode(nl2br($data['date']))); 
						  $id = intval($data['id']); ?>
                    <div class="news">
                    <h3><?php echo $titre; ?><span class="date"><?php echo 'le '. date("j", strtotime($date)) .' '.affiche_mois($date).' '. date("Y", strtotime($date)) .''; ?></span></h3>
                    <p><?php echo $contenu; ?></p>
                	</div>
				<?php if($id > 0) {
                } else { ?>
                    <div class="bordure_bottom"></div>
                <?php } ?><br />
                <?php } ?>
            </div>
    </section>

    <section id="Competences">
        <h3 class="h3_contenu">Mes Compétences</h3>
        	<div class="section_contenu">
                <h4 class="competences">Web :</h4>
                    <div>HTML <div class="star">-</div>  <div class="star">-</div>  <div class="star disabled">-</div>  </div>
                    <div>PHP <div class="star">-</div>  <div class="star">-</div>  <div class="star disabled">-</div>  </div>
                    <div>CSS2 et inférieur <div class="star">-</div>  <div class="star">-</div>  <div class="star disabled">-</div>  </div>
                    <div>CSS3 <div class="star">-</div>  <div class="star">-</div>  <div class="star disabled">-</div>  </div>
                    <div>jQuery <div class="star">-</div>  <div class="star disabled">-</div>  <div class="star disabled">-</div>  </div>
					<div>AJAX <div class="star">-</div>  <div class="star disabled">-</div>  <div class="star disabled">-</div>  </div>
                <h4 class="competences">Programmes :</h4>
                    <div>AS2 <div class="star">-</div>  <div class="star disabled">-</div>  <div class="star disabled">-</div>  </div>
                    <div>AS3 <div class="star">-</div>  <div class="star disabled">-</div>  <div class="star disabled">-</div>  </div>
                <h4 class="competences">Bases de Données :</h4>
                    <div>MySql et PHP <div class="star">-</div>  <div class="star">-</div>  <div class="star disabled">-</div>  </div>
                    <div>PDO de PHP <div class="star">-</div>  <div class="star disabled">-</div>  <div class="star disabled">-</div>  </div>
                    <div>Requêtes MySql <div class="star">-</div>  <div class="star disabled">-</div>  <div class="star disabled">-</div>  </div>
                <h4 class="competences">Graphisme :</h4>
                    <div>Photoshop <div class="star">-</div>  <div class="star">-</div>  <div class="star disabled">-</div>  </div>
                    <div>Illustrator <div class="star">-</div>  <div class="star disabled">-</div>  <div class="star disabled">-</div>  </div>
            </div>
    </section>
    
    <section id="CV">
        <h3 class="h3_contenu">Mon CV</h3>
        	<div class="section_contenu">
                Disponible à cette adresse : <a href="CV.pdf">Cliquez</a>.
            </div>
    </section>
    
    <section id="Realisations">
        <h3 class="h3_contenu">Mes Réalisations</h3>
        	<div class="section_contenu">
				<?php $req = query("SELECT * FROM kevin_works");
                      while($data = mysql_fetch_assoc($req)) {
						  $titre = stripslashes($data['titre']);
						  $image = stripslashes($data['image']);
						  $description = stripslashes(utf8_encode(nl2br($data['description'])));
						  $tags =  explode(";", $data['tags']);
						  $id = intval($data['id']); ?>
                    <div class="realisation">
                    <h3><?php echo $titre; ?></h3>
                    <div class="img"><img src="<?php echo $image; ?>" alt="<?php echo $titre; ?> - Image en chargement ou corrompue" /></div>
                    <p><?php echo $description; ?></p>
                    <div class="tag">Ce qui a été utilisé :</div> 
                    <?php foreach($tags as $tag) {
						echo '<div class="tag">'.$tag.'</div>';
					} ?>
                    <div class="clear"></div>
                </div>
				<?php if($id > 1) {
                } else { ?>
                    <div class="bordure_bottom"></div>
                <?php } } ?>
            </div>
    </section>
    
    <section id="Contact">
        <h3 class="h3_contenu">Contact</h3>
        	<div class="section_contenu">
                <div id="contact-wrapper">
						<?php if(isset($hasError)) { //If errors are found ?>
                            <p class="error">Le Mail n'a pas pu être envoyé pour une ou plusieurs raisons. Vérifiez que vous avez rempli tous les champs avant de l'envoyer.</p>
                        <?php } ?>
                        
                        <?php if(isset($emailSent) && $emailSent == true) { //If email is sent ?>
                            <h3>Email envoyé !</h3>
                            <p>Merci <strong><?php echo $name;?></strong> pour avoir utilisé mon formulaire de contact.<br />Je vous recontacterais au plus vite.</p>
                        <?php } ?>                    
						<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>#Contact" id="contactform">
                        <div>
                            <label for="contactname"><strong>Nom :</strong></label>
                            <input type="text" name="contactname" id="contactname" value="" class="required" />
                        </div>
                
                        <div>
                            <label for="email"><strong>Email :</strong></label>
                            <input type="email" name="email" id="email" class="email required" value="" />
                        </div>
                
                        <div>
                            <label for="subject"><strong>Sujet :</strong></label>
                            <input type="text" name="subject" id="subject" class="required" value="" />
                        </div>
                
                        <div>
                            <label for="message"><strong>Message :</strong></label>
                            <textarea rows="5" name="message" id="message" class="required"></textarea>
                        </div>
                        <input type="submit" value="Envoyer" name="submit" />
                        <div class="clear"></div>
                    </form>
                </div>            
            </div>
    </section>
</div>

<div class="conteneur_footer">
    <footer>
        <div class="infobulle" title="Structuré en HTML 5"><img src="Images/HTML5_Logo_512.png" height="60" /></div>
        <div class="infobulle" title="Désigné en CSS3"><img src="Images/HTML5_Styling_512.png" height="60" /></div>
        <div class="infobulle" title="Développé en PHP"><img src="Images/php-med-trans.png" height="60" /></div>
        
        Site réalisé entièrement par Kévin Cocchi.<br />
        Crée en 2010. Dernière version le <?php echo date("d ", getlastmod()); echo affiche_mois(date("F ", getlastmod())); echo date(" Y", getlastmod());  ?>. Version 2.0.1<br />
		<?php echo "Page générée en ".$timer_requetes." secondes - "; echo "".$nb_requetes." requêtes exécutées"; ?><br />
        Site valide W3C - HTML5
	</footer>
</div>

</body>
</html>