<?php 
$titre_page_nom = "Le livre d'Or";
include("Header.php");
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/LivreOr.png" alt="" /> Le livre d'Or</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH; ?>">Accueil</a> > Livre d'Or</p>

<?php
if(isset($_POST["submit"])) {
	if(!isset($PSEUDO_MEMBRE)) {
		if(!empty($_POST['userCode']) && !empty($_POST["titre"]) && !empty($_POST["pseudo"]) && isset($_POST["note"]) && !empty($_POST["message"])) {
			$userCode = strtoupper($_POST['userCode']);
			
			if(md5($userCode) == $_SESSION['captcha']) {
				if(is_http($_POST["web"]) && strlen($_POST["pseudo"]) <= 200 && strlen($_POST["titre"]) <= 100) {
					$requete = $baseDeDonnes->prepare("
					INSERT INTO livre_or(titre, pseudo, date, site_web, message, note)
					VALUES(:titre, :pseudo, :date, :site, :message, :note)");
					$requete->bindValue(":titre", $_POST["titre"], PDO::PARAM_STR);
					$requete->bindValue(":pseudo", $_POST["pseudo"], PDO::PARAM_STR);
					$requete->bindValue(":date", time(), PDO::PARAM_INT);
					$requete->bindValue(":site", $_POST["web"], PDO::PARAM_STR);
					$requete->bindValue(":message", $_POST["message"], PDO::PARAM_STR);
					$requete->bindValue(":note", $_POST["note"], PDO::PARAM_INT);
					$requete->execute();

					echo '<p class="encadreInfos valide"><span class="right">Votre message a été ajouté. Merci d\'avoir donné votre avis !</span></p>';
				}
				else {
					if(!is_http($_POST["web"])) {
						echo '<p class="encadreInfos erreur"><span class="right">Le lien vers le site web est de forme incorrecte (http://exemple.fr).</span></p>';
					}
					if(strlen($_POST["pseudo"]) > 200) {
						echo '<p class="encadreInfos erreur"><span class="right">Le pseudo fait plus de 200 caratères.</span></p>';
					}
					if(strlen($_POST["titre"]) > 100) {
						echo '<p class="encadreInfos erreur"><span class="right">Le titre fait plus de 100 caratères.</span></p>';
					}
				}
			}
			else {
				echo '<p class="encadreInfos erreur"><span class="right">Le code n\'a pas été recopié correctement.</span></p>';
			}
		}
		else {
			echo '<p class="encadreInfos erreur"><span class="right">Un ou plusieurs des champs n\'ont pas été remplis.</span></p>';
		}
	}
	else {
		if(!empty($_POST["titre"]) && !empty($_POST["pseudo"]) && isset($_POST["note"]) && !empty($_POST["message"])) {
			if(is_http($_POST["web"]) && strlen($_POST["pseudo"]) <= 200 && strlen($_POST["titre"]) <= 100) {
				$requete = $baseDeDonnes->prepare("
				INSERT INTO livre_or(titre, pseudo, date, site_web, message, note)
				VALUES(:titre, :pseudo, :date, :site, :message, :note)");
				$requete->bindValue(":titre", $_POST["titre"], PDO::PARAM_STR);
				$requete->bindValue(":pseudo", $_POST["pseudo"], PDO::PARAM_STR);
				$requete->bindValue(":date", time(), PDO::PARAM_INT);
				$requete->bindValue(":site", $_POST["web"], PDO::PARAM_STR);
				$requete->bindValue(":message", $_POST["message"], PDO::PARAM_STR);
				$requete->bindValue(":note", $_POST["note"], PDO::PARAM_INT);
				$requete->execute();

				echo '<p class="encadreInfos valide"><span class="right">Votre message a été ajouté. merci d\'avoir donné votre avis !</span></p>';
			}
			else {
				if(!is_http($_POST["web"])) {
					echo '<p class="encadreInfos erreur"><span class="right">Le lien vers le site web est de forme incorrecte (http://exemple.fr).</span></p>';
				}
				if(strlen($_POST["pseudo"]) > 200) {
					echo '<p class="encadreInfos erreur"><span class="right">Le pseudo fait plus de 200 caratères.</span></p>';
				}
				if(strlen($_POST["titre"]) > 100) {
					echo '<p class="encadreInfos erreur"><span class="right">Le titre fait plus de 100 caratères.</span></p>';
				}
			}
		}
		else {
			echo '<p class="encadreInfos erreur"><span class="right">Un ou plusieurs des champs n\'ont pas été remplis.</span></p>';
		}
	}
}

$requete = $baseDeDonnes->query("SELECT COUNT(message) AS nb_messages, AVG(note) AS moyenne FROM livre_or");
$data = $requete->fetch();

$nbMessages = $data["nb_messages"];
$moyenne = round($data["moyenne"], 2);
?>

<p class="encadreInfos">
<span class="right">
Voici le livre d'or du site vous permettant de donner votre avis sur le site.<br />
Les seules règles sont d'être compréhensible et d'éviter le hors sujet.<br />
Pour le moment, <?php echo $nbMessages; ?> message(s) a/ont été envoyé(s), ce qui donne une moyenne de <?php echo $moyenne; ?> / 5.
</span>
</p>

<div class="bloc">
	<form method="post">
		<?php
		echo '<p><label for="titre">Titre du message <span class="rouge">*</span> : </label><input type="text" maxlength="50" size="40" name="titre" id="titre" /></p>';
		
		if(isset($ID_MEMBRE)) {
			echo '<p><label for="pseudo">Pseudo : </label><input type="hidden" maxlength="50" size="40" name="pseudo" id="pseudo" value="'.$PSEUDO_MEMBRE.'" />'.$PSEUDO_MEMBRE.'. <a href="Membres/Deconnexion.php">Se déconnecter</a>.</p>';
			
			$requete = $baseDeDonnes->prepare("SELECT membre_web FROM membres WHERE membre_id = :id");
			$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
			$requete->execute();
			$data = $requete->fetch();
			
			if(!empty($data["membre_web"])) {
				echo '<p><label for="web">Site Web : </label><input type="hidden" maxlength="500" size="40" name="web" id="web" value="'.$data["membre_web"].'" />'.$data["membre_web"].'</p>';
			}
			else {
				echo '<p><label for="web">Site Web : </label><input type="text" maxlength="500" size="40" name="web" id="web" /></p>';
			}
		}
		else {
			echo '<p><label for="pseudo">Pseudo <span class="rouge">*</span> : </label><input type="text" maxlength="50" size="40" name="pseudo" id="pseudo" /><a href="javascript:connexion();" style="margin:0 5px;">Se connecter</a></p>';
			echo '<p><label for="web">Site Web : </label><input type="text" maxlength="500" size="40" name="web" id="web" /></p>';
		}
		
		echo '
		<p>
			<label>Votre note <span class="rouge">*</span> : </label>
			
			<input type="radio" name="note" value="0" id="note-0" checked="checked" /> <label for="note-0">0</label>
			<input type="radio" name="note" value="1" id="note-1" /> <label for="note-1">1</label>
			<input type="radio" name="note" value="2" id="note-2" /> <label for="note-2">2</label>
			<input type="radio" name="note" value="3" id="note-3" /> <label for="note-3">3</label>
			<input type="radio" name="note" value="4" id="note-4" /> <label for="note-4">4</label>
			<input type="radio" name="note" value="5" id="note-5" /> <label for="note-5">5</label>
		</p>';

		afficher_smileys('message');
		$FormatagePanel = new FormatagePanel(true, true, true, 'message');
		$FormatagePanel->showFormatagePanel();
		?>

		<p><textarea name="message" id="message" style="height:250px;"></textarea></p>
		
		<?php if(!isset($PSEUDO_MEMBRE)) { ?>
			<p><label for="userCode">Entrez le code de l'image : </label><input name="userCode" id="userCode" type="text" /></p>
			
			<p class="captcha">
				<img src="<?php echo ABSPATH; ?>captcha.php" alt="Captcha" /><br />
				<a href="javascript:void(0);" id="changerCaptcha">Nouvelle Image</a>
			</p>
			<div class="clear"></div>
		<?php } ?>
		
		<p align="center">
			<input type="button" id="previsualiser_bouton" value="Prévisualiser" onclick="previsualiser('message','previsualiser_commentaire','AJAX_news.php');" />
			<input type="submit" value="Envoyer" name="submit" />
		</p>
				
		<p>
			<span class="rouge">*</span> Les champs marqués de ce symbole sont obligatoires.
		</p>
	</form>
</div>

<p align="center" id="previsualiser_zone">
	<div class="preview" id="previsualiser_commentaire"></div>
</p>

<?php
$requete = $baseDeDonnes->query("SELECT * FROM livre_or ORDER BY date DESC");

while($data = $requete->fetch()) {
	$images = "";
	for($i = 1; $i <= 5; $i++) {
		if($i <= $data["note"]) {
			$images .= '<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/EtoilePleine.png" alt="" style="margin-right:1px;" />';
		}
		if($i > $data["note"]) {
			$images .= '<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/EtoileVide.png" alt="" style="margin-right:1px;" />';
		}
	}

	echo '<div class="news big">';
		echo '<div class="head">';
			echo '<h4>'.stripslashes($data["titre"]).'</h4>';
			echo '<div class="infos">';
				echo '<span class="date"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Profil.png" alt="" /> Écrit par '.stripslashes($data["pseudo"]).'</span> ';
				echo '<span class="tag"><img src="'.ABSPATH.'Images/Forum/Time.png" alt="" /> Posté le '.convertirTimestamp($data["date"]).'</span> ';
				if(!empty($data["site_web"])) {
					echo '<span class="comm"><a href="'.$data["site_web"].'"><img src="'.ABSPATH.'Images/Membres/WebMembre.png" alt="" /> Voir son site</a></span> ';
				}
				echo '<span class="comm">Sa note : '.$images.'</span>';
			echo '</div>';
		echo '</div>';
		
		echo '<div class="contenu">';
			echo formatage(smileys(nl2br(stripslashes($data["message"]))));
			
			if(!empty($data["reponse"])) {
				echo '<div class="divReponse"><h5 class="cinquieme_titre"><img src="'.ABSPATH.'Images/Membres/Administrateur.png" alt="" /> Réponse de Salixor :</h5>';
				echo '<div class="reponse">'.formatage(smileys(nl2br(stripslashes(utf8_encode($data["reponse"]))))).'</div></div>';
			}
		echo '</div>';
		
		echo '<div style="clear:left;"></div>';
	echo '</div>';
}

include("Footer.php"); ?>