<?php
$titre_page_nom = "Les Sondages";
include("Header.php");

$erreurs = array();

if(isset($_POST['go'])) {
	if(empty($_POST['choix']) OR empty($_POST['sondage_en_cours']) OR empty($_POST['choix']) OR empty($_POST['sondage_en_cours'])) {
		$erreur = 'Aucune réponse n\'a été choisie.';
	}
	else {	
		$ipUser = $_SERVER["REMOTE_ADDR"];
		$time = time();

		if(isset($ID_MEMBRE)) {
			$requete = $baseDeDonnes->prepare("
			SELECT sondage_questions.*, sondage_ip.* FROM sondage_questions
			LEFT JOIN sondage_ip ON sondage_ip.id_sondage = sondage_questions.id
			WHERE sondage_questions.id = :sondage AND (sondage_ip.ip = :ip OR sondage_ip.id_membre = :membre)");
			$requete->bindValue(":sondage", $_POST['sondage_en_cours'], PDO::PARAM_INT);
			$requete->bindValue(":ip", $ipUser, PDO::PARAM_STR);
			$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
			$requete->execute();
		}
		else {
			$requete = $baseDeDonnes->prepare("
			SELECT sondage_questions.*, sondage_ip.* FROM sondage_questions
			LEFT JOIN sondage_ip ON sondage_ip.id_sondage = sondage_questions.id
			WHERE sondage_questions.id = :sondage AND sondage_ip.ip = :ip");
			$requete->bindValue(":sondage", $_POST['sondage_en_cours'], PDO::PARAM_INT);
			$requete->bindValue(":ip", $ipUser, PDO::PARAM_STR);
			$requete->execute();
		}
		
		while($data = $requete->fetch()) {
			if(isset($ID_MEMBRE)) {
				if($data["id_membre"] == $ID_MEMBRE || $data["ip"] == $ipUser) {
					$vote = false;
				}
			}
			else {
				if($data["ip"] == $ipUser) {
					$vote = false;
				}
			}
		}
		
		if(!isset($vote)) {
			$requete = $baseDeDonnes->prepare("
			UPDATE sondage_reponses SET nb_reponses = nb_reponses + 1
			WHERE id_sondage = :sondage AND id = :choix");
			$requete->bindValue(":sondage", $_POST['sondage_en_cours'], PDO::PARAM_INT);
			$requete->bindValue(":choix", $_POST['choix'], PDO::PARAM_INT);
			$requete->execute();

			if(isset($ID_MEMBRE)) {
				$requete = $baseDeDonnes->prepare("INSERT INTO sondage_ip(id_sondage, ip, id_membre) VALUES (:sondage, :ip, :id)");
				$requete->bindValue(":sondage", $_POST['sondage_en_cours'], PDO::PARAM_INT);
				$requete->bindValue(":ip", $ipUser, PDO::PARAM_STR);
				$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
				$requete->execute();
			}
			else {
				$requete = $baseDeDonnes->prepare("INSERT INTO sondage_ip(id_sondage, ip) VALUES (:sondages, :ip)");
				$requete->bindValue(":sondage", $_POST['sondage_en_cours'], PDO::PARAM_INT);
				$requete->bindValue(":ip", $ipUser, PDO::PARAM_STR);
				$requete->execute();
			}
			
			$valide = "Merci d'avoir voté.";
		}
		else {
			array_push($erreurs, "Vous avez déjà voté !");
		}
	}
}

echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Sondage.png" alt="Sondage" /> Les Sondages</h2>';

if(isset($_GET['Question'])) {
	if(!empty($_GET["Question"])) {
		$ipUser = $_SERVER["REMOTE_ADDR"];
		$time = time();

		if(isset($ID_MEMBRE)) {
			$requete = $baseDeDonnes->prepare("
			SELECT sondage_questions.*, sondage_ip.* FROM sondage_questions
			LEFT JOIN sondage_ip ON sondage_ip.id_sondage = sondage_questions.id
			WHERE sondage_questions.id = :sondage AND (sondage_ip.ip = :ip OR sondage_ip.id_membre = :membre OR sondage_questions.date_fin < :time)");
			$requete->bindValue(":sondage", $_GET['Question'], PDO::PARAM_INT);
			$requete->bindValue(":ip", $ipUser, PDO::PARAM_STR);
			$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
			$requete->bindValue(":time", $time, PDO::PARAM_INT);
			$requete->execute();
		}
		else {
			$requete = $baseDeDonnes->prepare("
			SELECT sondage_questions.*, sondage_ip.* FROM sondage_questions
			LEFT JOIN sondage_ip ON sondage_ip.id_sondage = sondage_questions.id
			WHERE sondage_questions.id = :sondage AND (sondage_ip.ip = :ip OR sondage_questions.date_fin < :time)");
			$requete->bindValue(":sondage", $_GET['Question'], PDO::PARAM_INT);
			$requete->bindValue(":ip", $ipUser, PDO::PARAM_STR);
			$requete->bindValue(":time", $time, PDO::PARAM_INT);
			$requete->execute();
		}
		
		while($data = $requete->fetch()) {
			if(isset($ID_MEMBRE)) {
				if($data["id_membre"] == $ID_MEMBRE || $data["ip"] == $ipUser || $data["date_fin"] < $time) {
					if($data["id_membre"] == $ID_MEMBRE && $data["ip"] == $ipUser) {
						$ip = $data["ip"];
						$id_membre = $data["id_membre"];
					}
					$vote = false;
				}
			}
			else {
				if($data["ip"] == $ipUser || $data["date_fin"] < $time) {
					if($data["ip"] == $ipUser) {
						$ip = $data["ip"];
						$id_membre = $data["id_membre"];
					}
					$vote = false;
				}
			}
		}
		
		/*Le sondage est en cours, et on y a pas répondu*/
		if(!isset($vote) && !isset($_GET["Resultats"])) {
			$requete = $baseDeDonnes->prepare("
			SELECT sondage_questions.*, sondage_reponses.* FROM sondage_questions
			LEFT JOIN sondage_reponses ON sondage_reponses.id_sondage = sondage_questions.id
			WHERE sondage_questions.id = :sondage");
			$requete->bindValue(":sondage", $_GET['Question'], PDO::PARAM_INT);
			$requete->execute();
			
			if($requete->rowCount() == 0) {
				echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > Les Sondages</p>';
			
				echo '<p class="encadreInfos"><span class="right">Les Sondages ont pour but de me permettre de mieux connaître votre avis sur divers sujets en rapport avec le site. Venez voter pour faire avancer le site.</span></p>';
				
				array_push($erreurs, "Le sondage n'existe pas.");
			}
			else {
				$arrayId = array();
				$arrayReponses = array();
				
				while($data = $requete->fetch()) {
					$id = $data["id_sondage"];
					$dateFin = $data["date_fin"];
					$question = stripslashes($data["question"]);
					
					$arrayId[] = $data['id'];
					$arrayReponses[] = stripslashes(trim($data['reponse']));
				}
			
				echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="Sondage.php">Les Sondages</a> > Répondre à : '.$question.'</p>';
		
				echo '<p class="encadreInfos"><span class="right">'.$question.'</span></p>';
				
				echo '<div class="bloc">';
					
					echo '<form method="post" class="reponses_sondage" action="'.$_SERVER['PHP_SELF'].'">';
						
						for($i = 0; $i < count($arrayId); $i++) { 
							echo '<p><input type="radio" name="choix" id="choix'.$arrayId[$i].'" value="'.$arrayId[$i].'" /> <label for="choix'.$arrayId[$i].'" style="width:600px; text-align:left;">'.$arrayReponses[$i].'</label></p>'; 
						}
						
						echo '<input type="hidden" name="sondage_en_cours" value="'.$id.'" />';
						echo '
						<p align="center">
							<input type="submit" name="go" value="Voter" />
							<a href="?Question='.$_GET["Question"].'&amp;Resultats" class="allNews">
								<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/ResultatsSondage.png" alt="" /> Résultats
							</a>
						</p>';
					
					echo '</form>';
				
				echo '</div>';
			}
		}
		/*Le sondage est fini, ou on y a répondu*/
		elseif(isset($vote) || isset($_GET["Resultats"])) {
			$requete = $baseDeDonnes->prepare("
			SELECT sondage_questions.*, sondage_reponses.* FROM sondage_questions
			LEFT JOIN sondage_reponses ON sondage_reponses.id_sondage = sondage_questions.id
			WHERE sondage_questions.id = :sondage");
			$requete->bindValue(":sondage", $_GET['Question'], PDO::PARAM_INT);
			$requete->execute();
						
			if($requete->rowCount() == 0) {
				echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > Les Sondages</p>';
			
				echo '<p class="encadreInfos"><span class="right">Les Sondages ont pour but de me permettre de mieux connaître votre avis sur divers sujets en rapport avec le site. Venez voter pour faire avancer le site.</span></p>';
				
				array_push($erreurs, "Le sondage n'existe pas.");
			}
			else {
				$tableauReponses = array();
				$tableauNbReponses = array();
				$couleursSondage = array("#b3c7d7", "#a8bfd1", "#9cb7cb", "#91afc6", "#86a7c0", "#7b9fba", "#7097b4", "#b3c7d7", "#a8bfd1", "#9cb7cb", "#91afc6", "#86a7c0", "#7b9fba", "#7097b4");
				
				while($data = $requete->fetch()) {
					$dateFin = $data["date_fin"];
					$question = stripslashes($data["question"]);
					
					$tableauReponses[] = $data['reponse'];
					$tableauNbReponses[] = $data['nb_reponses'];
				}
			
				if(isset($ip) && isset($id_membre)) {
					if(isset($ID_MEMBRE)) {
						if($ipUser == $ip || $ID_MEMBRE == $id_membre) {
							$etat = '<img src="Images/icones_pages/LittlesIcones/YellowBullet.png" alt="" style="margin:3px 0 -3px;" /> A voté';
						}
					}
					else {
						if($ipUser == $ip) {
							$etat = '<img src="Images/icones_pages/LittlesIcones/YellowBullet.png" alt="" style="margin:3px 0 -3px;" /> A voté';
						}
					}
				}
				else {
					if($dateFin < time()) {
						$etat = '<img src="Images/icones_pages/LittlesIcones/MaintenanceDesactive.png" alt="" style="margin:3px 0 -3px;" /> Terminé';
					}
					if($dateFin > time()) {
						$etat = '<img src="Images/icones_pages/LittlesIcones/MaintenanceActive.png" alt="" style="margin:3px 0 -3px;" /> En cours';
					}
				}
				
				echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="Sondage.php">Les Sondages</a> > '.$question.'</p>';
		
				echo '<p class="encadreInfos"><span class="left">'.$etat.'</span><span class="right">'.$question.'</span></p>';
			
				$nbReponsesSondage = count($tableauReponses);
				$nbTotalReponse = array_sum($tableauNbReponses);
				
				for($i = 0; $i < $nbReponsesSondage; $i++) {
					$pourcentage = 0;
					if($nbTotalReponse != 0) {
						$pourcentage = ($tableauNbReponses[$i] * 100) / $nbTotalReponse;
					}
					$pourcentage = round($pourcentage, 1);
					
					echo '<div class="jauge">';
						echo '<span class="fill" rel="'.$pourcentage.'" style="width:'.$pourcentage.'%; background-color:'.$couleursSondage[$i].';">Pas bien de regarder les fichiers sources !</span>';
						echo '<div class="value">'.$tableauReponses[$i].' - '.$pourcentage.'%</div>';
						if($tableauNbReponses[$i] == 0) {
							echo '<span class="right">Aucun vote</span>';
						}
						elseif($tableauNbReponses[$i] == 1) {
							echo '<span class="right">1 vote</span>';
						}
						elseif($tableauNbReponses[$i] > 1) {
							echo '<span class="right">'.$tableauNbReponses[$i].' votes</span>';
						}
					echo '</div>';
				}
				
				if($nbTotalReponse == 0) {
					echo '<p class="encadreInfos">';
						echo '<span class="right"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Vote.png" alt="Vote" /> Aucun vote pour l\'instant.</span>';
					echo '</p>';
				}
				else {
					echo '<p class="encadreInfos">';
						echo '<span class="right"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Vote.png" alt="Vote" /> '.$nbTotalReponse.' personnes ont voté.</span>';
					echo '</p>';
				}
				
				if(!isset($vote)) {
					echo '
					<p align="center">
						<a href="?Question='.$_GET["Question"].'" class="allNews">
							<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/ResultatsSondage.png" alt="" /> Répondre au sondage
						</a>
					</p>';
				}
			}
		}
	}
	else {
		array_push($erreurs, "Aucun sondage est défini.");
	}
}
else {
	echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > Les Sondages</p>';
		
	echo '<p class="encadreInfos"><span class="right">Les Sondages ont pour but de me permettre de mieux connaître votre avis sur divers sujets en rapport avec le site. Venez voter pour faire avancer le site.</span></p>';
	
	$ipUser = $_SERVER["REMOTE_ADDR"];

	$requete = $baseDeDonnes->prepare("
	SELECT sondage_questions.*, sondage_ip.id_membre, sondage_ip.ip FROM sondage_questions
	LEFT JOIN sondage_ip ON sondage_questions.id = sondage_ip.id_sondage AND sondage_ip.ip = :ip
	ORDER BY sondage_questions.id DESC");
	$requete->bindValue(":ip", $ipUser, PDO::PARAM_INT);
	$requete->execute();
	
	echo '
	<table>
		<tr>
			<th width="430">Question</th>
			<th width="120">Début</th>
			<th width="120">Fin</th>
			<th width="90">État</th>
		</tr>';

		$i = 0;
		while($data = $requete->fetch()) {
			$dateFin = $data["date_fin"];
			
			if(isset($ID_MEMBRE)) {
				if($ipUser != $data["ip"] && $ID_MEMBRE != $data["id_membre"]) {
					if($dateFin < time()) {
						$etat = '<td><img src="Images/icones_pages/LittlesIcones/MaintenanceDesactive.png" alt="" style="margin:3px 0 -3px;" /> Terminé</td>';
					}
					else {
						$etat = '<td><img src="Images/icones_pages/LittlesIcones/MaintenanceActive.png" alt="" style="margin:3px 0 -3px;" /> En cours</td>';
					}
				}
				if($ipUser == $data["ip"] || $ID_MEMBRE == $data["id_membre"]) {
					$etat = '<td><img src="Images/icones_pages/LittlesIcones/YellowBullet.png" alt="" style="margin:3px 0 -3px;" /> A voté</td>';
				}
			}
			else {
				if($ipUser != $data["ip"]) {
					if($dateFin < time()) {
						$etat = '<td><img src="Images/icones_pages/LittlesIcones/MaintenanceDesactive.png" alt="" style="margin:3px 0 -3px;" /> Terminé</td>';
					}
					else {
						$etat = '<td><img src="Images/icones_pages/LittlesIcones/MaintenanceActive.png" alt="" style="margin:3px 0 -3px;" /> En cours</td>';
					}
				}
				if($ipUser == $data["ip"]) {
					$etat = '<td><img src="Images/icones_pages/LittlesIcones/YellowBullet.png" alt="" style="margin:3px 0 -3px;" /> A voté</td>';
				}
			}
			
			echo '
			<tr>
				<td><a href="?Question='.$data["id"].'">'.stripslashes($data["question"]).'</a></td>
				<td>'.date("j", $data['date_debut']).' '.afficherMois($data['date_debut']).' à '.date("h\hi", $data['date_debut']).'</td>
				<td>'.date("j", $data['date_fin']).' '.afficherMois($data['date_fin']).' à '.date("h\hi", $data['date_fin']).'</td>
				'.$etat.'
			</tr>';
		}

	echo '</table>';
}

if(isset($erreurs) AND $erreurs != NULL) {
	echo '<p class="encadreInfos">';
		echo '<span class="left erreur block"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreurs</span>';
		foreach($erreurs as $erreur_array) {
			echo '<span class="right">';
				echo $erreur_array;
			echo '</span>';
		}
	echo '</p>';
}
if(isset($valide)) {
	echo '<p class="encadreInfos">';
		echo '<span class="left valide block"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Valide.png" alt="Confirmation" /> Confirmation</span>';
		echo '<span class="right">'.$valide.'</span>';
	echo '</p>';
}
elseif(isset($information)) {
	echo '
	<p class="encadreInfos">
		<span class="left information"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Information.png" alt="Information" /> Information</span>
		<span class="right">'.$information.'</span>
	</p>';
}

include("Footer.php"); ?>