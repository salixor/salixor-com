<?php 
$titre_page_nom = "Modifier un commentaire";
include("Header.php");

echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Modifier_commentaire.png" alt="Modifier_commentaire" /> Modifier un Commentaire</h2>';

echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="'.ABSPATH.'?Page=1">Les News</a> > Modifier un Commentaire</p>';

if(isset($_POST["envoyer"])) {
	if(isset($_POST["commentaire"]) AND isset($_POST["redirection"])) {
		if(!empty($_POST["commentaire"]) AND !empty($_POST["redirection"])) {
			$requete = $baseDeDonnes->prepare('
			UPDATE commentaires SET commentaire = :commentaire, date_edition = :time, membre_edition = :membre WHERE id = :id');
			$requete->bindValue(':commentaire', $_POST["commentaire"], PDO::PARAM_STR);
			$requete->bindValue(':time', time(), PDO::PARAM_INT);
			$requete->bindValue(':membre', $ID_MEMBRE, PDO::PARAM_INT);
			$requete->bindValue(':id', $_GET["id"], PDO::PARAM_INT);
			$requete->execute();
			
			$valide = "Le commentaire a été modifié. <a href=\"".$_POST["redirection"]."#Commentaire".$_GET["id"]."\">Retourner</a> sur la news.";
		}
		else {
			$erreur = "Le commentaire est vide.";
		}
	}
	else {
		$erreur = "Un problème est survenu.";
	}
}

if(isset($erreur)) {
	echo '<p class="encadreInfos">';
		echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
		echo '<span class="right">'.$erreur.'</span>';
	echo '</p>';
}
if(isset($valide)) {
	echo '<p class="encadreInfos">';
		echo '<span class="left valide"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Valide.png" alt="Confirmation" /> Confirmation</span>';
		echo '<span class="right">'.$valide.'</span>';
	echo '</p>';
}

if(isset($_GET["id"]) AND !isset($valide)) { 
	$requete = $baseDeDonnes->prepare('SELECT * FROM commentaires WHERE id = :id');
	$requete->bindValue(':id', $_GET["id"], PDO::PARAM_INT);
	$requete->execute();

	$data = $requete->fetch();
	
	if($requete->rowCount() > 0) { 
		if($ID_MEMBRE == $data["id_membre"] || $NIVEAU_MEMBRE == 5) { ?>
			
			<div class="bloc">
				<form method="post" action="#AjoutConfirmation">
						
				<?php
				afficher_smileys('commentaire');
				$FormatagePanel = new FormatagePanel(true, true, true, 'commentaire');
				$FormatagePanel->showFormatagePanel();
				?>
			
				<input type="hidden" value="<?php echo $_SERVER["HTTP_REFERER"]; ?>" name="redirection" />
			
				<p style="clear:left;">
					<textarea name="commentaire" id="commentaire" style="height:250px;"><?php echo stripslashes($data["commentaire"]); ?></textarea>
				</p>		
								
				<p align="center">
					<input type="submit" value="Envoyer" name="envoyer" />
					<input type="button" id="previsualiser_bouton" value="Prévisualiser" onclick="previsualiser('commentaire', 'previsualiserCommentaire', 'AJAX_news.php');" />
				</p>
				</form>
			</div>
			
			<p align="center" id="previsualiser_zone">
				<div class="preview" id="previsualiserCommentaire" style="display:none;"></div>
			</p>
    
	<?php
		}
		else {
			echo '<p class="encadreInfos">';
				echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
				echo '<span class="right">Ce commentaire ne vous appartient pas.</span>';
			echo '</p>';
		}
	}
	else {
		echo '<p class="encadreInfos">';
			echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
			echo '<span class="right">Ce commentaire n\'existe pas.</span>';
		echo '</p>';
	}
}
elseif(!isset($_GET["id"])) {
	echo '<p class="encadreInfos">';
		echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
		echo '<span class="right">Aucun commentaire n\'est défini.</span>';
	echo '</p>';
}

include("Footer.php"); ?>