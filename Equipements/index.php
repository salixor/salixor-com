<?php
require_once("../Includes/Fonctions.php");
require_once("../Includes/Config.php");

$arrayEquipements = array("Cape", "Chapeau", "Amulette", "Sac", "Dofus", "Anneau", "Botte", "Ceinture", "Bouclier", "Arc", "Baguette", "Baton", "Dague", "Epee", "Marteau", "Pelle", "Hache", "Pioche", "Faux");

$arrayPluriels = array("Bouclier" => "boucliers", "Cape" => "capes", "Chapeau" => "chapeaux", "Amulette" => "amulettes", "Anneau" => "anneaux", "Dofus" => "dofus", "Ceinture" => "ceintures", "Botte" => "bottes", "Sac" => "sacs", "Arc" => "arcs", "Baguette" => "baguettes", "Baton" => "batons", "Dague" => "dagues", "Epee" => "epees", "Marteau" => "marteaux", "Pelle" => "pelles", "Hache" => "haches", "Pioche" => "pioches", "Faux" => "faux");

$requeteItemBasique = "
SELECT equipements.id AS equi_id, equipements.*, panoplies.*
FROM equipements
LEFT JOIN panoplies
ON panoplies.id = equipements.panoplie
WHERE equipements.type = :type AND equipement_ajoute = '2'
ORDER BY equipements.niveau, equipements.id ASC";
global $requeteItemBasique;

function pluriel() {
	global $arrayPluriels;
	return $arrayPluriels[NOM_EQUIPEMENT];
}

if(isset($_GET['Page'])) {
	if(in_array($_GET["Page"], $arrayEquipements)) { 
		define('Afficher_Equipement', 'NOT');
		define('NOM_EQUIPEMENT', $_GET['Page']);

		$titre_page_nom = "Les ".ucfirst(pluriel())."";

		if(isset($_GET['Niveau']) || isset($_GET['Niveau2']) || isset($_GET["Nom"]) && !empty($_GET['Ordre'])) {
			if(!empty($_GET['Nom'])) {
				if(strlen($_GET['Nom']) < 3) {
					$erreur = "Le nom recherché fait moins de 3 caractères."; 
					$titre_page_nom .= " - Erreur";
					
					$requeteItem = $baseDeDonnes->prepare($requeteItemBasique);
					$requeteItem->bindValue(":type", pluriel(), PDO::PARAM_STR);
					$requeteItem->execute();
				}
				else {
					$message = 'Affichage des '.pluriel().' dont le nom contient "'.stripslashes($_GET['Nom']).'".';
					$titre_page_nom .= " - Recherche : ".stripslashes($_GET['Nom']);
					
					$requeteItem = $baseDeDonnes->prepare(
					"SELECT equipements.id AS equi_id, equipements.*, panoplies.*
					FROM equipements
					LEFT JOIN panoplies
					ON panoplies.id = equipements.panoplie
					WHERE equipements.nom COLLATE utf8_unicode_ci LIKE :search AND equipements.type = :type AND equipement_ajoute = '2'
					ORDER BY equipements.niveau, equipements.id ASC");
					$requeteItem->bindValue(":search", "%".utf8_decode($_GET['Nom'])."%", PDO::PARAM_STR);
					$requeteItem->bindValue(":type", pluriel(), PDO::PARAM_STR);
					$requeteItem->execute();
				}
			}
			elseif(empty($_GET['Niveau'])) {
				$erreur = "Veuillez remplir le niveau.";
				$titre_page_nom .= " - Erreur";
				
				$requeteItem = $baseDeDonnes->prepare($requeteItemBasique);
				$requeteItem->bindValue(":type", pluriel(), PDO::PARAM_STR);
				$requeteItem->execute();
			}
			elseif(!empty($_GET['Niveau'])) {
				if($_GET['Ordre'] == "Inferieur") {
					$signe_texte = 'inférieur à '.$_GET['Niveau'].'.';
					$signe = "AND equipements.niveau < :niveau";
					$titre_page_nom .= " - Inférieur au niveau ".$_GET['Niveau'];
				}
				if($_GET['Ordre'] == "Egal") {
					$signe_texte = 'égal à '.$_GET['Niveau'].'.';
					$signe = "AND equipements.niveau = :niveau";
					$titre_page_nom .= " - Égal au niveau ".$_GET['Niveau'];
				}
				if($_GET['Ordre'] == "Superieur") {
					$signe_texte = 'supérieur à '.$_GET['Niveau'].'.';
					$signe = "AND equipements.niveau > :niveau";
					$titre_page_nom .= " - Supérieur au niveau ".$_GET['Niveau'];
				}
				if($_GET['Ordre'] == "Entre") {
					$signe_texte = 'compris entre '.$_GET['Niveau'].' et '.$_GET['Niveau2'].'.';
					$signe = "AND equipements.niveau BETWEEN :niveau AND :niveau2";
					$titre_page_nom .= " - Niveau ".$_GET['Niveau']." à ".$_GET['Niveau2'];
				}
				
				if(empty($_GET['Niveau2']) && $_GET['Ordre'] == "Entre") {
					$erreur = "La tranche de niveau de niveau est incomplète."; 
					$titre_page_nom .= " - Erreur";
					
					$requeteItem = $baseDeDonnes->prepare($requeteItemBasique);
					$requeteItem->bindValue(":type", pluriel(), PDO::PARAM_STR);
					$requeteItem->execute();
				}
				else {
					$requeteItem = $baseDeDonnes->prepare(
					"SELECT equipements.id AS equi_id, equipements.*, panoplies.*
					FROM equipements
					LEFT JOIN panoplies
					ON panoplies.id = equipements.panoplie
					WHERE equipements.type = :type ".$signe." AND equipement_ajoute = '2'
					ORDER BY equipements.niveau, equipements.id ASC");
					$requeteItem->bindValue(":type", pluriel(), PDO::PARAM_STR);
					$requeteItem->bindValue(":niveau", $_GET['Niveau'], PDO::PARAM_INT);
					if($_GET['Ordre'] == "Entre")    $requeteItem->bindValue(":niveau2", $_GET['Niveau2'], PDO::PARAM_INT);
					$requeteItem->execute();
					
					$message = 'Affichage des '.pluriel().' de niveau '.$signe_texte; 
				}
			}
		}
		else {			
			$message = 'Affichage de tous les '.pluriel().'.'; 
			
			$requeteItem = $baseDeDonnes->prepare($requeteItemBasique);
			$requeteItem->bindValue(":type", pluriel(), PDO::PARAM_STR);
			$requeteItem->execute();
		}

		$nbEquipements = $requeteItem->rowCount();
		
		include("../Header.php");
		?>
		
		<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/<?php echo pluriel(); ?>.png" alt="<?php echo pluriel(); ?>" /> Les <?php echo ucfirst(pluriel()) ?></h2>

		<p class="Arianne">> <a href="<?php echo ABSPATH ?>">Accueil</a> > <a href="./">Équipements</a> > <?php echo ucfirst(pluriel()); ?></p>
		
		<div class="bloc">
			<p>
				<h4 class="quatrieme_titre">Recherche par niveau</h4>
				<ul class="navigationPage equipement">
					<?php
					$arrayNiveaux = array("1", "21", "41", "61", "101", "151", "1");
					$arrayNiveaux2 = array("20", "40", "60", "100", "150", "200", "200");
					if(isset($_GET["Niveau"]) && isset($_GET["Niveau2"])) {
						$i = 0;
						foreach($arrayNiveaux as $niveau) {
							if($_GET["Niveau"] == $niveau && $_GET["Niveau2"] == $arrayNiveaux2[$i]) {
								echo '<li class="active">'.$niveau.' à '.$arrayNiveaux2[$i].'</li>';
							}
							else {
								echo '<a href="?Page='.NOM_EQUIPEMENT.'&amp;Niveau='.$niveau.'&amp;Niveau2='.$arrayNiveaux2[$i].'&amp;Ordre=Entre" title="Afficher les '.pluriel().' de niveau '.$niveau.' à '.$arrayNiveaux2[$i].'"><li>'.$niveau.' à '.$arrayNiveaux2[$i].'</li></a>';
							}
							$i++;
						}
					}
					else {
						$i = 0;
						foreach($arrayNiveaux as $niveau) {
							if($niveau == 1 && $arrayNiveaux2[$i] == 200) {
								echo '<li class="active">'.$niveau.' à '.$arrayNiveaux2[$i].'</li>';
							}
							else {
								echo '<a href="?Page='.NOM_EQUIPEMENT.'&amp;Niveau='.$niveau.'&amp;Niveau2='.$arrayNiveaux2[$i].'&amp;Ordre=Entre" title="Afficher les '.pluriel().' de niveau '.$niveau.' à '.$arrayNiveaux2[$i].'"><li>'.$niveau.' à '.$arrayNiveaux2[$i].'</li></a>';
							}
							$i++;
						}
					}
					?>
				</ul>
				<div style="clear:left;"></div>
			</p>
			
			<form method="get" action name="recherche_equipement">
				<h4 class="quatrieme_titre">Recherche personnalisée</h4>
				<p>
					<label for="Ordre">Par Niveau :</label>
					<input type="hidden" name="Page" value="<?php echo NOM_EQUIPEMENT; ?>" />
					<select name="Ordre" id="Ordre" onchange="if(this.selectedIndex == 0) { $('#Niveau2').fadeIn(500);} else { $('#Niveau2').fadeOut(500); }">
						<option value="Entre">Entre</option>
						<option value="Inferieur">Inférieur à</option>
						<option value="Egal">Égal à</option>
						<option value="Superieur">Supérieur à</option>
						</optgroup>
					</select>
					<input name="Niveau" type="text" size="4" />
					<span id="Niveau2">et <input name="Niveau2" type="text" size="4" /></span>
				</p>
				<p>
					<label for="Nom">Par Nom :</label>
					
					<input name="Nom" id="Nom" type="text" style="width:240px; position:relative; z-index:1001;" rel="<?php echo pluriel(); ?>" />
					<span class="delete_value_champ" style="position:absolute; z-index:1002;">×</span>
					<ul class="selectionPersonnage maximum" id="results" style="width:256px; box-shadow:0 0 10px #BBB; margin:-10px 0 0 195px; position:absolute; z-index:1000;"></ul>
				</p>	
				<p align="center">
					<input type="submit" value="Rechercher" />
				</p>
			</form>
		</div>

		<?php 	
		echo '<p class="encadreInfos">';
			if(isset($erreur))     echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
			
			echo '<span class="right">';
			
			if(isset($erreur))     echo $erreur;
			else {
				echo '<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Loupe.png" alt="" /> <span>';
					if(isset($message))    echo $message."<br />";
					
					if($nbEquipements == 0)   echo 'Aucun équipement n\'a été trouvé pour ce critère de recherche. <a href="?Page='.NOM_EQUIPEMENT.'">Réinitialiser</a> la recherche.';
					if($nbEquipements == 1)   echo "1 équipement a été trouvé pour ce critère de recherche.";
					if($nbEquipements > 1)    echo $nbEquipements." équipements ont été trouvés pour ce critère de recherche.";
				echo '</span>';
			}
		echo '</span></p>';
			
		while($data = $requeteItem->fetch()) {
			$data["nom_panoplie"] = utf8_encode($data["nom_panoplie"]);
			$niveau = $data['niveau'];
			$image = lienImageEquipement(ucfirst($data["type"]), $data['nom']);

			if(!file_exists('../'.$image))     $image = 'Images/Items/Unknown.png';
			
			$nom = stripslashes(utf8_encode($data['nom']));
			$craft = nl2br(changerCraft(stripslashes(utf8_encode($data['craft']))));
			$boost = nl2br(utf8_encode($data['boost']));
			$caracteristiques = nl2br(utf8_encode($data['caracteristiques']));
			$equipementDescription = nl2br(utf8_encode($data['description']));
			$equipementConditions = nl2br(utf8_encode(stripslashes($data['conditions'])));
			$type = array('Chapeaux' => 'Chapeau', 'Capes' => 'Cape', 'Sacs' => 'Sac', 'Anneaux' => 'Anneau', 'Amulettes' => 'Amulette', 'Bottes' => 'Botte', 'Ceintures' => 'Ceinture', 'Dofus' => 'Dofus', 'Boucliers' => 'Bouclier', "Arcs" => "Arc", "Baguettes" => "Baguette", "Batons" => "Baton", "Dagues" => "Dague", "Epees" => "Epee", "Marteaux" => "Marteau", "Pelles" => "Pelle", "Haches" => "Hache", "Pioches" => "Pioche", "Faux" => "Faux"); ?>
	
				<table class="equipementTable" rel="<?php echo pluriel(); ?>" id="Item-<?php echo $data['equi_id']; ?>">
					<tr class="equipementInformations">
						<th>
							Niveau <?php echo $niveau ?></th>
						<th colspan="2">
							<?php
							if(isset($PSEUDO_MEMBRE)) {
								echo '
								<img class="click_ajouter_equipement" alt="" title="Attacher à un des mes personnages" onclick="equipement('.$data['equi_id'].', \''.$data["type"].'\');" src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Epingle.png" />
								';
								echo '<span style="clear:left;"></span>';
							}
							echo $nom;
							?>
						</th>
					</tr>
					
					<tr class="equipementTitres">
						<td class="screen"></td>
						<td width="320">Obtention</td>
						<td width="320">Effets</td>
					</tr>
					
					<?php if(empty($caracteristiques)) { ?>
						<tr>
							<td class="screen"><img src="<?php echo ABSPATH.$image ?>" alt="" /></td>         
							<td class="obtention"><?php echo stripslashes($craft); ?></td>
							<td class="effets"><?php echo stripslashes($boost); ?></td>
						</tr>
					<?php }
					else { ?>					
						<tr>
							<td class="screen" rowspan="3"><img src="<?php echo ABSPATH.$image ?>" alt="" /></td>         
							<td class="obtention" style="height:50%;"><?php echo $craft; ?></td>
							<td class="effets" rowspan="3"><?php echo colorationDommages(stripslashes($boost)); ?></td>
						</tr>
						
						<tr class="equipementTitres">
							<td width="320">Caractéristiques</td>
						</tr>
					
						<tr>
							<td class="obtention" style="height:50%;"><?php echo stripslashes($caracteristiques); ?></td>
						</tr>
					<?php }

						echo '<tr class="panoplie">
								<td colspan="3">
									<span>Description, Conditions :</span>';

						if(!empty($data["nom_panoplie"]) AND !empty($data["niveau_panoplie"])) {
							echo '<a href="'.ABSPATH.'Equipements/Panoplies.php?Panoplie='.$data["id"].'">'.$data["nom_panoplie"].' (Niveau '.$data["niveau_panoplie"].')</a>';
						}
						
						echo '</td>
							</tr>';
					?>
					
					<tr>
						<td colspan="3" class="description"><b>&laquo;</b> <?php echo stripslashes($equipementDescription); ?> <b>&raquo;</b></td>
					</tr>
					
					<?php if(!empty($equipementConditions)) { ?>
						<tr>
							<td colspan="3" class="conditions"><b>Conditions :</b> <?php echo $equipementConditions ?></td>
						</tr>
					<?php } ?>
					
				</table>
			<?php
		}
	}
	else {
		$titre_page_nom = "Introuvable";

		include("../Header.php");
		
		echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Erreur.png" alt="404" /> Erreur</h2>';
		
		echo '<p class="Arianne">> <a href="<?php echo ABSPATH ?>">Accueil</a> > <a href="./">Équipements</a></p>';
		
		echo '<p class="encadreInfos">';
			echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
			echo '<span class="right">Cette page n\'existe pas.</span>';
		echo '</p>';
	}
}
else {
	$titre_page_nom = "Les Équipements";
	
	include("../Header.php");
	?>

	<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Panoplie.png" alt="Equipements" /> Les Équipements</h2>

	<p class="Arianne">> <a href="<?php echo ABSPATH ?>">Accueil</a> > Équipements</p>

	<p class="encadreInfos">
		<span class="right">
			Voici le catalogue des équipements que vous pouvez retrouver en jeu.<br />
			Si vous êtes membre, vous pouvez les attacher à l'un des vos personnages, pour partager votre équipement.
		</span>
	</p>

	<h3 class="troisieme_titre">Classiques</h3>
		<ul class="navigationPage left">
			<a href="?Page=Anneau&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Anneaux</li></a>
			<a href="?Page=Amulette&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Amulettes</li></a>
			<a href="?Page=Botte&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Bottes</li></a>
			<a href="?Page=Cape&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Capes</li></a>
			<a href="Familiers.php"><li>Familiers</li></a>
		</ul>
		
		<ul class="navigationPage right">
			<a href="?Page=Ceinture&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Ceintures</li></a>
			<a href="?Page=Chapeau&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Chapeaux</li></a>
			<a href="?Page=Bouclier"><li>Boucliers</li></a>
			<a href="?Page=Sac"><li>Sacs</li></a>
			<a href="?Page=Dofus"><li>Dofus</li></a>
		</ul>
		
	<h3 class="troisieme_titre">Armes</h3>
		<ul class="navigationPage left">
			<a href="?Page=Arc&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Les Arcs</li></a>		
			<a href="?Page=Baguette&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Les Baguettes</li></a>			
			<a href="?Page=Baton&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Les Bâtons</li></a>			
			<a href="?Page=Dague&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Les Dagues</li></a>			
			<a href="?Page=Epee&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Les Épées</li></a>			
		</ul>
		
		<ul class="navigationPage right">
			<a href="?Page=Marteau&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Les Marteaux</li></a>			
			<a href="?Page=Pelle&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Les Pelles</li></a>		
			<a href="?Page=Hache&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Les Haches</li></a>		
			<a href="?Page=Pioche&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Les Pioches</li></a>			
			<a href="?Page=Faux&amp;Niveau=1&amp;Niveau2=20&amp;Ordre=Entre"><li>Les Faux</li></a>	
		</ul>
			
	<h3 class="troisieme_titre">Les Panoplies</h3>
		<ul class="navigationPage">
			<a href="Panoplies.php"><li>Panoplies</li></a>
		</ul>
	
	<p align="center" style="clear:left;">
		<a href="<?php echo ABSPATH ?>Admin/Admin_equipements.php" class="allNews">
			<img src="<?php echo ABSPATH ?>Images/icones_pages/LittlesIcones/Contribuer.png" alt="" /> Contribuer
		</a>
	</p>
	
<?php
}

include("../Footer.php"); ?>