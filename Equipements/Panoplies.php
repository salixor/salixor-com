<?php
if(isset($_GET['Panoplie'])) {
	if(!empty($_GET['Panoplie'])) {
		$idPanoplie = $_GET['Panoplie'];
		
		require_once("../Includes/Fonctions.php");
		require_once("../Includes/Config.php");
		
		$sqlBase = "SELECT equipements.id AS equi_id, equipements.*, panoplies.*, (SELECT COUNT(id) FROM equipements WHERE panoplie = :panoplie AND equipement_ajoute = '2') AS nb_equipements
					FROM equipements
					LEFT JOIN panoplies
					ON panoplies.id = equipements.panoplie
					WHERE panoplies.id = :panoplie AND equipements.equipement_ajoute = '2'
					ORDER BY type COLLATE utf8_unicode_ci";
		$sqlNext = "SELECT *
					FROM panoplies
					WHERE id = :panoplie";
					
		$requetePano = $baseDeDonnes->prepare($sqlBase);
		$requetePano->bindValue(":panoplie", $idPanoplie, PDO::PARAM_INT);
		$requetePano->execute();
		
		if($requetePano->rowCount() > 0) {
			$data = $requetePano->fetch();
			$nomPanoplie = utf8_encode(stripslashes($data["nom_panoplie"]));
			$nbEquipements = $data["nb_equipements"];
			
			$titre_page_nom = $nomPanoplie;
			
			$message = "<span>Cette panoplie requiert le niveau ".$data["niveau_panoplie"]." pour être équipée en entier.</span><br />Elle contient ".$nbEquipements." éléments.";
		}
		else {
			$erreur = 'Cette panoplie n\'existe pas. Tentez d\'accéder à une autre panoplie <a href="Panoplies.php">ici</a>.';
			
			$titre_page_nom = "Panoplie inexistante";
			$nomPanoplie = "Panoplie inexistante";
		}

		include("../Header.php");
		?>
		
		<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Panoplie.png" alt="Panoplies" /> <?php echo $nomPanoplie; ?></h2>

		<p class="Arianne">> <a href="<?php echo ABSPATH ?>">Accueil</a> > <a href="./">Équipements</a> > <a href="Panoplies.php">Panoplies</a> > <?php echo $nomPanoplie; ?></p>
		
		<?php
		echo '<p class="encadreInfos">';
			if(isset($erreur))     echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
			
			echo '<span class="right">';
			
			if(isset($erreur))     echo $erreur;
			else {
				echo '<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Equipements.png" alt="" /> <span>';
					if(isset($message))    echo $message;
				echo '</span>';
			}
		echo '</span></p>';
		
		if(!isset($erreur)) {
			echo '
			<ul class="navigationPage left">
				<a href="#BonusItems"><li>Bonus par Équipements</li></a>
				<a href="#BonusRecetteComplets"><li>Bonus et Recette complets</li></a>
				
			</ul>
			';
		}

		$idEquis = array();
		$typeEquis = array();
		$i = 0;
		
		$requetePano = $baseDeDonnes->prepare($sqlBase);
		$requetePano->bindValue(":panoplie", $idPanoplie, PDO::PARAM_INT);
		$requetePano->execute();
		
		if($requetePano->rowCount() > 0) {
			while($data = $requetePano->fetch()) {
				array_push($idEquis, $data['equi_id']);
				array_push($typeEquis, $data['type']);

				if(isset($PSEUDO_MEMBRE) && $i + 1 == $requetePano->rowCount()) {
					$ids = "";
					$types = "";

					foreach($idEquis as $id) {
						$ids .= $id.";";
					}
					foreach($typeEquis as $type) {
						$types .= $type.";";
					}
					echo '
					<div style="position:absolute; top:290px;">
						<div style="position:relative; width:760px;">
							<a style="position:absolute; right:0; font-size:13px;" class="add-panoplie" onclick="equipement(\''.$ids.'\', \''.$types.'\', \'panoplie\');" href="javascript:void(0);">
								<img class="click_ajouter_equipement" alt="" src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Epingle.png" style="margin:3px 0 -3px;" /> Attacher la panoplie à un personnage
							</a>
						</div>
					</div>';
				}
			
				$data["nom_panoplie"] = utf8_encode($data["nom_panoplie"]);
				$niveau = $data['niveau'];
				$image = lienImageEquipement(ucfirst($data["type"]), $data['nom']);

				if(!file_exists('../'.$image))     $image = 'Images/Items/Unknown.png';
				
				$nom = stripslashes(utf8_encode($data['nom']));
				$craft = nl2br(changerCraft(stripslashes(utf8_encode($data['craft']))));
				$boost = nl2br(utf8_encode(formatage($data['boost'])));
				
				$intervalle = nl2br(stripslashes(utf8_encode(formatage($data['intervalle']))));
				$pdv = nl2br(stripslashes(utf8_encode(formatage($data['points_de_vie']))));
				$zonePredilection = nl2br(stripslashes(utf8_encode(formatage($data['zones_predilections']))));
				$bonusPredilection = nl2br(stripslashes(utf8_encode(formatage($data['bonus_predilections']))));
				
				$caracteristiques = nl2br(utf8_encode($data['caracteristiques']));
				$equipementDescription = nl2br(utf8_encode($data['description']));
				$equipementConditions = nl2br(utf8_encode($data['conditions']));
				$type = array('Chapeaux' => 'Chapeau', 'Capes' => 'Cape', 'Sacs' => 'Sac', 'Anneaux' => 'Anneau', 'Amulettes' => 'Amulette', 'Bottes' => 'Botte', 'Ceintures' => 'Ceinture', 'Dofus' => 'Dofus', 'Boucliers' => 'Bouclier');
				
				if($data["type"] != "familiers") { ?>
			
					<table class="equipementTable" rel="<?php echo $data["type"] ?>" id="Item-<?php echo $data['equi_id']; ?>">
						<tr class="equipementInformations">
							<th>Niveau <?php echo $niveau ?></th>
							<th colspan="2">
								<?php
								if(isset($PSEUDO_MEMBRE)) {
									echo '
									<img class="click_ajouter_equipement" alt="" title="Attacher à un des mes personnages" onclick="equipement('.$data['equi_id'].', \''.$data["type"].'\');" src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Epingle.png" />
									';
									echo '<span style="clear:left;"></span>';
								}
								echo $nom;
								?>
							</th>
						</tr>
						
						<tr class="equipementTitres">
							<td class="screen"></td>
							<td width="320">Obtention</td>
							<td width="320">Effets</td>
						</tr>
						<?php if(empty($caracteristiques)) { ?>
							<tr>
								<td class="screen"><img src="<?php echo ABSPATH.$image ?>" alt="" /></td>         
								<td class="obtention"><?php echo $craft; ?></td>
								<td class="effets"><?php echo stripslashes($boost); ?></td>
							</tr>
						<?php }
						else { ?>					
							<tr>
								<td class="screen" rowspan="3"><img src="<?php echo ABSPATH.$image ?>" alt="" /></td>         
								<td class="obtention" style="height:50%;"><?php echo $craft; ?></td>
								<td class="effets" rowspan="3"><?php echo colorationDommages(stripslashes($boost)); ?></td>
							</tr>
							
							<tr class="equipementTitres">
								<td width="320">Caractéristiques</td>
							</tr>
						
							<tr>
								<td class="obtention" style="height:50%;"><?php echo stripslashes($caracteristiques); ?></td>
							</tr>
						<?php }

							echo '
							<tr class="panoplie">
								<td colspan="3">
									<span>Description, Conditions :</span>
								</td>
							</tr>';
						?>
						
						<tr>
							<td colspan="3" class="description"><b>&laquo;</b> <?php echo stripslashes($equipementDescription); ?> <b>&raquo;</b></td>
						</tr>
						
						<?php if(!empty($equipementConditions)) { ?>
							<tr>
								<td colspan="3" class="conditions"><b>Conditions :</b> <?php echo $equipementConditions ?></td>
							</tr>
						<?php } ?>
						
					</table>
				
				<?php
				}
				else {
				?>
			
					<table class="equipementTable">
						<tr class="equipementInformations">
							<th>
								<a href="<?php echo ABSPATH; ?>Equipements?Page=<?php echo $type[ucfirst($data["type"])]; ?>#Item-<?php echo $data['equi_id']; ?>" title="Lien vers cet équipement." class="lienEquipement">#</a>
								Niveau <?php echo $niveau ?></th>
							<th colspan="2">
								<?php
								if(isset($PSEUDO_MEMBRE)) {
									echo '
									<img class="click_ajouter_equipement" alt="" title="Attacher à un des mes personnages" onclick="equipement('.$data['equi_id'].', \''.$data["type"].'\');" src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Epingle.png" />
									';
									echo '<span style="clear:left;"></span>';
								}
								echo $nom;
								?>
							</th>
						</tr>
						
						<tr class="equipementTitres">
							<td class="screen"></td>
							<td width="320">Obtention</td>
							<td width="320">Nourriture</td>
						</tr>
						
						<tr>
							<td class="screen big"><img src="<?php echo ABSPATH.$image ?>" alt="" /></td>         
							<td class="obtention" style="width:220px;"><?php echo stripslashes($craft); ?></td>
							<td class="effets"><?php echo stripslashes($boost); ?></td>
						</tr>
					
						<?php
						if(!empty($intervalle)) {
							echo '
							<tr class="clair">
								<td>Intervalle entre 2 repas</td>
								<td class="right" colspan="2">'.$intervalle.'</td>
							</tr>';
						}
						if(!empty($pdv)) {
							echo '
							<tr class="clair left">
								<td>Points de vie</td>
								<td class="right" colspan="2">'.$pdv.' PDV</td>
							</tr>';
						}
						if(!empty($zonePredilection)) {
							echo '
							<tr class="clair">
								<td>Zone(s) de prédilection</td>
								<td class="right" colspan="2">'.$zonePredilection.'</td>
							</tr>';
						}
						if(!empty($bonusPredilection)) {
							echo '
							<tr class="clair left">
								<td>Bonus de prédilection</td>
								<td class="right" colspan="2">'.$bonusPredilection.'</td>
							</tr>
							';
						}
						?>
						
						<tr>
							<td colspan="3" class="description"><b>&laquo;</b> <?php echo stripslashes($equipementDescription); ?> <b>&raquo;</b></td>
						</tr>
						
						<?php if(!empty($equipementConditions)) { ?>
							<tr>
								<td colspan="3" class="conditions"><b>Conditions :</b> <?php echo $equipementConditions ?></td>
							</tr>
						<?php } ?>
						
					</table>
			<?php
				}
				$i++;
			}
		}
		
		$requetePano = $baseDeDonnes->prepare($sqlNext);
		$requetePano->bindValue(":panoplie", $idPanoplie, PDO::PARAM_INT);
		$requetePano->execute();
		
		if($requetePano->rowCount() > 0) {
			$data = $requetePano->fetch();
			$bonusItems = explode(";", utf8_encode(nl2br($data["bonus_items"])));
				
			$bonusComplets = stripslashes(utf8_encode(nl2br($data["bonus_complets"])));
			$recetteComplete = stripslashes(utf8_encode(nl2br(changerCraft($data["recette_complete"]))));

			echo '<h3 class="troisieme_titre" id="InfoPano">Informations sur la Panoplie</h3>';
			
				echo '<h4 class="quatrieme_titre">Bonus par Équipements</h4>';

				echo '<div class="bonusParEquipements">';
				
					$i = 0;
					$j = 1;
					while($j < count($bonusItems)) {	
						echo '<div>';
							echo '<span class="left">'.$bonusItems[$i].'</span>';
							echo '<span class="right">'.$bonusItems[$j].'</span>';
							echo '<div style="clear:left;"></div>';
						echo '</div>';
						
						$i += 2;
						$j += 2;
					}
				
				echo '</div>';
							
				echo '<h4 class="quatrieme_titre">Bonus et Recette complets</h4>';

				echo '<div class="panoplieComplet">';
					echo '<div>';
						echo '<span class="left haut">Bonus</span>';
						echo '<span class="right haut">Recette</span>';
						echo '<span class="left bas">'.$bonusComplets.'</span>';
						echo '<span class="right bas">'.$recetteComplete.'</span>';
						echo '<div style="clear:left;"></div>';
					echo '</div>';					
				echo '</div>';
		}
	}
}
else {
	require_once("../Includes/Fonctions.php");
	require_once("../Includes/Config.php");

	define('Afficher_Equipement', 'NOT');

	$titre_page_nom = "Les Panoplies";

	if(isset($_GET['Page']) && is_numeric($_GET['Page']))    $page = $_GET['Page'];
	else                                                     $page = 1;

	$pagination = 20;
	$limit_start = ($page - 1) * $pagination;
	
	$requeteItemBasique = 
	'SELECT equipements.*, panoplies.*
	FROM equipements
	LEFT JOIN panoplies
	ON panoplies.id = equipements.panoplie
	WHERE equipements.panoplie != 0 AND equipements.equipement_ajoute = "2"
	GROUP BY panoplies.id
	ORDER BY panoplies.niveau_panoplie, panoplies.id ASC
	LIMIT '.$limit_start.', '.$pagination;
	
	if(isset($_GET['Niveau']) || isset($_GET['Niveau2']) || isset($_GET["Nom"]) && !empty($_GET['Ordre'])) {
		if(!empty($_GET['Nom'])) {
			if(strlen($_GET['Nom']) < 3) {
				$erreur = "Le nom recherché fait moins de 3 caractères."; 
				$titre_page_nom .= " - Erreur";
				
				$requeteItem = $baseDeDonnes->prepare($requeteItemBasique);
				$requeteItem->bindValue(":type", pluriel(), PDO::PARAM_STR);
				$requeteItem->execute();

				$requete = $baseDeDonnes->prepare("SELECT COUNT(*) AS nb_total FROM panoplies");
				$requete->execute();
			}
			else {
				$message = 'Affichage des panoplies dont le nom contient "'.stripslashes($_GET['Nom']).'".';
				$titre_page_nom .= " - Recherche : ".stripslashes($_GET['Nom']);
				
				$requeteItem = $baseDeDonnes->prepare(
				'SELECT equipements.*, panoplies.*
				FROM equipements
				LEFT JOIN panoplies
				ON panoplies.id = equipements.panoplie
				WHERE panoplies.nom_panoplie COLLATE utf8_unicode_ci LIKE :search AND equipement_ajoute = "2"
				GROUP BY panoplies.id
				ORDER BY panoplies.niveau_panoplie, panoplies.id ASC
				LIMIT '.$limit_start.', '.$pagination);
				$requeteItem->bindValue(":search", "%".utf8_decode($_GET['Nom'])."%", PDO::PARAM_STR);
				$requeteItem->execute();
				
				$requete = $baseDeDonnes->prepare("SELECT COUNT(*) AS nb_total FROM panoplies WHERE nom_panoplie COLLATE utf8_unicode_ci LIKE :search");
				$requete->bindValue(":search", "%".utf8_decode($_GET['Nom'])."%", PDO::PARAM_STR);
				$requete->execute();
			}
		}
		elseif(empty($_GET['Niveau'])) {
			$erreur = "Veuillez remplir le niveau.";
			$titre_page_nom .= " - Erreur";
			
			$requeteItem = $baseDeDonnes->prepare($requeteItemBasique);
			$requeteItem->execute();

			$requete = $baseDeDonnes->prepare("SELECT COUNT(*) AS nb_total FROM panoplies");
			$requete->execute();
		}
		elseif(!empty($_GET['Niveau'])) {
			if($_GET['Ordre'] == "Inferieur") {
				$signe_texte = 'inférieur à '.$_GET['Niveau'].'.';
				$signe = "panoplies.niveau_panoplie < :niveau";
				$titre_page_nom .= " - Inférieur au niveau ".$_GET['Niveau'];
			}
			if($_GET['Ordre'] == "Egal") {
				$signe_texte = 'égal à '.$_GET['Niveau'].'.';
				$signe = "panoplies.niveau_panoplie = :niveau";
				$titre_page_nom .= " - Égal au niveau ".$_GET['Niveau'];
			}
			if($_GET['Ordre'] == "Superieur") {
				$signe_texte = 'supérieur à '.$_GET['Niveau'].'.';
				$signe = "panoplies.niveau_panoplie > :niveau";
				$titre_page_nom .= " - Supérieur au niveau ".$_GET['Niveau'];
			}
			if($_GET['Ordre'] == "Entre") {
				$signe_texte = 'compris entre '.$_GET['Niveau'].' et '.$_GET['Niveau2'].'.';
				$signe = "panoplies.niveau_panoplie BETWEEN :niveau AND :niveau2";
				$titre_page_nom .= " - Niveau ".$_GET['Niveau']." à ".$_GET['Niveau2'];
			}
			
			if(empty($_GET['Niveau2']) && $_GET['Ordre'] == "Entre") {
				$erreur = "La tranche de niveau de niveau est incomplète."; 
				$titre_page_nom .= " - Erreur";
				
				$requeteItem = $baseDeDonnes->prepare($requeteItemBasique);
				$requeteItem->execute();

				$requete = $baseDeDonnes->prepare("SELECT COUNT(*) AS nb_total FROM panoplies");
				$requete->execute();
			}
			else {
				$requeteItem = $baseDeDonnes->prepare(
				"SELECT equipements.*, panoplies.*
				FROM equipements
				LEFT JOIN panoplies
				ON panoplies.id = equipements.panoplie
				WHERE ".$signe." AND equipement_ajoute ='2'
				GROUP BY panoplies.id
				ORDER BY panoplies.niveau_panoplie, panoplies.id ASC
				LIMIT ".$limit_start.", ".$pagination);
				$requeteItem->bindValue(":niveau", $_GET['Niveau'], PDO::PARAM_INT);
				if($_GET['Ordre'] == "Entre")    $requeteItem->bindValue(":niveau2", $_GET['Niveau2'], PDO::PARAM_INT);
				$requeteItem->execute();
				
				$requete = $baseDeDonnes->prepare('SELECT COUNT(*) AS nb_total FROM panoplies WHERE '.$signe);
				$requete->bindValue(":niveau", $_GET['Niveau'], PDO::PARAM_INT);
				if($_GET['Ordre'] == "Entre")    $requete->bindValue(":niveau2", $_GET['Niveau2'], PDO::PARAM_INT);
				$requete->execute();
				
				$message = 'Affichage des panoplies de niveau '.$signe_texte;
			}
		}
	}
	else {			
		$message = 'Affichage de toutes les panoplies.'; 
		
		$requeteItem = $baseDeDonnes->prepare($requeteItemBasique);
		$requeteItem->execute();
		
		$requete = $baseDeDonnes->prepare("SELECT COUNT(*) AS nb_total FROM panoplies");
		$requete->execute();
	}
	
	$nb_total = $requete->fetch();
	$nb_total = $nb_total['nb_total'];
	$nb_pages = ceil($nb_total / $pagination);
	
	if(!isset($_GET["OnlyNav"])) {
		include("../Header.php");
		?>
		
		<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Panoplie.png" alt="Panoplies" /> Les Panoplies</h2>

		<p class="Arianne">> <a href="<?php echo ABSPATH ?>">Accueil</a> > <a href="./">Équipements</a> > Panoplies</p>
		
		<div class="bloc">
			<p>
				<h4 class="quatrieme_titre">Recherche par niveau</h4>
				<ul class="navigationPage equipement">
					<?php
					$arrayNiveaux = array("1", "21", "41", "61", "101", "151", "1");
					$arrayNiveaux2 = array("20", "40", "60", "100", "150", "200", "200");
					if(isset($_GET["Niveau"]) && isset($_GET["Niveau2"])) {
						$i = 0;
						foreach($arrayNiveaux as $niveau) {
							if($_GET["Niveau"] == $niveau && $_GET["Niveau2"] == $arrayNiveaux2[$i]) {
								echo '<li class="active">'.$niveau.' à '.$arrayNiveaux2[$i].'</li>';
							}
							else {
								echo '<a href="?Niveau='.$niveau.'&amp;Niveau2='.$arrayNiveaux2[$i].'&amp;Ordre=Entre" title="Afficher les panoplies de niveau '.$niveau.' à '.$arrayNiveaux2[$i].'"><li>'.$niveau.' à '.$arrayNiveaux2[$i].'</li></a>';
							}
							$i++;
						}
					}
					else {
						$i = 0;
						foreach($arrayNiveaux as $niveau) {
							if($niveau == 1 && $arrayNiveaux2[$i] == 200) {
								echo '<li class="active">'.$niveau.' à '.$arrayNiveaux2[$i].'</li>';
							}
							else {
								echo '<a href="?Niveau='.$niveau.'&amp;Niveau2='.$arrayNiveaux2[$i].'&amp;Ordre=Entre" title="Afficher les panoplies de niveau '.$niveau.' à '.$arrayNiveaux2[$i].'"><li>'.$niveau.' à '.$arrayNiveaux2[$i].'</li></a>';
							}
							$i++;
						}
					}
					?>
				</ul>
				<div style="clear:left;"></div>
			</p>
			
			<form method="get" action name="recherche_equipement">
				<h4 class="quatrieme_titre">Recherche personnalisée</h4>
				<p>
					<label for="Ordre">Par Niveau :</label>
					<select name="Ordre" id="Ordre" onchange="if(this.selectedIndex == 0) { $('#Niveau2').fadeIn(500);} else { $('#Niveau2').fadeOut(500); }">
						<option value="Entre">Entre</option>
						<option value="Inferieur">Inférieur à</option>
						<option value="Egal">Égal à</option>
						<option value="Superieur">Supérieur à</option>
						</optgroup>
					</select>
					<input name="Niveau" type="text" size="4" />
					<span id="Niveau2">et <input name="Niveau2" type="text" size="4" /></span>
				</p>
				<p>
					<label for="Nom">Par Nom :</label>
					
					<input name="Nom" id="Nom" type="text" style="width:240px; position:relative; z-index:1001;" rel="panoplies" />
					<span class="delete_value_champ" style="position:absolute; z-index:1002;">×</span>
					<ul class="selectionPersonnage maximum" id="results" style="width:256px; box-shadow:0 0 10px #BBB; margin:-10px 0 0 195px; position:absolute; z-index:1000;"></ul>
				</p>	
				<p align="center">
					<input type="submit" value="Rechercher" />
				</p>
			</form>
		</div>

		<?php 	
		echo '<p class="encadreInfos">';
			if(isset($erreur))     echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
			
			echo '<span class="right">';
			
			if(isset($erreur))     echo $erreur;
			else {
				echo '<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Loupe.png" alt="" /> <span>';
					if(isset($message))    echo $message."<br />";
					
					if($nb_total == 0)   echo 'Aucune panoplie n\'a été trouvée pour ce critère de recherche. <a href="Panoplies.php">Réinitialiser</a> la recherche.';
					if($nb_total == 1)   echo "1 panoplie a été trouvée pour ce critère de recherche.";
					if($nb_total > 1)    echo $nb_total." panoplies ont été trouvées pour ce critère de recherche.";
				echo '</span>';
			}
		echo '</span></p>';
	}
		
	echo '<div id="receiver-pano-page">';
		if($nb_total > 0) {
			echo '
			<table>
				<tr id="pano-table-first-line">
					<th></th>
					<th width="560">Nom de la Panoplie</th>
					<th width="200">Niveau</th>
				</tr>
				<tbody>';
					while($data = $requeteItem->fetch()) {
						$image = lienImageEquipement(ucfirst($data["type"]), $data['nom']);

						if(!file_exists('../'.$image))     $image = 'Images/Items/Unknown.png';

						echo '
						<tr>
							<td><img src="'.ABSPATH.$image.'" alt="" height="30" style="background:#FFF" /></td>
							<td><a href="?Panoplie='.$data["id"].'">'.utf8_encode(stripslashes($data["nom_panoplie"])).'</a></td>
							<td align="center">'.$data["niveau_panoplie"].'</td>
						</tr>';
					}
			echo '</tbody>
			</table>';
			
			if($nb_pages > 1)      echo '<p class="pagination" align="center" style="margin:15px 0 0;" id="pagination-pano">'.pagination($page, $nb_pages, ABSPATH."Equipements/Panoplies.php?Page=%d").'</p>';
		}
	echo '</div>';
}

if(!isset($_GET["OnlyNav"])) {
	include("../Footer.php");
}
?>