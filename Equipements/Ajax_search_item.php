<?php
include('../Includes/Fonctions.php');
require("../Includes/Config.php");

if(isset($_POST["search"]) AND isset($_POST["search_type"])) {
	if(strlen($_POST["search"]) > 3) {
		if($_POST["search_type"] != "panoplies") {
			$requete = $baseDeDonnes->prepare("
			SELECT * FROM equipements
			WHERE type = '".$_POST["search_type"]."' AND equipement_ajoute = '2' AND nom COLLATE utf8_unicode_ci LIKE :search
			ORDER BY niveau, id ASC");
			$requete->bindValue(":search", "%".$_POST["search"]."%", PDO::PARAM_STR);
			$requete->execute();

			if($requete->rowCount() > 0) {
				while($data = $requete->fetch()) {
					$nom = stripslashes(utf8_encode(preg_replace("#(".$_POST["search"]."+)#i", "<b>$1</b>", $data["nom"])));
					echo '
					<a href="javascript:void(0);" class="recherche-result">
						<li>
							<span class="fleche"></span>
							<span class="pseudo">'.$nom.'</span>
						</li>
					</a>';
				}
			}
			else {
				echo '<a href="javascript:void(0);""><li>Aucun résultat</li></a>';
			}
		}
		else {
			$requete = $baseDeDonnes->prepare("
			SELECT * FROM panoplies
			WHERE nom_panoplie COLLATE utf8_unicode_ci LIKE :search
			ORDER BY niveau_panoplie, id ASC");
			$requete->bindValue(":search", "%".$_POST["search"]."%", PDO::PARAM_STR);
			$requete->execute();

			if($requete->rowCount() > 0) {
				while($data = $requete->fetch()) {
					$nom = stripslashes(utf8_encode(preg_replace("#(".$_POST["search"]."+)#i", "<b>$1</b>", $data["nom_panoplie"])));
					$niveau = $data["niveau_panoplie"];
					echo '
					<a href="javascript:void(0);" class="recherche-result">
						<li>
							<span class="fleche"></span>
							<span class="pseudo">'.$nom.'</span>
							<span class="little">Panoplie de niveau '.$niveau.'</span>
						</li>
					</a>';
				}
			}
			else {
				echo '<a href="javascript:void(0);"><li>Aucun résultat</li></a>';
			}
		}
	}
}
?>