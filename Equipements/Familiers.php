<?php
require_once("../Includes/Fonctions.php");
require_once("../Includes/Config.php");

$arrayPluriels = array("Familier" => "familiers");

$requeteItemBasique = "
SELECT equipements.id AS equi_id, equipements.*, panoplies.*
FROM equipements
LEFT JOIN panoplies
ON panoplies.id = equipements.panoplie
WHERE equipements.type = :type AND equipement_ajoute = '2'
ORDER BY equipements.niveau, equipements.id ASC";
global $requeteItemBasique;

function pluriel() {
	global $arrayPluriels;
	return $arrayPluriels[NOM_EQUIPEMENT];
}

define('Afficher_Equipement', 'NOT');
define('NOM_EQUIPEMENT', "Familier");

$titre_page_nom = "Les ".ucfirst(pluriel())."";

if(isset($_GET["Nom"])) {
	if(!empty($_GET['Nom'])) {
		if(strlen($_GET['Nom']) < 3) {
			$erreur = "Le nom recherché fait moins de 3 caractères."; 
			$titre_page_nom .= " - Erreur";
			
			$requeteItem = $baseDeDonnes->prepare($requeteItemBasique);
			$requeteItem->bindValue(":type", pluriel(), PDO::PARAM_STR);
			$requeteItem->execute();
		}
		else {
			$message = 'Affichage des '.pluriel().' dont le nom contient "'.stripslashes($_GET['Nom']).'".';
			$titre_page_nom .= " - Recherche : ".stripslashes($_GET['Nom']);
			
			$requeteItem = $baseDeDonnes->prepare(
			"SELECT equipements.id AS equi_id, equipements.*, panoplies.*
			FROM equipements
			LEFT JOIN panoplies
			ON panoplies.id = equipements.panoplie
			WHERE equipements.nom COLLATE utf8_unicode_ci LIKE :search AND equipements.type = :type AND equipement_ajoute = '2'
			ORDER BY equipements.niveau, equipements.id ASC");
			$requeteItem->bindValue(":search", "%".utf8_decode($_GET['Nom'])."%", PDO::PARAM_STR);
			$requeteItem->bindValue(":type", pluriel(), PDO::PARAM_STR);
			$requeteItem->execute();
		}
	}
}
else {			
	$message = 'Affichage de tous les '.pluriel().'.'; 
	
	$requeteItem = $baseDeDonnes->prepare($requeteItemBasique);
	$requeteItem->bindValue(":type", pluriel(), PDO::PARAM_STR);
	$requeteItem->execute();
}

$nbEquipements = $requeteItem->rowCount();

include("../Header.php");
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/<?php echo pluriel(); ?>.png" alt="<?php echo pluriel(); ?>" /> Les <?php echo ucfirst(pluriel()) ?></h2>

<p class="Arianne">> <a href="<?php echo ABSPATH ?>">Accueil</a> > <a href="./">Équipements</a> > <?php echo ucfirst(pluriel()); ?></p>

<div class="bloc">
	<form method="get" action name="recherche_equipement">
		<h4 class="quatrieme_titre">Recherche personnalisée</h4>
		<p>
			<label for="Nom">Par Nom :</label>
			
			<input name="Nom" id="Nom" type="text" style="width:240px; position:relative; z-index:1001;" rel="<?php echo pluriel(); ?>" />
			<span class="delete_value_champ" style="position:absolute; z-index:1002;">×</span>
			<ul class="selectionPersonnage maximum" id="results" style="width:256px; box-shadow:0 0 10px #BBB; margin:-10px 0 0 195px; position:absolute; z-index:1000;"></ul>
		</p>	
		<p align="center">
			<input type="submit" value="Rechercher" />
		</p>
	</form>
</div>

<?php 	
echo '<p class="encadreInfos">';
	if(isset($erreur))     echo '<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>';
	
	echo '<span class="right">';
	
	if(isset($erreur))     echo $erreur;
	else {
		echo '<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Loupe.png" alt="" /> <span>';
			if(isset($message))    echo $message."<br />";
			
			if($nbEquipements == 0)   echo 'Aucun familier n\'a été trouvé pour ce critère de recherche. <a href="?Page='.NOM_EQUIPEMENT.'">Réinitialiser</a> la recherche.';
			if($nbEquipements == 1)   echo "1 familier a été trouvé pour ce critère de recherche.";
			if($nbEquipements > 1)    echo $nbEquipements." familiers ont été trouvés pour ce critère de recherche.";
		echo '</span>';
	}
echo '</span></p>';
	
while($data = $requeteItem->fetch()) {
	$data["nom_panoplie"] = utf8_encode(stripslashes($data["nom_panoplie"]));
	$niveau = $data["niveau"];
	$image = imageEquipement(ucfirst($data["type"]), $data['nom']);

	if(!file_exists('../'.$image))     $image = 'Images/Items/Unknown.png';
	
	$nom = stripslashes(utf8_encode($data['nom']));
	$craft = nl2br(stripslashes(utf8_encode($data['craft'])));
	$boost = nl2br(stripslashes(utf8_encode(formatage($data['boost']))));
	
	$intervalle = nl2br(stripslashes(utf8_encode(formatage($data['intervalle']))));
	$pdv = nl2br(stripslashes(utf8_encode(formatage($data['points_de_vie']))));
	$zonePredilection = nl2br(stripslashes(utf8_encode(formatage($data['zones_predilections']))));
	$bonusPredilection = nl2br(stripslashes(utf8_encode(formatage($data['bonus_predilections']))));
	
	$equipementDescription = nl2br(utf8_encode(stripslashes($data['description'])));
	$equipementConditions = nl2br(utf8_encode(stripslashes($data['conditions'])));
	$type = array('Familiers' => 'Familier'); ?>

		<table class="equipementTable" rel="<?php echo pluriel(); ?>" id="Item-<?php echo $data['equi_id']; ?>">
			<tr class="equipementInformations">
				<th>
					Niveau <?php echo $niveau ?></th>
				<th colspan="2">
					<?php
					if(isset($PSEUDO_MEMBRE)) {
						echo '
						<img class="click_ajouter_equipement" alt="" title="Attacher à un des mes personnages" onclick="equipement('.$data['equi_id'].', \''.$data["type"].'\');" src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Epingle.png" />
						';
						echo '<span style="clear:left;"></span>';
					}
					echo $nom;
					?>
				</th>
			</tr>
			
			<tr class="equipementTitres">
				<td class="screen"></td>
				<td width="200">Obtention</td>
				<td width="320">Nourriture</td>
			</tr>
			
			<tr>
				<td class="screen big"><img src="<?php echo ABSPATH.$image ?>" alt="" /></td>         
				<td class="obtention" style="width:220px;"><?php echo $craft; ?></td>
				<td class="effets"><?php echo $boost; ?></td>
			</tr>
		
			<?php
			if(!empty($intervalle)) {
				echo '
				<tr class="clair">
					<td>Intervalle repas</td>
					<td class="right" colspan="2">'.$intervalle.'</td>
				</tr>';
			}
			if(!empty($pdv)) {
				echo '
				<tr class="clair left">
					<td>Points de vie</td>
					<td class="right" colspan="2">'.$pdv.' PDV</td>
				</tr>';
			}
			if(!empty($zonePredilection)) {
				echo '
				<tr class="clair">
					<td>Zones prédilection</td>
					<td class="right" colspan="2">'.$zonePredilection.'</td>
				</tr>';
			}
			if(!empty($bonusPredilection)) {
					echo '
				<tr class="clair left">
					<td>Bonus prédilection</td>
					<td class="right" colspan="2">'.$bonusPredilection;
					if(!empty($data["nom_panoplie"]) AND !empty($data["niveau_panoplie"])) {
						echo '<a href="'.ABSPATH.'Equipements/Panoplies.php?Panoplie='.$data["id"].'">'.$data["nom_panoplie"].' (Niveau '.$data["niveau_panoplie"].')</a>';
					}
				echo '</td>
				</tr>
				';
			}
			
			?>
			
			<tr>
				<td colspan="3" class="description"><b>&laquo;</b> <?php echo $equipementDescription; ?> <b>&raquo;</b></td>
			</tr>
			
			<?php if(!empty($equipementConditions)) { ?>
				<tr>
					<td colspan="3" class="conditions"><b>Conditions :</b> <?php echo $equipementConditions ?></td>
				</tr>
			<?php } ?>
			
		</table>
	<?php
}

include("../Footer.php"); ?>
