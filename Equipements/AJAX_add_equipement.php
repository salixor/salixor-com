<?php
require('../Includes/Fonctions.php');
require('../Includes/Config.php');
	
$array_pluriels = array("arcs", "baguettes", "batons", "dagues", "epees", "marteaux", "pelles", "haches", "pioches", "faux");

if(isset($_POST["delete"]) && isset($_POST["id"]) && isset($_POST["categorie"]) && isset($_POST["id_personnage"])) {
	if($_POST["delete"] == "panoplie" && !empty($_POST["id"]) && !empty($_POST["categorie"])) {
		$requete = $baseDeDonnes->prepare("SELECT * FROM membres WHERE membre_id = :membre");
		$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
		$requete->execute();
	
		if($requete->rowCount() == 0) {
			echo '<img src="'.ABSPATH.'Images/Notifications/Erreur.jpg" /><span>L\'ajout au personnage n\'a pas abouti.</span>';
		}
		else {
			$ids = explode(";", $_POST["id"]);
			$ids = array_unique($ids);
			$ids = array_filter($ids);
			$ids = array_values($ids);	

			$types = explode(";", $_POST["categorie"]);
			$types = array_unique($types);
			$types = array_filter($types);
			$types = array_values($types);	
			
			$i = 0;
			
			foreach($ids AS $id) {
				$actualEquipements = "";
				$equipementsArray = array();
				
				if(in_array($types[$i], $array_pluriels)) {
					$requete = $baseDeDonnes->prepare("SELECT membre_personnage_arme FROM membres_personnages WHERE membre_possesseur_id = :membre AND personnage_id = :personnage");
					$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
					$requete->bindValue(":personnage", $_POST["id_personnage"], PDO::PARAM_INT);
					$requete->execute();
					
					$data = $requete->fetch();
					
					$equipementsArray = explode(":", $data["membre_personnage_arme"]);
				}
				else {
					$requete = $baseDeDonnes->prepare("SELECT membre_personnage_".$types[$i]." FROM membres_personnages WHERE membre_possesseur_id = :membre AND personnage_id = :personnage");
					$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
					$requete->bindValue(":personnage", $_POST["id_personnage"], PDO::PARAM_INT);
					$requete->execute();
					
					$data = $requete->fetch();
					
					$equipementsArray = explode(" ", $data["membre_personnage_".$types[$i].""]);
				}
				
				if($types[$i] == "dofus")          $limite = 6; 
				elseif($types[$i] == "anneaux")    $limite = 2;
				else                               $limite = 1;
				
				array_push($equipementsArray, $id);
				$equipementsArray = array_unique($equipementsArray);
				$equipementsArray = array_filter($equipementsArray);
				$equipementsArray = array_values($equipementsArray);
				if(count($equipementsArray) > $limite) {
					$nb_values = count($equipementsArray);
					$nb_values -= $limite;

					for($a = 0; $a < $nb_values; $a++) {
						unset($equipementsArray[$a]);
					}
				}
			
			
				$j = 0;
				foreach($equipementsArray as $equipement) {
					if($j == count($equipementsArray))   $actualEquipements .= $equipement;
					else                                 $actualEquipements .= $equipement." ";
					
					$j++;
				}
				
				if(!isset($messageFinal)) {
					if(in_array($types[$i], $array_pluriels)) {
						$requete = $baseDeDonnes->prepare("
						UPDATE membres_personnages SET membre_personnage_arme = :equipements WHERE membre_possesseur_id = :membre AND personnage_id = :personnage");
						$requete->bindValue(":equipements", trim($actualEquipements).":".$types[$i], PDO::PARAM_STR);
						$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
						$requete->bindValue(":personnage", $_POST["id_personnage"], PDO::PARAM_INT);
						$requete->execute();				
					}
					else {
						$requete = $baseDeDonnes->prepare("
						UPDATE membres_personnages SET membre_personnage_".$types[$i]." = :equipements WHERE membre_possesseur_id = :membre AND personnage_id = :personnage");
						$requete->bindValue(":equipements", trim($actualEquipements), PDO::PARAM_STR);
						$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
						$requete->bindValue(":personnage", $_POST["id_personnage"], PDO::PARAM_INT);
						$requete->execute();	
					}
				}
				$i++;
				
			}
			
			$messageFinal = '<img src="'.ABSPATH.'Images/Notifications/Validation.jpg" /><span>La panoplie a été ajoutée au personnage.</span>';
		}
	}
	elseif($_POST["delete"] != "true" && $_POST["delete"] != "panoplie" && !empty($_POST["id"]) && !empty($_POST["categorie"]) && !empty($_POST["id_personnage"])) {
		$requete = $baseDeDonnes->prepare("SELECT * FROM membres_personnages WHERE membre_possesseur_id = :membre AND personnage_id = :personnage");
		$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
		$requete->bindValue(":personnage", $_POST["id_personnage"], PDO::PARAM_INT);
		$requete->execute();
	
		if($requete->rowCount() == 0) {
			$messageFinal = '<img src="'.ABSPATH.'Images/Notifications/Erreur.jpg" /><span>L\'ajout au personnage n\'a pas abouti.</span>';
		}
		else {
			$actualEquipements = "";
			$equipementsArray = array();
			
			if($_POST["categorie"] == "dofus")          $limite = 6; 
			elseif($_POST["categorie"] == "anneaux")    $limite = 2;
			else                                        $limite = 1;
			
			if(in_array($_POST["categorie"], $array_pluriels)) {
				$requete = $baseDeDonnes->prepare("SELECT membre_personnage_arme FROM membres_personnages WHERE membre_possesseur_id = :membre AND personnage_id = :personnage");
				$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
				$requete->bindValue(":personnage", $_POST["id_personnage"], PDO::PARAM_INT);
				$requete->execute();
				
				$data = $requete->fetch();
				
				$equipementsArray = explode(":", $data["membre_personnage_arme"]);
			}
			else {
				$requete = $baseDeDonnes->prepare("SELECT membre_personnage_".$_POST["categorie"]." FROM membres_personnages WHERE membre_possesseur_id = :membre AND personnage_id = :personnage");
				$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
				$requete->bindValue(":personnage", $_POST["id_personnage"], PDO::PARAM_INT);
				$requete->execute();
				
				$data = $requete->fetch();
				
				$equipementsArray = explode(" ", $data["membre_personnage_".$_POST["categorie"].""]);
			}
			
			if(!in_array($_POST["id"], $equipementsArray)) {
				array_push($equipementsArray, $_POST["id"]);
				$equipementsArray = array_unique($equipementsArray);
				$equipementsArray = array_filter($equipementsArray);
				$equipementsArray = array_values($equipementsArray);
				if(count($equipementsArray) > $limite) {
					$nb_values = count($equipementsArray);
					$nb_values -= $limite;

					for($i = 0; $i < $nb_values; $i++) {
						unset($equipementsArray[$i]);
					}
				}
			}
			else {
				$messageFinal = '<img src="'.ABSPATH.'Images/Notifications/Erreur.jpg" /><span>Cet équipement est déjà présent sur le personnage.</span>';
			}
		
			$i = 0;
			foreach($equipementsArray as $equipement) {
				if($i == count($equipementsArray))   $actualEquipements .= $equipement;
				else                                 $actualEquipements .= $equipement." ";
				
				$i++;
			}
			
			if(!isset($messageFinal)) {
				if(in_array($_POST["categorie"], $array_pluriels)) {
					$requete = $baseDeDonnes->prepare("
					UPDATE membres_personnages SET membre_personnage_arme = :equipements WHERE membre_possesseur_id = :membre AND personnage_id = :personnage");
					$requete->bindValue(":equipements", trim($actualEquipements).":".$_POST["categorie"], PDO::PARAM_STR);
					$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
					$requete->bindValue(":personnage", $_POST["id_personnage"], PDO::PARAM_INT);
					$requete->execute();				
				
					$messageFinal = '<img src="'.ABSPATH.'Images/Notifications/Validation.jpg" /><span>L\'arme a été ajoutée au personnage.</span>';
				}
				else {
					$requete = $baseDeDonnes->prepare("
					UPDATE membres_personnages SET membre_personnage_".$_POST["categorie"]." = :equipements WHERE membre_possesseur_id = :membre AND personnage_id = :personnage");
					$requete->bindValue(":equipements", trim($actualEquipements), PDO::PARAM_STR);
					$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
					$requete->bindValue(":personnage", $_POST["id_personnage"], PDO::PARAM_INT);
					$requete->execute();	
				
					$messageFinal = '<img src="'.ABSPATH.'Images/Notifications/Validation.jpg" /><span>L\'équipement a été ajouté au personnage.</span>';
				}
			}
		}
	}
	elseif($_POST["delete"] == "true" && $_POST["delete"] != "panoplie" && !empty($_POST["id"]) && !empty($_POST["categorie"]) && !empty($_POST["id_personnage"])) {
		$requete = $baseDeDonnes->prepare("SELECT * FROM membres_personnages WHERE membre_possesseur_id = :membre AND personnage_id = :personnage");
		$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
		$requete->bindValue(":personnage", $_POST["id_personnage"], PDO::PARAM_INT);
		$requete->execute();
	
		if($requete->rowCount() == 0) {
			$messageFinal = '<img src="'.ABSPATH.'Images/Notifications/Erreur.jpg" /><span>La suppression de l\'équipement n\'a pas aboutie.</span>';
		}
		else {
			$actualEquipements = "";
			$equipementsArray = array();
			
			if($_POST["categorie"] == "dofus")          $limite = 6; 
			elseif($_POST["categorie"] == "anneaux")    $limite = 2;
			else                                        $limite = 1;
			
			if(in_array($_POST["categorie"], $array_pluriels)) {
				$requete = $baseDeDonnes->prepare("SELECT membre_personnage_arme FROM membres_personnages WHERE membre_possesseur_id = :membre AND personnage_id = :personnage");
				$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
				$requete->bindValue(":personnage", $_POST["id_personnage"], PDO::PARAM_INT);
				$requete->execute();
				
				$data = $requete->fetch();
				
				$equipementsArray = explode(":", $data["membre_personnage_arme"]);
			}
			else {
				$requete = $baseDeDonnes->prepare("SELECT membre_personnage_".$_POST["categorie"]." FROM membres_personnages WHERE membre_possesseur_id = :membre AND personnage_id = :personnage");
				$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
				$requete->bindValue(":personnage", $_POST["id_personnage"], PDO::PARAM_INT);
				$requete->execute();
				
				$data = $requete->fetch();
				
				$equipementsArray = explode(" ", $data["membre_personnage_".$_POST["categorie"].""]);
			}

			if(in_array($_POST["id"], $equipementsArray)) {
				array_push($equipementsArray, $_POST["id"]);
				$equipementsArray = array_unique($equipementsArray);
				$equipementsArray = array_filter($equipementsArray);
				$equipementsArray = array_values($equipementsArray);
				if(count($equipementsArray) > $limite) {
					$nb_values = count($equipementsArray);
					$nb_values -= $limite;

					for($i = 0; $i < $nb_values; $i++) {
						unset($equipementsArray[$i]);
					}
				}
			}
			else {
				$messageFinal = '<img src="'.ABSPATH.'Images/Notifications/Erreur.jpg" /><span>Cet équipement n\'est pas présent sur le personnage.</span>';
			}
				
			foreach($equipementsArray as $key => $equipement) {
				if($equipement == $_POST["id"]) {
					unset($equipementsArray[$key]);
				}
			}
			
			$i = 0;
			foreach($equipementsArray as $equipement) {
				if($i == count($equipementsArray))   $actualEquipements .= $equipement;
				else                                 $actualEquipements .= $equipement." ";
				
				$i++;
			}
			
			if(!isset($messageFinal)) {
				if(in_array($_POST["categorie"], $array_pluriels)) {
					$requete = $baseDeDonnes->prepare("
					UPDATE membres_personnages SET membre_personnage_arme = :equipements WHERE membre_possesseur_id = :membre AND personnage_id = :personnage");
					$requete->bindValue(":equipements", "", PDO::PARAM_STR);
					$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
					$requete->bindValue(":personnage", $_POST["id_personnage"], PDO::PARAM_INT);
					$requete->execute();				
				
					$messageFinal = '<img src="'.ABSPATH.'Images/Notifications/Suppression.jpg" /><span>L\'arme a été supprimée du personnage.</span>';
				}
				else {
					$requete = $baseDeDonnes->prepare("
					UPDATE membres_personnages SET membre_personnage_".$_POST["categorie"]." = :equipements WHERE membre_possesseur_id = :membre AND personnage_id = :personnage");
					$requete->bindValue(":equipements", trim($actualEquipements), PDO::PARAM_STR);
					$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
					$requete->bindValue(":personnage", $_POST["id_personnage"], PDO::PARAM_INT);
					$requete->execute();	
				
					$messageFinal = '<img src="'.ABSPATH.'Images/Notifications/Suppression.jpg" /><span>L\'équipement a été supprimé du personnage.</span>';
				}
			}
		}
	}	
	else {
		$messageFinal = '<img src="'.ABSPATH.'Images/Membres/Erreur.png" /><span>Une erreur est survenue.</span>';
	}
	
	echo $messageFinal;
}

if(!isset($_POST["delete"]) || !isset($_POST["id"]) || !isset($_POST["categorie"]) || !isset($_POST["id_personnage"])) {
	$requete = $baseDeDonnes->prepare("
	SELECT *
	FROM membres_personnages
	WHERE membre_possesseur_id = :id");
	$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
	$requete->execute();

	$i = 0;
	while($data = $requete->fetch()) {
		$sexe = strtolower(substr($data["membre_sexe_personnage"], 0, 1));
		$classe = strtolower(strtr($data["membre_classe_personnage"], $GLOBALS['normalizeChars']));
		$image = strtolower($classe).'-'.$sexe.'.png';
		
		if($i == 0) {
			$imageA = $image;
			$pseudo = $data["membre_nom_personnage"];
			$classeA = $data["membre_classe_personnage"];
			$niveau = $data["membre_niveau_personnage"];
		}	
		
		echo '<div class="equipementMembre choixPersonnage" rel="'.$data["personnage_id"].'" rel-pseudo="'.$data["membre_nom_personnage"].'" rel-classe="'.$data["membre_classe_personnage"].'" rel-niveau="'.$data["membre_niveau_personnage"].'">
				  <img src="'.ABSPATH.'Images/Personnages/'.$image.'" alt="" />
			  </div>';
			
		$i++;
	}

	echo '
	<div class="choixPersonnageInfos">
		<div class="image"><img src="'.ABSPATH.'Images/Personnages/'.$imageA.'" alt="" /></div>
		<div class="pseudo">'.$pseudo.'</div>
		<div class="little">'.$classeA.' de niveau '.$niveau.'</div>
	</div>';

	echo '<p><input type="submit" class="disabled" value="Choisir" disabled="disabled" /></p>';
}
?>