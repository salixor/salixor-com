<?php
require_once("Includes/Fonctions.php");
require_once("Includes/Config.php");

//Si le formulaire est envoyé
if(isset($_POST['submit'])) {
	//Vérification du pseudo
	if(trim($_POST['contactname']) == "")     $hasError = true;
	else                                      $name = trim($_POST['contactname']);

	//Vérification du sujet
	if(trim($_POST['subject']) == "")     $hasError = true;
	else                                  $subject = trim($_POST['subject']);

	//Vérification de l'email
	if(trim($_POST['emailContact']) == "")                                                       $hasError = true;
	elseif(strpos("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim($_POST['emailContact'])))    $hasError = true;
	else                                                                                         $email = trim($_POST['emailContact']);

	//Vérification du message
	if(trim($_POST['message']) == "")     $hasError = true;
	else                                  $comments = stripslashes(trim(formatage(smileys($_POST['message']))));

	//Envoi du mail
	if(!isset($hasError)) {
		$emailTo = 'kevin.cocchi@gmail.com'; //Email de contact
		$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<title>Mail de Salixor.com</title>
				<style type="text/css">
					body {
						font-family:"Trebuchet MS";
					}
				</style>
				<div style="margin:15px 0;">
					<b style="background:#F7F7F7; padding:10px; width:95%; display:block; color:#DD4B39;">
						<span style="width:150px; display:inline-block;">Pseudo</span>
						<span style="font-weight:normal; color:#333;">'.$name.'</span>
					</b>
				</div>
				<div style="margin:15px 0;">
					<b style="background:#F7F7F7; padding:10px; width:95%; display:block; color:#DD4B39;">
						<span style="width:150px; display:inline-block;">Sujet</span>
						<span style="font-weight:normal; color:#333;">'.$subject.'</span>
					</b>
				</div>
				<div style="margin:15px 0;">
					<b style="background:#F7F7F7; padding:10px; width:95%; display:block; color:#DD4B39;">
						<span style="width:150px; display:inline-block;">Email</span>
						<span style="font-weight:normal; color:#333;">'.$email.'</span>
					</b>
				</div>
				<div style="margin:15px 0;">
					<b style="background:#F7F7F7; padding:10px; width:95%; display:block; color:#DD4B39;">
						<span style="width:150px; display:block; float:left; margin-bottom:-200px; padding-bottom:200px;">Message</span>
						<span style="font-weight:normal; color:#333;">'.$comments.'</span>
					</b>
				</div>';
		$headers = 'Mime-Version: 1.0'."\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
		$headers .= "\r\n";

		mail($emailTo, stripslashes($subject), stripslashes($body), $headers);
		$emailSent = true;
	}
}

$titre_page_nom = "Contact";
include("Header.php");
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/contact.png" alt="Contact" /> Contact</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH ?>">Accueil</a> > Contacter le Webmaster</p>

<p class="encadreInfos">
	<span class="right">
		Une question, un remerciement ou un encouragement ? C'est bien ici qu'il faut vous rendre !
	</span>
</p>

<?php
if(isset($hasError)) {
	echo '
	<p class="encadreInfos">
		<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>
		<span class="right">Le Mail n\'a pas pu être envoyé. Réessayez une seconde fois.</span>
	</p>';
}
if(isset($emailSent)) {
	echo '
	<p class="encadreInfos">
		<span class="left valide"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Valide.png" alt="Confirmation" /> Confirmation</span>
		<span class="right">Votre email est envoyé. Je vous recontacterais au plus vite.</span>
	</p>';
}
?>

<div id="contactform" class="bloc">              
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="contactForm">
		<p>
			<label for="contactname" id="contactname_label">Pseudo : </label>
			<?php
			if(isset($PSEUDO_MEMBRE)) {
				echo $PSEUDO_MEMBRE.'. <a href="'.ABSPATH.'Membres/Deconnexion.php">Se déconnecter</a>.';
				echo '<input type="hidden" name="contactname" id="contactname" class="required" style="width:400px;" value="'.$PSEUDO_MEMBRE.'" />';
			}
			else {
				echo '<input type="text" name="contactname" id="contactname" class="required" style="width:400px;" /><a href="javascript:connexion();" style="margin:0 5px;">Se connecter</a>';
			}
			?>
		</p>
		
		<p>
			<label for="emailContact" id="email_label">Email : </label>
			<?php
			if(isset($PSEUDO_MEMBRE)) {
				$requete = $baseDeDonnes->prepare("SELECT membre_mail FROM membres WHERE membre_id = :id");
				$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
				$requete->execute();
				
				$data = $requete->fetch();
				
				if(!empty($data["membre_mail"])) {
					echo $data["membre_mail"];
					echo '<input type="hidden" name="emailContact" id="emailContact" class="required" style="width:400px;" value="'.$data["membre_mail"].'" />';
				}
				else {
					echo '<input type="text" name="emailContact" id="emailContact" class="required" style="width:400px;" />';
				}
			}
			else {
				echo '<input type="text" name="emailContact" id="emailContact" class="required" style="width:400px;" />';
			}
			?>
		</p>
		
		<p>
			<label for="subject" id="subject_label">Sujet : </label>
			<input type="text" name="subject" id="subject" class="required" style="width:400px;" />
		</p>
		
		<?php
		afficher_smileys('message');
		$FormatagePanel = new FormatagePanel(true, true, true, 'message');
		$FormatagePanel->showFormatagePanel();
		?>
		
		<p>
			<textarea name="message" id="message" class="required" style="height:300px; margin-top:40px;"></textarea>
		</p>
		
		<p align="center">
			<input type="button" id="previsualiser_bouton" value="Prévisualiser" onclick="previsualiser('message', 'previsualiserMessage', 'AJAX_news.php');" />
			<input type="reset" value="Réinitialiser" />
			<input type="submit" value="Envoyer" name="submit" id="submit" />
		</p>
    </form>
</div>
						
<p align="center" id="previsualiser_zone">
	<div class="preview" id="previsualiserMessage"></div>
</p>

<?php include("Footer.php"); ?>