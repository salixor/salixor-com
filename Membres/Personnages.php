<?php 
$titre_page_nom = "Liste des Personnages";
include("../Header.php");
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Membres.png" alt="Membres" /> Liste des Personnages</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH ?>">Accueil</a> > <a href="./">Membres</a> > Liste des Personnages</p>

<p class="encadreInfos"><span class="right">Voici la liste des personnages des membres du site. Rejoignez nous <a href="Inscription.php">ici</a> pour vous aussi ajouter vos personnage.</span></p>

<?php
$requete = $baseDeDonnes->query("
SELECT membres.*, membres_personnages.* FROM membres_personnages
LEFT JOIN membres ON membres.membre_id = membres_personnages.membre_possesseur_id
ORDER BY membre_niveau_personnage DESC, personnage_id ASC");
$requete->setFetchMode(PDO::FETCH_OBJ);

echo '
<table>
	<tr>
		<th width="30"></th>
		<th width="150">Membre</th>
		<th width="150">Personnage</th>
		<th width="40">Niveau</th>
		<th width="100">Classe</th>
		<th width="150">Serveur</th>
	</tr>';

	while($data = $requete->fetch()) {
		$sexe = strtolower(substr($data->membre_sexe_personnage, 0, 1));
		$classe = strtolower(strtr($data->membre_classe_personnage, $GLOBALS['normalizeChars']));
		$image = strtolower($classe).'-'.$sexe.'.png';
		
		$serveur = $data->membre_serveur_personnage;
		$imageServeur = strtr($serveur, $GLOBALS['normalizeChars']);
		$imageServeur = preg_replace("#\s#", "", $serveur);
		
		$avatar = '<img src="../Images/Personnages/'.$image.'" height="30" alt="" />';
		$imageServeur = '<img src="../Images/Serveurs/'.$imageServeur.'.png" height="30" alt="" style="vertical-align:middle; position:relative; top:-2px; bottom:-2px; margin-right:2px;" />';
		
		echo '
		<tr>
			<td align="center">'.$avatar.'</td>
			<td><a href="Profil.php?id='.$data->membre_possesseur_id.'">'.stripslashes($data->membre_pseudo).'</a></td>
			<td><a href="Profil.php?id='.$data->membre_possesseur_id.'&amp;Personnage='.$data->personnage_id.'">'.$data->membre_nom_personnage.'</a></td>
			<td align="center">'.$data->membre_niveau_personnage.'</td>
			<td>'.$data->membre_classe_personnage.'</td>';
			
			if(!empty($serveur))    echo '<td style="padding:7px 5px 3px;">'.$imageServeur.' '.$serveur.'</td>';
			else                    echo '<td>Non renseigné</td>';
			
		echo '</tr>';
	}

echo '</table>';

include("../Footer.php"); ?>