<?php
require_once("../Includes/Fonctions.php");
require_once("../Includes/Config.php");
require_once("../Includes/Class/ShowPersonnagePicture.class.php");

$i = 0;

$requete = $baseDeDonnes->prepare("
SELECT *
FROM membres_personnages
LEFT JOIN guildes ON membres_personnages.personnage_guilde = guildes.guilde_id
WHERE membre_possesseur_id = :id AND personnage_id = :personnage");
$requete->bindValue(":id", $_GET["id_membre"], PDO::PARAM_INT);
$requete->bindValue(":personnage", $_GET["id"], PDO::PARAM_INT);
$requete->execute();

$data = $requete->fetch();

$id = $data["personnage_id"];
$personnage = stripslashes($data["membre_nom_personnage"]);
$classe = stripslashes($data["membre_classe_personnage"]);
$niveau = stripslashes($data["membre_niveau_personnage"]);
$serveur = stripslashes($data["membre_serveur_personnage"]);
$sexe = stripslashes($data["membre_sexe_personnage"]);

$guilde = stripslashes($data["guilde_nom"]);
$guildeId = $data["guilde_id"];

$metiersPersonnage = explode(";", $data["membre_personnage_metiers"]);
$metiersPersonnage = array_filter($metiersPersonnage);
$dataEquipements = $data;

$sexeB = strtolower(substr($sexe, 0, 1));
$classeB = strtolower(strtr($classe, $GLOBALS['normalizeChars']));
$image = strtolower($classeB).'-'.$sexeB.'.png';

$imageServeur = strtr($serveur, $GLOBALS['normalizeChars']);
$imageServeur = preg_replace("#\s#", "", $serveur);

echo '<div class="fichePerso">';
	echo '<p style="float:left;"><h4 class="quatrieme_titre" id="EquipementMembreTitre" style="border:none; color:#D98026; margin:0 15px; padding:0; width:auto;">Personnage</h4></p>';
		
	echo '
	<div class="choisirPerso perso">
		<span class="image"><img src="'.ABSPATH.'Images/Personnages/'.$image.'" alt="" /></span>
		<span class="pseudo">'.$personnage.'</span>
		<span class="infos">'.$classe.' niveau '.$niveau.'</span>
	</div>';
	
	if(!empty($serveur) || !empty($guilde)) {
		echo '<div class="choisirPerso perso">';
			if(!empty($serveur)) {
				echo '
				<span class="image"><img src="../Images/Serveurs/'.$imageServeur.'.png" alt="" /></span>
				<span class="pseudo">Joue sur '.$serveur.'</span>';
			}
			else {
				echo '
				<span class="pseudo">Serveur non connu</span>';
			}
			if(!empty($guilde)) {
				echo '<span class="infos">Guilde : <a href="'.ABSPATH.'Membres/Guildes/Voir.php?Guilde='.$guildeId.'"><b>'.$guilde.'</b></a></span>';
			}
			else {
				echo '<span class="infos">Guilde non renseignée</span>';
			}
		echo '</div>';
	}
	
	if(!empty($metiersPersonnage)) {
		echo '<div class="choisirPerso perso">';
			foreach($metiersPersonnage as $metier) {
				$metier = explode("-", $metier);
				echo '<span class="infos"><b>'.$metier[0].'</b> niveau '.$metier[1].'</span>';
			}
		echo '</div>';
	}

	$PersonnagePicture = new ShowPersonnagePicture($personnage, $serveur, $niveau);
echo '</div>';

$array_equipements = array( array('boucliers', 'Bouclier'),
							array('chapeaux',  'Chapeau'),
							array('capes',     'Cape'),
							array('sacs',      'Sac'),
							array('ceintures', 'Ceinture'),
							array('bottes',    'Botte'),
							array('amulettes', 'Amulette'),
							array('anneaux',   'Anneau'),
							array('arme',      'Arme'),
							array('familiers', 'Familier'),
							array('dofus',     'Dofus') );
							
for($i = 0; $i < count($array_equipements); $i++) {
	$equipements_array[$i] = array_filter(explode(" ", $dataEquipements["membre_personnage_".$array_equipements[$i][0]]));
	$maxValue = count($equipements_array[$i]);
	
	$equipements_array[$i][$maxValue] = $array_equipements[$i][0];
	
	if(count($equipements_array[$i]) == 0)    unset($equipements_array[$i]);
	
	$equipements_array[$i] = array_unique($equipements_array[$i]);
}

$equipements_array = array_values($equipements_array);

$sql = "SELECT * FROM equipements WHERE ";
			
$arrayArmes = array("arcs" => "Arc", "baguettes" => "Baguette", "batons" => "Baton", "dagues" => "Dague", "epees" => "Epee", "marteaux" => "Marteau", "pelles" => "Pelle", "haches" => "Hache", "pioches" => "Pioche", "faux" => "Faux");
	
if(count($equipements_array[8]) > 1) {
	$arme = explode(":", $equipements_array[8][0]);
	array_push($arme, $arrayArmes[$arme[1]]);
}

foreach($equipements_array as $cle => $equipement) {
	$actual = $cle;
		
	foreach($equipement as $item) {
		if(is_numeric($item))   $sql .= "id = ".$item ." AND type = '".$array_equipements[$actual][0]."' OR ";
	}
}

if(isset($arme))     $sql .= "id = ".$arme[0]." AND type = '".$arme[1]."' ";

$sql = preg_replace("#OR $#", "", $sql);

$sql .= 'ORDER BY FIELD(type, "anneaux", "chapeaux", "capes", "amulettes", "bottes", "ceintures", "sacs", "boucliers", "arcs", "baguettes", "batons", "dagues", "epees", "marteaux", "pelles", "haches", "pioches", "faux", "familiers", "dofus")';

echo '<p style="float:left;"><h4 class="quatrieme_titre" id="EquipementMembreTitre" style="border:none; color:#D98026; margin:10px 12px; padding:0; width:auto;">Son équipement</h4></p>';

echo '<div id="EquipementMembre">';
		
if($sql != "" && $sql != 'SELECT * FROM equipements WHERE ORDER BY FIELD(type, "anneaux", "chapeaux", "capes", "amulettes", "bottes", "ceintures", "sacs", "boucliers", "arcs", "baguettes", "batons", "dagues", "epees", "marteaux", "pelles", "haches", "pioches", "faux", "familiers", "dofus")') {
	$requete = $baseDeDonnes->query($sql);

	if($requete->rowCount() > 0) {
		$i = 0;
		while($data = $requete->fetch()) {
			$image = lienImageEquipement(ucfirst($data["type"]), $data['nom']);

			if(!file_exists('../'.$image))     $image = '<img src="../Images/Items/Unknown.png" alt="" />';
			else                               $image = '<img src="../'.$image.'" alt="" />';
			
			$i++;
			if($i < count($array_equipements))     $lastkey = array_pop(array_keys($array_equipements[$i]));
			elseif($i > count($array_equipements)) $lastkey = count($array_equipements);
			
			$supprimer = "";
			if(isset($ID_MEMBRE)) {
				if($ID_MEMBRE == $_GET["id_membre"]) {
					$supprimer = '<span class="deleteEquipement" onclick="equipement('.$data["id"].', \''.$data["type"].'\', \'supprimer\');" title="Supprimer l\'objet">×</span>';
				}
			}
			
			if($niveau >= $data["niveau"]) {   $add = ""; }
			else {                            $add = "erreur"; }
			echo '
			<div class="equipementMembre '.$add.'" id="Item-'.$data["id"].'" rel="'.stripslashes(utf8_encode($data["nom"])).'" rel-niveau="'.$data["niveau"].'" rel-type="'.$data["type"].'">
				<a class="receive-item-fiche" href="javascript:void(0);" rel="'.utf8_encode($data["nom"]).'">'.$image.'</a>
				'.$supprimer.'
			</div>';
		}
	}
	else {
		echo '<div class="new-perso" style="margin-top:2px;">';
			echo '<div class="choisirPerso"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Equipements.png" alt="" /> Ce personnage n\'a pas d\'équipement.</div>';
			echo '<div style="clear:left;"></div>';
		echo '</div>';
	}
}
else {
	echo '<div class="new-perso" style="margin-top:2px;">';
		echo '<div class="choisirPerso"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Equipements.png" alt="" /> Ce personnage n\'a pas d\'équipement.</div>';
		echo '<div style="clear:left;"></div>';
	echo '</div>';
}
	
echo '</div>';

echo '<div style="clear:left;"></div>';
?>