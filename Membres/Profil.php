<?php 
$titre_page_nom = "Voir un profil";
include("../Header.php");


echo '<div class="ajouter_equipement" id="information_ajouter_equipement"><div class="contenu"></div></div>';

if(isset($_GET["id"])) {
	$erreurs = array();
	
	$requete = $baseDeDonnes->prepare("
	SELECT membres.*, membres_titres.titre AS membre_titre
	FROM membres
	LEFT JOIN membres_titres
	ON membres.membre_titre_id = membres_titres.id
	WHERE membres.membre_id = :id");
	$requete->bindValue(':id', $_GET["id"], PDO::PARAM_INT);
	$requete->execute();
	
	$data = $requete->fetch();
	
	$pseudo = stripslashes($data["membre_pseudo"]);
	$avatar = $data["membre_avatar"];
	$rang = $MEMBRES_RANGS[$data["membre_niveau"]];
	$date_inscription = date_jour_mois_annee($data["membre_inscription"]);
	$dateDerniereVisite = afficher_date_difference($data["membre_derniere_visite"]);
	$nbPosts = $data["membre_post"];
	$nbCommentaires = $data["membre_commentaires"];	
	$citation = stripslashes(trim($data["membre_citation"]));

	if($data["membre_naissance"] != 0) {
		$date_naissance = convertirTimestamp($data["membre_naissance"]);
		
		$anneeNaissance = date("Y", $data["membre_naissance"]);
		$moisNaissance = date("n", $data["membre_naissance"]);
		$jourNaissance = date("j", $data["membre_naissance"]);
		
		$age = age($anneeNaissance, $moisNaissance, $jourNaissance)." ans";
	}
	
	$membreSexe = $data["membre_sexe"];
	$membreProfession = $data["membre_profession"];
	$loisirs = stripslashes($data["membre_loisirs"]);
	$site_web = stripslashes($data["membre_web"]);
	$titre_membre = utf8_encode(stripslashes($data["membre_titre"]));
	
	switch($data["membre_style"]) {
		case "Normal":   $style = "Thème Normal";     break;
		case "Noel":     $style = "Thème de Noël";    break;
		case "Ete":      $style = "Thème d'Été";      break;
		default:         $style = "Thème Normal";     break;
	}
	
	$membre_nb_visite = $data["membre_nb_visite"];

	echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Profil.png" alt="Profil" /> Profil</h2>';

	echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > Voir un profil</p>';
	
	if($requete->rowCount() > 0) {
			echo '<div class="avatarMembre">';
				echo '<h4 class="quatrieme_titre">'.$pseudo.'</h4>';
				
				echo '<div class="zoneAvatar">';
					if(!empty($avatar))      echo '<img src="../Images/Avatars/'.$avatar.'" alt="" style="background:#222;" />';
					else                     echo '<img src="../Images/Avatars/avatar_normal.png" alt="" />';
					
					if(isset($ID_MEMBRE)) {
						if($ID_MEMBRE != $_GET["id"])                           echo '<a href="Messagerie.php?Action=Envoyer&amp;Pseudo='.$pseudo.'" class="buttons buttonSend"></a>';
						if($ID_MEMBRE == $_GET["id"])                           echo '<a href="Modifier_Profil.php" class="buttons buttonEdit"></a>';
						if($NIVEAU_MEMBRE >= 5 && $ID_MEMBRE != $_GET["id"])    echo '<a href="javascript:void(0);" title="A faire" class="buttons buttonBan"></a>';
						echo '<div style="clear:left;"></div>';
					}
				echo '</div>';
				
				if(!empty($citation))          echo '<span class="citationProfil">&quot;'.$citation.'&quot;</span>';
				if(!empty($titre_membre))      echo '<span class="titreProfil">&laquo;'.$titre_membre.'&raquo;</span>';
				
				echo '<div class="infosConnexion">';
					if(!empty($rang)) {
						echo '<div>';
							if($rang == "Membre Banni")           echo '<span class="MembreBanni"><img src="../Images/Membres/Banni.png" alt="" /> ';
							elseif($rang == "Membre")             echo '<span class="Membre"><img src="../Images/Membres/Membre.png" alt="" /> ';
							elseif($rang == "Béta Testeur")       echo '<span class="BetaTesteur"><img src="../Images/Membres/BetaTesteur.png" alt="" /> ';
							elseif($rang == "Modérateur")         echo '<span class="Moderateur"><img src="../Images/Membres/Moderateur.png" alt="" /> ';
							elseif($rang == "Administrateur")     echo '<span class="Administrateur"><img src="../Images/Membres/Administrateur.png" alt="" /> ';
							else                                  echo '<span class="MembreBanni"><img src="../Images/Membres/Banni.png" alt="" /> ';
							
							echo $rang.'</span>';
						echo '</div>';
					}			
					
					if($data["membre_connecte"] == 1)   echo '<div><span class="MembreConnecte"><img src="../Images/Membres/MembreConnecte.png" alt="" /> Connecté(e)</span></div>';
					else                                echo '<div><span class="MembreDeconnecte"><img src="../Images/Membres/MembreDeconnecte.png" alt="" /> Déconnecté(e)</span></div>';
					
					if(!empty($membreSexe)) {
						if($membreSexe == "Homme")      echo '<div><span class="male gras"><img src="../Images/Forum/Male.png" alt="" style="margin:2px 0 -2px;" /> Homme</span></div>';
						if($membreSexe == "Femme")      echo '<div><span class="female gras"><img src="../Images/Forum/Femelle.png" alt="" style="margin:2px 0 -2px;" /> Femme</span></div>';
					}
					
					if(!empty($site_web))  echo '<div><a href="'.$site_web.'"><img src="../Images/Membres/WebMembre.png" alt="" style="margin:2px 0 -2px;" /> Voir son site</a></div>';
				echo '</div>';
			echo '</div>';
			
			echo '<div class="infosMembre">';
				if(!empty($date_inscription))     echo '<div>Ce membre est inscrit depuis le '.$date_inscription.'.</div>';
				if(!empty($dateDerniereVisite))   echo '<div><b>Dernière visite :</b> '.$dateDerniereVisite.'.</div>';
				if(!empty($membre_nb_visite))     echo '<div>'.$pseudo.' a rendu '.$membre_nb_visite.' visites sur le site.</div>';
				
				if(!empty($style))                echo '<div>Utilise le '.$style.'.</div>';
				
				echo '<br />';
				
				if(!empty($nbCommentaires) || !empty($nbPosts)) {
					echo '
					<div class="membreCountPosts">
						<img src="../Images/Membres/MembreComment.png" alt="" />
						<span>
							Ce membre a écrit '.$nbCommentaires.' commentaires.<br />
							Ce membre a écrit '.$nbPosts.' messages sur le forum.
						</span>
					</div><br />';
				}

				if(isset($date_naissance) && !empty($age)) {
					echo '
					<div class="membreCountPosts">
						<img src="../Images/Membres/MembreBirthday.png" alt="" />
						<span>
							'.$pseudo.' est né le '.$date_naissance.'.<br />
							Ce membre a donc '.$age.'.
						</span>
					</div>';
					echo '<br />';
				}
				
				if(!empty($membreProfession)) {
					echo '<div><img src="../Images/Membres/MembreWork.png" alt="" style="margin:3px 0 -3px;" /> Exerce la profession de &quot;<b>'.$membreProfession.'</b>&quot;</div>';
				}
				if(!empty($loisirs)) {
					echo '<div><img src="../Images/Membres/MembreHeart.png" alt="" style="margin:2px 0 -2px;" /> '.$loisirs.'</div>';
				}
			echo '</div>';
			
			echo '<div class="left"></div>';
		
		$requete->closeCursor();
			
			$requete = $baseDeDonnes->prepare("
			SELECT *
			FROM membres_personnages
			WHERE membre_possesseur_id = :id
			ORDER BY membre_niveau_personnage DESC, membre_nom_personnage DESC");
			$requete->bindValue(":id", $_GET["id"], PDO::PARAM_INT);
			$requete->execute();
			
			if($requete->rowCount() > 0) {
				echo '<h3 class="troisieme_titre">En jeu</h3>';
				
				echo '<ul class="selectionPersonnage right">';
				
				echo '<a href="javascript:void(0);" class="active"><li>Choisir un personnage<span class="fleche"></span></li></a>';
				
				echo '<div class="abso">';
				$final = "";
				while($data = $requete->fetch()) {
					$id = $data["personnage_id"];
					$personnageA = stripslashes($data["membre_nom_personnage"]);
					$classeA = stripslashes($data["membre_classe_personnage"]);
					$niveauA = stripslashes($data["membre_niveau_personnage"]);
					$sexeA = stripslashes($data["membre_sexe_personnage"]);

					$sexeB = strtolower(substr($sexeA, 0, 1));
					$classeB = strtolower(strtr($classeA, $GLOBALS['normalizeChars']));
					$imageA = strtolower($classeB).'-'.$sexeB.'.png';
					
					if(isset($_GET["Personnage"])) {
						if($_GET["Personnage"] == $id) {
							echo '
							<a href="javascript:void(0);" class="deactivate"
							rel="'.$id.'" rel-membre="'.$_GET["id"].'" rel-pseudo="'.$personnageA.'" rel-classe="'.$classeA.'" rel-niveau="'.$niveauA.'" rel-image="'.ABSPATH.'Images/Personnages/'.$imageA.'">
								<li>
									<span class="fleche"></span>
									<span class="image"><img src="'.ABSPATH.'Images/Personnages/'.$imageA.'" alt="" /></span>
									<span class="pseudo">'.$personnageA.'</span>
									<span class="little">'.$classeA.' de niveau '.$niveauA.'</span>
								</li>
							</a>';
						}
						else {
							$final .= '
							<a href="javascript:void(0);"
							rel="'.$id.'" rel-membre="'.$_GET["id"].'" rel-pseudo="'.$personnageA.'" rel-classe="'.$classeA.'" rel-niveau="'.$niveauA.'" rel-image="'.ABSPATH.'Images/Personnages/'.$imageA.'">
								<li>
									<span class="fleche"></span>
									<span class="image"><img src="'.ABSPATH.'Images/Personnages/'.$imageA.'" alt="" /></span>
									<span class="pseudo">'.$personnageA.'</span>
									<span class="little">'.$classeA.' de niveau '.$niveauA.'</span>
								</li>
							</a>';
						}
					}
					else {
						echo '
						<a href="javascript:void(0);"
						rel="'.$id.'" rel-membre="'.$_GET["id"].'" rel-pseudo="'.$personnageA.'" rel-classe="'.$classeA.'" rel-niveau="'.$niveauA.'" rel-image="'.ABSPATH.'Images/Personnages/'.$imageA.'">
							<li>
								<span class="fleche"></span>
								<span class="image"><img src="'.ABSPATH.'Images/Personnages/'.$imageA.'" alt="" /></span>
								<span class="pseudo">'.$personnageA.'</span>
								<span class="little">'.$classeA.' de niveau '.$niveauA.'</span>
							</li>
						</a>';
					}
				}
				
				echo $final;
				
				echo '</div>';
				
				echo '</ul>';

				echo '<div class="new-perso">';
					if(isset($_GET["Personnage"])) {
						$temp = $_GET["id"];
						$_GET["id_membre"] = $_GET["id"];
						$_GET["id"] = $_GET["Personnage"];
						include('changementPerso.ajax.php');
					}
					else {
						echo '<div class="choisirPerso"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Personnages.png" alt="" onclick="showPersonnages(event);" /> Sélectionnez un personnage à afficher dans la liste à droite.</div>';
						echo '<div style="clear:left;"></div>';
					}
				echo '</div>';
				
				$requete->closeCursor();
			}
		
		echo '<h3 class="troisieme_titre">Activité sur le site</h3>';
		
		$requete->closeCursor();

		if(isset($temp))    $_GET["id"] = $temp;
		
		$requete = $baseDeDonnes->prepare("
		SELECT news.*, commentaires.*, commentaires.id AS id_comm FROM commentaires
		LEFT JOIN news ON news.id = commentaires.id_news
		WHERE commentaires.id_membre = :id
		ORDER BY commentaires.id DESC");
		$requete->bindValue(':id', $_GET["id"], PDO::PARAM_INT);
		
		$requete2 = $baseDeDonnes->prepare("
		SELECT forum_post.*, forum_topic.* FROM forum_post
		LEFT JOIN forum_topic ON forum_post.topic_id = forum_topic.topic_id
		WHERE forum_post.post_createur = :id
		ORDER BY forum_post.post_id DESC");
		$requete2->bindValue(':id', $_GET["id"], PDO::PARAM_INT);

		if(isset($ID_MEMBRE)) {
			if($ID_MEMBRE == $_GET["id"]) {
				$pageDuMembre = 'true';
			}
			else {
				$pageDuMembre = 'false';
			}
		}
		else {
			$pageDuMembre = 'false';
		}
		
		$i = 0;
		$requete->execute();
		
		echo '<div style="float:left">';
			echo '<h4 class="quatrieme_titre left">Sur les News</h4>';
			echo '<ul class="navigationPage left">';
			if($requete->rowCount() > 0) {
				while($data = $requete->fetch()) {
					if($i <= 4) {
						echo '
						<a href="'.ABSPATH.'?News='.$data["id_news"].'&amp;Titre='.optimiser_titre_et_classe(utf8_encode($data["titre"])).'#Commentaire'.$data["id_comm"].'">
							<li>
								'.utf8_encode(stripslashes($data["titre"]));
								if($pageDuMembre == 'true') {
									echo '
									<a href="'.ABSPATH.'Modifier_commentaire.php?id='.$data["id_comm"].'" title="Modifier le commentaire" style="float:right;">
										<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Modifier.png" />
									</a>
									<a href="'.ABSPATH.'Supprimer_commentaire.php?id='.$data["id_comm"].'" title="Supprimer le commentaire" style="float:right; margin-right:4px;">
										<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Supprimer.png" />
									</a>';
								}
							echo '</li>
						</a>';
					}
					$i++;
				}
			}
			else {
				echo '<li class="new">Ce membre n\'a encore pas posté de commentaire.</li>';
			}
			echo '</ul>';
		echo '</div>';
		
		$i = 0;
		$requete2->execute();
		
		echo '<div style="float:left">';
			echo '<h4 class="quatrieme_titre left">Sur le Forum</h4>';
			echo '<ul class="navigationPage left">';
			if($requete2->rowCount() > 0) {
				while($data2 = $requete2->fetch()) {
					if($i <= 4) {
						$nombreDeMessagesParPage = 15;
						$nbPost = $data2['topic_post']+2;
						$page = ceil($nbPost / $nombreDeMessagesParPage);
					
						echo '
						<a href="'.ABSPATH.'Forum/voirsujet.php?t='.$data2["topic_id"].'&amp;page='.$page.'#Post-'.$data2["post_id"].'">
							<li>
								'.stripslashes($data2["topic_titre"]);
								if($pageDuMembre == 'true') {
									echo '
									<a href="'.ABSPATH.'Forum/poster.php?p='.$data2["post_id"].'&amp;action=edit" title="Modifier le post" style="float:right;">
										<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Modifier.png" />
									</a>
									<a href="'.ABSPATH.'Forum/poster.php?p='.$data2["post_id"].'&amp;action=delete" title="Supprimer le post" style="float:right; margin-right:4px;">
										<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Supprimer.png" />
									</a>';
								}
							echo '</li>
						</a>';
					}
					$i++;
				}
			}
			else {
				echo '<li class="new">Ce membre n\'a encore pas réagit sur le forum.</li>';
			}
			echo '</ul>';
		echo '</div>';
		
		if($requete->rowCount() > 5 || $requete2->rowCount() > 5) {
			echo '<div id="afficherPlusResultats1"></div>';
			
			echo '<p align="center" class="afficherPlusParagraphe" style="width:760px;">';
				echo '
				<a class="afficherPlus allNews" href="#afficherPlusResultats1" onclick="afficherPlus(5, '.$requete->rowCount().', '.$_GET["id"].', \''.$pageDuMembre.'\');">
					<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/AvisWhite.png" alt="" /> Afficher 5 de plus
				</a>';
				
				echo '
				<a class="afficherPlusChargement allNews disabled" href="javascript:void(0);" style="display:none;">
					<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/AvisWhite.png" alt="" /> Chargement
				</a>';
			echo '</p>';
		}
		
		$requete->closeCursor();
		$requete2->closeCursor();
	}
	else {
		array_push($erreurs, "Ce membre n'existe pas.");
	}


	if(isset($erreurs) AND $erreurs != NULL) {
		echo '<div class="encadre_infos">';
		foreach($erreurs as $erreur_array) {
			echo '<p><img src="'.ABSPATH.'Images/erreur.png" alt="Erreur" align="top" /> '.$erreur_array.'</p>';
		}
		echo '</div>';
	}
	if(isset($valide)) {
		echo '<div class="encadre_infos"><p><img src="'.ABSPATH.'Images/tick.png" alt="Valide" align="top" /> '.$valide.'</p></div>';
	}
	elseif(isset($information)) {
		echo '<div class="encadre_infos"><p><img src="'.ABSPATH.'Images/information.png" alt="Information" align="top" /> '.$information.'</p></div>';
	}
}
else {
	echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Profil.png" alt="Profil" /> Profil</h2>';
	echo '<div class="encadre_infos"><p><img src="'.ABSPATH.'Images/erreur.png" alt="Erreur" align="top" /> Cette page n\'existe pas. <a href="Profil.php?id=1">Voir</a> le premier membre.</p></div>';
} ?>
	
<?php include("../Footer.php"); ?>