<?php
include('../Includes/Fonctions.php');
require("../Includes/Config.php");

if(isset($_POST["search"])) {
	$requete = $baseDeDonnes->prepare("SELECT * FROM membres WHERE membre_pseudo COLLATE utf8_unicode_ci LIKE :search ORDER BY membre_pseudo, membre_id ASC");
	$requete->bindValue(":search", "%".$_POST["search"]."%", PDO::PARAM_STR);
	$requete->execute();
	
	if($requete->rowCount() > 0) {
		while($data = $requete->fetch()) {
			$pseudo = stripslashes(utf8_encode(preg_replace("#(".$_POST["search"]."+)#i", "<b>$1</b>", $data["membre_pseudo"])));
			echo '
			<a href="javascript:void(0);" class="recherche-result">
				<li>
					<span class="fleche"></span>
					<span class="pseudo">'.$pseudo.'</span>
				</li>
			</a>';
		}
	}
	else {
		echo '<a href="javascript:void(0);""><li>Aucun résultat</li></a>';
	}
}
?>