var actualEquipementImage;
var actualEquipementNom;
var actualEquipementType;
var actualEquipementNiveau;
var actualEquipementId;
var actualEquipementCategorie;
var actualPersonnageId;
var deleterEquipement;

function equipement(id, categorie, deleter) {
	$('.click_ajouter_equipement, .deleteEquipement, .add-panoplie').live("click", function() {		
		actualEquipementCategorie = categorie;
		actualEquipementId = id;
		
		if($("#information_ajouter_equipement").is(":hidden")) {
			$("#information_ajouter_equipement").fadeIn(300);
			
			if($("#information_ajouter_equipement .contenu").html() == '') {
				if(!deleter) {
					deleterEquipement = false;
				}
				else {
					if(deleter != "panoplie") {
						deleterEquipement = true;
					}
					else {
						deleterEquipement = "panoplie";
					}
				}
				
				$("#information_ajouter_equipement .conteneur").css({"margin-left":"0"});
				$.ajax({
					url: host+"Equipements/AJAX_add_equipement.php",
					success:function(msg) {
						$("#information_ajouter_equipement .contenu").html(msg);
						
						//Si toutefois le membre n'a qu'un personnage, on ne va pas l'embrouiller avec une sélection de personnage
						if($('.choixPersonnage').length == 1) {
							$('.choixPersonnage').addClass('validate');
							
							actualPersonnageId = $('.choixPersonnage.validate').attr("rel");
							
							var pseudo = $('.choixPersonnage.validate').attr("rel-pseudo");
							var classe = $('.choixPersonnage.validate').attr("rel-classe");
							var niveau = $('.choixPersonnage.validate').attr("rel-niveau");
							var image = $('.choixPersonnage.validate').children("img").attr("src");
							
							$('.choixPersonnageInfos.validate').children('.image').html('<img src="'+image+'" alt="" />');
							$('.choixPersonnageInfos.validate').children('.pseudo').html(pseudo);
							$('.choixPersonnageInfos.validate').children('.little').html(classe+' de niveau '+niveau);
						}
						
						//Si l'on est en mode suppression
						if($(".deleteEquipement").length > 0) {							
							$('.choixPersonnage').each(function() {
								if($('ul.selectionPersonnage li.active').length == 1) {
									if($(this).attr("rel") == $('ul.selectionPersonnage li.active').attr("rel")) {
										$(this).addClass('validate');
									}
								}
							});
						}

						//Affichage pour ces deux cas
						if($('.choixPersonnage').length == 1 || $(".deleteEquipement").length > 0) {
							$("#information_ajouter_equipement .conteneur").css({"margin-left":"-500px"});
							
							$('span.changerPerso a').hide();
						}
					}
				});
			}
		}
	});
}

$(document).ready(function() {
	/*Choix pour les panoplies*/
	
	$('.add-panoplie').live("click", function() {
		$('input[type="submit"]#sendPersonnage.disabled').removeClass('disabled').removeAttr("disabled");
								
		actualEquipementImage = $('.equipementTable').children("tbody").children("tr:eq(2)").children(".screen").children("img").attr("src");
		actualEquipementNom = trim($('h2').text());
		actualEquipementNiveau = $('.encadreInfos span.right span').find("span").text();
		actualEquipementNiveau = trim(actualEquipementNiveau.replace(new RegExp("[^(0-9)]", "g"), ''));

		$('.choixPersonnageInfos.equipement').children('.image').html('<img src="'+actualEquipementImage+'" alt="" />');
		$('.choixPersonnageInfos.equipement').children('.pseudo').html(actualEquipementNom);
		$('.choixPersonnageInfos.equipement').children('.little').html('Panoplie de niveau '+actualEquipementNiveau);
		
		$('#info-equipement').text("La panoplie sera visible sur ce personnage :");
	});

	/*Choix du personnage pour les équipements*/
	
	//Récupération des données pour l'équipement choisi (Ajout)
	$('.click_ajouter_equipement').live("click", function() {
		$('input[type="submit"]#sendPersonnage.disabled').removeClass('disabled').removeAttr("disabled");
								
		actualEquipementImage = $(this).parent("th").parent(".equipementInformations").parent("tbody").children("tr:eq(2)").children(".screen").children("img").attr("src");
		actualEquipementNom = trim($(this).parent("th").text());
		actualEquipementNiveau = $(this).parent("th").parent(".equipementInformations").children("th:eq(0)").text();
		actualEquipementNiveau = trim(actualEquipementNiveau.replace(new RegExp("[^(0-9)]", "g"), ''));
		actualEquipementType = ucfirst($(this).parent("th").parent(".equipementInformations").parent("tbody").parent(".equipementTable").attr("rel"));
		if(actualEquipementType != "Faux" && actualEquipementType != "Dofus") {
			actualEquipementType = actualEquipementType.substring(0, actualEquipementType.length - 1);
		}
		actualEquipementId = $(this).parent("th").parent(".equipementInformations").parent("tbody").parent(".equipementTable").attr("id");
		actualEquipementId = trim(actualEquipementId.replace(new RegExp("[^(0-9)]", "gi"), ''));
		actualEquipementCategorie = trim($(this).parent("th").parent(".equipementInformations").parent("tbody").parent(".equipementTable").attr("rel"));

		$('.choixPersonnageInfos.equipement').children('.image').html('<img src="'+actualEquipementImage+'" alt="" />');
		$('.choixPersonnageInfos.equipement').children('.pseudo').html(actualEquipementNom);
		$('.choixPersonnageInfos.equipement').children('.little').html(actualEquipementType+' de niveau '+actualEquipementNiveau);
		
		$('#info-equipement').text("L\'équipement sera visible sur ce personnage :");
	});
		
		
	//Changements de personnage
	$('input[type="submit"]#sendChoixPersonnage').live("click", function() {
		$("#information_ajouter_equipement .conteneur").animate({"margin-left":"-500px"}, 400);
	});
	
	$('span.changerPerso a').live("click", function() {
		$("#information_ajouter_equipement .conteneur").animate({"margin-left":"0"}, 400);
	});
	
	
	//Envoi du formulaire par AJAX
	if(deleterEquipement == "panoplie") {
		$('input[type="submit"]#sendPersonnage').live("click", function() {
			if($(this).hasClass('disabled')) {
			}
			else {
				$(this).addClass('disabled').attr({disabled:"disabled"});
				
				$.ajax({
					type: "POST",
					url: host+"Equipements/AJAX_add_equipement.php",
					data: "id="+actualEquipementId+"&categorie="+actualEquipementCategorie+"&id_personnage="+actualPersonnageId+"&delete="+deleterEquipement,
					success:function(msg) {
						$('input[type="submit"]#sendPersonnage').addClass('disabled').attr({disabled:"disabled"});
						
						$('.choixPersonnageInfos.equipement').children('.image').html('<img src="'+host+'Images/Items/Unknown.png" alt="" />');
						$('.choixPersonnageInfos.equipement').children('.pseudo').html('Aucun équipement');
						$('.choixPersonnageInfos.equipement').children('.little').html('Sélectionnez un équipement');
						
						$('.notification').html(msg).fadeIn(300).delay(2000).fadeOut(200);
					}
				});
			}
		});
	}
	else if($(".deleteEquipement").length == 0) {
		$('input[type="submit"]#sendPersonnage').live("click", function() {
			if($(this).hasClass('disabled')) {
			}
			else {
				$(this).addClass('disabled').attr({disabled:"disabled"});
				
				$.ajax({
					type: "POST",
					url: host+"Equipements/AJAX_add_equipement.php",
					data: "id="+actualEquipementId+"&categorie="+actualEquipementCategorie+"&id_personnage="+actualPersonnageId+"&delete="+deleterEquipement,
					success:function(msg) {
						$('input[type="submit"]#sendPersonnage').addClass('disabled').attr({disabled:"disabled"});
						
						$('.choixPersonnageInfos.equipement').children('.image').html('<img src="'+host+'Images/Items/Unknown.png" alt="" />');
						$('.choixPersonnageInfos.equipement').children('.pseudo').html('Aucun équipement');
						$('.choixPersonnageInfos.equipement').children('.little').html('Sélectionnez un équipement');
						
						$('.notification').html(msg).fadeIn(300).delay(2000).fadeOut(200);
					}
				});
			}
		});
	}
	else {
		$('input[type="submit"]#sendPersonnage').live("click", function() {
			if($(this).hasClass('disabled')) {
			}
			else {
				$(this).addClass('disabled').attr({disabled:"disabled"});
								
				$.ajax({
					type: "POST",
					url: host+"Equipements/AJAX_add_equipement.php",
					data: "id="+actualEquipementId+"&categorie="+actualEquipementCategorie+"&id_personnage="+actualPersonnageId+"&delete="+deleterEquipement,
					success:function(msg) {
						$('input[type="submit"]#sendPersonnage').addClass('disabled').attr({disabled:"disabled"});
						
						$('.choixPersonnageInfos.equipement').children('.image').html('<img src="'+host+'Images/Items/Unknown.png" alt="" />');
						$('.choixPersonnageInfos.equipement').children('.pseudo').html('Aucun équipement');
						$('.choixPersonnageInfos.equipement').children('.little').html('Sélectionnez un équipement');
						
						$('.notification').html(msg).fadeIn(300).delay(2000).fadeOut(200);
					}
				});
			}
		});
	}

	//Choix du personnage
	$('.choixPersonnage').live("click", function() {
		if($(this).hasClass('active')) {
			$('.choixPersonnage').removeClass('active');
			$('.choixPersonnage').children('.img').remove();
			$('input[type="submit"]#sendChoixPersonnage').attr({disabled:'disabled'}).addClass('disabled');
			actualPersonnageId = NULL;
		}
		else {
			$('.choixPersonnage').removeClass('active');
			$('.choixPersonnage').children('.img').remove();
			$(this).addClass('active');
			$(this).prepend('<div class="img"><img src="'+host+'Images/icones_pages/LittlesIcones/Tick.png" alt="" /></div>');
			
			actualPersonnageId = $(this).attr("rel");
			
			$('.contenu input[type="submit"].disabled').removeAttr('disabled').removeClass('disabled').attr({id:'sendChoixPersonnage'});
			
			var pseudo = $(this).attr("rel-pseudo");
			var classe = $(this).attr("rel-classe");
			var niveau = $(this).attr("rel-niveau");
			var image = $(this).children("img").attr("src");
			
			$('.choixPersonnageInfos.validate').children('.image').html('<img src="'+image+'" alt="" />');
			$('.choixPersonnageInfos.validate').children('.pseudo').html(pseudo);
			$('.choixPersonnageInfos.validate').children('.little').html(classe+' de niveau '+niveau);
		}
	});
	
	//Informations du personnage au survol
	$('.choixPersonnage').live("mouseover", function() {
		var pseudo = $(this).attr("rel-pseudo");
		var classe = $(this).attr("rel-classe");
		var niveau = $(this).attr("rel-niveau");
		var image = $(this).children("img").attr("src");
		
		$('.choixPersonnageInfos').not('.validate').not('.equipement').children('.image').html('<img src="'+image+'" alt="" />');
		$('.choixPersonnageInfos').not('.validate').not('.equipement').children('.pseudo').html(pseudo);
		$('.choixPersonnageInfos').not('.validate').not('.equipement').children('.little').html(classe+" de niveau "+niveau);
	});

	
	/*Suppression d'un équipement*/			
	$(".deleteEquipement").live("click", function() {	
		$('input[type="submit"]#sendPersonnage.disabled').removeClass('disabled').removeAttr("disabled");
		
		$("#information_ajouter_equipement .conteneur").css({"margin-left":"-500px"});
		
		actualEquipementImage = $(this).parent(".equipementMembre").children("a").children("img").attr("src");
		actualEquipementNom = trim($(this).parent(".equipementMembre").attr("rel"));
		actualEquipementNiveau = $(this).parent(".equipementMembre").attr("rel-niveau");
		actualEquipementType = ucfirst($(this).parent(".equipementMembre").attr("rel-type"));
		if(actualEquipementType != "Faux" && actualEquipementType != "Dofus") {
			actualEquipementType = actualEquipementType.substring(0, actualEquipementType.length - 1);
		}
		actualEquipementId = $(this).parent(".equipementMembre").attr("id");
		actualEquipementId = trim(actualEquipementId.replace(new RegExp("[^(0-9)]", "gi"), ''));
		actualEquipementCategorie = trim($(this).parent(".equipementMembre").attr("rel-type"));

		$('.choixPersonnageInfos.equipement').children('.image').html('<img src="'+actualEquipementImage+'" alt="" />');
		$('.choixPersonnageInfos.equipement').children('.pseudo').html(actualEquipementNom);
		$('.choixPersonnageInfos.equipement').children('.little').html(actualEquipementType+' de niveau '+actualEquipementNiveau);
		
		$('#info-equipement').text("L\'équipement sera supprimé de ce personnage :");
		
		//Update du personnage
		var image = $('ul.selectionPersonnage a.deactivate').attr("rel-image");
		var pseudo = $('ul.selectionPersonnage a.deactivate').attr("rel-pseudo");
		var classe = $('ul.selectionPersonnage a.deactivate').attr("rel-classe");
		var niveau = $('ul.selectionPersonnage a.deactivate').attr("rel-niveau");
		
		actualPersonnageId = $('ul.selectionPersonnage a.deactivate').attr("rel");
		
		$('.choixPersonnageInfos.validate').children('.image').html('<img src="'+image+'" alt="" />');
		$('.choixPersonnageInfos.validate').children('.pseudo').html(pseudo);
		$('.choixPersonnageInfos.validate').children('.little').html(classe+' de niveau '+niveau);
	});
	
	
	//Si l'on change de personnage
	$('ul.selectionPersonnage a').not('.on').not('.active').not('.deactivate').click(function() {			
		$('.choixPersonnageInfos.validate').children('.image').html('<img src="'+image+'" alt="" />');
		$('.choixPersonnageInfos.validate').children('.pseudo').html(pseudo);
		$('.choixPersonnageInfos.validate').children('.little').html(classe+' de niveau '+niveau);

		//Vidage de l'équipement
		$('.choixPersonnageInfos.equipement').children('.image').html('<img src="'+host+'Images/Items/Unknown.png" alt="" />');
		$('.choixPersonnageInfos.equipement').children('.pseudo').html('Aucun équipement');
		$('.choixPersonnageInfos.equipement').children('.little').html('Sélectionnez un équipement');
		
		$('input[type="submit"]#sendPersonnage').addClass('disabled').attr({disabled:"disabled"});
	});
});