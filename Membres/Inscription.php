<?php 
$titre_page_nom = "Inscription";
include("../Header.php");

$erreurs = array();

if(!isset($PSEUDO_MEMBRE)) {
	if(isset($_POST["submit"])) {
		if(isset($_POST["pseudo"]) AND isset($_POST["password"]) AND isset($_POST["password_confirmation"]) AND isset($_POST["email"])) {
			if(!empty($_POST["pseudo"]) AND !empty($_POST["password"]) AND !empty($_POST["password_confirmation"]) AND !empty($_POST["email"])) {
				$sql = "SELECT * FROM membres WHERE membre_pseudo = '".$_POST["pseudo"]."'";
				$req = query($sql);
				if(mysql_num_rows($req) == 0) {
					if(strlen($_POST["pseudo"]) > 30) {
						array_push($erreurs, "Le pseudo fait plus de 30 caractères.");
					}
					elseif($_POST["password"] == $_POST["password_confirmation"] AND is_email($_POST["email"])) {
						extract($_POST);
							$pseudo = mysql_real_escape_string($_POST["pseudo"]);
							$password = mysql_real_escape_string(sha1($_POST["password"]));
							$email = mysql_real_escape_string($_POST["email"]);
							
						//Envoi de l'Email
						$emailTo = $email;
						$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
								<html xmlns="http://www.w3.org/1999/xhtml">
								<head>
								<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
								<title>Inscription validée !</title>
								<style type="text/css">
									body {
										font-family:"Trebuchet MS";
									}
								</style>
								Merci de votre inscription. Vous pouvez dès à présent vous connecter sur le site et enregistrer vos informations.
								<div style="margin:10px 0;"> <b style="background:#DDD;padding:4px;display:block;width:100%;">Votre Pseudo :         </b> '.$pseudo.'                                  </div>
								<div style="margin:10px 0;"> <b style="background:#DDD;padding:4px;display:block;width:100%;">Votre Mot de Passe :   </b> Le Mot de Passe choisi lors de l\'inscription</div>
								<div style="margin:10px 0;"> <b style="background:#DDD;padding:4px;display:block;width:100%;">Votre Email :          </b> '.$email.'                                   </div>
								Pour tous changements, vous devez vous rendre sur la gestion de votre compte.<br />
								A très vite sur le site !';
						$headers = 'Mime-Version: 1.0'."\r\n";
						$headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
						$headers .= "\r\n";

						mail($emailTo, "Inscription validée !", stripslashes($body), $headers);
						
						//Requête
						$inscription = date('YmdHis');
						$sql = "INSERT INTO membres(membre_pseudo, membre_mdp, membre_mail, membre_inscription, membre_niveau) VALUES ('".$pseudo."', '".$password."', '".$email."',".$inscription.", 2)";
						query($sql);
						
						$valide = 'Votre inscription est validée. Un email vous a été envoyé avec vos informations. <a href="Connexion.php">Connexion</a>.';
					}
					if($_POST["password"] != $_POST["password_confirmation"]) {
						array_push($erreurs, "Le Mot de Passe ne correspond pas.");
					}
					if(is_email($_POST["email"]) == false) {
						array_push($erreurs, "L'Email est incorrect.");
					}
				}
				else {
					array_push($erreurs, "Un membre possède déjà ce pseudo.");
				}
			}
			elseif(empty($_POST["pseudo"]) OR empty($_POST["password"]) OR empty($_POST["password_confirmation"]) OR empty($_POST["email"])) {
				array_push($erreurs, "Un ou plusieurs des champs n'ont pas été remplis.");
			}
		}
		else {
			array_push($erreurs, "Un ou plusieurs des champs n'ont pas été remplis.");
		}
	}
}
else {
	$information = "Vous êtes déjà inscrit.";
}
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Inscription.png" alt="Inscription" /> Inscription</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH ?>">Accueil</a> > <a href="./">Membres</a> > Inscription</p>

<?php
if(isset($erreurs) AND $erreurs != NULL) {
	echo '<p class="encadreInfos">';
		echo '<span class="left erreur block"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreurs</span>';
		foreach($erreurs as $erreur_array) {
			echo '<span class="right">';
				echo $erreur_array;
			echo '</span>';
		}
	echo '</p>';
}
if(isset($valide)) {
	echo '
	<p class="encadreInfos">
		<span class="left valide"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Valide.png" alt="Confirmation" /> Confirmation</span>
		<span class="right">'.$valide.'</span>
	</p>';
}
if(isset($information)) {
	echo '
	<p class="encadreInfos">
		<span class="left information"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Information.png" alt="Information" /> Information</span>
		<span class="right">'.$information.'</span>
	</p>';
}

if(!isset($valide) AND !isset($PSEUDO_MEMBRE)) {
	echo '<p class="encadreInfos">';
		echo '<span class="right">Vous voulez rejoindre la petite communauté du site ? C\'est par ici, et c\'est rapide !<br />Déjà inscrit ? <a href="Connexion.php">Connectez</a> vous.</span>';
	echo '</p>';
	?>

	<div class="bloc">
		<div class="miniBloc right">N'utilisez pas votre compte Ankama pour l'inscription.</div>
		<form action method="post">
			<p><label for="pseudo">Pseudo : </label><input type="text" id="pseudo" name="pseudo" /></p>
			<p><label for="password">Mot de Passe : </label><input type="password" id="password" name="password" /></p>
			<p><label for="password_confirmation">Confirmation du MdP : </label><input type="password" id="password_confirmation" name="password_confirmation" /></p>
			<p><label for="email">Votre Email : </label><input type="text" id="email" name="email" /></p>
			<p align="center"><input type="submit" name="submit" value="Confirmer" /></p>
		</form>
	</div>

<?php }

include("../Footer.php"); ?>