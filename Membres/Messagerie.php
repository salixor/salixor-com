<?php
if(isset($_GET["Action"])) {
	switch($_GET["Action"]) {
		case "Voir":
			$titre_page_nom = "Visionner un message";
		break;
		case "Envoyer":
			if(isset($_GET["Repondre"])) {
				$titre_page_nom = "Répondre à un message";
			}
			else {
				$titre_page_nom = "Envoyer un message";
			}
		break;
		case "Supprimer":
			$titre_page_nom = "Supprimer un message";
		break;
		case "Envoyes":
			$titre_page_nom = "Boîte d'envoi";
		break;
	}
}
else {
	$titre_page_nom = "Ma Messagerie";
}

include("../Header.php");
 

$erreurs = array();

if(!isset($ID_MEMBRE)) {
	array_push($erreurs, "Vous n'êtes pas connecté.");
}
elseif(isset($_GET["Action"])) {
	$Action = $_GET["Action"];
	
	switch($Action) {
		case "Voir":			
			if(isset($_GET["Message"])) {
				if(!empty($_GET["Message"])) {
					$requete = $baseDeDonnes->prepare("
					SELECT messagerie.*
					FROM messagerie
					WHERE messagerie.id = :message");
					$requete->bindValue(":message", $_GET["Message"], PDO::PARAM_INT);
					$requete->execute();
										
					$data = $requete->fetch();
					$sujet = $data["sujet"];
					
					if($requete->rowCount() == 0) {
						echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Messagerie.png" alt="Messagerie" /> Ma Messagerie</h2>';
				
						echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Messagerie.php">Ma Messagerie</a> > Voir un Message</p>';

						echo '<p class="encadreInfos"><span class="right">Voici votre messagerie privée, sur laquelle vous pouvez envoyer et recevoir des messages.</span></p>';
							
						array_push($erreurs, 'Ce message n\'existe pas.');
					}
					elseif($ID_MEMBRE != $data["destinataire"] AND $ID_MEMBRE != $data["expediteur"]) {
						echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Messagerie.png" alt="Messagerie" /> Ma Messagerie</h2>';
				
						echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Messagerie.php">Ma Messagerie</a> > Voir un Message</p>';

						echo '<p class="encadreInfos"><span class="right">Voici votre messagerie privée, sur laquelle vous pouvez envoyer et recevoir des messages.</span></p>';
							
						array_push($erreurs, 'Ce message ne vous concerne pas.');
					}
					else {
						if($ID_MEMBRE == $data["destinataire"]) {
							$requete = $baseDeDonnes->prepare("UPDATE messagerie SET vu = '1' WHERE id = :message");
							$requete->bindValue(":message", $_GET["Message"], PDO::PARAM_INT);
							$requete->execute();
						}
						
						$requete = $baseDeDonnes->prepare("
						SELECT messagerie.*, membres.*, membres_titres.*, messagerie.id AS message_id
						FROM messagerie
						INNER JOIN membres ON messagerie.expediteur = membres.membre_id
						LEFT JOIN membres_titres ON membres_titres.id = membres.membre_titre_id
						WHERE messagerie.sujet = :sujet
						ORDER BY messagerie.id DESC");
						$requete->bindValue(":sujet", $sujet, PDO::PARAM_INT);
						$requete->execute();

						echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Messagerie.png" alt="Messagerie" /> Ma Messagerie</h2>';
				
						echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Messagerie.php">Ma Messagerie</a> > Discussion : '.$sujet.'</p>';

						echo '<p class="encadreInfos"><span class="right">Voici votre messagerie privée, sur laquelle vous pouvez envoyer et recevoir des messages.</span></p>';
							
						while($data = $requete->fetch()) {
							$sujet = $data["sujet"];
							$pseudoExpediteur = $data["membre_pseudo"];
							$message = smileys(formatage(stripslashes(nl2br($data["message"]))));
							$rang = $MEMBRES_RANGS[$NIVEAU_MEMBRE];
							
							echo '<div class="postForum">';
							
								echo '<div class="head">';
									$image = '';
									if(!empty($data["membre_pseudo"])) {
										if($data["membre_connecte"] == 1) {
											$image = '<img src="'.ABSPATH.'Images/Forum/BulletGreen.png" alt="" />';
										}
										else {
											$image = '<img src="'.ABSPATH.'Images/Forum/BulletRed.png" alt="" />';
										}
										echo '<a href="'.ABSPATH.'Membres/Profil.php?id='.$data['membre_id'].'" class="left">'.$image.stripslashes(utf8_encode(htmlspecialchars($data['membre_pseudo']))).'</a>';
									}
									else {
										echo '<span class="left">'.stripslashes(utf8_encode(htmlspecialchars($data['pseudo']))).'</span>';
									}
									
									echo '<span class="right"><img src="'.ABSPATH.'Images/Forum/Time.png" alt="" style="margin:3px 0 -3px;" /> Écrit le '.convertirTimestamp($data['date']).' à '.date('H\hi', $data['date']).'</span>';
									
									echo '<div style="clear:left;"></div>';
								echo '</div>';
								
								echo '<div class="side">';
									if(!empty($data["membre_pseudo"])) {
										if(!empty($data["membre_avatar"])) {
											echo '<div class="avatar-forum"><img src="../Images/Avatars/'.$data["membre_avatar"].'" width="100" height="100" alt="" /></div>';
										}
										else {
											echo '<div class="avatar-forum"><img src="../Images/Avatars/avatar_normal.png" width="100" height="100" alt="" /></div>';
										}
									}
									else {
										echo '<div class="avatar-forum"><img src="Images/Avatars/not_membre.png" width="100" height="100" alt="" /></div>';
									}
									
									if(!empty($data["membre_pseudo"])) {
										echo '<div class="contenu-centre contenu-gras">'.$rang.'</div>';   
										echo '<div class="contenu-centre bleu">&laquo;'.stripslashes(htmlspecialchars(utf8_encode($data['titre']))).'&raquo;</div>';   
									}
									else {
										echo '<div class="contenu-centre contenu-gras">Visiteur</div>';   
									}
								echo '</div>';		
									   
								echo '<div class="contenu">';
									echo $message;
									
									if(!empty($data['membre_signature'])) {
										echo "<hr />";
										echo smileys(nl2br(stripslashes(formatage($data['membre_signature']))));
									}

									echo '<div style="clear:left;"></div>';
								echo '</div>';
								
								echo '<div class="foot">';
									echo '<a href="?Action=Supprimer&amp;Message='.$data["message_id"].'" class="lien"><img src="'.ABSPATH.'Images/Forum/Supprimer.png" alt="" /> Supprimer</a>';
									if($ID_MEMBRE == $data["destinataire"]) {
										echo '<a href="?Action=Envoyer&amp;Repondre='.$data["message_id"].'" class="lien"><img src="'.ABSPATH.'Images/Reponse.png" alt="" /> Répondre</a>';
									}
									echo '<a class="lien left">-</a>';
									echo '<div style="clear:left;"></div>';
								echo '</div>';
								
							echo '</div>';
						}
					}
				}
				else {
					echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Messagerie.png" alt="Messagerie" /> Ma Messagerie</h2>';
			
					echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Messagerie.php">Ma Messagerie</a> > Voir un Message</p>';

					echo '<p class="encadreInfos"><span class="right">Voici votre messagerie privée, sur laquelle vous pouvez envoyer et recevoir des messages.</span></p>';
							
					array_push($erreurs, 'Aucun message est défini.');
				}
			}
			else {
				echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Messagerie.png" alt="Messagerie" /> Ma Messagerie</h2>';
		
				echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Messagerie.php">Ma Messagerie</a> > Voir un Message</p>';

				echo '<p class="encadreInfos"><span class="right">Voici votre messagerie privée, sur laquelle vous pouvez envoyer et recevoir des messages.</span></p>';
							
				array_push($erreurs, 'Un paramètre est manquant.');
			}
		break;
		
		case "Envoyer":
			if(isset($_POST["submit"])) {
				if(!empty($_POST['sujet']) AND !empty($_POST['pseudoDestinataire']) AND !empty($_POST['message'])) {
					$requete = $baseDeDonnes->prepare("
					SELECT COUNT(*) AS nb_entrees FROM membres
					WHERE membre_pseudo = :pseudo");
					$requete->bindValue(":pseudo", $_POST['pseudoDestinataire'], PDO::PARAM_STR);
					$requete->execute();
					
					$data = $requete->fetch();
					
					if($data['nb_entrees'] == 1) {
						$requete = $baseDeDonnes->prepare("
						SELECT membre_id FROM membres
						WHERE membre_pseudo = :pseudo");
						$requete->bindValue(":pseudo", $_POST['pseudoDestinataire'], PDO::PARAM_STR);
						$requete->execute();		
					
						$data = $requete->fetch();
						$destinataire = $data['membre_id'];
						
						$sujet = $_POST['sujet'];
						$message = $_POST['message'];
						$expediteur = $ID_MEMBRE;
						$date = time();
						
						$requete = $baseDeDonnes->prepare("
						SELECT destinataire, sujet, message FROM messagerie
						WHERE expediteur = :expediteur
						ORDER BY id DESC LIMIT 0, 1");
						$requete->bindValue(":expediteur", $expediteur, PDO::PARAM_INT);
						$requete->execute();	
						
						$data = $requete->fetch();
						
						if($data['destinataire'] == $destinataire AND $data['sujet'] == $sujet AND $data['message'] == $message) {
							echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Messagerie.png" alt="Messagerie" /> Ma Messagerie</h2>';
					
							echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Messagerie.php">Ma Messagerie</a> > Nouveau Message</p>';

							echo '<p class="encadreInfos"><span class="right">Voici votre messagerie privée, sur laquelle vous pouvez envoyer et recevoir des messages.</span></p>';
										
							array_push($erreurs, "Votre dernier message est le même que celui que vous venez d'envoyer. Il n'est pas pris en compte.");
						}
						else {
							echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Messagerie.png" alt="Messagerie" /> Ma Messagerie</h2>';
					
							echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Messagerie.php">Ma Messagerie</a> > Nouveau Message</p>';

							echo '<p class="encadreInfos"><span class="right">Voici votre messagerie privée, sur laquelle vous pouvez envoyer et recevoir des messages.</span></p>';
									
							$valide = 'Votre message a bien été envoyé à '.$_POST['pseudoDestinataire'].'.';
							
							$requete = $baseDeDonnes->prepare("
							INSERT INTO messagerie(sujet, expediteur, destinataire, message, date, vu)
							VALUES(:sujet, :expediteur, :destinataire, :message, :date, '0')");
							$requete->bindValue(":sujet", $sujet, PDO::PARAM_STR);
							$requete->bindValue(":expediteur", $expediteur, PDO::PARAM_INT);
							$requete->bindValue(":destinataire", $destinataire, PDO::PARAM_INT);
							$requete->bindValue(":message", $message, PDO::PARAM_STR);
							$requete->bindValue(":date", $date, PDO::PARAM_INT);
							$requete->execute();	
						}
					}
					else {
						array_push($erreurs, 'Le destinataire est introuvable.');
					}
				}
				else {
					array_push($erreurs, 'Un ou plusieurs des champs sont vides.');
				}
			}
			if(isset($_GET["Repondre"]) AND !isset($valide)) {
				if(!empty($_GET["Repondre"])) {
					$requete = $baseDeDonnes->prepare("
					SELECT sujet, expediteur, destinataire, membre_pseudo FROM messagerie
					INNER JOIN membres ON membres.membre_id = messagerie.expediteur
					WHERE id = :repondre");
					$requete->bindValue(":repondre", $_GET["Repondre"], PDO::PARAM_INT);
					$requete->execute();
					
					$data = $requete->fetch();
					
					if($requete->rowCount() == 0) {
						echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Messagerie.png" alt="Messagerie" /> Ma Messagerie</h2>';
				
						echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Messagerie.php">Ma Messagerie</a> > Répondre à un Message</p>';

						echo '<p class="encadreInfos"><span class="right">Voici votre messagerie privée, sur laquelle vous pouvez envoyer et recevoir des messages.</span></p>';
									
						array_push($erreurs, 'Ce message n\'existe pas.');
					}
					elseif($data["destinataire"] != $ID_MEMBRE) {
						echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Messagerie.png" alt="Messagerie" /> Ma Messagerie</h2>';
				
						echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Messagerie.php">Ma Messagerie</a> > Répondre à un Message</p>';

						echo '<p class="encadreInfos"><span class="right">Voici votre messagerie privée, sur laquelle vous pouvez envoyer et recevoir des messages.</span></p>';
									
						array_push($erreurs, 'Ce message ne vous concerne pas.');
					}
					else {
						echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Messagerie.png" alt="Messagerie" /> Ma Messagerie</h2>';
				
						echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Messagerie.php">Ma Messagerie</a> > Répondre à '.$data["membre_pseudo"].'</p>';

						echo '<p class="encadreInfos"><span class="right">Voici votre messagerie privée, sur laquelle vous pouvez envoyer et recevoir des messages.</span></p>';
							
						echo '
						<ul class="navigationPage left">
							<a href="Messagerie.php"><li><img src="../Images/Membres/MPEnvoyes.png" alt="" /> Messages Envoyés</li></a>
						</ul>';
						echo '
						<ul class="navigationPage right">
							<a href="Messagerie.php"><li><img src="../Images/Membres/MPEnvoyes.png" alt="" /> Messages Reçus</li></a>
						</ul>';
						echo '<div style="clear:left;"></div>';
							
						echo '
						<div class="bloc">
							<form action method="post">
								<p><label for="sujet">Sujet : </label><input type="text" name="sujet" id="sujet" style="width:240px;" value="'.$data["sujet"].'" /></p>
								<p>
									<label for="pseudoDestinataire">Destinataire : </label><input name="pseudoDestinataire" id="pseudoDestinataire" value="'.$data["membre_pseudo"].'" type="text" style="width:240px; position:relative; z-index:1001;" />
								</p>
								<p>';
								
									afficher_smileys('message');
									$FormatagePanel = new FormatagePanel(true, true, true, 'message');
									$FormatagePanel->showFormatagePanel();

									echo '<textarea name="message" id="message" style="height:150px;"></textarea>
								</p>
								
								<p align="center">
									<input type="submit" name="submit" value="Envoyer" />
									<input type="button" id="previsualiser_bouton" value="Prévisualiser" onclick="previsualiser(\'message\',\'previsualiserMessage\',\'../AJAX_news.php\');" />
								</p>
							</form>
						</div>
						
						<p align="center" id="previsualiser_zone">
							<div class="preview" id="previsualiserMessage" style="display:none;"></div>
						</p>
						';
					}
				}
				else {
					echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Messagerie.png" alt="Messagerie" /> Ma Messagerie</h2>';
			
					echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Messagerie.php">Ma Messagerie</a> > Répondre à un Message</p>';

					echo '<p class="encadreInfos"><span class="right">Voici votre messagerie privée, sur laquelle vous pouvez envoyer et recevoir des messages.</span></p>';
								
					array_push($erreurs, 'Aucun message est défini.');
				}
			}
			elseif(!isset($valide)) {
				$pseudo = '';
				$pseudoEnd = '';
				if(isset($_GET["Pseudo"])) {
					$pseudo = $_GET["Pseudo"];
					$pseudoEnd = ' à '.$pseudo;
				}
				
				echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Messagerie.png" alt="Messagerie" /> Ma Messagerie</h2>';
		
				echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Messagerie.php">Ma Messagerie</a> > Nouveau Message'.$pseudoEnd.'</p>';

				echo '<p class="encadreInfos"><span class="right">Voici votre messagerie privée, sur laquelle vous pouvez envoyer et recevoir des messages.</span></p>';
					
				echo '
				<ul class="navigationPage left">
					<a href="Messagerie.php"><li><img src="../Images/Membres/MPEnvoyes.png" alt="" /> Messages Envoyés</li></a>
				</ul>';
				echo '
				<ul class="navigationPage right">
					<a href="Messagerie.php"><li><img src="../Images/Membres/MPEnvoyes.png" alt="" /> Messages Reçus</li></a>
				</ul>';
				echo '<div style="clear:left;"></div>';
					
				echo '
				<div class="bloc">
					<form action method="post">
						<p><label for="sujet">Sujet : </label><input type="text" name="sujet" id="sujet" style="width:240px;" /></p>
						<p>
							<label for="pseudoDestinataire">Destinataire : </label><input name="pseudoDestinataire" id="pseudoDestinataire" type="text" style="width:240px; position:relative; z-index:1001;" />';
							if($pseudo == '') {
								echo '<span class="delete_value_champ" style="position:absolute; z-index:1002;">×</span>';
							}
							echo '
							<ul class="selectionPersonnage maximum" id="resultas" style="width:256px; box-shadow:0 0 10px #BBB; margin:-10px 0 0 191px; position:absolute; z-index:1000;"></ul>
						</p>
						<p>';
						
							afficher_smileys('message');
							$FormatagePanel = new FormatagePanel(true, true, true, 'message');
							$FormatagePanel->showFormatagePanel();

							echo '<textarea name="message" id="message" style="height:150px;"></textarea>
						</p>
						
						<p align="center">
							<input type="submit" name="submit" value="Envoyer" />
						<input type="button" id="previsualiser_bouton" value="Prévisualiser" onclick="previsualiser(\'message\',\'previsualiserMessage\',\'../AJAX_news.php\');" />
						</p>
					</form>
				</div>
				
				<p align="center" id="previsualiser_zone">
					<div class="preview" id="previsualiserMessage" style="display:none;"></div>
				</p>
				';
			}
		break;
		
		case "Supprimer":
			if(isset($_GET["Message"])) {	
				echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Messagerie.png" alt="Messagerie" /> Ma Messagerie</h2>';
		
				echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Messagerie.php">Ma Messagerie</a> > Suppression d\'un Message</p>';

				echo '<p class="encadreInfos"><span class="right">Voici votre messagerie privée, sur laquelle vous pouvez envoyer et recevoir des messages.</span></p>';
				
				if(!empty($_GET["Message"])) {
					$requete = $baseDeDonnes->prepare("SELECT expediteur, destinataire FROM messagerie WHERE id = :message");
					$requete->bindValue(":message", $_GET["Message"], PDO::PARAM_INT);
					$requete->execute();
					
					$data = $requete->fetch();
					
					if($requete->rowCount() == 0) {
						array_push($erreurs, 'Ce message n\'existe pas.');
					}
					elseif($ID_MEMBRE != $data["destinataire"] AND $ID_MEMBRE != $data["expediteur"]) {
						array_push($erreurs, 'Ce message ne vous concerne pas.');
					}
					else {
						if(isset($_GET["Suppression"])) {
							$requete = $baseDeDonnes->prepare("DELETE FROM messagerie WHERE id = :message");
							$requete->bindValue(":message", $_GET["Message"], PDO::PARAM_INT);
							$requete->execute();

							$valide = 'Votre message est bien supprimé.';
						}
						else {
							echo '
							<p class="ForumSend">
								<span class="titreSend"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png"> Attention</span>
								<span class="contenuSend">
									<span id="a">En supprimant ce message, vous ne pourrez pas revenir en arrière. Etes vous sûr ?</span>
									<span id="b" align="center"><a href="?Action=Supprimer&amp;Message='.$_GET["Message"].'&amp;Suppression" class="choice">Oui</a><a href="Messagerie.php" class="choice">Non</a></span>
								</span>
							</p>';
						}
					}
				}
				else {
					array_push($erreurs, 'Aucun message est défini.');
				}
			}
			else {
				echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Messagerie.png" alt="Messagerie" /> Ma Messagerie</h2>';
		
				echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Messagerie.php">Ma Messagerie</a> > Suppression d\'un Message</p>';

				echo '<p class="encadreInfos"><span class="right">Voici votre messagerie privée, sur laquelle vous pouvez envoyer et recevoir des messages.</span></p>';
				
				array_push($erreurs, 'Un paramètre est manquant.');
			}
		break;
				
		case "Envoyes":
			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Messagerie.png" alt="Messagerie" /> Ma Messagerie</h2>';
	
			echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Messagerie.php">Ma Messagerie</a> > Boîte d\'Envoi</p>';

			echo '<p class="encadreInfos"><span class="right">Voici votre messagerie privée, sur laquelle vous pouvez envoyer et recevoir des messages.</span></p>';
			
			echo '
			<ul class="navigationPage left">
				<a href="Messagerie.php"><li><img src="../Images/Membres/MPEnvoyes.png" alt="" /> Messages Reçus</li></a>
			</ul>';
			echo '
			<ul class="navigationPage right">
				<a href="?Action=Envoyer"><li><img src="../Images/Membres/SendMP.png" alt="" /> Nouveau Message</li></a>
			</ul>';
			echo '<div style="clear:left;"></div>';
			
			$requete = $baseDeDonnes->prepare("
			SELECT messagerie.*, membres.*
			FROM messagerie
			INNER JOIN membres
			ON messagerie.expediteur = membres.membre_id
			WHERE messagerie.expediteur = :id
			ORDER BY id DESC");
			$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
			$requete->execute();
			
			if($requete->rowCount() > 0) { ?>
				<table>
					<tr>
						<th width="50">Lu</th>
						<th width="305">Sujet</th>
						<th width="300">Date</th>
						<th width="100">Supprimer</th>
					</tr>
					<?php
					while($data = $requete->fetch()) {
						$sujet = stripslashes($data['sujet']);
						$expediteur = stripslashes($data['membre_pseudo']);
						$date = $data['date'];
							
						if($data['vu'] == 0) {
							$vu = '<b>Non lu</b>';
						}
						else {
							$vu = 'Lu';
						}
						echo '
						<tr>
							<td align="center">'.$vu.'</td>
							<td><a href="?Action=Voir&amp;Message='.$data['id'].'">'.$sujet.'</a></td>
							<td>Le '.convertirTimestamp($date).' à '.date('H\hi', $date).'</td>
							<td align="center"><a href="?Action=Supprimer&amp;Suppr=1&amp;Message='.$data['id'].'">Supprimer</a>
							</td>
						</tr>';
					}   
				echo '</table>';
			}
			else {
				echo '<p class="encadreInfos"><span class="left">Remplissage</span><span class="right">Votre boîte d\'envoi est vide.</span></p>';
			}

		break;
	}
}
else {
	echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Messagerie.png" alt="Messagerie" /> Ma Messagerie</h2>';
	
	echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > Ma Messagerie</p>';

	echo '<p class="encadreInfos"><span class="right">Voici votre messagerie privée, sur laquelle vous pouvez envoyer et recevoir des messages.</span></p>';
	
	echo '
	<ul class="navigationPage left">
		<a href="?Action=Envoyes"><li><img src="../Images/Membres/MPEnvoyes.png" alt="" /> Messages Envoyés</li></a>
	</ul>';
	echo '
	<ul class="navigationPage right">
		<a href="?Action=Envoyer"><li><img src="../Images/Membres/SendMP.png" alt="" /> Nouveau Message</li></a>
	</ul>';
	echo '<div style="clear:left;"></div>';
	
	$requete = $baseDeDonnes->prepare("SELECT COUNT(*) AS nb_messages FROM messagerie WHERE destinataire = :id");
	$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
	$requete->execute();
	
	$data = $requete->fetch();
	
	$nbMessages = $data["nb_messages"];
	$pourcentage = $nbMessages*100/50;
	
	if($nbMessages > 50) {
		$supp = $nbMessages - 50;
		$sql2 = "DELETE FROM messagerie ORDER BY id ASC LIMIT ".$supp;
		$req2 = query($sql2);
		
		$nbMessages = 50;
		$pourcentage = 100;
	}
	
	$requete = $baseDeDonnes->prepare("SELECT COUNT(*) AS nb_messages_non_lus FROM messagerie WHERE destinataire = :id AND vu = '0'");
	$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
	$requete->execute();
	
	$data = $requete->fetch();
	
	if($data["nb_messages_non_lus"] > 0) {
		$nbNonLus = 'Vous avez '.$data["nb_messages_non_lus"].' nouveau(x) message(s). <a href="javascript:void(0);" class="read-messages">Marquer comme lu(s)</a>.';
	}
	else {
		$nbNonLus = 'Vous n\'avez pas de nouveaux messages.';
	}
	$requete = $baseDeDonnes->prepare("
	SELECT messagerie.*, membres.*
	FROM messagerie
	INNER JOIN membres
	ON messagerie.expediteur = membres.membre_id
	WHERE messagerie.destinataire = :id
	ORDER BY messagerie.vu ASC, messagerie.date DESC");
	$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
	$requete->execute();
	
	if($nbMessages > 0) {
		echo '
		<p class="encadreInfos">
			<span class="left">Remplissage</span>
			<span class="right">Votre boîte de réception est pleine à '.$pourcentage.'%.</span>
			<span class="right read-messages-ligne">'.$nbNonLus.'</span>
		</p>';
	}
	else {
		echo '<p class="encadreInfos"><span class="left">Remplissage</span><span class="right">Votre boîte de réception est vide.</span></p>';
	}
	
	if($requete->rowCount() > 0) { ?>
		<table>
			<tr>
				<th width="50">Lu</th>
				<th width="205">Sujet</th>
				<th width="205">Expéditeur</th>
				<th width="200">Date</th>
				<th width="100">Supprimer</th>
			</tr>
			<?php
			while($data = $requete->fetch()) {
				$sujet = stripslashes($data['sujet']);
				$expediteur = stripslashes($data['membre_pseudo']);
				$date = $data['date'];
				
				if($data['vu'] == 0) {
					$vu = '<b>Non lu</b>';
				}
				else {
					$vu = 'Lu';
				}
				echo '
				<tr>
					<td align="center">'.$vu.'</td>
					<td><a href="?Action=Voir&amp;Message='.$data['id'].'">'.$sujet.'</a></td>
					<td>'.$expediteur.'</td>
					<td>Le '.convertirTimestamp($date).' à '.date('H\hi', $date).'</td>
					<td align="center"><a href="?Action=Supprimer&amp;Suppr=1&amp;Message='.$data['id'].'">Supprimer</a>
					</td>
				</tr>';
			}
		echo '</table>';
	}
}

if(isset($erreurs) AND $erreurs != NULL) {
	echo '<p class="encadreInfos">';
		echo '<span class="left erreur block"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreurs</span>';
		foreach($erreurs as $erreur_array) {
			echo '<span class="right">';
				echo $erreur_array;
			echo '</span>';
		}
	echo '</p>';
}
if(isset($valide)) {
	echo '<p class="encadreInfos">';
		echo '<span class="left valide block"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Valide.png" alt="Confirmation" /> Confirmation</span>';
		echo '<span class="right">'.$valide.'</span>';
	echo '</p>';
}
elseif(isset($information)) {
	echo '
	<p class="encadreInfos">
		<span class="left information"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Information.png" alt="Information" /> Information</span>
		<span class="right">'.$information.'</span>
	</p>';
}

include("../Footer.php"); ?>
