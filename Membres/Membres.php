<?php 
$titre_page_nom = "Liste des Membres";
include("../Header.php");
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Membres.png" alt="Membres" /> Liste des Membres</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH ?>">Accueil</a> > <a href="./">Membres</a> > Liste des Membres</p>

<p class="encadreInfos"><span class="right">Voici la liste des membres du site. Si vous voulez les rejoindre, c'est par <a href="Inscription.php">ici</a>.</span></p>

<?php
$requete = $baseDeDonnes->query("
SELECT membres.*, (SELECT COUNT(equipements.id) FROM equipements WHERE id_ajouteur = membres.membre_id) AS equipements_count
FROM membres
ORDER BY membres.membre_niveau DESC, membres.membre_id ASC");
$requete->setFetchMode(PDO::FETCH_OBJ);

echo '
<table>
	<tr>
		<th width="30"></th>
		<th width="150">Pseudo</th>
		<th width="180">Dernière visite</th>
		<th width="70">Visites</th>
		<th width="70">État</th>
		<th width="100">Rang</th>
		<th width="70">Équipements</th>
	</tr>';

	$i = 0;
	while($data = $requete->fetch()) {
		$i++;
		
		if(!empty($data->membre_avatar))    $avatar = '<img src="../Images/Avatars/'.$data->membre_avatar.'" height="30" alt="" style="background:#FFF;" />';
		else                                $avatar = '<img src="../Images/Avatars/avatar_normal.png" height="30" alt="" style="background:#FFF;" />';
		
		echo '
		<tr>
			<td align="center">'.$avatar.'</td>
			<td><a href="Profil.php?id='.$data->membre_id.'">'.stripslashes($data->membre_pseudo).'</a></td>
			<td>'.afficher_date_difference($data->membre_derniere_visite).'</td>
			<td align="center">'.$data->membre_nb_visite.'</td>';
			if($data->membre_connecte == 1)     echo '<td align="center"><img src="../Images/Membres/MembreConnecte.png" alt="" /></td>';
			else                                echo '<td align="center"><img src="../Images/Membres/MembreDeconnecte.png" alt="" /></td>';
			echo '
			<td>'.$MEMBRES_RANGS[$data->membre_niveau].'</td>
			<td align="center">'.$data->equipements_count.'</td>
		</tr>';
	}

echo '</table>';

include("../Footer.php"); ?>