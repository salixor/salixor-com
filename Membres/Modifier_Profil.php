<?php 
session_start();
$erreurs = array();
$valide = array();

$array_actions_membre = array("MotDePasse" => "Changer de Mot de Passe",
							  "Restauration" => "Restauration de Mot de Passe",
							  "Email" => "Changer d'Email",
							  "Signature" => "Changer de Signature",
							  "Titre" => "Changer de Titre",
							  "Citation" => "Changer sa Citation",
							  "Dofus" => "Mes Personnages",
							  "Publiques" => "Informations Publiques",
							  "Avatar" => "Changer son Avatar",
							  "Style" => "Modifier le Thème personnalisé");
							  
if(isset($_GET["Action"])) {
	if(isset($array_actions_membre[$_GET["Action"]])) {
		if(in_array($array_actions_membre[$_GET["Action"]], $array_actions_membre)) {
			$titre_page_nom = $array_actions_membre[$_GET["Action"]];
		}
		else     $titre_page_nom = "Modifier son Profil";
	}
	else     $titre_page_nom = "Modifier son Profil";
}
else {
	$titre_page_nom = "Modifier son Profil";
}

require_once("../Includes/Fonctions.php");
require_once("../Includes/Config.php");

if(!isset($PSEUDO_MEMBRE)) {
	//Récupération de Mot de Passe
	if(isset($_POST["recup_send"])) {
		if(!empty($_POST["email_recup"]) AND !empty($_POST["pseudo_recup"])) {
			extract($_POST);
				$emailRecup = mysql_real_escape_string($_POST["email_recup"]);
				$pseudoRecup = mysql_real_escape_string($_POST["pseudo_recup"]);
				
			if(is_email($emailRecup)) {
				$requete = $baseDeDonnes->prepare("SELECT * FROM membres WHERE membre_mail COLLATE utf8_unicode_ci = :email AND membre_pseudo COLLATE utf8_unicode_ci = :pseudo");
				$requete->bindValue(":email", $emailRecup, PDO::PARAM_STR);
				$requete->bindValue(":pseudo", $pseudoRecup, PDO::PARAM_STR);
				$requete->execute();
					
				if($requete->rowCount() == 1) {
					$nouveauMotDePasse = code_aleatoire(10);
					$data = $requete->fetch();
					
					$idMembre = $data["membre_id"];
					$emailTo = $data["membre_mail"];
					
					$body = addslashes('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
							<title>Nouveau Mot de Passe</title>
							<style type="text/css">
								body {
									font-family:"Trebuchet MS";
								}
							</style>
							Voici votre nouveau mot de Passe. Conservez le ou changez en :
							<div style="margin:10px 0;"><b style="background:#DDD;padding:4px;display:block;width:100%;">Mot de Passe :</b> '.$nouveauMotDePasse.'<div>');
					$headers = 'Mime-Version: 1.0'."\r\n";
					$headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
					$headers .= "\r\n";
					
					mail($emailTo, "Nouveau Mot de Passe - Salixor.com", stripslashes($body), $headers);
					
					$requete = $baseDeDonnes->prepare("UPDATE membres SET membre_mdp = :mdp WHERE membre_id = :id");
					$requete->bindValue(":mdp", sha1($nouveauMotDePasse), PDO::PARAM_STR);
					$requete->bindValue(":id", $idMembre, PDO::PARAM_INT);
					$requete->execute();

					array_push($valide, "Un nouveau mot de passe vous a été envoyé. Vous pourrez le changer à votre prochaine connexion.<p>Le mail peut prendre du temps pour arriver.</p>");
				}
				elseif(mysql_num_rows($requete) > 1)         array_push($erreurs, "Deux membres ou plus ont été trouvés. Contactez l'administrateur pour corriger cette erreur.");
				else                                         array_push($erreurs, "Une ou plusieurs des informations sont erronées.");
			}
			else {
				array_push($erreurs, "L'Email n'est pas de forme valide.");
			}
		}
		else {
			array_push($erreurs, "Un ou plusieurs des champs n'ont pas été remplis.");
		}
	}
}

if(isset($PSEUDO_MEMBRE)) {
	//Changement de Mot de Passe
	if(isset($_POST["submit_password"])) {
		if(!empty($_POST["password_actuel"]) AND !empty($_POST["password_new"]) AND !empty($_POST["password_confirmation_new"])) {			
			$requete = $baseDeDonnes->prepare("SELECT * FROM membres WHERE membre_id = :id AND membre_mdp = :mdp");
			$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
			$requete->bindValue(":mdp", sha1($_POST["password_actuel"]), PDO::PARAM_STR);
			$requete->execute();
				
			if($requete->rowCount() > 0) {
				if($_POST["password_new"] == $_POST["password_confirmation_new"]) {
					$requete = $baseDeDonnes->prepare("UPDATE membres SET membre_mdp = :mdp WHERE membre_id = :id");
					$requete->bindValue(":mdp", sha1($_POST["password_new"]), PDO::PARAM_STR);
					$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
					$requete->execute();
					
					array_push($valide, "Votre Mot de Passe a été changé.");
				}
				else {
					array_push($erreurs, "Les mots de passse entrés ne correspondent pas.");
				}
			}
			else {
				array_push($erreurs, "Le Mot de Passe actuel est incorrect.");
			}
		}
		else {
			array_push($erreurs, "Un ou plusieurs des champs n'ont pas été remplis.");
		}
	}

	//Changement de l'Email
	if(isset($_POST["submit_email"])) {
		if(!empty($_POST["password"]) AND !empty($_POST["email_new"]) AND !empty($_POST["email_confirmation_new"])) {				
			if(is_email($_POST["email_new"]) AND is_email($_POST["email_confirmation_new"])) {
				$requete = $baseDeDonnes->prepare("SELECT * FROM membres WHERE membre_id = :id AND membre_mdp = :mdp");
				$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
				$requete->bindValue(":mdp", sha1($_POST["password"]), PDO::PARAM_STR);
				$requete->execute();
					
				if($requete->rowCount() > 0) {
					if($_POST["email_new"] == $_POST["email_confirmation_new"]) {
						$requete = $baseDeDonnes->prepare("UPDATE membres SET membre_mail = :mail WHERE membre_id = :id");
						$requete->bindValue(":mail", $_POST["email_new"], PDO::PARAM_STR);
						$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
						$requete->execute();
						
						array_push($valide, "Votre adresse Email a été changée.");
					}
					else {
						array_push($erreurs, "Les adresses Email entrées ne correspondent pas.");
					}
				}
				else {
					array_push($erreurs, "Le Mot de Passe actuel est incorrect.");
				}
			}
			else {
				array_push($erreurs, "L'Email n'est pas de forme valide.");
			}
		}
		else {
			array_push($erreurs, "Un ou plusieurs des champs n'ont pas été remplis.");
		}
	}

	//Changement de titre
	if(isset($_POST["submit_titre"])) {
		if(!empty($_POST["choix_titre"])) {
			$requete = $baseDeDonnes->prepare("UPDATE membres SET membre_titre_id = :titre WHERE membre_id = :id");
			$requete->bindValue(":titre", $_POST["choix_titre"], PDO::PARAM_STR);
			$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
			$requete->execute();
						
			array_push($valide, "Le titre a bien été enregistré.");
		}
		else {
			array_push($erreurs, "Aucun titre n'a été choisi.");
		}
	}

	//Changement de signature
	if(isset($_POST["submit_signature"])) {
		if(strlen($_POST["message"]) <= 500) {
			$requete = $baseDeDonnes->prepare("UPDATE membres SET membre_signature = :signature WHERE membre_id = :id");
			$requete->bindValue(":signature", $_POST["message"], PDO::PARAM_STR);
			$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
			$requete->execute();

			array_push($valide, "La signature a été enregistrée.");
		}
		else {
			array_push($erreurs, "La signature fait plus de 500 caractères.");
		}
	}
	
	//Citation
	if(isset($_POST["submit_citation"])) {
		$_POST["citation"] = trim($_POST["citation"]);
		
		if(strlen($_POST["citation"]) <= 50) {
			$requete = $baseDeDonnes->prepare("UPDATE membres SET membre_citation = :citation WHERE membre_id = :id");
			$requete->bindValue(":citation", $_POST["citation"], PDO::PARAM_STR);
			$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
			$requete->execute();
			
			array_push($valide, "La citation a été enregistrée.");
		}
		else {
			array_push($erreurs, "La citation fait plus de 50 caractères.");
		}
	}
	
	//Informations Publiques
	if(isset($_POST["submit_publiques"])) {
		if($_POST["jour"] != 0 AND $_POST["mois"] != 0 AND $_POST["annee"] != 0 ) {
			$date_naissance = mktime(0, 0, 0, $_POST["mois"], $_POST["jour"], $_POST["annee"]);
			
			array_push($valide, "La date de naissance a été enregistrée.");
			
			$requete = $baseDeDonnes->prepare("UPDATE membres SET membre_naissance = :naissance WHERE membre_id = :id");
			$requete->bindValue(":naissance", $date_naissance, PDO::PARAM_INT);
			$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
			$requete->execute();
		}
		
		if(!empty($_POST["profession"])) {
			if(strlen($_POST["profession"]) <= 100) {
				array_push($valide, "La profession a été enregistrée.");
			
				$requete = $baseDeDonnes->prepare("UPDATE membres SET membre_profession = :profession WHERE membre_id = :id");
				$requete->bindValue(":profession", $_POST["profession"], PDO::PARAM_STR);
				$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
				$requete->execute();
			}
			else {
				array_push($erreurs, "Le nom de la profession fait plus de 100 caractères.");
			}
		}
		
		if(!empty($_POST["site_web"])) {
			if(is_http($_POST["site_web"]) AND strlen($_POST["site_web"]) <= 70) {
				array_push($valide, "Le site web a été enregistré.");
			
				$requete = $baseDeDonnes->prepare("UPDATE membres SET membre_web = :web WHERE membre_id = :id");
				$requete->bindValue(":web", $_POST["site_web"], PDO::PARAM_STR);
				$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
				$requete->execute();
			}
			if(strlen($_POST["site_web"]) > 70) {
				array_push($erreurs, "Le site web fait plus de 70 caractères.");
			}
			elseif(is_http($_POST["site_web"]) == false) {
				array_push($erreurs, "Le site web n'est pas de forme valide.");
			}
		}
		
		if($_POST["sexe"] != "0" && ($_POST["sexe"] == "Homme" || $_POST["sexe"] == "Femme")) {
			array_push($valide, "Le sexe a été enregistré.");
			
			$requete = $baseDeDonnes->prepare("UPDATE membres SET membre_sexe = :sexe WHERE membre_id = :id");
			$requete->bindValue(":sexe", $_POST["sexe"], PDO::PARAM_STR);
			$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
			$requete->execute();
		}
		
		if(!empty($_POST["loisirs"])) {
			if(strlen($_POST["loisirs"]) <= 150) {
				array_push($valide, "Les loisirs ont été enregistrés.");
			
				$requete = $baseDeDonnes->prepare("UPDATE membres SET membre_loisirs = :loisirs WHERE membre_id = :id");
				$requete->bindValue(":loisirs", $_POST["loisirs"], PDO::PARAM_STR);
				$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
				$requete->execute();
			}
			else {
				array_push($erreurs, "La liste de loisirs fait plus de 150 caractères.");
			}
		}
		
		if($_POST["jour"] == 0 AND $_POST["mois"] == 0 AND $_POST["annee"] == 0 AND empty($_POST["site_web"]) AND empty($_POST["loisirs"])) {
			array_push($erreurs, "Aucune donnée n'a été remplie.");
		}
	}
	
	//Gestion des personnages
	if(isset($_POST["submit_dofus"])) {
		if(!empty($_POST["nom_personnage"]) && $_POST["classe_personnage"] != "0" && !empty($_POST["niveau_personnage"]) && $_POST["sexe_personnage"] != "0") {
			if(!empty($_POST["nom_personnage"])) {
				if(strlen($_POST["nom_personnage"]) < 30) {
					array_push($valide, "Le nom du personnage a été enregistré.");
					
					$requete = $baseDeDonnes->prepare("
					UPDATE membres_personnages
					SET membre_nom_personnage = :nom_personnage
					WHERE personnage_id = :personnage");
					$requete->bindValue(":nom_personnage", $_POST["nom_personnage"], PDO::PARAM_STR);
					$requete->bindValue(":personnage", $_GET["Personnage"], PDO::PARAM_INT);
					$requete->execute();
				}
				else {
					array_push($erreurs, "Le nom du personnage fait plus de 30 caractères.");
				}
			}
			if($_POST["classe_personnage"] != "0") {		
				array_push($valide, "La classe du personnage a été enregistrée.");
					
				$requete = $baseDeDonnes->prepare("
				UPDATE membres_personnages
				SET membre_classe_personnage = :classe_personnage
				WHERE personnage_id = :personnage");
				$requete->bindValue(":classe_personnage", $_POST["classe_personnage"], PDO::PARAM_STR);
				$requete->bindValue(":personnage", $_GET["Personnage"], PDO::PARAM_INT);
				$requete->execute();
			}
			if(!empty($_POST["niveau_personnage"])) {
				if($_POST["niveau_personnage"] >= 1 && $_POST["niveau_personnage"] <= 200) {
					array_push($valide, "Le niveau du personnage a été enregistré.");
					
					$requete = $baseDeDonnes->prepare("
					UPDATE membres_personnages
					SET membre_niveau_personnage = :niveau_personnage
					WHERE personnage_id = :personnage");
					$requete->bindValue(":niveau_personnage", $_POST["niveau_personnage"], PDO::PARAM_INT);
					$requete->bindValue(":personnage", $_GET["Personnage"], PDO::PARAM_INT);
					$requete->execute();
				}
				else {
					array_push($erreurs, "Le niveau du personnage est incorrect.");
				}
			}
			if($_POST["serveur_personnage"] != "0") {
				array_push($valide, "Le serveur du personnage a été enregistré.");
					
				$requete = $baseDeDonnes->prepare("
				UPDATE membres_personnages
				SET membre_serveur_personnage = :serveur_personnage
				WHERE personnage_id = :personnage");
				$requete->bindValue(":serveur_personnage", $_POST["serveur_personnage"], PDO::PARAM_STR);
				$requete->bindValue(":personnage", $_GET["Personnage"], PDO::PARAM_INT);
				$requete->execute();
			}
			if($_POST["sexe_personnage"] != "0" && $_POST["sexe_personnage"] == "Masculin" || $_POST["sexe_personnage"] == "Féminin") {
				array_push($valide, "Le sexe du personnage a été enregistré.");
					
				$requete = $baseDeDonnes->prepare("
				UPDATE membres_personnages
				SET membre_sexe_personnage = :sexe_personnage
				WHERE personnage_id = :personnage");
				$requete->bindValue(":sexe_personnage", $_POST["sexe_personnage"], PDO::PARAM_STR);
				$requete->bindValue(":personnage", $_GET["Personnage"], PDO::PARAM_INT);
				$requete->execute();
			}
			
			if(!empty($_POST["guilde_personnage"])) {
				array_push($valide, "La guilde du personnage a été enregistrée.");
				
				$requete = $baseDeDonnes->prepare("
				UPDATE membres_personnages
				SET personnage_guilde = :guilde_personnage
				WHERE personnage_id = :personnage");
				$requete->bindValue(":guilde_personnage", $_POST["guilde_personnage"], PDO::PARAM_INT);
				$requete->bindValue(":personnage", $_GET["Personnage"], PDO::PARAM_INT);
				$requete->execute();
			}
			
			$metierFinal = "";
			
			for($i = 0; $i <= 3; $i++) {
				if(!empty($_POST["metier_".$i]) && !empty($_POST["metier_".$i."_level"])) {
					if($_POST["metier_".$i."_level"] <= 100 && $_POST["metier_".$i."_level"] >= 0) {
						$metier = mysql_real_escape_string($_POST["metier_".$i]);
						$level = mysql_real_escape_string($_POST["metier_".$i."_level"]);
							
						array_push($valide, "Le ".$i."e métier a été enregistré.");
						$metierFinal .= $metier."-".$level.";";
					}
					else {
						array_push($erreurs, "Le niveau du ".$i."e métier est incorrect.");
					}
				}
			}
			
			$requete = $baseDeDonnes->prepare("
			UPDATE membres_personnages
			SET membre_personnage_metiers = :metierFinal
			WHERE personnage_id = :personnage");
			$requete->bindValue(":metierFinal", $metierFinal, PDO::PARAM_STR);
			$requete->bindValue(":personnage", $_GET["Personnage"], PDO::PARAM_INT);
			$requete->execute();
		}
		else {
			array_push($erreurs, "Les champs nécessaires n'ont pas été remplis.");
		}
	}
	
	//Ajout d'un personnage
	if(isset($_POST["submit_dofus_add"])) {
		$requete = $baseDeDonnes->prepare("
		SELECT *
		FROM membres_personnages
		WHERE membre_possesseur_id = :id");
		$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
		$requete->execute();
		
		if($requete->rowCount() <= 8) {
			if(!empty($_POST["nom_personnage"]) && $_POST["classe_personnage"] != "0" && !empty($_POST["niveau_personnage"]) && $_POST["sexe_personnage"] != "0" && $_POST["serveur_personnage"] != '0') {
				if($_POST["niveau_personnage"] < 1)      $_POST["niveau_personnage"] = 1;
				if($_POST["niveau_personnage"] > 200)    $_POST["niveau_personnage"] = 200;
				
				if(!empty($_POST["guilde_personnage"])) {
					$requete = $baseDeDonnes->prepare("SELECT * FROM guildes WHERE guilde_serveur = :serveur_personnage");
					$requete->bindValue(":serveur_personnage", $_POST["serveur_personnage"], PDO::PARAM_STR);
					$requete->execute();
					
					if($requete->rowCount() != 1) {
						array_push($erreurs, "La guilde choisie n'est pas sur le serveur du personnage.");
						$_POST["guilde_personnage"] = 0;
					}
				}
				
				$requete = $baseDeDonnes->prepare("SELECT COUNT(*) nb_persos FROM membres_personnages WHERE membre_possesseur_id = :id");
				$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
				$requete->execute();
				
				$data = $requete->fetch();
				if($data["nb_persos"] < 8) {
					array_push($valide, "Le personnage a été crée.");
					$metierFinal = "";
					
					for($i = 0; $i <= 3; $i++) {
						if(!empty($_POST["metier_".$i]) && !empty($_POST["metier_".$i."_level"])) {
							if($_POST["metier_".$i."_level"] <= 100 && $_POST["metier_".$i."_level"] >= 0) {
								$metier = mysql_real_escape_string($_POST["metier_".$i]);
								$level = mysql_real_escape_string($_POST["metier_".$i."_level"]);
									
								$metierFinal .= $metier."-".$level.";";
							}
							else {
								array_push($erreurs, "Le niveau du ".$i."e métier est incorrect.");
							}
						}
					}
					
					$requete = $baseDeDonnes->prepare("
					INSERT INTO
					membres_personnages(membre_nom_personnage, membre_classe_personnage, membre_niveau_personnage, membre_serveur_personnage, membre_sexe_personnage, personnage_guilde, membre_personnage_metiers, membre_possesseur_id)
					VALUES(:nom_personnage, :classe_personnage, :niveau_personnage, :serveur_personnage, :sexe_personnage, :guilde_personnage, :metierFinal, :id)");
					$requete->bindValue(":nom_personnage", $_POST["nom_personnage"], PDO::PARAM_STR);
					$requete->bindValue(":classe_personnage", $_POST["classe_personnage"], PDO::PARAM_STR);
					$requete->bindValue(":niveau_personnage", $_POST["niveau_personnage"], PDO::PARAM_INT);
					$requete->bindValue(":serveur_personnage", $_POST["serveur_personnage"], PDO::PARAM_STR);
					$requete->bindValue(":sexe_personnage", $_POST["sexe_personnage"], PDO::PARAM_INT);
					$requete->bindValue(":guilde_personnage", $_POST["guilde_personnage"], PDO::PARAM_STR);
					$requete->bindValue(":metierFinal", $metierFinal, PDO::PARAM_STR);
					$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
					$requete->execute();
				}
				else {
					array_push($erreurs, "Le nombre maximum de personnages a été atteint.");
				}
			}
			else {
				array_push($erreurs, "Les champs nécessaires n'ont pas été remplis.");
			}
		}
		else {
			array_push($erreurs, "Le nombre maximum de personnages a été atteint.");
		}
	}
	
	if(isset($_GET["SupprimerPerso"])) {
		if($_GET["SupprimerPerso"] == "Ok") {
			$requete = $baseDeDonnes->prepare("SELECT COUNT(*) nb_persos FROM membres_personnages WHERE membre_possesseur_id = :id AND personnage_id = :personnage");
			$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
			$requete->bindValue(":personnage", $_GET["Personnage"], PDO::PARAM_INT);
			$requete->execute();
			
			$data = $requete->fetch();
			if($data["nb_persos"] == 1) {
				array_push($valide, "Le personnage a été supprimé.");
				
				$requete = $baseDeDonnes->prepare("
				DELETE FROM	membres_personnages WHERE personnage_id = :personnage AND membre_possesseur_id = :id");
				$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
				$requete->bindValue(":personnage", $_GET["Personnage"], PDO::PARAM_INT);
				$requete->execute();
			}
			else {
				array_push($erreurs, "Ce personnage n'est pas lié à votre compte.");
			}
		}
	}
	
	//Envoi de l'avatar		
	if(isset($_POST["envoyer_avatar"])) {
		if($_FILES['avatar']['error'] == 0) {
			if($_FILES['avatar']['size'] <= 500000) {
				$infosfichier = pathinfo($_FILES['avatar']['name']);
				$extension_upload = $infosfichier['extension'];
				$extensions_autorisees = array('jpg', 'jpeg', 'gif', 'png', 'bmp', 'JPG', 'JPEG', 'GIF', 'PNG', 'BMP');
				if(in_array($extension_upload, $extensions_autorisees)) {
					$avatar_numero = code_aleatoire(20);
					
					$requete = $baseDeDonnes->prepare("SELECT membre_avatar FROM membres WHERE membre_id = :id");
					$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
					$requete->execute();

					$data = $requete->fetch();
					
					if($data["membre_avatar"] != NULL) {
						if(file_exists("../Images/Avatars/".$data["membre_avatar"])) {
							unlink("../Images/Avatars/".$data["membre_avatar"]);
						}
					}
					
					$requete = $baseDeDonnes->prepare("UPDATE membres SET membre_avatar = :avatar WHERE membre_id = :id");
					$requete->bindValue(":avatar", $avatar_numero.".".$infosfichier['extension'], PDO::PARAM_STR);
					$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
					$requete->execute();
									
					move_uploaded_file($_FILES['avatar']['tmp_name'], '../Images/Avatars/'.$avatar_numero.'.'.$infosfichier['extension']);
					array_push($valide, "L'avatar a été enregistré.");
				}
				else {
					array_push($erreurs, "L'extension de l'avatar est incorrecte.");
				}
			}
			elseif($_FILES['avatar']['size'] > 500000) {
				array_push($erreurs, "L'avatar est trop lourd.");
			}
		}
	}

	//Suppression de l'avatar
	if(isset($_POST["supprimer_avatar"])) {
		$requete = $baseDeDonnes->prepare("SELECT membre_avatar FROM membres WHERE membre_id = :id");
		$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
		$requete->execute();

		$data = $requete->fetch();
		
		if($data["membre_avatar"] != NULL) {
			if(file_exists("../Images/Avatars/".$data["membre_avatar"])) {
				unlink("../Images/Avatars/".$data["membre_avatar"]);
				array_push($valide, "Votre avatar a bien été supprimé.");
				
				$requete = $baseDeDonnes->prepare("UPDATE membres SET membre_avatar = '' WHERE membre_id = :id");
				$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
				$requete->execute();
			}
			else {
				array_push($erreurs, "L'avatar n'a pas pu être supprimé.");
			}
		}
		else {
			array_push($erreurs, "L'avatar n'a pas pu être supprimé.");
		}
	}
	
	//Ajout de l'image du thème
	if(isset($_POST["envoyer_image_theme"])) {
		if($_FILES['image_theme']['error'] == 0) {
			if($_FILES['image_theme']['size'] <= 750000) {
				$infosFichier = pathinfo($_FILES['image_theme']['name']);
				$extensionUpload = $infosFichier['extension'];
				$extensionsAutorisees = array('jpg', 'jpeg', 'gif', 'png', 'bmp', 'JPG', 'JPEG', 'GIF', 'PNG', 'BMP');
				
				if(in_array($extensionUpload, $extensionsAutorisees)) {
					$requete = $baseDeDonnes->prepare("UPDATE membres SET membre_style = :pseudo WHERE membre_id = :id");
					$requete->bindValue(":pseudo", $PSEUDO_MEMBRE, PDO::PARAM_STR);
					$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
					$requete->execute();
					
					if(file_exists(ABSPATH.'Images/Themes/'.$PSEUDO_MEMBRE.'.png'))      unlink(ABSPATH.'Images/Themes/'.$PSEUDO_MEMBRE.'.png');					
									
					move_uploaded_file($_FILES['image_theme']['tmp_name'], '../Images/Themes/'.$PSEUDO_MEMBRE.'.png');
					array_push($valide, "L'image de thème a été enregistrée.");
				}
				else {
					array_push($erreurs, "L'extension de l'image de thème est incorrecte.");
				}
			}
			elseif($_FILES['image_theme']['size'] > 750000) {
				array_push($erreurs, "L'image de thème est trop lourde.");
			}
		}
	}
	
	//Suppression de l'image du thème
	if(isset($_POST["supprimer_image_theme"])) {
		$requete = $baseDeDonnes->prepare("SELECT membre_avatar FROM membres WHERE membre_id = :id");
		$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
		$requete->execute();

		$data = $requete->fetch();
		
		if($data["membre_avatar"] != NULL) {
			if(file_exists("../Images/Avatars/".$data["membre_avatar"])) {
				unlink("../Images/Avatars/".$data["membre_avatar"]);
				array_push($valide, "Votre avatar a bien été supprimé.");
				
				$requete = $baseDeDonnes->prepare("UPDATE membres SET membre_avatar = '' WHERE membre_id = :id");
				$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
				$requete->execute();
			}
			else {
				array_push($erreurs, "L'avatar n'a pas pu être supprimé.");
			}
		}
		else {
			array_push($erreurs, "L'avatar n'a pas pu être supprimé.");
		}
	}
}

include("../Header.php");

if(!isset($PSEUDO_MEMBRE)) {
	if(isset($_GET["Action"])) {
		if($_GET["Action"] == "Restauration") {
			$h2 = $array_actions_membre[$_GET["Action"]];
				
			if(isset($h2)) {
				echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Modifier_Profil.png" alt="Modifier_Profil" /> '.$h2.'</h2>';
			}
			
			if($valide == NULL) { 
				echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > Restauration de Mot de Passe</p>';
				
				echo '<p class="encadreInfos"><span class="right">Vous avez oublié votre mot de Passe ? Récupérez en un par mail en redonnant votre mail et pseudo.</span></p>'; ?>
				
				<div class="bloc">
					<form action method="post">
						<p><label for="pseudo_recup">Votre pseudo : </label><input type="text" name="pseudo_recup" id="pseudo_recup" style="width:250px" /></p>
						<p><label for="email_recup">Votre email : </label><input type="text" name="email_recup" id="email_recup" style="width:250px" /></p>
						<p align="center"><input type="submit" name="recup_send" value="Récupération" /></p>
					</form>
				</div>
					
			<?php }
		}
	}
}
if(isset($PSEUDO_MEMBRE)) {
	if(isset($_GET["Action"])) {
		if(!in_array($_GET["Action"], $array_actions_membre) && $_GET["Action"] != "Restauration") {
			
			$h2 = $array_actions_membre[$_GET["Action"]];
				
			if(isset($h2))          echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Modifier_Profil.png" alt="Modifier_Profil" /> '.$h2.'</h2>';
			
			switch($_GET["Action"]) {
			
				case "MotDePasse":
					echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Modifier_Profil.php">Modifier mon profil</a> > Changer de Mot de Passe</p>';
					
					echo '<p class="encadreInfos"><span class="right">Votre mot de passe ne vous plaît plus ? Pas de souci, changez en ici !</span></p>';
					
					if($valide == NULL) { ?>
						
						<div class="bloc">
							<form action method="post">
								<p><label for="password_actuel">Mot de Passe actuel : </label><input type="password" name="password_actuel" id="password_actuel" /></p>
								<p><label for="password_new">Nouveau Mot de Passe : </label><input type="password" name="password_new" id="password_new" /></p>
								<p><label for="password_confirmation_new">Confirmation du Mdp : </label><input type="password" name="password_confirmation_new" id="password_confirmation_new" /></p>
								<p align="center"><input type="submit" name="submit_password" value="Changer" /></p>
							</form>
						</div>
							
					<?php }	
				break;
				
				case "Email":
					echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Modifier_Profil.php">Modifier mon profil</a> > Changer d\'Email</p>';
					
					echo '<p class="encadreInfos"><span class="right">Si votre adresse Email n\'est plus correcte, changez là pour rester en contact avec le site.</span></p>';
					
					if($valide == NULL) { ?>
						
						<div class="bloc">
							<form action method="post">
								<p><label for="password">Mot de Passe : </label><input type="password" name="password" id="password" /></p>
								<p><label for="email_new">Nouvel Email : </label><input type="text" name="email_new" id="email_new" /></p>
								<p><label for="email_confirmation_new">Confirmation Email : </label><input type="text" name="email_confirmation_new" id="email_confirmation_new" /></p>
								<p align="center"><input type="submit" name="submit_email" value="Changer" /></p>
							</form>
						</div>
					
					<?php }	
				break;

				case "Titre":
					echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Modifier_Profil.php">Modifier mon profil</a> > Changer de Titre</p>';
					
					echo '<p class="encadreInfos"><span class="right">Vous pouvez ici changer votre titre visible un peu partout sur le site. Les titres se débloquent durant des évènements ou l\'intervention d\'un administrateur, et parfois par des accomplissements.</span></p>';
					
					if($valide == NULL) { ?>
						
						<div class="bloc">
							<form action method="post">
							<?php
							$requete2 = $baseDeDonnes->prepare("SELECT membre_titre_id FROM membres WHERE membre_id = :id");
							$requete2->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
							$requete2->execute();
							$data2 = $requete2->fetch();
							
							$requete1 = $baseDeDonnes->query("SELECT * FROM membres_titres");
							
							$i = 0;
							while($data = $requete1->fetch()) {
								$i++;
								$autorise = explode(" ", $data["id_autorise"]);
								if(in_array($ID_MEMBRE, $autorise)) {
									$checked = '';
									if($data2["membre_titre_id"] == $i)          $checked = ' checked="checked"';
									
									echo '
									<p>
										<input type="radio" name="choix_titre" id="choix_titre'.$i.'" value="'.$i.'" '.$checked.' /> <label for="choix_titre'.$i.'">'.utf8_encode($data["titre"]).'</label>
										'.utf8_encode($data["description"]).'
									</p>';
								}
							}
							
							if($i == 0)           echo 'Vous ne pouvez choisir aucun titre.';
							else                  echo '<p align="center"><input type="submit" name="submit_titre" value="Choisir" /></p>';
							?>
							</form>
						</div>
						
					<?php }	
				break;

				case "Signature":
					echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Modifier_Profil.php">Modifier mon profil</a> > Changer de Signature</p>';
					
					echo '<p class="encadreInfos"><span class="right">Vous pouvez ici changer votre signature de 500 caractères maximum visible sur les forums.</span></p>';
					
					if($valide == NULL) {
						$requete = $baseDeDonnes->prepare("SELECT membre_signature FROM membres WHERE membre_id = :id");
						$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
						$requete->execute();
						
						$data = $requete->fetch();
						$signatureActuelle = stripslashes($data["membre_signature"]); ?>
						
						<div class="bloc">
							<form action method="post">
								<?php
								afficher_smileys('message');
								$FormatagePanel = new FormatagePanel(true, true, true, 'message');
								$FormatagePanel->showFormatagePanel();
								?>

								<p><textarea id="message" name="message" style="height:250px;"><?php echo $signatureActuelle ?></textarea></p>
								
								<p align="center">
									<input type="submit" name="submit_signature" value="Enregistrer" />
									<input type="button" id="previsualiser_bouton" value="Prévisualiser" onclick="previsualiser('message','previsualiser_commentaire','../AJAX_news.php');" />
								</p>
							</form>
						</div>
						
						<p align="center" id="previsualiser_zone">
							<div class="preview" id="previsualiser_commentaire"></div>
						</p>
								
					<?php }	
				break;
				
				case "Citation":
					echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Modifier_Profil.php">Modifier mon profil</a> > Changer sa Citation</p>';
					
					echo '<p class="encadreInfos"><span class="right">Vous pouvez ici enregistrer votre citation favorite de 50 caractères maximum.</span></p>';
					
					if($valide == NULL) {
						$requete = $baseDeDonnes->prepare("SELECT membre_citation FROM membres WHERE membre_id = :id");
						$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
						$requete->execute();
						
						$data = $requete->fetch();
						$citationActuelle = stripslashes($data["membre_citation"]);
						?>
						
						<div class="bloc">
							<form action method="post">
								<p><label for="citation">Citation : </label><input type="text" name="citation" id="citation" style="width:400px;" value="<?php echo $citationActuelle ?>" /></p>
								<p align="center"><input type="submit" name="submit_citation" value="Enregistrer" /></p>
							</form>
						</div>
					
					<?php }	
				break;
				
				case "Publiques":
					$mois = array("Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre");
					
					echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Modifier_Profil.php">Modifier mon profil</a> > Informations Publiques</p>';
					
					echo '<p class="encadreInfos"><span class="right">Vous pouvez ici enregistrer diverses informations. Les champs non remplis ne seront pas affichés sur le profil.</span></p>';
						
					if($valide == NULL) {
						$requete = $baseDeDonnes->prepare("SELECT membre_naissance, membre_web, membre_loisirs, membre_sexe, membre_profession FROM membres WHERE membre_id = :id");
						$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
						$requete->execute();
						
						$data = $requete->fetch();
						
						$naissance = $data["membre_naissance"];
						$year = date("Y", $naissance);
						$day = date("d", $naissance);
						$month = date("m", $naissance);
						
						$actualWeb = $data["membre_web"];
						$actualLoisirs = $data["membre_loisirs"];
						$actualSexe = $data["membre_sexe"];
						$actualProfession = $data["membre_profession"];
						?>
						
						<div class="bloc">
							<form action method="post">
								<p>
									<label for="date_naissance">Date de Naissance : </label>
									<select name="jour" id="date_naissance">
										<?php
											for($i = 1; $i < 32; $i++) {
												if($i < 10) {
													$i = "0".$i;
												}
												if($day == $i) {
													echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
												}
												else {
													echo '<option value="'.$i.'">'.$i.'</option>';
												}
											}
										?>
									</select> 
									
									<select name="mois">
										<?php
											$i = 0;
											foreach($mois as $un_mois) {
												$i++;
												if($i < 10) {
													$i = "0".$i;
												}
												if($month == $i) {
													echo '<option value="'.$i.'" selected="selected">'.$un_mois.'</option>';
												}
												else {
													echo '<option value="'.$i.'">'.$un_mois.'</option>';
												}
											}
										?>
									</select> 
									
									<select name="annee">
										<?php
											for($i = 2011; $i > 1950; $i--) {
												if($year == $i) {
													echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
												}
												else {
													echo '<option value="'.$i.'">'.$i.'</option>';
												}
											}
										?>
									</select>
								</p>
								<p>
									<label for="sexe">Sexe : </label>
									<select name="sexe" id="sexe">
										<option value="0">Choisir</option>
										<?php
										if($actualSexe == "Homme") {
											echo '<option value="Homme" selected="selected">Homme</option>';
											echo '<option value="Homme">Homme</option>';
										}
										elseif($actualSexe == "Femme") {
											echo '<option value="Homme">Homme</option>';
											echo '<option value="Femme" selected="selected">Femme</option>';
										}
										else {
											echo '<option value="Homme">Homme</option>';
											echo '<option value="Femme">Femme</option>';
										}
										?>
									</select>
								</p>
								<p><label for="site_web">Site Web : </label><input type="text" name="site_web" id="site_web" value="<?php echo $actualWeb ?>" style="width:400px;" /></p>
								<p><label for="profession">Profession : </label><input type="text" name="profession" id="profession" value="<?php echo $actualProfession ?>" style="width:400px;" /></p>
								<p><label for="loisirs">Loisirs : </label><input type="text" name="loisirs" id="loisirs" value="<?php echo $actualLoisirs ?>" style="width:400px;" /></p>
								<p align="center"><input type="submit" name="submit_publiques" value="Enregistrer" /></p>
							</form>
						</div>
					
					<?php }	
				break;
				
				case "Dofus":

				$classes = array("Crâ", "Ecaflip", "Eniripsa", "Enutrof", "Féca", "Iop", "Osamodas", "Pandawa", "Sacrieur", "Sadida", "Sram", "Xélor", "Roublard", "Zobal");
				sort($classes);
				$serveurs_francais = array("Agride", "Allister", "Amayiro", "Bowisse", "Brumaire", "Crocoburio", "Danathor", "Djaul", "Domen", "Farle", "Goultard",
										   "Hecate", "Hel Munster", "Helsephine", "Hyrkul", "Jiva", "Kuri", "Li Crounch", "Lily", "Maimane", "Many", "Menalt",
										   "Mylaise", "Otomaï", "Pouchecot", "Raval", "Rykke-Errel", "Silouate", "Silvosse", "Sumens", "Ulette", "Vil Smisse");
										   
				$serveurs_internationnal = array("Aermyne", "Oto-Mustam", "Rosal", "Rushu", "Solar", "Zatoïshwan");
				
				if(isset($_GET["Personnage"]) && !isset($_GET["SupprimerPerso"])) {
					$requete = $baseDeDonnes->prepare("
					SELECT membres_personnages.*, guildes.guilde_id AS guilde_id_base, guildes_liste.* FROM membres_personnages
					LEFT JOIN guildes ON guildes.guilde_id = membres_personnages.personnage_guilde
					LEFT JOIN guildes AS guildes_liste ON guildes_liste.guilde_serveur = membres_personnages.membre_serveur_personnage
					WHERE personnage_id = :personnage AND membre_possesseur_id = :membre");
					$requete->bindValue(":personnage", $_GET["Personnage"], PDO::PARAM_INT);
					$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
					$requete->execute();
					
					$guildes = array();
					
					while($data = $requete->fetch()) {
						if(!empty($data["membre_nom_personnage"]))      $actualNom = stripslashes($data["membre_nom_personnage"]);
						else                                            $actualNom = "Erreur";
						
						$actualClasse = $data["membre_classe_personnage"];
						$actualNiveau = $data["membre_niveau_personnage"];
						$actualServeur = $data["membre_serveur_personnage"];
						$actualSexe = $data["membre_sexe_personnage"];
						$actualGuilde = $data["guilde_id_base"];
						$actualMetierActual = explode(";", $data["membre_personnage_metiers"]);
						
						$guildes[] = $data;
					}
					
					$actualMetier = array();
					$i = 0;
					foreach($actualMetierActual as $partie) {
						array_push($actualMetier, explode("-", $partie));
					}
					
					echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Modifier_Profil.php">Modifier mon profil</a> > <a href="?Action=Dofus">Mes personnages</a> > '.$actualNom.'</p>';
					
					echo '<p class="encadreInfos"><span class="right">Voici la page de gestion de vos personnages. Vous pouvez en créer, supprimer ou éditer à la limite de 8 personnages.</span></p>';
				
					if($valide == NULL) {
						if($requete->rowCount() > 0) { ?>
						
							<div class="bloc">
								<form action method="post">
									<p><label for="nom_personnage">Nom du personnage <span class="rouge">*</span> : </label><input type="text" name="nom_personnage" id="nom_personnage" value="<?php echo $actualNom ?>" /></p>
									<p>
										<label for="classe_personnage">Classe du personnage <span class="rouge">*</span> : </label>
										<select name="classe_personnage" id="classe_personnage">
											<?php
												foreach($classes as $classe) {
													if($classe == $actualClasse) {
														echo '<option value="'.$classe.'" selected="selected">'.$classe.'</option>';
													}
													else {
														echo '<option value="'.$classe.'">'.$classe.'</option>';
													}
												}
											?>
										</select>
									</p>
									<p><label for="niveau_personnage">Niveau du personnage <span class="rouge">*</span> : </label><input type="text" name="niveau_personnage" id="niveau_personnage" value="<?php echo $actualNiveau ?>" style="width:30px;" /></p>
									<p>
										<label for="sexe_personnage">Sexe du personnage <span class="rouge">*</span> : </label>
										<select name="sexe_personnage" id="sexe_personnage">
											<?php
											if($actualSexe == "Masculin") {
												echo '<option value="Masculin" selected="selected">Masculin</option>';
												echo '<option value="Féminin">Féminin</option>';
											}
											elseif($actualSexe == "Féminin") {
												echo '<option value="Masculin">Masculin</option>';
												echo '<option value="Féminin" selected="selected">Féminin</option>';
											}
											else {
												echo '<option value="Masculin">Masculin</option>';
												echo '<option value="Féminin">Féminin</option>';
											}
											?>
										</select>
									</p>
									<p>
										<label for="serveur_personnage">Serveur du personnage : </label>
										<select name="serveur_personnage" id="serveur_personnage">
											<optgroup label="Français">
												<?php
													foreach($serveurs_francais as $serveur) {
														if($serveur == $actualServeur) {
															echo '<option value="'.$serveur.'" selected="selected">'.$serveur.'</option>';
														}
														else {
															echo '<option value="'.$serveur.'">'.$serveur.'</option>';
														}
													}
												?>
											</optgroup>
											<optgroup label="Internationnal">
												<?php
													foreach($serveurs_internationnal as $serveur) {
														if($serveur == $actualServeur) {
															echo '<option value="'.$serveur.'" selected="selected">'.$serveur.'</option>';
														}
														else {
															echo '<option value="'.$serveur.'">'.$serveur.'</option>';
														}
													}
												?>
											</optgroup>
										</select>
									</p>
									<p>
									<label for="guilde_personnage">Guilde du personnage : </label>
										<select name="guilde_personnage" id="guilde_personnage">
											<?php
											foreach($guildes as $guilde) {
												if(!empty($actualServeur))   $add = '';
												else                         $add = '('.$data["guilde_serveur"].')';
												
												if($guilde["guilde_id"] == $actualGuilde) {
													echo '<option value="'.$guilde["guilde_id"].'" selected="selected">'.stripslashes($guilde["guilde_nom"]).' '.$add.'</option>';
												}
												else {
													echo '<option value="'.$guilde["guilde_id"].'">'.stripslashes($guilde["guilde_nom"]).' '.$add.'</option>';
												}
											}
											?>
										</select>
										<?php
										if(!empty($actualServeur))  echo 'Guildes sur votre serveur uniquement.';
										else                        echo 'Guildes de tous les serveurs (En parenthèses, le serveur).';
										?>
									</p>
									<p>
										<label for="metier_1">Premier Métier : </label><input type="text" name="metier_1" id="metier_1" value="<?php echo $actualMetier[0][0]; ?>" />
										<input type="text" name="metier_1_level" id="metier_1_level" value="<?php echo $actualMetier[0][1]; ?>" />
									</p>
									<p>
										<label for="metier_2">Second Métier : </label><input type="text" name="metier_2" id="metier_2" value="<?php echo $actualMetier[1][0]; ?>" />
										<input type="text" name="metier_2_level" id="metier_2_level" value="<?php echo $actualMetier[1][1]; ?>" />
									</p>
									<p>
										<label for="metier_3">Troisième Métier : </label><input type="text" name="metier_3" id="metier_3" value="<?php echo $actualMetier[2][0]; ?>" />
										<input type="text" name="metier_3_level" id="metier_3_level" value="<?php echo $actualMetier[2][1]; ?>" />
									</p>
									<p align="center">
										<a href="?Action=Dofus&amp;SupprimerPerso&amp;Personnage=<?php echo $_GET["Personnage"] ?>"><input type="button" class="reset" name="submit_dofus" value="Supprimer perso." /></a>
										<input type="submit" name="submit_dofus" value="Enregistrer" />
									</p>
								</form>
							</div>
						
					<?php 
						}
						else {
							array_push($erreurs, "Ce personnage n'est pas lié à votre compte.");
						}
					}
				}
				elseif(isset($_GET["NouveauPersonnage"])) {
					echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Modifier_Profil.php">Modifier mon profil</a> > <a href="?Action=Dofus">Mes personnages</a> > Création d\'un personnage</p>';
					
					echo '<p class="encadreInfos"><span class="right">Voici la page de gestion de vos personnages. Vous pouvez en créer, supprimer ou éditer à la limite de 8 personnages.</span></p>';
					
					if($valide == NULL) {
						$requete = $baseDeDonnes->prepare("
						SELECT * FROM membres_personnages
						WHERE membre_possesseur_id = :id");
						$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
						$requete->execute();
						
						if($requete->rowCount() <= 8) {
							$requete = $baseDeDonnes->query("
							SELECT * FROM guildes ORDER BY guilde_serveur DESC, guilde_niveau ASC");
							$requete->execute();
							?>
						
							<div class="bloc">
								<form action method="post">
									<p><label for="nom_personnage">Nom du personnage <span class="rouge">*</span> : </label><input type="text" name="nom_personnage" id="nom_personnage" /></p>
									<p>
										<label for="classe_personnage">Classe du personnage <span class="rouge">*</span> : </label>
										<select name="classe_personnage" id="classe_personnage">
											<?php
												foreach($classes as $classe) {
													echo '<option value="'.$classe.'">'.$classe.'</option>';
												}
											?>
										</select>
									</p>
									<p><label for="niveau_personnage">Niveau du personnage <span class="rouge">*</span> : </label><input type="text" name="niveau_personnage" id="niveau_personnage" style="width:30px;" /></p>
									<p>
										<label for="sexe_personnage">Sexe du personnage <span class="rouge">*</span> : </label>
										<select name="sexe_personnage" id="sexe_personnage">
											<?php
											echo '<option value="Masculin">Masculin</option>';
											echo '<option value="Féminin">Féminin</option>';
											?>
										</select>
									</p>
									<p>
										<label for="serveur_personnage">Serveur du personnage <span class="rouge">*</span> : </label>
										<select name="serveur_personnage" id="serveur_personnage">
											<optgroup label="Français">
												<?php
													foreach($serveurs_francais as $serveur) {
														echo '<option value="'.$serveur.'">'.$serveur.'</option>';
													}
												?>
											</optgroup>
											<optgroup label="Internationnal">
												<?php
													foreach($serveurs_internationnal as $serveur) {
														echo '<option value="'.$serveur.'">'.$serveur.'</option>';
													}
												?>
											</optgroup>
										</select>
									</p>
									<p>
									<label for="guilde_personnage">Guilde du personnage : </label>
										<select name="guilde_personnage" id="guilde_personnage">
											<?php
											echo '<option value="0">Aucune</option>';
											while($data = $requete->fetch()) {
												echo '<option value="'.$data["guilde_id"].'">'.stripslashes($data["guilde_nom"]).' ('.$data["guilde_serveur"].')</option>';
											}
											?>
										</select>
										Choisissez une guilde de votre serveur.
									</p>
									<p>
										<label for="metier_1">Premier Métier : </label><input type="text" name="metier_1" id="metier_1" />
										<input type="text" name="metier_1_level" id="metier_1_level" />
									</p>
									<p>
										<label for="metier_2">Second Métier : </label><input type="text" name="metier_2" id="metier_2" />
										<input type="text" name="metier_2_level" id="metier_2_level" />
									</p>
									<p>
										<label for="metier_3">Troisième Métier : </label><input type="text" name="metier_3" id="metier_3" />
										<input type="text" name="metier_3_level" id="metier_3_level" />
									</p>
									<p align="center"><input type="submit" name="submit_dofus_add" value="Enregistrer" /></p>
								</form>
							</div>
						
					<?php 
							}
							else {
								array_push($erreurs, "Le nombre maximum de personnage a été atteint.");
							}
						}
					}
					elseif(isset($_GET["SupprimerPerso"])) {
						$requete = $baseDeDonnes->prepare("
						SELECT COUNT(*) nb_persos
						FROM membres_personnages
						WHERE personnage_id = :personnage AND membre_possesseur_id = :membre");
						$requete->bindValue(":personnage", $_GET["Personnage"], PDO::PARAM_INT);
						$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
						$requete->execute();
						
						$data = $requete->fetch();
						
						echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Modifier_Profil.php">Modifier mon profil</a> > <a href="?Action=Dofus">Mes personnages</a> > Suppression d\'un personnage</p>';
						
						echo '<p class="encadreInfos"><span class="right">Voici la page de gestion de vos personnages. Vous pouvez en créer, supprimer ou éditer à la limite de 8 personnages.</span></p>';
						
						if($valide == NULL) {
							if($data["nb_persos"] == 1) {
								echo '
								<p class="ForumSend">
									<span class="titreSend"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png"> Attention</span>
									<span class="contenuSend">
										<span id="a">En supprimant ce personnage, vous ne pourrez pas revenir en arrière. Etes vous sûr ?</span>
										<span id="b" align="center"><a href="?Action=Dofus&amp;SupprimerPerso=Ok&amp;Personnage='.$_GET["Personnage"].'" class="choice">Oui</a><a href="?Action=Dofus" class="choice">Non</a></span>
									</span>
								</p>';
							}
							else {
								array_push($erreurs, "Ce personnage n'est pas lié à votre compte.");
							}
						}
					}
					else {
						echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Modifier_Profil.php">Modifier mon profil</a> > Mes personnages</p>';
						
						echo '<p class="encadreInfos"><span class="right">Voici la page de gestion de vos personnages. Vous pouvez en créer, supprimer ou éditer à la limite de 8 personnages.</span></p>';
						
						$requete = $baseDeDonnes->prepare("
						SELECT *
						FROM membres_personnages
						WHERE membre_possesseur_id = :id");
						$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
						$requete->execute();
						
						echo '<ul class="navigationPage">';
						while($data = $requete->fetch()) {
							echo '<a href="?Action=Dofus&amp;Personnage='.$data["personnage_id"].'"><li>'.$data["membre_nom_personnage"].'</li></a>';
						}
						echo '</ul>';
						
						if($requete->rowCount() < 8) {
							echo '<ul class="navigationPage">';
								echo '<a href="?Action=Dofus&amp;NouveauPersonnage"><li><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Ajouter.png" alt="" /> Créer un personnage</li></a>';
							echo '</ul>';
						}
					}
				break;
				
				case "Avatar": 
					echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Modifier_Profil.php">Modifier mon profil</a> > Changer son Avatar</p>';
					
					echo '<p class="encadreInfos"><span class="right">Vous pouvez ici changer ou supprimer votre avatar.
					<br />Sélectionnez une image sur votre ordinateur de type .png, .jpg, .jpeg, .gif ou .bmp, et de maximum 500ko. La taille recommandée pour votre image est de 100x100 pixels, sinon l\'image sera déformée.</span></p>';
						
					if($valide == NULL) {
						$requete = $baseDeDonnes->prepare("SELECT membre_avatar FROM membres WHERE membre_id = :id");
						$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
						$requete->execute();
						
						$data = $requete->fetch();

						if(!empty($data["membre_avatar"])) {
							$avatar = ABSPATH."Images/Avatars/".$data["membre_avatar"];
						}
						else {
							$avatar = ABSPATH."Images/Avatars/avatar_normal.png";
						}
						?>
						
						<div class="bloc">
							<form action method="post" enctype="multipart/form-data">
								<img src="<?php echo $avatar ?>" alt="" class="avatar image" />
								<p><label for="avatar">Choisir l'avatar : </label><input type="file" name="avatar" id="avatar" /></p>
								<div style="clear:left;"></div>
								<p align="center"><input type="submit" class="reset" name="supprimer_avatar" value="Supprimer" /><input type="submit" name="envoyer_avatar" value="Enregistrer" /></p>
							</form>
						</div>
						
					<?php }
				break;
				
				case "Style":
					echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Modifier_Profil.php">Modifier mon profil</a> > Modifier le Thème personnalisé</p>';
					
					echo '<p class="encadreInfos"><span class="right">Cette page vous permet de modifier votre Thème personnalisé afin de pouvoir l\'utiliser dans vos thèmes.</span></p>';
					
					if(isset($_POST["change_style"])) {
						$arrayThemes = array("Aucun", "Normal", "Noel", "Ete", "Ballotin", $PSEUDO_MEMBRE);
						
						if(in_array($_POST["change_style"], $arrayThemes)) {
							$requete = $baseDeDonnes->prepare("UPDATE membres SET membre_style = :theme WHERE membre_id = :id");
							$requete->bindValue(":theme", $_POST["change_style"], PDO::PARAM_STR);
							$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
							
							$requete->execute();
						}
					}
					
					if($valide == NULL) {
						$requete = $baseDeDonnes->prepare("SELECT membre_style FROM membres WHERE membre_id = :id");
						$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
						$requete->execute();
						
						$data = $requete->fetch();
						
						echo '<div class="bloc">';
							if(file_exists(ABSPATH.'Images/Themes/'.$PSEUDO_MEMBRE.'.png'))   $link = $PSEUDO_MEMBRE;
							else                                                              $link = $data["membre_style"];
							
							echo '<img src="'.ABSPATH.'Images/Themes/'.$link.'.png" style="width:735px; margin:5px;" alt="" id="imageThemeActuelModificationTheme" />';
						?>
						
							<form action method="post" enctype="multipart/form-data">
								<p><label for="avatar">Choisir l'image de thème : </label><input type="file" name="image_theme" id="image_theme" /></p>
								<p>Merci de choisir une image de 1000 pixels de largeur pour 230 pixels de hauteur.</p>
								<div style="clear:left;"></div>
								<p align="center"><input type="submit" class="reset" name="supprimer_image_theme" value="Supprimer" /><input type="submit" name="envoyer_image_theme" value="Enregistrer et Appliquer" /></p>
							</form>
						</div>
						
					<?php }
				break;
			}
		}
		elseif($_GET["Action"] == "Restauration") {
			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Modifier_Profil.png" alt="Modifier_Profil" /> Modifier mon Profil</h2>';
							
			echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Modifier_Profil.php">Modifier mon profil</a> > Restauration de Mot de Passe</p>';
			
			$information = "Cette page est accessible uniquement lorsque vous êtes déconnecté.";
		}
		else {
			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Modifier_Profil.png" alt="Modifier_Profil" /> Modifier mon Profil</h2>';
							
			echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="Modifier_Profil.php">Modifier mon profil</a></p>';
			
			array_push($erreurs, 'Cette page n\'existe pas.');
		}
	}
	else {
		echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Modifier_Profil.png" alt="Modifier_Profil" /> Modifier mon Profil</h2>';
						
		echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > Modifier mon profil</p>';
		
		echo '<p class="encadreInfos"><span class="right">Voici la page de modification de profil, pour vous permettre de personnaliser votre profil.</span></p>';
		?>
		<h3 class="troisieme_titre">Informations du compte</h3>
		
			<ul class="navigationPage">
				<a href="?Action=MotDePasse"><li><img src="<?php echo ABSPATH ?>Images/icones_pages/LittlesIcones/MotDePasse.png" alt="" /> Modifier le Mot de Passe</li></a>
				<a href="?Action=Email"><li><img src="<?php echo ABSPATH ?>Images/icones_pages/LittlesIcones/EmailEdit.png" alt="" /> Modifier son Adresse Email</li></a>
				<a href="?Action=Titre"><li><img src="<?php echo ABSPATH ?>Images/icones_pages/LittlesIcones/TitreEdit.png" alt="" /> Modifier son Titre</li></a>
				<a href="?Action=Signature"><li><img src="<?php echo ABSPATH ?>Images/icones_pages/LittlesIcones/Signature.png" alt="" /> Modifier sa Signature</li></a>
			</ul>
			
		<h3 class="troisieme_titre">Votre Profil</h3>
		
			<ul class="navigationPage">
				<a href="?Action=Citation"><li><img src="<?php echo ABSPATH ?>Images/icones_pages/LittlesIcones/Citation.png" alt="" /> Citation</li></a>
				<a href="?Action=Dofus"><li><img src="<?php echo ABSPATH ?>Images/icones_pages/LittlesIcones/Personnages.png" alt="" /> Mes personnages</li></a>
				<a href="?Action=Publiques"><li><img src="<?php echo ABSPATH ?>Images/icones_pages/LittlesIcones/Publiques.png" alt="" /> Informations publiques</li></a>
				<a href="?Action=Avatar"><li><img src="<?php echo ABSPATH ?>Images/icones_pages/LittlesIcones/Avatar.png" alt="" /> Avatar</li></a>
			</ul>

	<?php }
}
else {
	if(isset($_GET["Action"])) {
		if($_GET["Action"] != "Restauration") {
			echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Modifier_Profil.png" alt="Modifier_Profil" /> Modifier son Profil</h2>';
			echo '<div class="encadre_infos"><p><img src="'.ABSPATH.'Images/information.png" alt="Information" align="top" /> Vous n\'êtes pas connecté.</p></div>';
		}
	}
	else {
		echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Modifier_Profil.png" alt="Modifier_Profil" /> Modifier son Profil</h2>';
		echo '<div class="encadre_infos"><p><img src="'.ABSPATH.'Images/information.png" alt="Information" align="top" /> Vous n\'êtes pas connecté.</p></div>';
	}
}

if(isset($erreurs) AND $erreurs != NULL) {
	echo '<p class="encadreInfos">';
		echo '<span class="left erreur block"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreurs</span>';
		foreach($erreurs as $erreur_array) {
			echo '<span class="right">';
				echo $erreur_array;
			echo '</span>';
		}
	echo '</p>';
}
if(isset($valide) AND $valide != NULL) {
	echo '<p class="encadreInfos">';
		echo '<span class="left valide block"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Valide.png" alt="Confirmation" /> Confirmation <a href="Profil.php?id='.$ID_MEMBRE.'">Voir mon profil</a></span>';
		foreach($valide as $valide_array) {
			echo '<span class="right">';
				echo $valide_array;
			echo '</span>';
		}
	echo '</p>';
}
elseif(isset($information)) {
	echo '
	<p class="encadreInfos">
		<span class="left information"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Information.png" alt="Information" /> Information</span>
		<span class="right">'.$information.'</span>
	</p>';
}

include("../Footer.php"); ?>