<?php
require_once("../Includes/Fonctions.php");
require_once("../Includes/Config.php");

if(isset($_SESSION["id_membre"]) OR isset($_COOKIE["id_membre"])) {
	$valide = 'Vous avez correctement été déconnecté.';
	
	if($_SERVER['SERVER_NAME'] == 'localhost') {
		setcookie("id_membre", NULL, time()-1, "/");
		setcookie("pseudo", NULL, time()-1, "/");
		setcookie("password", NULL, time()-1, "/");
	}
	else {
		setcookie("id_membre", NULL, time()-1, "/", ".salixor.com");
		setcookie("pseudo", NULL, time()-1, "/", ".salixor.com");
		setcookie("password", NULL, time()-1, "/", ".salixor.com");
	}
	session_destroy();

	header("Location: ".$_SERVER["HTTP_REFERER"]."");
}
else {
	$information = "Vous n'êtes pas connecté.";
}

$titre_page_nom = "Déconnexion";
include("../Header.php");
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Deconnexion.png" alt="Déconnexion" /> Déconnexion</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH ?>">Accueil</a> > <a href="./">Membres</a> > Déconnexion</p>

<?php
if(isset($valide)) {
	echo '
	<p class="encadreInfos">
		<span class="left valide"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Valide.png" alt="Confirmation" /> Confirmation</span>
		<span class="right">'.$valide.'</span>
	</p>';
}
if(isset($information)) {
	echo '
	<p class="encadreInfos">
		<span class="left information"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Information.png" alt="Information" /> Information</span>
		<span class="right">'.$information.'</span>
	</p>';
}

include("../Footer.php"); ?>