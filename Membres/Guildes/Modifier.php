<?php 
$titre_page_nom = "Modifier une Guilde";
include("../../Header.php");
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Guildes.png" alt="Guildes" /> Modifier une guilde</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH ?>">Accueil</a> > <a href="../">Membres</a> > <a href="./">Guildes</a> > Modifier une guilde</p>

<?php
if(isset($ID_MEMBRE)) {
	$erreurs = array();

	$ListeExtension = array('jpg' => 'image/jpeg', 'jpeg' => 'image/jpeg', 'png' => 'image/png', 'gif' => 'image/gif');
	$ListeExtensionIE = array('jpg' => 'image/pjpg', 'jpeg'=>'image/pjpeg');
	$NouvelleLargeur = 220;

	if(isset($_POST["submit"])) {
		$requete = $baseDeDonnes->prepare("SELECT * FROM guildes WHERE guilde_id = :id AND guilde_createur = :membre");
		$requete->bindValue(":id", $_GET["Guilde"], PDO::PARAM_INT);
		$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
		$requete->execute();
		
		if($requete->rowCount() == 1) {
			if(!empty($_POST["niveau_guilde"]) && !empty($_POST["meneur_guilde"])) {
				if(!is_numeric($_POST["niveau_guilde"])) {
					array_push($erreurs, "Le niveau de la guilde n'est pas un nombre.");
					$_POST["niveau_guilde"] = "";
				}
				if(!is_http($_POST["pageperso_guilde"])) {
					array_push($erreurs, "Le lien vers la page perso de guilde est de forme incorrecte.");
					$_POST["pageperso_guilde"] = "";
				}
				if(!is_http($_POST["site_guilde"])) {
					array_push($erreurs, "Le lien vers le site de guilde est de forme incorrecte.");
					$_POST["site_guilde"] = "";
				}
				if(!is_http($_POST["forum_guilde"])) {
					array_push($erreurs, "Le lien vers le forum de guilde est de forme incorrecte.");
					$_POST["forum_guilde"] = "";
				}
				
				$requeteB = $baseDeDonnes->prepare("
				UPDATE guildes SET guilde_niveau = :niveau, guilde_meneur = :meneur, guilde_pageperso = :pageperso, guilde_site = :site, guilde_forum = :forum WHERE guilde_id = :id");
				$requeteB->bindValue(":niveau", $_POST["niveau_guilde"], PDO::PARAM_INT);
				$requeteB->bindValue(":meneur", $_POST["meneur_guilde"], PDO::PARAM_STR);
				$requeteB->bindValue(":pageperso", $_POST["pageperso_guilde"], PDO::PARAM_STR);
				$requeteB->bindValue(":site", $_POST["site_guilde"], PDO::PARAM_STR);
				$requeteB->bindValue(":forum", $_POST["forum_guilde"], PDO::PARAM_STR);
				$requeteB->bindValue(":id", $_GET["Guilde"], PDO::PARAM_INT);
				$requeteB->execute();
				
				$valide = "La guilde a bien été modifiée.";
				
				if(!empty($_FILES['embleme_guilde'])) {
					if($_FILES['embleme_guilde']['error'] <= 0) {
						if($_FILES['embleme_guilde']['size'] <= 500000) {
							$ImageGuilde = $_FILES['embleme_guilde']['name'];
							
							$ExtensionPresumee = explode('.', $ImageGuilde);
							$ExtensionPresumee = strtolower($ExtensionPresumee[count($ExtensionPresumee)-1]);
							if($ExtensionPresumee == 'jpg' || $ExtensionPresumee == 'jpeg' || $ExtensionPresumee == 'pjpg' || $ExtensionPresumee == 'pjpeg' || $ExtensionPresumee == 'gif' || $ExtensionPresumee == 'png') {
								$ImageGuilde = getimagesize($_FILES['embleme_guilde']['tmp_name']);
								
								if($ImageGuilde['mime'] == $ListeExtension[$ExtensionPresumee]  || $ImageGuilde['mime'] == $ListeExtensionIE[$ExtensionPresumee]) {
									if($ExtensionPresumee == 'jpg' || $ExtensionPresumee == 'jpeg' || $ExtensionPresumee == 'pjpg') {
										$ImageChoisie = imagecreatefromjpeg($_FILES['embleme_guilde']['tmp_name']);
									}
									if($ExtensionPresumee == 'gif')   $ImageChoisie = imagecreatefromgif($_FILES['embleme_guilde']['tmp_name']);
									if($ExtensionPresumee == 'png')   $ImageChoisie = imagecreatefrompng($_FILES['embleme_guilde']['tmp_name']);

									$TailleImageChoisie = getimagesize($_FILES['embleme_guilde']['tmp_name']);
									
									//Grand emblême
									$NouvelleHauteur = 100;
									$NouvelleLargeur = 100;
									
									$NouvelleImage = imagecreatetruecolor($NouvelleLargeur, $NouvelleHauteur) or die ("Erreur");

									$transparent = imagecolorallocate($ImageChoisie, 255, 255, 255);
									imagefill($ImageChoisie, 0, 0, $transparent);

									imagecopyresampled($NouvelleImage, $ImageChoisie, 0, 0, 0, 0, $NouvelleLargeur, $NouvelleHauteur, $TailleImageChoisie[0], $TailleImageChoisie[1]);

									//Mini emblême
									$NouvelleHauteur = 16;
									$NouvelleLargeur = 16;
									
									$NouvelleImageBis = imagecreatetruecolor($NouvelleLargeur, $NouvelleHauteur) or die ("Erreur");
									
									$transparent = imagecolorallocate($NouvelleImageBis, 255, 255, 255);
									imagefill($NouvelleImageBis, 0, 0, $transparent);
									
									imagecopyresampled($NouvelleImageBis, $ImageChoisie, 0, 0, 0, 0, $NouvelleLargeur, $NouvelleHauteur, $TailleImageChoisie[0], $TailleImageChoisie[1]);

									$ImageGuilde = $_FILES['embleme_guilde']['name'];
									
									$data = $requete->fetch();
									
									if(file_exists('../../Images/Guildes/'.stripslashes($data["guilde_nom"]).'-16x16.png')) {
										unlink('../../Images/Guildes/'.stripslashes($data["guilde_nom"]).'-16x16.png');
									}
									if(file_exists('../../Images/Guildes/'.stripslashes($data["guilde_nom"]).'-100x100.png')) {
										unlink('../../Images/Guildes/'.stripslashes($data["guilde_nom"]).'-100x100.png');
									}
									
									imagepng($NouvelleImageBis, '../../Images/Guildes/'.stripslashes($data["guilde_nom"]).'-16x16.png');
									imagepng($ImageChoisie, '../../Images/Guildes/'.stripslashes($data["guilde_nom"]).'-100x100.png');
									
									imagedestroy($ImageChoisie);
								}
								else {
									array_push($erreurs, "Une erreur est survenue lors de l'enregistrement de l'emblême.");
								}
							}
							else {
								array_push($erreurs, "Le format de l'emblême de guilde est incorrect.");
							}
						}
						else {
							array_push($erreurs, "L'emblême de guilde est trop lourd.");
						}
					}
					else {
						array_push($erreurs, "Une erreur est survenue lors de l'enregistrement de l'emblême.");
					}
				}
			}
			else {
				echo '<p class="encadreInfos erreur"><span class="right">Un ou plusieurs des champs obligatoires n\'ont pas été remplis.</span></p>';
			}
		}
		else {
			echo '<p class="encadreInfos erreur"><span class="right">La guilde indiquée n\'existe pas ou ce n\'est pas vous qui l\'avez ajoutée.</span></p>';
		}
	}
	
	if(!isset($_GET["Guilde"])) {
		echo
		'<p class="encadreInfos">
		<span class="right">Vous pouvez sur cette page modifier une des guildes que vous avez ajoutées au site.</span>
		</p>';
		
		$requete = $baseDeDonnes->prepare("SELECT * FROM guildes WHERE guilde_createur = :id");
		$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
		$requete->execute();
		
		if($requete->rowCount() > 0) {
			echo '<div class="listeModifierGuilde">';
				while($data = $requete->fetch()) {
					if(file_exists('../../Images/Guildes/'.$data["guilde_nom"].'-16x16.png')) {
						echo '<span class="aGuilde"><img src="../../Images/Guildes/'.$data["guilde_nom"].'-16x16.png" alt="" /> <b>'.stripslashes($data["guilde_nom"]).'</b> - Niveau '.$data["guilde_niveau"].'<a class="modif" href="?Guilde='.$data["guilde_id"].'">Modifier</a></span>';
					}
					else {
						echo '<span class="aGuilde"><img src="../../Images/Guildes/BlasonGuilde-16x16.png" alt="" /> <b>'.stripslashes($data["guilde_nom"]).'</b> - Niveau '.$data["guilde_niveau"].'<a class="modif" href="?Guilde='.$data["guilde_id"].'">Modifier</a></span>';
					}
				}
			echo '</div>';
		}
		else {
			echo '<p class="encadreInfos information"><span class="right">Vous n\'avez pas ajouté de guilde.</span></p>';
		}
	}
	else {
		$requete = $baseDeDonnes->prepare("SELECT * FROM guildes WHERE guilde_id = :id AND guilde_createur = :membre");
		$requete->bindValue(":id", $_GET["Guilde"], PDO::PARAM_INT);
		$requete->bindValue(":membre", $ID_MEMBRE, PDO::PARAM_INT);
		$requete->execute();
		
		if($requete->rowCount() == 1) {
			if(count($erreurs) > 0) {
				echo '<p class="encadreInfos erreur"><span class="right">';
				foreach($erreurs as $erreur) {
					echo '- '.$erreur.'<br />';
				}
				echo '</span></p>';
			}

			if(isset($valide)) {
				$add = '';
				if(count($erreurs) > 0)   $add = ' Vous pourrez rectifier les erreurs ci-dessus dans la gestion de guilde.';
				echo '<p class="encadreInfos valide"><span class="right">'.$valide.$add.'</span></p>';
				
				echo '
				<p align="center">
					<a href="'.ABSPATH.'Membres/Guildes/Voir.php?Guilde='.$_GET["Guilde"].'" class="allNews">
						<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Publiques.png" alt="" /> Voir la guilde
					</a>
				</p>';
			}

			if(!isset($valide)) {		
				$data = $requete->fetch();
				
				echo '<div class="bloc">';
					echo '<form method="post" enctype="multipart/form-data">';
						echo '<p><label for="niveau_guilde">Niveau de la guilde <span class="rouge">*</span> : </label><input type="text" name="niveau_guilde" id="niveau_guilde" value="'.$data["guilde_niveau"].'" /></p>';
						echo '<p><label for="meneur_guilde">Meneur de la guilde <span class="rouge">*</span> : </label><input type="text" name="meneur_guilde" id="meneur_guilde" value="'.stripslashes($data["guilde_meneur"]).'" /></p>';
						
						echo '<p><label for="pageperso_guilde">Page perso de guilde : </label><input type="text" name="pageperso_guilde" id="pageperso_guilde" value="'.stripslashes($data["guilde_pageperso"]).'" /></p>';
						echo '<p><label for="site_guilde">Site de guilde : </label><input type="text" name="site_guilde" id="site_guilde" value="'.stripslashes($data["guilde_site"]).'"  /></p>';
						echo '<p><label for="forum_guilde">Forum de guilde : </label><input type="text" name="forum_guilde" id="forum_guilde" value="'.stripslashes($data["guilde_forum"]).'"  /></p>';
						
						echo '<p><label for="embleme_guilde">Nouvel emblême ? </label><input type="file" name="embleme_guilde" id="embleme_guilde" /></p>';
						
						echo '<p align="center"><input type="submit" value="Modifier" name="submit" /></p>';
					echo '</form>';
				echo '</div>';
			}
		}
		else {
			echo '<p class="encadreInfos erreur"><span class="right">La guilde indiquée n\'existe pas ou ce n\'est pas vous qui l\'avez ajoutée.</span></p>';
		}
	}
}
else {
	echo '<p class="encadreInfos information"><span class="right">Vous devez être connecté pour accéder à cette fonctionnalité.</span></p>';
}

include("../../Footer.php"); ?>