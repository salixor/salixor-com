<?php 
$titre_page_nom = "Liste des Guildes";
include("../../Header.php");
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Guildes.png" alt="Guildes" /> Liste des Guildes</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH ?>">Accueil</a> > <a href="./">Membres</a> > Guildes</p>

<p class="encadreInfos"><span class="right">Voici la liste des guildes ayant été ajoutées par des membres au site.</span></p>

<?php
$serveurs = array("Agride", "Allister", "Amayiro", "Bowisse", "Brumaire", "Crocoburio", "Danathor", "Djaul", "Domen", "Farle", "Goultard", "Hecate", "Hel Munster", "Helsephine", "Hyrkul", "Jiva", "Kuri", "Li Crounch", "Lily", "Maimane", "Many", "Menalt", "Mylaise", "Otomaï", "Pouchecot", "Raval", "Rykke-Errel", "Silouate", "Silvosse", "Sumens", "Ulette", "Vil Smisse", "Aermyne", "Oto-Mustam", "Rosal", "Rushu", "Solar", "Zatoïshwan");

$requete = $baseDeDonnes->query("
SELECT membres.*, guildes.* FROM guildes
LEFT JOIN membres ON membres.membre_id = guildes.guilde_createur
ORDER BY guilde_serveur ASC, guilde_niveau DESC, guilde_id ASC");
$requete->setFetchMode(PDO::FETCH_OBJ);

$serveur = "";
$i = 0;
$j = 1;
$a = 0;

while($data = $requete->fetch()) {
	if($j == 1 || $j == 14 || $j == 27)     echo '<div class="guildes">';
	
	if($serveur != $data->guilde_serveur) {
		if($j > 1)   echo '</div>';
		$i = 0;
		
		$serveur = $data->guilde_serveur;
		
		$imageServeur = strtr($serveur, $GLOBALS['normalizeChars']);
		$imageServeur = preg_replace("#\s#", "", $serveur);
		$imageServeur = preg_replace("#ï#", "i", $imageServeur);
		
		$imageServeur = '<img src="'.ABSPATH.'Images/Serveurs/'.$imageServeur.'.png" />';
		
		echo '<div class="boutonGuilde on" rel="'.$j.'">'.$imageServeur.$data->guilde_serveur.'</div>';
		
		foreach($serveurs as $indice => $serveurA) {
			if($serveurA == $serveur)    unset($serveurs[$indice]);
		}
		
		$a = $j;
		$j++;
	}
	
	if($i == 0)  echo '<div class="listeGuildes" id="openGuilde-'.$a.'">';
		echo '<div class="ligne">';
		if(file_exists('../../Images/Guildes/'.$data->guilde_nom.'-16x16.png'))
			echo '<img src="'.ABSPATH.'Images/Guildes/'.$data->guilde_nom.'-16x16.png" alt="" /> ';
		else
			echo '<img src="'.ABSPATH.'Images/Guildes/BlasonGuilde-16x16.png" alt="" /> ';
		echo '<a href="Voir.php?Guilde='.$data->guilde_id.'"><span class="nom">'.tronquer($data->guilde_nom, 25, " ...").'</span></a><span class="niveau">Niv. '.$data->guilde_niveau.'</span></div>';
	
	if($j == 13 || $j == 26 || $j == 38)     echo '</div>';
	
	$i++;
}
		
foreach($serveurs as $serveur) {
	if($i != 0)     echo '</div>';
	
	if($j == 1 || $j == 14 || $j == 27)     echo '<div class="guildes">';
	
	$imageServeur = strtr($serveur, $GLOBALS['normalizeChars']);
	$imageServeur = preg_replace("#\s#", "", $serveur);
	$imageServeur = preg_replace("#ï#", "i", $imageServeur);
	
	$imageServeur = '<img src="'.ABSPATH.'Images/Serveurs/'.$imageServeur.'.png" />';

	echo '<div class="boutonGuilde on" rel="'.$j.'">'.$imageServeur.$serveur.'</div>';
	echo '<div class="listeGuildes" id="openGuilde-'.$j.'"><div class="ligne"><img src="'.ABSPATH.'Images/General/None.png" alt="" /> Aucune guilde sur ce serveur.</div></div>';
	
	if($j == 13 || $j == 26 || $j == 38)     echo '</div>';
	
	$j++;
	$i = 0;
}

echo '<h2 class="titreSecond" style="clear:left;"><img src="'.ABSPATH.'Images/icones_pages/Ajouter.png" alt="Ajouter" /> Ajouter une Guilde</h2>';

if(!isset($ID_MEMBRE)) {
	echo
	'<p class="encadreInfos">
		<span class="right">
			Avant d\'ajouter votre guilde, vous devez vous connecter ou vous inscrire.
		</span>
	</p>';
	
	echo '
	<p align="center">
		<a href="javascript:connexion();" class="allNews">
			<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Connexion.png" alt="" class="quimby_search_image"> Connexion
		</a>
		
		<a href="'.ABSPATH.'Membres/Inscription.php" class="allNews">
			<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Inscription.png" alt="" class="quimby_search_image"> Inscription
		</a>
	</p>';
	
	echo
	'<p class="encadreInfos">
		<span class="right">
			Après avoir ajouté votre guilde, vous aurez accès à des fonctionnalités comme des news de guildes ou une gestion des membres de la guilde.
		</span>
	</p>';
}
else {
	echo
	'<p class="encadreInfos">
		<span class="right">
			Pour ajouter votre guilde, il suffit de cliquer et de suivre les consignes !
		</span>
	</p>';
	
	echo '
	<p align="center">
		<a href="'.ABSPATH.'Membres/Guildes/Ajout.php" class="allNews">
			<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/GuildeAjout.png" alt="" /> Ajouter une guilde
		</a>
	</p>';

	echo
	'<p class="encadreInfos">
		<span class="right">
			Après avoir ajouté votre guilde, vous aurez accès à des fonctionnalités comme des news de guildes ou une gestion des membres de la guilde.
		</span>
	</p>';
}

include("../../Footer.php"); ?>