<?php 
$titre_page_nom = "Ajouter une Guilde";
include("../../Header.php");
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Guildes.png" alt="Guildes" /> Ajouter une guilde</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH ?>">Accueil</a> > <a href="../">Membres</a> > <a href="./">Guildes</a> > Ajouter une guilde</p>

<?php
$erreurs = array();

$ListeExtension = array('jpg' => 'image/jpeg', 'jpeg' => 'image/jpeg', 'png' => 'image/png', 'gif' => 'image/gif');
$ListeExtensionIE = array('jpg' => 'image/pjpg', 'jpeg'=>'image/pjpeg');
$NouvelleLargeur = 220;

if(isset($_POST["submit"])) {
	if(!empty($_POST["nom_guilde"]) && !empty($_POST["niveau_guilde"]) && !empty($_POST["meneur_guilde"]) && !empty($_POST["serveur_guilde"])) {
		if(!is_numeric($_POST["niveau_guilde"])) {
			array_push($erreurs, "Le niveau de la guilde n'est pas un nombre.");
			$_POST["niveau_guilde"] = "";
		}
		if(!is_http($_POST["pageperso_guilde"])) {
			array_push($erreurs, "Le lien vers la page perso de guilde est de forme incorrecte.");
			$_POST["pageperso_guilde"] = "";
		}
		if(!is_http($_POST["site_guilde"])) {
			array_push($erreurs, "Le lien vers le site de guilde est de forme incorrecte.");
			$_POST["site_guilde"] = "";
		}
		if(!is_http($_POST["forum_guilde"])) {
			array_push($erreurs, "Le lien vers le forum de guilde est de forme incorrecte.");
			$_POST["forum_guilde"] = "";
		}
		
		$requete = $baseDeDonnes->prepare("
		INSERT INTO guildes(guilde_createur, guilde_nom, guilde_niveau, guilde_meneur, guilde_serveur, guilde_pageperso, guilde_site, guilde_forum)
		VALUES(:createur, :nom, :niveau, :meneur, :serveur, :pageperso, :site, :forum)");
		$requete->bindValue(":createur", $ID_MEMBRE, PDO::PARAM_INT);
		$requete->bindValue(":nom", $_POST["nom_guilde"], PDO::PARAM_STR);
		$requete->bindValue(":niveau", $_POST["niveau_guilde"], PDO::PARAM_INT);
		$requete->bindValue(":meneur", $_POST["meneur_guilde"], PDO::PARAM_STR);
		$requete->bindValue(":serveur", $_POST["serveur_guilde"], PDO::PARAM_STR);
		$requete->bindValue(":pageperso", $_POST["pageperso_guilde"], PDO::PARAM_STR);
		$requete->bindValue(":site", $_POST["site_guilde"], PDO::PARAM_STR);
		$requete->bindValue(":forum", $_POST["forum_guilde"], PDO::PARAM_STR);
		$requete->execute();
		
		$lastId = $baseDeDonnes->lastInsertId();

		$valide = "La guilde a bien été crée.";
		
		if(!empty($_FILES['embleme_guilde'])) {
			if($_FILES['embleme_guilde']['error'] <= 0) {
				if($_FILES['embleme_guilde']['size'] <= 500000) {
					$ImageGuilde = $_FILES['embleme_guilde']['name'];
					
					$ExtensionPresumee = explode('.', $ImageGuilde);
					$ExtensionPresumee = strtolower($ExtensionPresumee[count($ExtensionPresumee)-1]);
					if($ExtensionPresumee == 'jpg' || $ExtensionPresumee == 'jpeg' || $ExtensionPresumee == 'pjpg' || $ExtensionPresumee == 'pjpeg' || $ExtensionPresumee == 'gif' || $ExtensionPresumee == 'png') {
						$ImageGuilde = getimagesize($_FILES['embleme_guilde']['tmp_name']);
						
						if($ImageGuilde['mime'] == $ListeExtension[$ExtensionPresumee]  || $ImageGuilde['mime'] == $ListeExtensionIE[$ExtensionPresumee]) {
							if($ExtensionPresumee == 'jpg' || $ExtensionPresumee == 'jpeg' || $ExtensionPresumee == 'pjpg') {
								$ImageChoisie = imagecreatefromjpeg($_FILES['embleme_guilde']['tmp_name']);
							}
							if($ExtensionPresumee == 'gif')   $ImageChoisie = imagecreatefromgif($_FILES['embleme_guilde']['tmp_name']);
							if($ExtensionPresumee == 'png')   $ImageChoisie = imagecreatefrompng($_FILES['embleme_guilde']['tmp_name']);

							$TailleImageChoisie = getimagesize($_FILES['embleme_guilde']['tmp_name']);
							
							//Grand emblême
							$NouvelleHauteur = 100;
							$NouvelleLargeur = 100;
							
							$NouvelleImage = imagecreatetruecolor($NouvelleLargeur, $NouvelleHauteur) or die ("Erreur");

							$transparent = imagecolorallocate($ImageChoisie, 255, 255, 255);
							imagefill($ImageChoisie, 0, 0, $transparent);

							imagecopyresampled($NouvelleImage, $ImageChoisie, 0, 0, 0, 0, $NouvelleLargeur, $NouvelleHauteur, $TailleImageChoisie[0], $TailleImageChoisie[1]);

							//Mini emblême
							$NouvelleHauteur = 16;
							$NouvelleLargeur = 16;
							
							$NouvelleImageBis = imagecreatetruecolor($NouvelleLargeur, $NouvelleHauteur) or die ("Erreur");
							
							$transparent = imagecolorallocate($NouvelleImageBis, 255, 255, 255);
							imagefill($NouvelleImageBis, 0, 0, $transparent);
							
							imagecopyresampled($NouvelleImageBis, $ImageChoisie, 0, 0, 0, 0, $NouvelleLargeur, $NouvelleHauteur, $TailleImageChoisie[0], $TailleImageChoisie[1]);

							$ImageGuilde = $_FILES['embleme_guilde']['name'];
							
							imagepng($NouvelleImageBis, '../../Images/Guildes/'.$_POST["nom_guilde"].'-16x16.png');
							imagepng($ImageChoisie, '../../Images/Guildes/'.$_POST["nom_guilde"].'-100x100.png');
							
							imagedestroy($ImageChoisie);
						}
						else {
							array_push($erreurs, "Une erreur est survenue lors de l'enregistrement de l'emblême.");
						}
					}
					else {
						array_push($erreurs, "Le format de l'emblême de guilde est incorrect.");
					}
				}
				else {
					array_push($erreurs, "L'emblême de guilde est trop lourd.");
				}
			}
			else {
				array_push($erreurs, "Une erreur est survenue lors de l'enregistrement de l'emblême.");
			}
		}
	}
	else {
		echo '<p class="encadreInfos erreur"><span class="right">Un ou plusieurs des champs obligatoires n\'ont pas été remplis.</span></p>';
	}
}
else {
	echo
	'<p class="encadreInfos">
	<span class="right">Vous pouvez sur cette page ajouter une guilde au site, en renseignant les champs demandés.<br />Une fois cette opération effectuée, vous pourrez gérer la guilde.</span>
	</p>';
}

if(count($erreurs) > 0) {
	echo '<p class="encadreInfos erreur"><span class="right">';
	foreach($erreurs as $erreur) {
		echo '- '.$erreur.'<br />';
	}
	echo '</span></p>';
}

if(isset($valide)) {
	$add = '';
	if(count($erreurs) > 0)   $add = ' Vous pourrez rectifier les erreurs ci-dessus dans la gestion de guilde.';
	echo '<p class="encadreInfos valide"><span class="right">'.$valide.$add.'</span></p>';
	
	echo '
	<p align="center">
		<a href="'.ABSPATH.'Membres/Guildes/Voir.php?Guilde='.$lastId.'" class="allNews">
			<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Publiques.png" alt="" /> Voir la guilde
		</a>
		<a href="'.ABSPATH.'Membres/Guildes/Editer.php?Guilde='.$lastId.'" class="allNews">
			<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Modifier.png" alt="" /> Modifier la guilde
		</a>
	</p>';
}

if(isset($ID_MEMBRE)) {
	if(!isset($valide)) {
		$serveurs_francais = array("Agride", "Allister", "Amayiro", "Bowisse", "Brumaire", "Crocoburio", "Danathor", "Djaul", "Domen", "Farle", "Goultard", "Hecate", "Hel Munster", "Helsephine", "Hyrkul", "Jiva", "Kuri", "Li Crounch", "Lily", "Maimane", "Many", "Menalt", "Mylaise", "Otomaï", "Pouchecot", "Raval", "Rykke-Errel", "Silouate", "Silvosse", "Sumens", "Ulette", "Vil Smisse");
		$serveurs_internationnal = array("Aermyne", "Oto-Mustam", "Rosal", "Rushu", "Solar", "Zatoïshwan");

		echo '<div class="bloc">';
			echo '<form method="post" enctype="multipart/form-data">';
				echo '<p><label for="nom_guilde">Nom de la guilde <span class="rouge">*</span> : </label><input type="text" name="nom_guilde" id="nom_guilde" /></p>';
				echo '<p><label for="niveau_guilde">Niveau de la guilde <span class="rouge">*</span> : </label><input type="text" name="niveau_guilde" id="niveau_guilde" /></p>';
				echo '<p><label for="meneur_guilde">Meneur de la guilde <span class="rouge">*</span> : </label><input type="text" name="meneur_guilde" id="meneur_guilde" /></p>';
				
				echo '<p><label for="serveur_guilde">Serveur de la guilde <span class="rouge">*</span> : </label>
					<select name="serveur_guilde" id="serveur_guilde">
						<optgroup label="Français">';
							foreach($serveurs_francais as $serveur) {
								if($serveur == $actualServeur) {
									echo '<option value="'.$serveur.'" selected="selected">'.$serveur.'</option>';
								}
								else {
									echo '<option value="'.$serveur.'">'.$serveur.'</option>';
								}
							}
						echo '
						</optgroup>
						<optgroup label="Internationnal">';
							foreach($serveurs_internationnal as $serveur) {
								if($serveur == $actualServeur) {
									echo '<option value="'.$serveur.'" selected="selected">'.$serveur.'</option>';
								}
								else {
									echo '<option value="'.$serveur.'">'.$serveur.'</option>';
								}
							}
						echo
						'</optgroup>
					</select>
				</p>';
				
				echo '<p><label for="pageperso_guilde">Page perso de guilde : </label><input type="text" name="pageperso_guilde" id="pageperso_guilde" /></p>';
				echo '<p><label for="site_guilde">Site de guilde : </label><input type="text" name="site_guilde" id="site_guilde" /></p>';
				echo '<p><label for="forum_guilde">Forum de guilde : </label><input type="text" name="forum_guilde" id="forum_guilde" /></p>';
				
				echo '<p><label for="embleme_guilde">Choisir l\'emblême : </label><input type="file" name="embleme_guilde" id="embleme_guilde" /></p>';
				
				echo '<p align="center"><input type="submit" value="Ajouter" name="submit" /></p>';
			echo '</form>';
		echo '</div>';
	}
}
else {
	echo '<p class="encadreInfos information"><span class="right">Vous devez être connecté pour accéder à cette fonctionnalité.</span></p>';
}

include("../../Footer.php"); ?>