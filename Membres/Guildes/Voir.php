<?php 
if(isset($_GET["Guilde"])) {
	require_once("../../Includes/Fonctions.php");
	require_once("../../Includes/Config.php");
	
	$requete = $baseDeDonnes->prepare("
	SELECT guildes.*, membre_ajouteur.membre_id AS membre_ajouteur_id, membre_ajouteur.membre_pseudo AS membre_ajouteur_pseudo, membres_personnages.* FROM guildes
	LEFT JOIN membres AS membre_ajouteur ON membre_ajouteur.membre_id = guildes.guilde_createur
	LEFT JOIN membres_personnages ON membres_personnages.personnage_guilde = guildes.guilde_id
	WHERE guildes.guilde_id = :id");
	$requete->bindValue(":id", $_GET["Guilde"], PDO::PARAM_INT);
	$requete->execute();
	
	$membres = array();
	
	if($requete->rowCount() > 0) {
		while($data = $requete->fetch()) {
			$guildeCreateur = $data["guilde_createur"];
		
			$guildeNom = $data["guilde_nom"];
			$guildeServeur = $data["guilde_serveur"];
			$guildeLvl = $data["guilde_niveau"];
			
			$guildePagePerso = $data["guilde_pageperso"];
			$guildeSite = $data["guilde_site"];
			$guildeForum = $data["guilde_forum"];
			
			$meneurGuilde = $data["guilde_meneur"];
			
			$membreAjouteurID = $data["membre_ajouteur_id"];
			$membreAjouteurPSEUDO = $data["membre_ajouteur_pseudo"];
			
			$membres[] = $data;
		}

		$titre_page_nom = $guildeNom;
		include("../../Header.php");

		echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Guildes.png" alt="Guildes" /> '.$guildeNom.'</h2>';
		echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="./">Guildes</a> > '.$guildeNom.'</p>';

		echo '<div class="avatarMembre">';
			echo '<h4 class="quatrieme_titre">La guilde</h4>';
			
			echo '<div class="zoneAvatar">';
				if(file_exists('../../Images/Guildes/'.$guildeNom.'-100x100.png'))   echo '<img src="'.ABSPATH.'Images/Guildes/'.$guildeNom.'-100x100.png" alt="" />';
				else                                                                 echo '<img src="'.ABSPATH.'Images/Guildes/BlasonGuilde-100x100.png" alt="" />';
				
				if(isset($ID_MEMBRE)) {
					if($ID_MEMBRE == $guildeCreateur)  echo '<a href="Modifier.php?Guilde='.$_GET["Guilde"].'" class="buttons buttonEdit"></a>';
					if($ID_MEMBRE == $guildeCreateur)  echo '<a href="News.php?Action=Ajouter&amp;Guilde='.$_GET["Guilde"].'" class="buttons buttonNews"></a>';
				}
			echo '</div>';
			
			echo '<div class="infosConnexion">';
				echo '<div><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Ajouter.png" alt="" style="margin:2px 0 -2px;" /> Ajoutée par <a href="../Profil.php?id='.$membreAjouteurID.'">'.$membreAjouteurPSEUDO.'.</a></div>';
				echo '<div><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Arrow.png" alt="" style="margin:-2px 4px 2px;" /> <b>Serveur :</b> '.$guildeServeur.'</div>';
				echo '<div><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Asterisque.png" alt="" style="margin:2px 0 -2px;" /> Niveau '.$guildeLvl.'</div>';
				
				if(!empty($meneurGuilde))
					echo '<div><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Valide.png" alt="" style="margin:2px 0 -2px;" /> <b>Meneur :</b> '.$meneurGuilde.'</div>';
				
				echo '<br />';
				
				if(!empty($guildePagePerso))
					echo '<div><a href="'.$guildePagePerso.'"><img src="'.ABSPATH.'Images/General/Hasard.png" alt="" style="margin:2px 0 -2px;" /> Page perso de guilde</a></div>'
					;
				if(!empty($guildeSite))
					echo '<div><a href="'.$guildeSite.'"><img src="'.ABSPATH.'Images/Membres/WebMembre.png" alt="" style="margin:2px 0 -2px;" /> Site de guilde</a></div>';
					
				if(!empty($guildeForum))
					echo '<div><a href="'.$guildeForum.'"><img src="'.ABSPATH.'Images/Membres/MembreComment.png" alt="" style="margin:2px 0 -2px;" /> Forum de guilde</a></div>';
			echo '</div>';
		echo '</div>';
		
		echo '<div class="membresGuilde">';
			echo '<h4 class="quatrieme_titre">Les membres</h4>';
			
			foreach($membres as $membre) {
				if(!empty($membre["membre_classe_personnage"])) {
					$classe = stripslashes($membre["membre_classe_personnage"]);
					$sexe = stripslashes($membre["membre_sexe_personnage"]);
					
					$sexeB = strtolower(substr($sexe, 0, 1));
					$classeB = strtolower(strtr($classe, $GLOBALS['normalizeChars']));
					$image = strtolower($classeB).'-'.$sexeB.'.png';
					
					echo '
					<a href="../Profil.php?id='.$membre["membre_possesseur_id"].'&amp;Personnage='.$membre["personnage_id"].'">
						<span class="aPersonnage"><img src="'.ABSPATH.'Images/Personnages/'.$image.'" alt="" /> '.$membre["membre_nom_personnage"].'</span>
					</a>';
				}
				else {
					echo '<span class="aPersonnage"><img src="'.ABSPATH.'Images/icones_pages/Administration.png" style="height:25px; width:25px; margin-bottom:-11px; padding-bottom:5px;" alt="" /> Aucun membre</span>';
				}
			}
		echo '</div>';
		
		echo '<div style="clear:left"></div>';
		
		$requete = $baseDeDonnes->prepare("
		SELECT guildes_news.*, membres.membre_pseudo
		FROM guildes_news
		LEFT JOIN membres ON guildes_news.id_posteur = membre_id
		WHERE id_guilde = :id ORDER BY guildes_news.id DESC");
		$requete->bindValue(":id", $_GET["Guilde"], PDO::PARAM_INT);
		$requete->execute();

		if($requete->rowCount() > 0) {
			echo '<h3 class="troisieme_titre">News de guilde</h3>';
			
			while($data = $requete->fetch()) {
				$titreNews     =   stripslashes(utf8_encode($data['titre']));
				$dateEdit      =   $data['edit_date']; 
				$dateNews      =   $data['date']; 
				$contenuNews   =   formatage(smileys(nl2br(stripslashes($data['contenu']))));
				$idNews        =   intval($data['id']);
				
				$posteur       =   stripslashes($data["membre_pseudo"]);

				$date = afficherDate($dateNews);
			
				echo '<div class="news">';
					echo '<div class="head" style="padding:0 10px;">';
						echo '<h4><a href="'.ABSPATH.'?News='.$idNews.'&amp;Titre='.optimiser_titre_et_classe($titreNews).'#News">'.$titreNews.'</a></h4>';
						echo '<div class="infos">';
							if($dateEdit == 0)    echo '<span class="date"><img src="'.ABSPATH.'Images/Forum/Time.png" alt="" /> Écrite le '.$date.'</span>';
							else                  echo '<span class="date"><img src="'.ABSPATH.'Images/Forum/Edition.png" alt="" /> Éditée le '.convertirTimestamp($dateEdit).'</span>';
							
							echo '
							<span class="tag"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Inscription.png" alt="" /> Écrite par '.$posteur.'</span>';
						echo '</div>';
					echo '</div>';
					
					echo '<div class="contenu" style="padding:0 5px 5px;">';
						echo $contenuNews;
					echo '</div>';

					echo '<div style="clear:left;"></div>';
				echo '</div>';
			}
		}
	}
	//Guilde inexistante
	else {
		$titre_page_nom = "Erreur";
		include("../../Header.php");
		
		echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Guildes.png" alt="Guildes" /> Voir une guilde</h2>';
		echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="./">Guildes</a> > Voir une guilde</p>';
		echo '<p class="encadreInfos erreur"><span class="right">La guilde recherchée n\'existe pas.</span></p>';
	}
}
else {
	$titre_page_nom = "Erreur";
	include("../../Header.php");
	
	echo '<h2 class="titreSecond"><img src="'.ABSPATH.'Images/icones_pages/Guildes.png" alt="Guildes" /> Voir une guilde</h2>';
	echo '<p class="Arianne">> <a href="'.ABSPATH.'">Accueil</a> > <a href="./">Membres</a> > <a href="./">Guildes</a> > Voir une guilde</p>';
	echo '<p class="encadreInfos erreur"><span class="right">Aucune guilde n\'a été indiquée.</span></p>';
}

include("../../Footer.php");	
?>