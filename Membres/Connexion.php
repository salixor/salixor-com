<?php 
require_once("../Includes/Fonctions.php");
require_once("../Includes/Config.php");

$erreurs = array();

if(isset($ID_MEMBRE)) {
	$information = "Vous êtes déjà connecté.";
}
elseif(isset($_POST["submit"])) {
	if(!empty($_POST["pseudo"]) AND !empty($_POST["password"])) {
		$adresse_redirection = $_POST["adresse_redirection"];

		$requete = $baseDeDonnes->prepare("SELECT * FROM membres WHERE membre_pseudo = :pseudo AND membre_mdp = :password");
		$requete->bindValue(":pseudo", $_POST["pseudo"], PDO::PARAM_STR);
		$requete->bindValue(":password", sha1($_POST["password"]), PDO::PARAM_STR);
		$requete->execute();
		
		$data = $requete->fetch();
		
		if($requete->rowCount() == 0) {
			array_push($erreurs, "Votre pseudo ou Mot de Passe est incorrect.");
		}
		elseif($data["membre_niveau"] == 0) {
			array_push($erreurs, "Vous avez été banni.");
		}
		elseif($requete->rowCount() > 0) {				
			$valide = 'Vous avez correctement été connecté en tant que '.$pseudo.'.';	

			$requete = $baseDeDonnes->prepare("UPDATE membres SET membre_connecte = 1, membre_derniere_visite = :time, membre_nb_visite = membre_nb_visite + 1 WHERE membre_id = :id");
			$requete->bindValue(":time", time(), PDO::PARAM_INT);
			$requete->bindValue(":id", $data["membre_id"], PDO::PARAM_INT);
			$requete->execute();
							
			session_start();
			if(isset($_POST["save_pseudo"])) {
				if($_SERVER['SERVER_NAME'] == 'localhost') {
					setcookie("id_membre", $data["membre_id"], time()+2592000, "/");
					setcookie("pseudo", $data["membre_pseudo"], time()+2592000, "/");
					setcookie("password", $data["membre_mdp"], time()+2592000, "/");
				}
				else {
					setcookie("id_membre", $data["membre_id"], time()+2592000, "/", ".salixor.com");
					setcookie("pseudo", $data["membre_pseudo"], time()+2592000, "/", ".salixor.com");
					setcookie("password", $data["membre_mdp"], time()+2592000, "/", ".salixor.com");
				}
			}
			else {
				$_SESSION["pseudo"] = $data["membre_pseudo"];
				$_SESSION["id_membre"] = $data["membre_id"];
				$_SESSION["membre_mdp"] = $data["membre_mdp"];
			}

			if(!empty($adresse_redirection))      header("Location: ".$adresse_redirection."");
			else                                  header("Location: http://www.salixor.com/");
		}
	}
	else {
		array_push($erreurs, "Un ou plusieurs des champs n'ont pas été remplis.");
	}
}

$titre_page_nom = "Connexion";
include("../Header.php");
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Connexion.png" alt="Connexion" /> Connexion</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH ?>">Accueil</a> > <a href="./">Membres</a> > Connexion</p>

<?php
if(isset($erreurs) AND $erreurs != NULL) {
	if(count($erreurs) > 1) {
		echo '<p class="encadreInfos">';
			echo '<span class="left erreur block"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreurs</span>';
			foreach($erreurs as $erreur_array) {
				echo '<span class="right">';
					echo $erreur_array;
				echo '</span>';
			}
		echo '</p>';
	}
	else {
		echo '
		<p class="encadreInfos">
			<span class="left erreur"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Erreur.png" alt="Erreur" /> Erreur</span>
			<span class="right">'.$erreurs[0].'</span>
		</p>';
	}
}

if(isset($valide)) {
	echo '
	<p class="encadreInfos">
		<span class="left valide"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Valide.png" alt="Confirmation" /> Confirmation</span>
		<span class="right">'.$valide.'</span>
	</p>';
}
elseif(isset($information)) {
	echo '
	<p class="encadreInfos">
		<span class="left information"><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Information.png" alt="Information" /> Information</span>
		<span class="right">'.$information.'</span>
	</p>';
}
else {
	if(!isset($valide) OR !isset($PSEUDO_MEMBRE)) {
		echo '<p class="encadreInfos">';
			echo '<span class="right">Vous pouvez ici vous identifier sur le site en quelques secondes.<br />Pas inscrit ? Par <a href="Inscription.php">ici</a>.</span>';
		echo '</p>';
	?>

	<div class="bloc">
		<a href="Modifier_Profil.php?Action=Restauration"><div class="miniBloc right">Mot de passe oublié ?</div></a>
		
		<form action method="post">
			<p><label for="pseudo">Pseudo : </label><input type="text" id="pseudo" name="pseudo" /></p>
			<p><label for="password">Mot de Passe : </label><input type="password" id="password" name="password" /></p>
			<p><label for="save_pseudo">Rester connecté : </label><input type="checkbox" name="save_pseudo" id="save_pseudo" /></p>
			<input type="hidden" name="adresse_redirection" value="<?php echo $_SERVER["HTTP_REFERER"]; ?>" />
			<p align="center"><input type="submit" name="submit" value="Connexion" /></p>
		</form>
	</div>

	<?php }
}

include("../Footer.php"); ?>