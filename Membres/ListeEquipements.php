<?php 
$titre_page_nom = "Mes équipements ajoutés";
include("../Header.php");

$arraySinguliers = array("familiers" => "Familier", "capes" => "Cape", "chapeaux" => "Chapeau", "amulettes" => "Amulette", "anneaux" => "Anneau", "dofus" => "Dofus", "ceintures" => "Ceinture", "bottes" => "Botte", "sacs" => "Sac", "boucliers" => "Bouclier", "arcs" => "Arc", "baguettes" => "Baguette", "batons" => "Baton", "dagues" => "Dague", "epees" => "Epee", "marteaux" => "Marteau", "pelles" => "Pelle", "haches" => "Hache", "pioches" => "Pioche", "faux" => "Faux");
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Contributions.png" alt="Contributions" /> Mes équipements ajoutés</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH ?>">Accueil</a> > <a href="./">Membres</a> > Mon Contenu > Mes équipements ajoutés</p>

<?php if(isset($ID_MEMBRE)) { ?>
	<p class="encadreInfos"><span class="right">Voici la liste des équipements que vous avez aidé à ajouter au site. Vous pouvez connaître leur état actuel, et éventuellement corriger une erreur.</span></p>

	<?php
	if(isset($_GET['Page']) && is_numeric($_GET['Page']))    $page = $_GET['Page'];
	else                                                     $page = 1;

	$pagination = 50;
	$limit_start = ($page - 1) * $pagination;

	$requete = $baseDeDonnes->prepare("SELECT COUNT(*) AS nb_total FROM equipements WHERE equipements.id_ajouteur = :id");
	$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
	$requete->execute();

	$nb_total = $requete->fetch();
	$nb_total = $nb_total['nb_total'];
	$nb_pages = ceil($nb_total / $pagination);

	$requete = $baseDeDonnes->prepare("
	SELECT * FROM equipements
	WHERE equipements.id_ajouteur = :id
	ORDER BY equipement_ajoute ASC, date_ajout DESC, equipements.niveau DESC, equipements.id ASC
	LIMIT $limit_start, $pagination");
	$requete->bindValue(":id", $ID_MEMBRE, PDO::PARAM_INT);
	$requete->execute();

	$requete->setFetchMode(PDO::FETCH_OBJ);

	echo '
	<table>
		<tr>
			<th width="30"></th>
			<th width="50">Niveau</th>
			<th width="200">Nom</th>
			<th width="100">Type</th>
			<th width="150">Date</th>
			<th width="90">État</th>
			<th width="70">Modifier</th>
		</tr>';

		if($requete->rowCount() > 0) {
			while($data = $requete->fetch()) {
				$image = lienImageEquipement(ucfirst($data->type), $data->nom);
				if(!file_exists('../'.$image))    $image = 'Images/Items/Unknown.png';
				
				if($data->date_ajout != 0)      $date = afficher_date_difference($data->date_ajout);
				else                            $date = "Date inconnue";
				
				$lien = tronquer(utf8_encode(stripslashes($data->nom)), 28, " ...");
				
				if($data->equipement_ajoute == '2') {
					if($data->type != "familiers")    $lien = '<a href="'.ABSPATH.'Equipements/?Page='.$arraySinguliers[$data->type].'#Item-'.$data->id.'">'.$lien.'</a>';
					else                              $lien = '<a href="'.ABSPATH.'Equipements/Familiers.php">'.$lien.'</a>';
				}
				
				if($data->equipement_ajoute == '0')        $etat = '<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/BulletRed.png" alt="" style="margin:3px 0 -3px;" /> Refusé';
				if($data->equipement_ajoute == '1')        $etat = '<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/BulletYellow.png" alt="" style="margin:3px 0 -3px;" /> En attente';
				if($data->equipement_ajoute == '2')        $etat = '<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/BulletGreen.png" alt="" style="margin:3px 0 -3px;" /> Accepté';
				
				$modifier = '';
				if($data->equipement_ajoute == '1' || $data->equipement_ajoute == '0')        $modifier = '<a href="'.ABSPATH.'Admin/Admin_equipements.php?Type='.$arraySinguliers[$data->type].'&amp;Action=Modifier&amp;id='.$data->id.'">Modifier</a>';
				
				echo '
				<tr>
					<td align="center"><img src="'.ABSPATH.$image.'" height="30" style="background:#FFF;" alt="" /></td>
					<td align="center">'.$data->niveau.'</td>
					<td>'.$lien.'</td>
					<td>'.$arraySinguliers[$data->type].'</td>
					<td>'.$date.'</td>
					<td>'.$etat.'</td>
					<td align="center">'.$modifier.'</td>
				</tr>';
			}
		}
		else {
			echo '<tr><td colspan="7" align="center" style="padding:7px;">Vous n\'avez encore pas ajouté d\'équipement sur le site.</td></tr>';
		}

	echo '</table>';

	if($nb_pages > 1)              echo '<p class="pagination" align="center" style="margin:15px 0 0; width:760px;">'.pagination($page, $nb_pages, "?Page=%d").'</p>';

	echo '<p align="center" style="margin:15px 0 0; width:760px;">
		<a href="'.ABSPATH.'Admin/Admin_equipements.php" class="allNews">
			<img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Contribuer.png" alt="" /> Contribuer
		</a>
	</p>';
}
else {
	echo '<p class="encadreInfos information"><span class="right">Vous devez être connecté pour accéder à cette fonctionnalité.</span></p>';
}

include("../Footer.php"); ?>