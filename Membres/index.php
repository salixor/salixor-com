<?php 
$titre_page_nom = "Espace Membre";
include("../Header.php");
?>

<h2 class="titreSecond"><img src="<?php echo ABSPATH; ?>Images/icones_pages/Membres.png" alt="Membres" /> Espace Membre</h2>

<p class="Arianne">> <a href="<?php echo ABSPATH ?>">Accueil</a> > Membres</p>

<p class="encadreInfos"><span class="right">Vous voici sur la page d'accueil de l'espace Membre.<br />Vous pouvez nous rejoindre, vous connecter ou encore accéder à votre profil et à la liste des membres.</span></p>

<?php
echo '<ul class="navigationPage">';

	if(!isset($ID_MEMBRE)) {
		echo '
		<a href="Inscription.php"><li><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Inscription.png" alt="" /> Inscription</li></a>
		<a href="Membres.php"><li><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Partenaires.png" alt="" /> Liste des Membres</li></a>
		<a href="Personnages.php"><li><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Personnages.png" alt="" /> Liste des Personnages</li></a>
		<a href="Connexion.php"><li><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Connexion.png" alt="" /> Connexion</li></a>
		';
	}
	if(isset($ID_MEMBRE)) {
		echo '
		<a href="Profil.php?id='.$ID_MEMBRE.'"><li><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Profil.png" alt="" /> Voir mon profil</li></a>
		<a href="Modifier_Profil.php"><li><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/ModifierProfil.png" alt="" /> Modifier mon profil</li></a>
		<a href="Membres.php"><li><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Partenaires.png" alt="" /> Liste des Membres</li></a>
		<a href="Personnages.php"><li><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Personnages.png" alt="" /> Liste des Personnages</li></a>
		<a href="Deconnexion.php"><li><img src="'.ABSPATH.'Images/icones_pages/LittlesIcones/Connexion.png" alt="" /> Déconnexion</li></a>
		';
	}

echo '</ul>';

include("../Footer.php"); ?>